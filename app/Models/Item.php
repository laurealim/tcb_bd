<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type_id',
        'unit',
        'created_by',
        'updated_by',
        'status',
    ];

    public function item_types()
    {
        return $this->belongsTo(ItemType::class,'type_id');
    }
}
