<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ministry extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'created_by',
        'updated_by',
    ];

    public function bivags()
    {
        return $this->hasMany(Bivag::class,'mins_id', 'id')->orderBy('name', 'ASC');
    }

    public function departments()
    {
        return $this->hasMany(Department::class,'mins_id', 'id')->orderBy('name', 'ASC');
    }
}
