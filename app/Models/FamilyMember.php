<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyMember extends Model
{
    use HasFactory;

    protected $fillable = [
        'family_id',
        'card_no',
        'member_data',
        'm_name',
        'm_dob',
        'm_gender',
        'm_nid',
        'm_phone',
        'm_occupation',
        'm_relation',
        'm_ssnp',
        'm_edu_level',
        'total_member',
        'total_male',
        'total_female',
        'total_others',
        'total_child',
        'status',
    ];

    public function family()
    {
        return $this->belongsTo(Family::class, 'family_id');
    }
}
