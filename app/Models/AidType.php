<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AidType extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'status',
    ];

    public function packages()
    {
        return $this->hasMany(Package::class,'aid_id');
    }
}
