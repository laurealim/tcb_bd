<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = [
        'name','bn_name','division_id', 'url', 'dis_code',
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }

        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $ticket->$key = $value;
            }
        }

        $ticket->save();
        return 1;
    }

    public function divisions()
    {
//        return $this->hasOne('App\Model\Division',"id", "division_id");
        return $this->belongsTo(Division::class);
    }

    public function upazilas()
    {
//        return $this->hasMany('App\Model\Upazila')->orderBy('name', 'ASC');
//        return $this->hasMany(Upazila::class,'district_id', 'id')->orderBy('name', 'ASC');
        return $this->hasMany(Upazila::class)->orderBy('name', 'ASC');
    }

    public function relief()
    {
        return $this->hasMany('App\Model\Relief')->orderBy('name', 'ASC');
    }
}
