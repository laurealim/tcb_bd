<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
//use Laravel\Sanctum\HasApiTokens;

class Dealer extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    protected $guard_name = 'web';

    protected $fillable = [
        'name',
        'email',
        'nid',
        'address',
        'g_name',
        'phone',
        'dob',
        'inst_name',
        'trd_license',
        'picture',
        'inst_address',
        'tin',
        'docs',
        'gender',
        'roles',
        'password',
        'div',
        'dist',
        'sub_dist',
        'union',
        'status',
        'created_by',
    ];

    public function getFullName()
    {
        return "{$this->name} {$this->g_name}";
    }

}
