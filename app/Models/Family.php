<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'f_h_name',
        'dob',
        'gender',
        'div',
        'div_code',
        'dist',
        'dist_code',
        'sub_dist',
        'sub_dist_code',
        'union',
        'union_code',
        'word',
        'add',
        'occupation',
        'nid',
        'phone',
        'ssnp',
        'income',
        'expense',
        'card_no',
        'house_type',
        'marital_status',
        'education',
        'religion',
    ];

    public function family_member()
    {
        return $this->hasOne(FamilyMember::class, 'family_id');
    }

    public function family_vs_package()
    {
        return $this->hasOne(FamilyVsPackage::class, 'family_id');
    }

}
