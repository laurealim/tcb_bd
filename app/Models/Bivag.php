<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bivag extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'mins_id',
        'created_by',
        'updated_by',
    ];

    public function departments()
    {
        return $this->hasMany(Department::class,'divs_id', 'id')->orderBy('name', 'ASC');
    }

    public function ministry()
    {
//        return $this->hasOne('App\Model\Division',"id", "division_id");
        return $this->belongsTo(Ministry::class,'mins_id');
    }
}
