<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'mins_id',
        'divs_id',
        'dept',
        'div',
        'dist',
        'sub_dist',
        'union',
        'status',
        'created_by',
        'updated_by',
    ];
}
