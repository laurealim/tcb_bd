<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyVsPackage extends Model
{
    use HasFactory;

    protected $fillable = [
        'family_id',
        'package_id',
    ];

    public function family()
    {
        return $this->belongsTo(Family::class, 'family_id');
    }

}
