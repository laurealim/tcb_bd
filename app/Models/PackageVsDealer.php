<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageVsDealer extends Model
{
    use HasFactory;

    protected $fillable = [
        'package_id',
        'div',
        'dist',
        'sub_dist',
        'dealer_id',
        'created_by',
        'updated_by',
    ];
}
