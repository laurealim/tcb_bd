<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DealerVsFamily extends Model
{
    use HasFactory;
    protected $fillable = [
        'div',
        'dist',
        'sub_dist',
        'dealer_id',
        'package_id',
        'families',
        'created_by',
    ];
}
