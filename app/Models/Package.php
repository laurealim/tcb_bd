<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'aid_id',
        'items',
        'created_by',
        'updated_by',
        'status',
        'assign_dist',
        'institute_id',
        'assign_div',
    ];

    public function aid_types()
    {
        return $this->belongsTo(AidType::class,'aid_id');
    }

    public function package_vs_family()
    {
        return $this->hasOne(PackageVsFamily::class, 'package_id');
    }

    public function lot_item_amount()
    {
        return $this->hasMany(LotItemAmount::class,'package_id');
    }
}
