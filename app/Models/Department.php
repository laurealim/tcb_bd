<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'mins_id',
        'divs_id',
        'created_by',
        'updated_by',
    ];

    public function ministry()
    {
//        return $this->hasOne('App\Model\Division',"id", "division_id");
        return $this->belongsTo(Ministry::class,'mins_id');
    }

    public function bivags()
    {
//        return $this->hasOne('App\Model\Division',"id", "division_id");
        return $this->belongsTo(Bivag::class,'divs_id');
    }
}
