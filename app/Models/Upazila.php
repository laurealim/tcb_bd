<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upazila extends Model
{
    protected $fillable = [
        'name', 'bn_name', 'district_id', 'url', 'up_code',
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }

        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $ticket->$key = $value;
            }
        }
        $ticket->save();
        return 1;
    }

    public function districts()
    {
//        return $this->hasOne('App\Model\District', "id", "district_id");
        return $this->belongsTo(District::class);
    }

    public function unions()
    {
//        return $this->hasMany('App\Model\Union')->orderBy('name', 'ASC');
        return $this->hasMany(Union::class,'upazilla_id', 'id')->orderBy('name', 'ASC');
    }

    public function relief()
    {
        return $this->hasMany('App\Model\Relief')->orderBy('name', 'ASC');
    }
}
