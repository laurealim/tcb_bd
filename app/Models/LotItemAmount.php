<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LotItemAmount extends Model
{
    use HasFactory;

    protected $fillable = [
        'lot_no',
        'item_amount',
        'total_people',
        'package_id',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
        'state',
        'status',
        'institute_id',
    ];

    public function packages()
    {
        return $this->belongsTo(Package::class,'package_id');
    }
}
