<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageVsFamily extends Model
{
    use HasFactory;

    protected $fillable = [
        'package_id',
        'family_id',
    ];

    public function packages()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }

}
