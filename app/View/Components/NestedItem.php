<?php

namespace App\View\Components;

use App\Models\Nested;
use Illuminate\View\Component;

class NestedItem extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $nested;
    public function __construct(Nested $nestedItems)
    {
        $this->nested = $nestedItems;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.nested-item');
    }
}
