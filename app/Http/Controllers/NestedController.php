<?php

namespace App\Http\Controllers;

use App\Models\Nested;
use App\Http\Requests\StoreNestedRequest;
use App\Http\Requests\UpdateNestedRequest;

class NestedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNestedRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNestedRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Nested  $nested
     * @return \Illuminate\Http\Response
     */
    public function show(Nested $nested)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nested  $nested
     * @return \Illuminate\Http\Response
     */
    public function edit(Nested $nested)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNestedRequest  $request
     * @param  \App\Models\Nested  $nested
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNestedRequest $request, Nested $nested)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Nested  $nested
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nested $nested)
    {
        //
    }
}
