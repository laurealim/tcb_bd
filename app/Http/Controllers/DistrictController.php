<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Division;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use DB;
use PHPUnit\Exception;

class DistrictController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('location-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        try {
            $page_title = 'জেলার তালিকা';
            $distLists = getDistLists();
            $divtLists = getDivLists();
//            $nesteds = Nested::tree()->get()->toTree()->where("id","=", 4);
//            dd($nesteds);

            if ($request->ajax()) {
                $query = DB::table('districts');
//                $query = userAccess($query);

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->addColumn('div_name', function ($row) use($divtLists) {
                        $div_name = "";
                        $div_name = $divtLists[$row->division_id];
                        return $div_name;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('location-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('districts.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('location-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('districts.destroy', $row->id) . ' class="btn-sm btn-danger delete-district action-btn"><i class="fa fa-trash-alt"></i></a>';
                        }

                        return $action;
                    })
                    ->rawColumns(['action','div_name'])
                    ->make(true);
            }
        } catch (\Exception $exception) {
//            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


        return view('districts.index', compact('page_title', 'distLists'));
    }

    public function create()
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন জেলা তৈরী';

        return view('districts.create', compact('page_title'));

    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'dis_code' => 'required',
                'division_id' => 'required',
            ],
                [
                    'name.required' => 'জেলার ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'জেলার বাংলা নাম প্রদান করুন',
                    'dis_code.required' => 'জেলার কোড প্রদান করুন',
                    'division_id.required' => 'বিভাগ নির্বাচন করুন',
                ]
            );

            $districtDataArr = $request->all();
            $lastInsertedId = District::create($districtDataArr);

            DB::commit();
            return redirect()->route('districts.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'জেলার তথ্য সংশোধন';
        DB::beginTransaction();
        try {
            $district = District::find($id);
//            dd($district->id);
            return view('districts.edit', compact('district', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'dis_code' => 'required',
                'division_id' => 'required',
            ],
                [
                    'name.required' => 'জেলার ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'জেলার বাংলা নাম প্রদান করুন',
                    'dis_code.required' => 'জেলার কোড প্রদান করুন',
                    'division_id.required' => 'বিভাগ নির্বাচন করুন',
                ]
            );

            $districtRes = District::find($id);
            $districtRes->name = $request->input('name');
            $districtRes->bn_name = $request->input('bn_name');
            $districtRes->dis_code = $request->input('dis_code');
            $districtRes->division_id = $request->input('division_id');
            $districtRes->save();

            DB::commit();
            return redirect()->route('districts.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('location-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                District::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $districts = new District();

        $districts = $districts->select('divisions.bn_name as division_name', 'districts.*')
            ->leftJoin('divisions', 'divisions.id', '=', 'districts.division_id')
            ->where('districts.name', 'like', '%' . $searchData . '%')
            ->orwhere('divisions.name', 'like', '%' . $searchData . '%')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('district.ajax_list', compact('districts'));
        } else {
            return view('district.adminList', compact('districts'));
        }
    }

    public function adminForm()
    {
        $divisionModel = new Division();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        return view('district.adminForm', compact('divisionData'));
    }

    public function adminStore(Request $request)
    {
        $districtModel = new District();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
            'division_id' => 'required',
        ], [
            'name.required' => 'District Name is required',
            'bn_name.required' => 'District Name (Bangla) is required',
            'division_id.required' => 'Please Select a Division',

        ]);

        $districtModel->saveData($request);
        return redirect('admin/district/list')->with('success', 'New District added successfully');
    }


    public function show(District $district)
    {
        //
    }


    public function adminEdit($id)
    {
        $divisionModel = new Division();

        try {
            $divisionList = $divisionModel->pluck('bn_name', 'id')->all();
            $districtData = District::where('id', $id)->first();
            return view('district.adminEdit', compact('divisionList', 'districtData', 'id'));
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
        }
    }


    public function adminUpdate(Request $request, District $district)
    {
        $districtModel = new District();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
            'division_id' => 'required',
        ], [
            'name.required' => 'District Name is required',
            'bn_name.required' => 'District Name (Bangla) is required',
            'division_id.required' => 'Please Select a Division',

        ]);

        $districtModel->updateData($request);

        return redirect('admin/district/list')->with('success', 'District edited successfully');
    }


    public function adminDestroy($id, Request $request)
    {
//        $apartmentModel = Apartment::findOrFail();
        $districtInfo = District::findOrFail($id);
        if (isset($request->id)) {
            $districtInfo->delete();
            $request->session()->flash('success', 'District Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'District Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function districtSelectAjaxList(Request $request)
    {
        try {
            if ($request->ajax()) {

                $query = DB::table('districts');
                $dist = auth()->user()->dist;
                if (!empty($dist)) {
                    $query->where('id', $dist);
                }
                $divisionID = $request->post('divId');
                $query->where("division_id", $divisionID);
                $districts = $query->pluck('bn_name', 'id');
                return json_encode($districts);

//
//            $districtModel = new District();
//            $divisionID = $request->post('divId');
//            $districts = $districtModel->where("division_id", $divisionID)->pluck("bn_name", "id")->all();
//            return json_encode($districts);
            }
        } catch (Exception $exception) {
            dd($exception);
        }
    }

    public function adminDistrictSelectAjaxList(Request $request)
    {
        return $this->_districtSelectAjaxList($request);
    }

    public function clientDistrictSelectAjaxList(Request $request)
    {
        return $this->_districtSelectAjaxList($request);
    }

    public function userDistrictSelectAjaxList(Request $request)
    {
        return $this->_districtSelectAjaxList($request);
    }

    private function _districtSelectAjaxList($request)
    {
        if ($request->ajax()) {

            $districtModel = new District();
            $divisionID = $request->post('divisionID');
            $districts = $districtModel->where("division_id", $divisionID)->pluck("bn_name", "id")->all();
            return json_encode($districts);
        }
    }

    function pr($data = array())
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}
