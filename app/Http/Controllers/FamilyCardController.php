<?php

namespace App\Http\Controllers;

use App\Models\Family;
use DataTables;
use DB;
use Illuminate\Http\Request;
use PDF;
use QrCode;

class FamilyCardController extends Controller
{
    public function pendingCards(Request $request)
    {
        if (!auth()->user()->can('pending-card-number')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $page_title = 'পারিবারিক কার্ড নম্বর ব্যতিত পরিবারে তালিকা';


//            $div = auth()->user()->div;
//            $dist = auth()->user()->dist;
//            $sub_dist = auth()->user()->sub_dist;
//            $union = auth()->user()->union;

            if ($request->ajax()) {
//                $data = Family::select('*')
//                    ->when($div, function ($query) use ($div) {
//                        return $query->where('div', $div);
//                    })
//                    ->when($dist, function ($query) use ($dist) {
//                        return $query->where('dist', $dist);
//                    })
//                    ->when($sub_dist, function ($query) use ($sub_dist) {
//                        return $query->where('sub_dist', $sub_dist);
//                    })
//                    ->when($union, function ($query) use ($union) {
//                        if ($union > -2) {
//                            return $query->where('union', $union);
//                        }
//                    })
//                    ->whereNull('card_no');

                $query = DB::table('families');
                userAccess($query);
                $query->whereNull('card_no');
                $query->where('status',config('constants.status.Active'));
//                dd($query->toSql());

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->make(true);
            }

            return view('family-card.pendingCards', compact('page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'পারিবারিক কার্ড নম্বর তৈরী সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function generateCardNumbers(Request $request)
    {
        if (!auth()->user()->can('generate-card-number')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            $data = Family::select('id', 'card_no', 'div_code', 'dist_code', 'sub_dist_code', 'union_code', 'div', 'dist', 'sub_dist', 'union', 'word')
                ->whereNull('card_no')
                ->where('status',config('constants.status.Active'))
                ->get();
//            pr($data);


            $chunks = $data->chunk(100);
            foreach($chunks as $chunk){
//                pr($chunk);
                cardNumberGeneration($chunk);
            }

//            Family::chunk(200, function ($data) {
//                cardNumberGeneration($data);
//            });
            $request->session()->flash('success', 'পারিবারিক কার্ড নম্বর তৈরী করা হয়েছে...');
            return response()->json(['status' => 'success']);

//            return redirect()->route('users.index')->with('success', 'তথ্য সফল্ভাবে মুছে ফেলা হয়েছে।।।');
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'সকল পরিবারের কার্ড নং তৈরি সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    // Generate PDF
    public function createCard(Request $request, $id)
    {
        if (!auth()->user()->can('print-family-card')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }
//        ini_set('max_execution_time', 180);
//        echo "<?php openssl_cipher_iv_length('whatever'); | php";
        try {
            $title = 'পারিবারিক কার্ড নম্বর ব্যতিত পরিবারে তালিকা';

            // retreive all records from db
            $dataArr = Family::where('id', $id)->get()->toArray();
            $data = $dataArr[0];
            $str = "name.: " . utf8_encode($data['name']) . ", ";
            $str .= "Husband/Father Name: " . utf8_encode($data['f_h_name']) . ", ";
            $str .= "Card No.: " . $data['card_no'] . ", ";
            $str .= "Phone: " . utf8_encode($data['phone']) . ", ";
            $str .= "DOB: " . $data['dob'] . ", ";
            $str .= "NID: " . utf8_encode($data['nid']) . ", ";
            if ($data['gender'] == 1)
                $str .= "Gender: Male, ";
            elseif ($data['gender'] == 2)
                $str .= "Gender: Female, ";
            else
                $str .= "Gender: Others, ";

            if (!hasQrCode($data['id'])) {
                generateQrCode($str, $data['id']);
            }

//            view()->share($dataArr);
//
//        if($request->download){
//            dd($request->download);
//        }else{
//            dd('pupupu');
//        }
            if ($request->download) {
                $pdf = PDF::loadView('family-card.cardTemplate', $dataArr);
                return $pdf->stream('document.pdf');

//            $pdf = PDF::loadView('family-card.testCard')
//            $pdf = PDF::loadView('family-card.cardTemplate',compact('data'));
//            return $pdf->download('family_card.pdf');
            }

            return view('family-card.cardTemplate', compact('dataArr','title'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function printCards(Request $request)
    {
        if (!auth()->user()->can('print-family-card')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
//        try {
        $divLists = getDivLists();
        $page_title = 'পারিবারিক কার্ড';
//        $countData = DB::table('families')
//            ->where('word', 7)
//            ->count();
//        pr($request->post());
        if ($request->post()) {
//            dd($countData);
            $request->validate(
                [
                    'div' => 'required',
                    'dist' => 'required',
                    'sub_dist' => 'required',
                    'union' => 'required',
                    'print_card' => 'required',
                ],
                [
                    'div.required' => 'বিভাগ নির্বাচন করুন',
                    'dist.required' => 'জেলা নির্বাচন করুন',
                    'sub_dist.required' => 'উপজেলা নির্বাচন করুন',
                    'union.required' => 'পৌরসভা না ইউনিয়ন নির্বাচন করুন',
                    'print_card.required' => 'কার্ড প্রিন্টের সংখ্যা নির্বাচন করুন',
                ]
            );
//                pr($request->post());
//                dd();
            $div = $request->div;
            $dist = $request->dist;
            $sub_dist = $request->sub_dist;
            $union = $request->union;
            $printCard = $request->print_card;
            $limit = 20;

//            dd($printCard);

            $title = "Print Card From ". (($printCard * $limit) + 1) ." to ". (($printCard +1) * $limit);

            $dataArr = Family::where('div', $div)
                ->where('dist', $dist)
                ->where('sub_dist', $sub_dist)
                ->where('union', $union)
                ->whereNotNull('card_no')
                ->offset($printCard * $limit)
                ->limit($limit)
                ->get()->toArray();
//                $query = DB::table('families');
//                $query->where('div', $div);
//                $query->where('dist', $dist);
//                $query->where('sub_dist', $sub_dist);
//                $query->where('union', $union);

//                if ($id) {
//                    $query->where('id', $id);
//                }
//                if ($distId) {
//                    $query->where('district_id', $distId);
//                }
//                $dataArr = $query->limit(20)->get()->toArray();
//                pr($dataArr);

//                $id = 0;
//                $dataArr = Family::where('id', $id)->get()->toArray();
            return view('family-card.cardTemplate', compact('dataArr','title'));
//                return view('family-card.printCard',compact('page_title', 'divLists'));

        }
        return view('family-card.printCard', compact('page_title', 'divLists'));
//            return view('families.index', compact('page_title', 'divLists'));
//        } catch (\Exception $exception) {
//            DB::rollback();
////            dd($exception);
//            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
//            return response()->json(['status' => 'error']);
//        }


//        pr($request->id);
        // retreive all records from db
//        $data = Family::where('id', $request->id)->get()->toArray();

//        dd($data);
//        $data = $data[0];

//        view()->share('items',$items);
//
//        if ($request->has('download')) {
//            $pdf = PDF::loadView('itemPdfView');
//            return $pdf->download('itemPdfView.pdf');
//        }
//        return view('itemPdfView');


//        dd($data);
        // share data to view
//        view()->share('data', $data);
//        if($request->has('download')){
//        $pdf = PDF::loadView('family-card.cardTemplate')->setPaper('letter', 'landscape');
        // download PDF file with download method
//        return $pdf->download('family_card.pdf');

//        }
    }

    public function chunkCards(Request $request)
    {
        if ($request->ajax()) {
            $divId = $request->post('divId');
            $districtID = $request->post('districtID');
            $upazilaID = $request->post('upazilaID');
            $unionID = $request->post('unionID');

            $postData = [
                'divId' => $divId,
                'districtID' => $districtID,
                'upazilaID' => $upazilaID,
                'unionID' => $unionID,
            ];

            $query = DB::table('families');
            $query = userAccess($query, $postData);
            $query->whereNotNull('card_no');
            $familyList = $query->count();
            return json_encode($familyList);
        }
    }

}
