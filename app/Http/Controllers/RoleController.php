<?php

namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DataTables;

class RoleController extends Controller
{

    function __construct()
    {
//        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
//        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        if (!auth()->user()->can('role-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $page_title = 'সকল রোল ';
            if ($request->ajax()) {
                $data = Role::select('*');
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $action = '<a class="btn btn-primary" id="edit-role" href= ' . route('roles.edit', $row->id) . '><i class="fa fa-pencil-alt"></i> </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id=' . $row->id . ' href=' . route('roles.destroy', $row->id) . ' class="btn btn-danger delete-role"><i class="fa fa-trash-alt"></i></a>';
                        return $action;
                    })
                    ->rawColumns(['action', 'roles'])
                    ->make(true);
            }
            return view('roles.index', compact('page_title'));
//            $data = Role::orderBy('id', 'DESC')->paginate(10);
//
//            return view('roles.index', compact('data'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function create()
    {
        if (!auth()->user()->can('role-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন রোল তৈরী';
        $permission = Permission::get();
        $permissionArrange = [];
        foreach ($permission as $key => $values) {
            if (!isset($permissionArrange[$values->tag])) {
                $permissionArrange[$values->tag] = [];
            }
            $filterData['id'] = $values->id;
            $filterData['name'] = $values->name;
            $permissionArrange[$values->tag][] = $filterData;
        }
//        dd($permissionArrange);

        return view('roles.create', compact('permission', 'page_title', 'permissionArrange'));

    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('role-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required|unique:roles,name',
                'permission' => 'required',
            ]);

            $role = Role::create(['name' => $request->input('name'), 'created_by' => auth()->user()->id, 'institute_id' => auth()->user()->institute_id]);
            $role->syncPermissions($request->input('permission'));

            return redirect()->route('roles.index')
                ->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show($id)
    {
        if (!auth()->user()->can('role-show')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

//        $role = Role::find($id);
//        dd($role);
//        $rolePermissions = Permission::join('role_has_permissions', 'role_has_permissions.permission_id', 'permissions.id')
//            ->where('role_has_permissions.role_id', $id)
//            ->get();
//
//        return view('roles.show', compact('role', 'rolePermissions'));
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('role-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'রোল সংশোধন';
        DB::beginTransaction();
        try {
            $role = Role::find($id);
            $permission = Permission::get();
            $permissionArrange = [];
            foreach ($permission as $key => $values) {
                if (!isset($permissionArrange[$values->tag])) {
                    $permissionArrange[$values->tag] = [];
                }
                $filterData['id'] = $values->id;
                $filterData['name'] = $values->name;
                $permissionArrange[$values->tag][] = $filterData;
            }
            $rolePermissions = DB::table('role_has_permissions')
                ->where('role_has_permissions.role_id', $id)
                ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
                ->all();
            return view('roles.edit', compact('role', 'permission', 'permissionArrange', 'rolePermissions', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('role-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'permission' => 'required',
            ]);

            $role = Role::find($id);
            $role->name = $request->input('name');
            $role->save();

            $role->syncPermissions($request->input('permission'));

            return redirect()->route('roles.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('role-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Role::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
//        Role::find($id)->delete();

//        return redirect()->route('roles.index')
//            ->with('success', 'Role deleted successfully');
    }
}
