<?php

namespace App\Http\Controllers;

use App\Models\User;
use DataTables;
use DB;
use Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    function __construct()
    {
//        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'store']]);
//        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        if (!auth()->user()->can('user-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'সকল ইউজার ';
        if ($request->ajax()) {
            $data = '';
            if(auth()->user()->can('is-su-admin')){
                $data = User::select('*');
            }else{
                $data = User::select('*')
                    ->where('institute_id', auth()->user()->institute_id)
                    ->when(true, function($query){
                        userAccess($query);
                    });
            }
            $sql= $data->toSql();
//            dd($sql);
//            $query = DB::table('users');
//            $query = userAccess($query);
//            $query = userDataAccess($query);
//            dd($query->get());
//            dd($data->get());

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('roles', function ($row) {
                    $str = '';
                    if (!empty($row->getRoleNames())) {
                        foreach ($row->getRoleNames() as $v) {
                            $str .= '<button type="button" class="btn bg-gradient-info">' . $v . '</button>&nbsp;';
//                            $str .= '<span class="badge badge-success btn">'. $v.'</span>&nbsp;';
                        }
                    }
                    return $str;
                })
                ->addColumn('action', function ($row) {
                    $action = '';
                    if (auth()->user()->can('user-edit')) {
                        $action .= '<a class="btn btn-primary" id="edit-role" href= ' . route('users.edit', $row->id) . '>
<i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}"> ';
                    }
                    if (auth()->user()->can('user-delete')) {
                        $action .= '<a id=' . $row->id . ' href=' . route('users.destroy', $row->id) . ' class="btn btn-danger delete-user">
<i class="fa fa-trash-alt"></i></a>';
                    }
                    return $action;
//                    $action = '<a class="btn btn-primary" id="edit-role" href= ' . route('users.edit', $row->id) . '><i class="fa fa-pencil-alt"></i> </a>
//<meta name="csrf-token" content="{{ csrf_token() }}">
//<a id=' . $row->id . ' href=' . route('users.destroy', $row->id) . ' class="btn btn-danger delete-user"><i class="fa fa-trash-alt"></i></a>';
//                    return $action;
                })
                ->rawColumns(['action', 'roles'])
                ->make(true);
        }
        return view('users.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('user-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন ইউজার তৈরী';
        $institutionList = getInstitutionList(auth()->user()->institute_id);

        $divListArr = getDivLists(null, true);
//        pr(auth()->user()->institute_id);
//        dd($institutionList);
        $roles = Role::pluck('name', 'id')->all();

        return view('users.create', compact('roles', 'page_title', 'institutionList','divListArr'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('user-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {

            $input = $request->all();
            $institutionInfo = getInstitutionInfo($input['institute'])->toArray();
            $institutionInfo = $institutionInfo[0];

//            dd($input);
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'phone' => 'required',
                'roles' => 'required',
                'institute' => 'required'
            ],[
                'name.required' => 'নাম প্রদান করুন।',
                'email.required|email' => 'ইমেইল প্রদান করুন।',
                'password.required' => 'পাসওয়ার্ড প্রদান করুন।',
                'phone.required' => 'মোবাইল নং প্রদান করুন।',
                'roles.required' => 'ইউজার রোল বাছাই করুন।',
                'institute.required' => 'প্রতিষ্ঠানের নাম প্রদান করুন।',
            ]);

            $input['password'] = Hash::make($input['password']);
            if (!empty($institutionInfo->div)) {
                $input['div'] = $institutionInfo->div;
            } else {
                $input['div'] = 0;
            }
            if (!empty($institutionInfo->dist)) {
                $input['dist'] = $institutionInfo->dist;
            } else {
                $input['dist'] = 0;
            }
            if (!empty($institutionInfo->sub_dist)) {
                $input['sub_dist'] = $institutionInfo->sub_dist;
            } else {
                $input['sub_dist'] = 0;
            }
            $input['union'] = -2;
            $input['institute_id'] = $institutionInfo->id;

            $user = User::create($input);
            $user->assignRole($request->input('roles'));

            DB::commit();

            return redirect()->route('users.index')
                ->with('success', 'নতুন ইউজার তৈরী সম্পন্ন হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংরক্ষন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show($id)
    {
        if (!auth()->user()->can('user-show')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $user = User::find($id);

        return view('users.show', compact('user'));
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('user-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

//        DB::beginTransaction();
        try {

            $page_title = 'ইউজার সংশোধন';
            $user = User::find($id);
            $roles = Role::pluck('name', 'id')->all();
            $userRole = $user->roles->pluck('id', 'name')->all();
            $institutionList = getInstitutionList(auth()->user()->institute_id);

            $div = $user->div;
            $divListArr = getDivLists(null, false);
            if(!empty($div)){
                $divListArr = getDivLists(null, true);
            }

            $dist = $user->dist;
//            $sub_dist = $user->sub_dist;

            $distListArr = $sub_distListArr = [];

            if (!empty($div)) {
                $distListArr = getDistLists($div, true);
            }

            if (!empty($dist)) {
                $sub_distListArr = getSubDistLists($dist, true);
            }


            return view('users.edit', compact('user', 'roles', 'userRole', 'page_title', 'institutionList','divListArr','distListArr','sub_distListArr'));
        } catch (\Exception $exception) {
//            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('user-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $input = $request->all();

            if (auth()->user()->can('is-su-admin') && (auth()->user()->id == $id)) {
                $this->validate($request, [
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email,' . $id,
                    'phone' => 'required',
                    'roles' => 'required'
                ],
                    [
                        'name.required' => 'নাম প্রদান করুন।',
                        'email.required|email|unique:users,email,' . $id => 'ইমেইল প্রদান করুন।',
                        'phone.required' => 'মোবাইল নং প্রদান করুন।',
                        'roles.required' => 'ইউজার রোল বাছাই করুন।',
                    ]);
            } else {
                $this->validate($request, [
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email,' . $id,
                    'phone' => 'required',
                    'roles' => 'required',
                    'institute' => 'required',
                ],
                    [
                        'name.required' => 'নাম প্রদান করুন।',
                        'email.required|email|unique:users,email,' . $id => 'ইমেইল প্রদান করুন।',
                        'phone.required' => 'মোবাইল নং প্রদান করুন।',
                        'roles.required' => 'ইউজার রোল বাছাই করুন।',
                        'institute.required' => 'প্রতিষ্ঠানের নাম প্রদান করুন।',
                    ]);

//                $institutionInfo = getInstitutionInfo($input['institute'])->toArray();
//                $institutionInfo = $institutionInfo[0];
//                dd($input['institute']);

                if (!empty($input['div'])) {
                    $input['div'] = $input['div'];
                } else {
                    $input['div'] = 0;
                }
                if (!empty($input['dist'])) {
                    $input['dist'] = $input['dist'];
                } else {
                    $input['dist'] = 0;
                }
                if (!empty($input['sub_dist'])) {
                    $input['sub_dist'] = $input['sub_dist'];
                } else {
                    $input['sub_dist'] = 0;
                }

//                $input['union'] = -2;
                $input['institute_id'] = $input['institute'];

            }

//        if (!empty($input['password'])) {
//            $input['password'] = Hash::make($input['password']);
//        } else {
//            $input = Arr::except($input, array('password'));
//        }

            $user = User::find($id);
            $user->update($input);

            DB::table('model_has_roles')
                ->where('model_id', $id)
                ->delete();

            $user->assignRole($request->input('roles'));

            DB::commit();

            return redirect()->route('users.index')
                ->with('success', 'তথ্য সঠিকভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();

            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('user-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            User::find($id)->delete();
            DB::commit();
            return response()->json(['status' => 'success']);

//            return redirect()->route('users.index')->with('success', 'তথ্য সফল্ভাবে মুছে ফেলা হয়েছে।।।');
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function changePassword(Request $request)
    {
        $page_title = 'ইউজার পাসওয়ার্ড সংশোধন';
        return view('users.change_pass', compact('page_title'));
    }

    public function updatePassword(Request $request)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();

            $this->validate($request, [
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password|min:6',
            ]);

            $user = User::find(auth()->user()->id);

            $user->password = Hash::make($input['password']);
            $user->save();

            DB::commit();

            return redirect()->route('home')
                ->with('success', 'তথ্য সঠিকভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());

            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');

            return response()->json(['status' => 'error']);
        }


    }
}
