<?php

namespace App\Http\Controllers;

use App\Exports\FamilyExport;
use App\Imports\FamilyImport;
use App\Models\Family;
use App\Models\FamilyMember;
use DataTables;
use DB;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\Exception;

class FamilyController extends Controller
{
    public function index(Request $request)
    {

        if (!auth()->user()->can('family-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }


        DB::beginTransaction();
        try {
            $divLists = getDivLists();
            $page_title = 'পরিবারের তালিকা';

            if ($request->ajax()) {
//                $div = auth()->user()->div;
//                $dist = auth()->user()->dist;
//                $sub_dist = auth()->user()->sub_dist;
//                $union = auth()->user()->union;

                $query = DB::table('families');
                userAccess($query);
                $query->orderByDesc('status');
                $query->orderByDesc('id');
//                dd($query->toSql());

//                $data = Family::select('*')
//                    ->when($div, function ($query) use ($div) {
//                        return $query->where('div', $div);
//                    })
//                    ->when($dist, function ($query) use ($dist) {
//                        return $query->where('dist', $dist);
//                    })
//                    ->when($sub_dist, function ($query) use ($sub_dist) {
//                        return $query->where('sub_dist', $sub_dist);
//                    })
//                    ->when($union, function ($query) use ($union) {
//                        if ($union > -2) {
//                            return $query->where('union', $union);
//                        }
//                    });


                return DataTables::of($query)
                    ->addIndexColumn()
                    ->editColumn('gender', function ($row) {
                        $gender = config('constants.gender.arr.' . $row->gender);
//                        $div_name = getDivName($row->div);
                        return $gender;
                    })
                    ->editColumn('status', function ($row) {
                        $status = config('constants.status.arr.' . $row->status);
//                        $div_name = getDivName($row->div);
                        return $status;
                    })
                    ->editColumn('occupation', function ($row) {
                        if ($row->occupation >= 1)
                            return config('constants.occupation.arr.' . $row->occupation);
                        else
                            return $row->occupation;
                    })
                    ->addColumn('address', function ($row) {
                        $address = $row->add . ', ' . getUnionName($row->union) . ', ' . getSubDistName($row->sub_dist) . ', ' . getDistName($row->dist) . ', ' . getDivName($row->div);
                        return $address;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('family-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('families.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if ($row->status == config('constants.status.Active')) {
                            if (auth()->user()->can('family-delete')) {
                                $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('families.destroy', $row->id) . ' class="btn-sm btn-danger delete-family action-btn"><i class="fa fa-trash-alt"></i></a>';
                            }

                            if (auth()->user()->can('print-family-card')) {
                                if (!empty($row->card_no)) {
                                    $action .= ' <a title="কার্ড প্রিন্ট"  id=' . $row->id . ' href=' . route('family-card.createCard', $row->id) . ' class="btn-sm btn-info action-btn" target="_blank">
<i class="fa fa-print"></i></a>';
                                    $action .= '<meta name="csrf-token" content="{{ csrf_token() }}">';
                                }
                            }
                            if (auth()->user()->can('family-member-edit')) {
                                $action .= " <a title='পরিবারের সদস্য যুক্ত করুন' href='" . route('family-members.edit', $row->id) . "' class='btn-sm btn-secondary action-btn'><i class='fa fa-users'></i></a><meta name='csrf-token' content='{{ csrf_token() }}' >";
                            }
                            if (auth()->user()->can('package-assign')) {
                                $action .= " <a title='প্যাকেজ এসাইন করুন' href='#' class='btn-success btn-sm action-btn' data-toggle='modal' data-target-id='$row->id' data-target='#modal-lg'><i class='fa fa-gifts'></i></a>";
                            }
                        }

                        return $action;
                    })
                    ->rawColumns(['action', 'gender', 'address'])
                    ->make(true);
            }

            DB::commit();
            return view('families.index', compact('page_title', 'divLists'));
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function create()
    {
        if (!auth()->user()->can('family-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $divLists = getDivLists();
        $page_title = 'নতুন পরিবারের তৈরি';
        return view('families.create', compact('page_title', 'divLists'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('family-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

//        dd($request->all());
        DB::beginTransaction();
        try {
            $this->validate($request, [
                'div' => 'required',
                'dist' => 'required',
                'sub_dist' => 'required',
                'union' => 'required',
                'word' => 'required',
                'phone' => 'required',
                'nid' => 'required',
                'name' => 'required',
                'dob' => 'required',
                'religion' => 'required',
                'education' => 'required',
                'f_h_name' => 'required',
                'gender' => 'required',
                'marital_status' => 'required',
                'house_type' => 'required',
                'add' => 'required',
                'income' => 'required',
                'expense' => 'required',
                'occupation' => 'required',
                'file' => 'max:1048576|mimes:jpeg,jpg,png',
            ],
                [
                    'div.required' => 'বিভাগ নির্বাচন করুন',
                    'dist.required' => 'জেলা নির্বাচন করুন',
                    'sub_dist.required' => 'উপজেলা নির্বাচন করুন',
                    'union.required' => 'পৌরসভা না ইউনিয়ন নির্বাচন করুন',
                    'word.required' => 'ওরার্ড নির্বাচন করুন',
                    'phone.required' => 'মোবাইল নং প্রদান করুন',
                    'nid.required' => 'জাতীয় পরিচয় পত্র প্রদান করুন',
                    'name.required' => 'পরিবার প্রধানের নাম প্রদান করুন',
                    'dob.required' => 'জন্ম তারিখ প্রদান করুন',
                    'religion.required' => 'ধর্ম নির্বাচন করুন',
                    'education.required' => 'শিক্ষাগত যোগ্যতা নির্বাচন করুন',
                    'f_h_name.required' => 'পিতা/স্বামীর নাম প্রদান করুন',
                    'gender.required' => 'লিঙ্গ নির্বাচন করুন',
                    'marital_status.required' => 'বৈবাহিক অবস্থা নির্বাচন করুন',
                    'house_type.required' => 'বাসার ধরন নির্বাচন করুন',
                    'add.required' => 'মহল্লা/রোড/হোল্ডিং/গ্রাম/বাসা নং প্রদান করুন',
                    'income.required' => 'পরিবারের মোট আয় প্রদান করুন',
                    'expense.required' => 'পরিবারের মোট ব্যয় প্রদান করুন',
                    'occupation.required' => 'পেশা নির্বাচন করুন',
                    'file.required' => '১-মেগাবাইট এর অনধিক .jpeg/.jpg/.png ফাইল নির্বাচন করুন।',
                ]
            );

            $familyDataArr = $request->all();
            $familyData[0] = $familyDataArr;
            $familyData['institute_id'] = auth()->user()->institute_id;

            $lastInsertedId = Family::create($familyDataArr)->id;

            $familyData[0]['id'] = $lastInsertedId;

            $familyCardId = cardNumberGenerationWithoutCode($familyData);
//            dd($familyCardId);

            Family::where('id', $lastInsertedId)->update(['card_no' => $familyCardId]);

            /*  File Manipulation   */
            $filename = '';
            try {
                if ($request->hasFile('picture')) {
//                    $lastInsertedId = 111;

                    $path = "images/families";
                    $filePath = $path . '/' . $lastInsertedId;

                    $uploadPath = public_path($filePath);

                    $file = $request->file('picture');
                    $filename = $lastInsertedId . '.png';
//                    $filename = $lastInsertedId . '.' . $file->getClientOriginalExtension();

                    try {
                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0755, true);
                        } else {

                            if (file_exists($uploadPath . $filename)) {
                                unlink($uploadPath . $filename);
                            }
                        }
//                    pr($filename);
                        $file->move($uploadPath, $filename); // Save in public folder
//                    $path = $file->storeAs($filePath, $filename); // Save in Storage/app folder
//                    $request->file->move(public_path($uploadPath), $filename);
                    } catch (\Exception $excp) {
                        DB::rollback();
//                        dd($excp);
                        var_dump($excp);
                    }
                }
            } catch (\Exception $exception) {
                DB::rollback();
                var_dump($exception);
            }
            DB::commit();
            return redirect()->route('families.index')
                ->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');

        } catch (Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('family-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'পরিবারের তথ্য সংশোধন';
//        DB::beginTransaction();
        try {
            $familyData = Family::find($id);
            $divLists = getDivLists();
            $distList = getDistLists($familyData->div);
            $sub_distList = getSubDistLists($familyData->dist);
            $unionList = getUnionLists($familyData->sub_dist, $familyData->union);
            if (auth()->user()->union === -2) {
                $unionList[-1] = 'সিটি কর্পোরেশন';
                $unionList[0] = 'পৌরসভা';
            }
//            dd($unionList);
//            DB::commit();
            return view('families.edit', compact('familyData', 'page_title', 'divLists', 'distList', 'sub_distList', 'unionList'));
        } catch (\Exception $exception) {
//            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('family-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $familyDataArr = Family::find($id);

        $required = [
            'div' => 'required',
            'dist' => 'required',
            'sub_dist' => 'required',
            'union' => 'required',
            'word' => 'required',
            'name' => 'required',
            'dob' => 'required',
            'religion' => 'required',
            'education' => 'required',
            'f_h_name' => 'required',
            'gender' => 'required',
            'marital_status' => 'required',
            'house_type' => 'required',
            'add' => 'required',
            'income' => 'required',
            'expense' => 'required',
            'occupation' => 'required',
            'file' => 'max:1048576|mimes:jpeg,jpg,png',
        ];

        $requiredMsg = [
            'div.required' => 'বিভাগ নির্বাচন করুন',
            'dist.required' => 'জেলা নির্বাচন করুন',
            'sub_dist.required' => 'উপজেলা নির্বাচন করুন',
            'union.required' => 'পৌরসভা না ইউনিয়ন নির্বাচন করুন',
            'word.required' => 'ওরার্ড নির্বাচন করুন',


            'name.required' => 'পরিবার প্রধানের নাম প্রদান করুন',
            'dob.required' => 'জন্ম তারিখ প্রদান করুন',
            'religion.required' => 'ধর্ম নির্বাচন করুন',
            'education.required' => 'শিক্ষাগত যোগ্যতা নির্বাচন করুন',
            'f_h_name.required' => 'পিতা/স্বামীর নাম প্রদান করুন',
            'gender.required' => 'লিঙ্গ নির্বাচন করুন',
            'marital_status.required' => 'বৈবাহিক অবস্থা নির্বাচন করুন',
            'house_type.required' => 'বাসার ধরন নির্বাচন করুন',
            'add.required' => 'মহল্লা/রোড/হোল্ডিং/গ্রাম/বাসা নং প্রদান করুন',
            'income.required' => 'পরিবারের মোট আয় প্রদান করুন',
            'expense.required' => 'পরিবারের মোট ব্যয় প্রদান করুন',
            'occupation.required' => 'পেশা নির্বাচন করুন',
            'file.required' => '১-মেগাবাইট এর অনধিক .jpeg/.jpg/.png ফাইল নির্বাচন করুন।',
        ];

        if ($familyDataArr->status == config('constants.status.Inactive')) {
            array_merge($required, (['phone' => 'required', 'nid' => 'required']));
            array_merge($requiredMsg, (['phone.required' => 'মোবাইল নং প্রদান করুন', 'nid.required' => 'জাতীয় পরিচয় পত্র প্রদান করুন']));
        }

        $this->validate($request, $required, $requiredMsg);

        DB::beginTransaction();
        try {
            $familyDataArr->nid = $request->input('nid');
            $familyDataArr->phone = $request->input('phone');
            $familyDataArr->name = $request->input('name');
            $familyDataArr->dob = $request->input('dob');
            $familyDataArr->religion = $request->input('religion');
            $familyDataArr->education = $request->input('education');
            $familyDataArr->f_h_name = $request->input('f_h_name');
            $familyDataArr->gender = $request->input('gender');
            $familyDataArr->marital_status = $request->input('marital_status');
            $familyDataArr->div = $request->input('div');
            $familyDataArr->dist = $request->input('dist');
            $familyDataArr->sub_dist = $request->input('sub_dist');
            $familyDataArr->union = $request->input('union');
            $familyDataArr->word = $request->input('word');
            $familyDataArr->add = $request->input('add');
            $familyDataArr->house_type = $request->input('house_type');
            $familyDataArr->income = $request->input('income');
            $familyDataArr->expense = $request->input('expense');
            $familyDataArr->occupation = $request->input('occupation');
            $familyDataArr->ssnp = $request->input('ssnp');
            $familyDataArr->institute_id = auth()->user()->institute_id;
//            dd($familyDataArr);
            $familyDataArr->update();
            /*  File Manipulation   */
            $filename = '';
            try {
                if ($request->hasFile('picture')) {
                    $path = "images/families";
                    $filePath = $path . '/' . $id;
                    $uploadPath = public_path($filePath);
                    $file = $request->file('picture');
                    $filename = $id . '.png';
//                    $filename = $lastInsertedId . '.' . $file->getClientOriginalExtension();
                    try {
                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0755, true);
                        } else {
                            if (file_exists($uploadPath . $filename)) {
                                unlink($uploadPath . $filename);
                            }
                        }
//                    pr($filename);
                        $file->move($uploadPath, $filename); // Save in public folder
//                    $path = $file->storeAs($filePath, $filename); // Save in Storage/app folder
//                    $request->file->move(public_path($uploadPath), $filename);
                    } catch (\Exception $excp) {
                        DB::rollback();
                        dd($excp);
//                        $request->session()->flash('errors', 'তথ্য সফলভাবে পরিবর্তন করা সম্ভব হয় নি...');
                        $request->session()->flash('errors', $excp->getMessage());
                        return response()->json(['status' => 'error']);
//                        var_dump($excp);
                    }
                }
            } catch (\Exception $exception) {
                DB::rollback();
                dd($exception);
                $request->session()->flash('errors', $exception->getMessage());
                return response()->json(['status' => 'error']);
//                var_dump($exception);
            }
            DB::commit();
            return redirect()->route('families.index')
                ->with('success', 'তথ্য সফলভাবে পরিবর্তন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
//            dd($exception);
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে পরিবর্তন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }

    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('family-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
//                Family::find($id)->delete();
//                $familyInfo = Family::find($id);
//                $familyInfo->status = config('constants.status.Inactive');

                Family::where('id', $id)
                    ->update(['status' => config('constants.status.Inactive'), 'phone' => null, 'nid' => null]);

                FamilyMember::where('family_id', $id)
                    ->update(['status' => config('constants.status.Inactive')]);


                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }


    public function export()
    {
        return Excel::download(new FamilyExport, 'users.xlsx');
    }

    public function import(Request $request)
    {
        $request->validate(
            [
                'div' => 'required',
                'dist' => 'required',
                'sub_dist' => 'required',
                'union' => 'required',
                'file' => 'required|max:8243873|mimes:xlsx,xls',
            ],
            [
                'div.required' => 'বিভাগ নির্বাচন করুন',
                'dist.required' => 'জেলা নির্বাচন করুন',
                'sub_dist.required' => 'উপজেলা নির্বাচন করুন',
                'union.required' => 'পৌরসভা না ইউনিয়ন নির্বাচন করুন',
                'file.required' => '৩-মেগাবাইট এর অনধিক .xlsx/xls ফাইল নির্বাচন করুন।'
            ]
        );

        if (request()->has('file')) {
            $paramsData = $request->all();
//            pr($paramsData);
            unset($paramsData['_token']);
            unset($paramsData['file']);
//            pr($paramsData);
//            dd();

            if (isset($_FILES['file'])) {
                if ($_FILES['file']['size'] > 8243873) { //10 MB (size is also in bytes)
                    $msg = ['ফাইলের সাইজ ৮ মেগাবাইটের বেশি। পুনরায় ৮ মেগাবাইটের ভিতরের ফাইল আপলোড করুন।।'];
                    return back()->with('no_access', 'ফাইলের সাইজ ৮ মেগাবাইটের বেশি। পুনরায় ৮ মেগাবাইটের ভিতরের ফাইল আপলোড করুন।।');
                } else {
                    $file = $request->file('file')->store("import");
//            $file = $request->file('file');
//            dd($file);
                    $import = new FamilyImport($paramsData);
                    $import->import($file);

                    if ($import->failures()->isNotEmpty()) {
                        return back()->withFailures($import->failures());
                    }
                    return back()->with('success', 'তথ্য যুক্ত হয়েছে। ');
                }
            }

        }
//        }catch (\Maatwebsite\Excel\Validators\Val idationException $e){
//            $failures = $e->failures();
//            dd($failures);
//        }

    }

    public function sampledatadownload()
    {
        $file = Storage::disk('public/family_sample_data.xlsx');
        return (new Response($file, 200))
            ->header('Content-Type', 'image/jpeg');
    }

    public function phoneDuplicateCheck(Request $request)
    {
        if ($request->ajax()) {
            $res = phoneCheck($request->phone);
            return response()->json($res);
//            try {
//                $this->validate($request, [
//                    'phone' => 'unique:families',
//                ]);
//                return response()->json(['msg' => '']);
//            } catch (\Exception $exception) {
//                return response()->json(['err' => 'নম্বরটি পূর্বে ব্যবহার করা হয়েছে।']);
//            }
        }
//        dd($request->post('phone'));
//        $phone = $request->post('phone');
//        $resData = phoneCheck($phone);
//        return response()->json($resData);
    }

    public function nidDuplicateCheck(Request $request)
    {
        if ($request->ajax()) {
            $res = nidCheck($request->nid);
            return response()->json($res);
//            try {
//                $this->validate($request, [
//                    'nid' => 'unique:families',
//                ]);
//                return response()->json(['msg' => '']);
//            } catch (\Exception $exception) {
//                return response()->json(['err' => 'নম্বরটি পূর্বে ব্যবহার করা হয়েছে।']);
//            }
        }
//        dd($request->post('phone'));
//        $phone = $request->post('phone');
//        $resData = phoneCheck($phone);
//        return response()->json($resData);
    }

    public function getFamilyInfo(Request $request)
    {
        if ($request->ajax()) {
            $res = getFamilyIdByCardNo($request->card_no, false);
            $res->add = $res->add . ", ওয়ার্ডঃ " . $res->word . ", ইউনিয়ন/পৌরসভাঃ " . getUnionName($res->union) . ", উপজেলাঃ " . getSubDistName($res->sub_dist) . ", জেলাঃ " . getDistName($res->dist) . ", বিভাগঃ " . getDivName($res->div);

            $res->occupation = getOccupation($res->occupation);
//            dd($res);
            return response()->json($res);
//            try {
//                $this->validate($request, [
//                    'nid' => 'unique:families',
//                ]);
//                return response()->json(['msg' => '']);
//            } catch (\Exception $exception) {
//                return response()->json(['err' => 'নম্বরটি পূর্বে ব্যবহার করা হয়েছে।']);
//            }
        }
//        dd($request->post('phone'));
//        $phone = $request->post('phone');
//        $resData = phoneCheck($phone);
//        return response()->json($resData);
    }
}
