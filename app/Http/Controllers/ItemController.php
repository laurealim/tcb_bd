<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Models\ItemType;
use Illuminate\Http\Request;
use DataTables;
use DB;

class ItemController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('item-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        try {
            $page_title = 'পণ্যের তালিকা';
            $itemTypeList = getItemTypeList();
            $unitArr = config('constants.unit.arr');

            if ($request->ajax()) {
                $query = DB::table('items');
//                $query = userAccess($query);

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->editColumn('type_id', function ($row) use($itemTypeList){
                        $type_id = $itemTypeList[$row->type_id];
                        return $type_id;
                    })
                    ->editColumn('unit', function ($row) use($unitArr){
                        $unit = $unitArr[$row->unit];
                        return $unit;
                    })
                    ->editColumn('status', function ($row){
                        $status = config('constants.status.arr.' . $row->status);
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('item-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('items.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('item-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('items.destroy', $row->id) . ' class="btn-sm btn-danger delete-item action-btn"><i class="fa fa-trash-alt"></i></a>';
                        }

                        return $action;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        } catch (\Exception $exception) {
            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


        return view('items.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('item-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন পণ্য তৈরী';
//        $permission = Permission::get();

        return view('items.create', compact('page_title'));

    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('item-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'type_id' => 'required',
                'unit' => 'required',
            ],
                [
                    'name.required' => 'পণ্যের নাম প্রদান করুন।',
                    'type_id.required' => 'পণ্যের ধরন বাছাই করুন।',
                    'unit.required' => 'পণ্যের ইউনিট বাছাই করুন।',
                ]
            );

            $itemDataArr = $request->all();
            $lastInsertedId = Item::create(array_merge($itemDataArr, ['institute_id' => auth()->user()->institute_id, 'created_by' => auth()->user()->id, "status" => config('constants.status.Active')]));

            DB::commit();
            return redirect()->route('items.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show(ItemType $itemType)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('item-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'পণ্যের তথ্য সংশোধন';
        DB::beginTransaction();
        try {
            $item = Item::find($id);
            return view('items.edit', compact('item', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('item-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'type_id' => 'required',
                'unit' => 'required',
                'status' => 'required',
            ],
                [
                    'name.required' => 'পণ্যের নাম প্রদান করুন',
                    'type_id.required' => 'পণ্যের ধরন বাছাই করুন',
                    'unit.required' => 'পণ্যের ইউনিট বাছাই করুন',
                    'status.required' => 'বর্তমান অবস্থা বাছাই করুন',
                ]);

            $itemRes = Item::find($id);
            $itemRes->name = $request->input('name');
            $itemRes->type_id = $request->input('type_id');
            $itemRes->unit = $request->input('unit');
            $itemRes->status = $request->input('status');
            $itemRes->updated_by = auth()->user()->id;
            $itemRes->institute_id = auth()->user()->institute_id;
            $itemRes->save();

            DB::commit();
            return redirect()->route('items.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('item-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Item::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
