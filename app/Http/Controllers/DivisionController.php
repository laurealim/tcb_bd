<?php

namespace App\Http\Controllers;

use App\Models\Division;
use App\Models\Country;
use App\Models\Nested;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use DB;

class DivisionController extends Controller
{

    public function index(Request $request)
    {
        if (!auth()->user()->can('location-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        try {
            $page_title = 'বিভাগের তালিকা';
            $divLists = getDivLists();
//            $nesteds = Nested::tree()->get()->toTree()->where("id","=", 4);
//            dd($nesteds);

            if ($request->ajax()) {
                $query = DB::table('divisions');
//                $query = userAccess($query);

                $serial = 1;
                return DataTables::of($query)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('location-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('divisions.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('location-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('divisions.destroy', $row->id) . ' class="btn-sm btn-danger delete-division action-btn"><i class="fa fa-trash-alt"></i></a>';
                        }

                        return $action;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        } catch (\Exception $exception) {
//            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


        return view('divisions.index', compact('page_title', 'divLists'));
    }

    public function create()
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন বিভাগ তৈরী';
//        $permission = Permission::get();

        return view('divisions.create', compact('page_title'));

    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'div_code' => 'required',
            ],
                [
                    'name.required' => 'বিভাগের ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'বিভাগের বাংলা নাম প্রদান করুন',
                    'div_code.required' => 'বিভাগের কোড প্রদান করুন',
                ]
            );

            $divisionDataArr = $request->all();
            $lastInsertedId = Division::create($divisionDataArr);

            DB::commit();
            return redirect()->route('divisions.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'বিভাগের তথ্য সংশোধন';
        DB::beginTransaction();
        try {
            $division = Division::find($id);
            return view('divisions.edit', compact('division', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'div_code' => 'required',
            ],
                [
                    'name.required' => 'বিভাগের ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'বিভাগের বাংলা নাম প্রদান করুন',
                    'div_code.required' => 'বিভাগের কোড প্রদান করুন',
                ]);

            $divisionRes = Division::find($id);
            $divisionRes->name = $request->input('name');
            $divisionRes->bn_name = $request->input('bn_name');
            $divisionRes->div_code = $request->input('div_code');
            $divisionRes->save();

            DB::commit();
            return redirect()->route('divisions.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('location-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Division::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $divisions = new Division();

        $divisions = $divisions->select('divisions.*')
            ->where('divisions.name', 'like', '%' . $searchData . '%')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('division.ajax_list', compact('divisions'));
        } else {
            return view('division.adminList', compact('divisions'));
        }
    }

    public function adminForm()
    {
        $countryData = Country::all();
        return view('division.adminForm', compact('countryData'));
    }

    public function adminStore(Request $request)
    {
        $divisionModel = new Division();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
        ], [
            'name.required' => 'Division Name is required',
            'bn_name.required' => 'Division Name (Bangla) is required',

        ]);

        $divisionModel->saveData($request);
        return redirect('admin/division/list')->with('success', 'New Division added successfully');
    }

    public function show(Division $division)
    {
        //
    }

    public function adminEdit(Division $division, $id)
    {
        $countryData = Country::all();
        $divisionData = Division::where('id', $id)->first();
        return view('division.adminEdit', compact('countryData', 'divisionData', 'id'));
    }

    public function adminUpdate(Request $request, Division $division)
    {
        $divisionModel = new Division();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
        ], [
            'name.required' => 'Division Name is required',
            'bn_name.required' => 'Division Name (Bangla) is required',

        ]);

        $divisionModel->updateData($request);

        return redirect('admin/division/list')->with('success', 'Division edited successfully');
    }

    public function adminDelete(Request $request, Division $division)
    {
        $divisionInfo = Division::findOrFail($request->id);
        if (isset($request->id)) {
            $divisionInfo->delete();
            $request->session()->flash('success', 'Division Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'Division Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminDivisionSelectAjaxList(Request $request)
    {
        return $this->_divisionSelectAjaxList($request);
    }

    public function clientDivisionSelectAjaxList(Request $request)
    {
        return $this->_divisionSelectAjaxList($request);
    }

    private function _divisionSelectAjaxList($request)
    {
        $divisions = array();
        if ($request->ajax()) {

            $divisionData = new Division();
            $countryID = $request->countryID;
            $divisions = $divisionData->where("country_id", $countryID)->pluck("name", "id")->all();

//            $states = DB::table('states')->where('id_country',$request->id_country)->pluck("name","id")->all();
//            $data = view('ajax-select',compact('states'))->render();
//            return response()->json(['options'=>$data]);
            return json_encode($divisions);
        }
    }
}
