<?php

namespace App\Http\Controllers;

use App\Imports\BulkPackageAssignImport;
use App\Models\Package;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PackageController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('package-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
//        $resultChild = getAllChildList(18);
//        $resultPatents = getAllParentsList(79);
//        pr($resultChild);
//        dd($resultPatents);

        DB::beginTransaction();
        try {
            $page_title = 'প্যাকেজের তালিকা';
            $aidType = getAidTypeList();
            $lotLists = getLotLists()->toArray();
//            dd($lotLists);

            $lotListsArr = [];
//            if (!empty($lotListsArr)) {
            foreach ($lotLists as $key => $values) {
                if (!isset($lotListsArr[$values->package_id])) {
                    $lotListsArr[$values->package_id] = [];
                }
                $lotListsArr[$values->package_id][$values->id] = $values;
            }
//            }
//            $lotListsArr = keyValueSwap($lotLists);
//            pr($lotListsArr);
//            dd(auth()->user()->div);
            if ($request->ajax()) {
                $query = DB::table('packages');
                if ((auth()->user()->div >= 1) && (auth()->user()->institute_id >= 1)) {
                    $query = getPackageAssignedDistricts($query);
                } else if ((auth()->user()->div < 1) && (auth()->user()->institute_id >= 1)) {
                    $query = getOtherInstitutionBySameMinistry($query, auth()->user()->institute_id);
//                    $query = userDataAccess($query);
                } else {
                }
//                $rreedd = $query->get()->toArray();
//                dd($rreedd);

//                dd($query->toSql());
//                $data = Package::select('*')
//                    ->where('institute_id', auth()->user()->institute_id);

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->addColumn('lot', function ($row) use ($lotListsArr) {
                        $lot = "";
                        $url = "#";
                        if (auth()->user()->can('lot-edit')) {
                            if (array_key_exists($row->id, $lotListsArr)) {
                                foreach ($lotListsArr[$row->id] as $p_id => $l_data) {
                                    if ($row->status === config('constants.status.Active')) {
                                        $class = "button-lot-waiting";
                                        if ($l_data->state === config('constants.state.Running')) {
                                            $class = "button-lot-running";
                                        }
                                        if ($l_data->state === config('constants.state.Finished')) {
                                            $class = "button-lot-finished";
                                        }

                                        $url = route('lot-item-amount.edit', [$row->id, $l_data->id]);
                                    } else {
                                        $class = "btn-secondary";
                                    }

//                                    pr($l_data);
                                    $lot .= ' <a title="লট" class="btn-sm btn ' . $class . '" id="lot_id_"' . $l_data->id . ' href= ' . $url . '> ' . en2bn($l_data->lot_no) . '</a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                                }
                            } else {
                                $lot .= "কোন লট তৈরী করা হয় নি।";
                            }
                        }
                        return $lot;
                    })
                    ->addColumn('action', function ($row) use ($lotListsArr) {
                        $action = '';
                        if (auth()->user()->can('package-edit')) {
                            $action .= ' <a title="সম্পাদন" class="btn-sms btn btn-primary" id="edit-package" href= ' . route('packages.edit', $row->id) . '><i class="fa fa-pencil-alt"></i> </a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
//                                $action .= '<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('package-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('packages.destroy', $row->id) . ' class="btn-sms btn btn-danger delete-package"><i class="fa fa-trash-alt"></i></a><meta name="delete"> ';
                        }
                        if ($row->status === config('constants.status.Active')) {
                            if (auth()->user()->can('package-item-add')) {
                                $action .= " <a title='পণ্য যুক্ত করুন' href='#' class='btn btn-info items-add btn-sms action-btn' data-toggle='modal' data-target-id='$row->id' data-target='#modal-lg'><i class='fa fa-gifts'></i></a>";
//                            $action .= '<a title="পণ্য যুক্ত করুন" id=' . $row->id . ' href='#' class="btn btn-info items-add"><i class="fa-solid fa-cart-plus"></i></a><meta name="addItems"> ';
                            }
                            if (auth()->user()->can('package-lot-add')) {
                                $action .= " <a title='লট যুক্ত করুন' href='" . route('lot-item-amount.add', $row->id) . "' class='btn button-lot items-add btn-sms action-btn'><i class='fa-solid fa-box-open'></i></a>";
//
                            }
                            if (array_key_exists($row->id, $lotListsArr)) {
                                if (auth()->user()->can('dealer-assign')) {
                                    $action .= " <a title='ডিলার এসাইন করুন' href='#' class='btn btn-assign-lot btn-sms action-btn' data-toggle='modal' data-target-id='$row->id' data-target='#modal-lg-dealer'><i class='fa-solid fa-user-tag'></i></a>";
//
                                }
                            }
                        }
                        return $action;
                    })
                    ->editColumn('aid_id', function ($row) use ($aidType) {
                        $aid_id = $aidType[$row->aid_id];
//                        $gender = config('constants.gender.arr.' . $row->gender);
//                        $div_name = getDivName($row->div);
                        return $aid_id;
                    })
                    ->editColumn('status', function ($row) {
                        $class = "button-lot-inactive";
                        if ($row->status === config('constants.status.Active')) {
                            $class = "button-lot-active";
                        }
                        $status = ' <a title="স্ট্যাটাস" class="btn-sm btn ' . $class . '" id="status" href= ""> ' . config('constants.status.arr.' . $row->status) . '</a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        return $status;
                    })
                    ->rawColumns(['action', 'lot', 'status'])
                    ->make(true);
            }
            return view('packages.index', compact('page_title', 'aidType'));
//            $data = Role::orderBy('id', 'DESC')->paginate(10);
//
//            return view('roles.index', compact('data'));
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function create()
    {
        if (!auth()->user()->can('package-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $distList = getDistLists(auth()->user()->div, true);

        $page_title = 'নতুন প্যাকেজ তৈরী';
        $aidType = getAidTypeList();

        return view('packages.create', compact('page_title', 'aidType', 'distList'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('package-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();

        $request->validate([
            'name' => 'required',
            'aid_id' => 'required',
            'assign_dist' => 'required',
        ],
            [
                'name.required' => 'প্যাকেজের নাম প্রদান করুন',
                'aid_id.required' => 'ত্রানের ধরন প্রদান করুন',
                'assign_dist.required' => 'জেলা বাছাই করুন',
            ]
        );
        try {
            $resDataArr = $request->all();
//            dd($resDataArr['assign_dist']);
            $assign_dist = $resDataArr['assign_dist'];
            $resDataArr['assign_dist'] = json_encode($resDataArr['assign_dist']);
            $resDataArr['status'] = config('constants.status.Active');
            $resDataArr['created_by'] = auth()->user()->id;
            $resDataArr['institute_id'] = auth()->user()->institute_id;
//            dd($assign_dist);

            $assignedDiv = getParentDiv($assign_dist);
            $resDataArr["assign_div"] = json_encode($assignedDiv, true);


            Package::create($resDataArr);

            return redirect()->route('packages.index')
                ->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('package-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'প্যাকেজ সংশোধন';
        DB::beginTransaction();
        try {
            $aidType = getAidTypeList();
            $distList = getDistLists(auth()->user()->div, true);
            $package = Package::find($id);
//            dd($distList);
            return view('packages.edit', compact('aidType', 'package', 'page_title', 'distList'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('package-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        $request->validate([
            'name' => 'required',
            'aid_id' => 'required',
            'assign_dist' => 'required',
        ],
            [
                'name.required' => 'প্যাকেজের নাম প্রদান করুন',
                'aid_id.required' => 'ত্রানের ধরন প্রদান করুন',
                'assign_dist.required' => 'জেলা বাছাই করুন',
            ]
        );
        try {
            $packages = Package::find($id);
            $packages->name = $request->input('name');
            $packages->aid_id = $request->input('aid_id');
            $packages->assign_dist = $request->input('assign_dist');
            $assignedDiv = getParentDiv($request->input('assign_dist'));
            $packages->assign_div = json_encode($assignedDiv, true);
//            dd($request->input('assign_dist'));
//            dd($assignedDiv);
//            ["4","8","9","10", "18", "22", "34", "33", "43", "29", "50","44", "64", "39", "41"];
            $packages->status = $request->input('status');
            $packages->updated_by = auth()->user()->id;
            $packages->save();

            return redirect()->route('packages.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('package-delete')) {
            $request->session()->flash('error', 'আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Package::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('error', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('error', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function getPackageLists(Request $request)
    {
        if (!auth()->user()->can('package-list')) {
            $request->session()->flash('error', 'আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }
        $family_id = $request->id;
        if ($request->ajax()) {
            try {
                $packageList = packageList($family_id);
                return response()->json(['status' => 'success', 'packageList' => $packageList, 'family_id' => $family_id]);
            } catch (\Exception $exception) {
                $request->session()->flash('error', $exception->getMessage());
                return response()->json(['status' => 'error']);
            }
        }


    }

    public function getAssignedPackageLists(Request $request)
    {
        if (!auth()->user()->can('package-list')) {
            $request->session()->flash('error', 'আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        if ($request->ajax()) {
            try {
                $query = DB::table('packages');
                $postData['divId'] = $request->divId;
                $postData['districtID'] = $request->districtID;
//                dd($postData);
//                    ->where('status', '!=', config('constants.status.Inactive'));
                $query = getPackageAssignedDistricts($query, null, null, $postData);
//                dd($query->toSql());
                $packageList = $query->pluck('packages.name', 'packages.id')->toArray();
                return response()->json(['status' => 'success', 'packageList' => $packageList]);
            } catch (\Exception $exception) {
                $request->session()->flash('error', $exception->getMessage());
                return response()->json(['status' => 'error']);
            }
        }
    }

    public function assignPackage(Request $request)
    {
        if (!auth()->user()->can('package-assign')) {
            $request->session()->flash('error', 'আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        $family_id = $request->familyId;
        $package_id = str2int($request->packageId);
        $dealere_id = str2int($request->dealerId);
//        dd(gettype($package_id));
        if ($request->ajax()) {
            try {
                $resData = assignPackage($family_id, $package_id, $dealere_id);
//                dd($resData);
                if(!empty($resData['code'])){
                    $request->session()->flash('error', 'পরিবারটি উক্ত ডিলারের অধিক্ষেত্রের অন্তরভুক্ত নয়...');
                    return response()->json(['status' => 'error']);
                }
                if ($resData) {
                    $request->session()->flash('success', 'প্যাকেজ এসাইন করা হয়েছে।');
                    return response()->json(['status' => 'success']);
                } else {
                    $request->session()->flash('error', 'প্যাকেজ এসাইন করা সম্ভব হয় নি।');
                    return response()->json(['status' => 'error']);
                }
            } catch (\Exception $exception) {
                dd($exception->getMessage());
                $request->session()->flash('error', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        }
    }

    public function unAssignPackage(Request $request)
    {
    }

    public function bulkAssignPackage(Request $request)
    {
        $page_title = 'বাল্ক প্যাকেজ ডাটা এসাইন';
        $packageList = getAllPackageList();
        $dealerList = getDealerLists();
//        dd($dealerList);
        return view('packages.assign_package', compact('page_title', 'packageList','dealerList'));
    }

    public function saveBulkAssignPackage(Request $request)
    {

        try {
            $path = $request->file('file')->getRealPath();
            $paramsData['packageId'] = $request->post('packageId');
            $paramsData['dealerId'] = $request->post('dealerId');
            $request->validate(
                [
                    'packageId' => 'required',
                    'dealerId' => 'required',
                    'file' => 'required|max:8243873|mimes:xlsx,xls',
                ],
                [
                    'packageId.required' => 'প্যাকেজ নির্বাচন করুন',
                    'dealerId.required' => 'ডিলার নির্বাচন করুন',
                    'file.required' => '৩-মেগাবাইট এর অনধিক .xlsx/xls ফাইল নির্বাচন করুন।'
                ]
            );

            Excel::import(new BulkPackageAssignImport($paramsData), $path);
            return redirect()->route('packages.bulkAssignPackage')->with('success', 'প্যাকেজ এসাইন সফল হয়েছে।');

        } catch (\Exception $exception) {
//            dd($exception->getMessage());
            return redirect()->route('packages.bulkAssignPackage')->with('error', $exception->getMessage());
//            $request->session()->flash('errors', $exception->getMessage());
//            return response()->json(['status' => 'error']);
        }

    }

    public function saveItem(Request $request)
    {
        $itemList = $request->post('itemList');
        $packageId = $request->post('packageId');

        if ($request->ajax()) {
            DB::beginTransaction();
            try {
                DB::table('packages')
                    ->where('id', $packageId)
                    ->update(array('items' => explode(",", $itemList)));

                DB::commit();
                $request->session()->flash('success', 'আইটেম যুক্ত করা হয়েছে।');
                return response()->json(['status' => 'success']);
            } catch (\Exception $exception) {
                DB::rollback();
                dd($exception->getMessage());
                $request->session()->flash('error', 'আইটেম যুক্ত করা সম্ভব হয় নি।');
                return response()->json(['status' => 'error']);
            }
        }
    }
}
