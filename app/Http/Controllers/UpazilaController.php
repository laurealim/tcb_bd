<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Division;
use App\Models\Upazila;
use Illuminate\Http\Request;
use DataTables;
use DB;

class UpazilaController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('location-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        try {
            $page_title = 'উপজেলার তালিকা';
            $sub_distLists = getSubDistLists();
            $distLists = getDistLists();
//            $nesteds = Nested::tree()->get()->toTree()->where("id","=", 4);
//            dd($nesteds);

            if ($request->ajax()) {
                $query = DB::table('upazilas');
//                $query = userAccess($query);

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->addColumn('dist_name', function ($row) use($distLists) {
                        $dist_name = "";
                        $dist_name = $distLists[$row->district_id];
                        return $dist_name;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('location-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('upazillas.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('location-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('upazillas.destroy', $row->id) . ' class="btn-sm btn-danger delete-upazilla action-btn"><i class="fa fa-trash-alt"></i></a>';
                        }

                        return $action;
                    })
                    ->rawColumns(['action','dist_name'])
                    ->make(true);
            }
        } catch (\Exception $exception) {
//            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


        return view('upazillas.index', compact('page_title', 'sub_distLists'));
    }

    public function create()
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন উপজেলা তৈরী';

        return view('upazillas.create', compact('page_title'));

    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'up_code' => 'required',
                'district_id' => 'required',
            ],
                [
                    'name.required' => 'উপজেলার ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'উপজেলার বাংলা নাম প্রদান করুন',
                    'up_code.required' => 'উপজেলার কোড প্রদান করুন',
                    'district_id.required' => 'জেলা নির্বাচন করুন',
                ]
            );

            $districtDataArr = $request->all();
            $lastInsertedId = Upazila::create($districtDataArr);

            DB::commit();
            return redirect()->route('upazillas.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'উপজেলার তথ্য সংশোধন';
        DB::beginTransaction();
        try {
            $sub_district = Upazila::find($id);
            return view('upazillas.edit', compact('sub_district', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'up_code' => 'required',
                'district_id' => 'required',
            ],
                [
                    'name.required' => 'উপজেলার ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'উপজেলার বাংলা নাম প্রদান করুন',
                    'up_code.required' => 'উপজেলার কোড প্রদান করুন',
                    'district_id.required' => 'জেলা নির্বাচন করুন',
                ]
            );

            $sub_districtRes = Upazila::find($id);
            $sub_districtRes->name = $request->input('name');
            $sub_districtRes->bn_name = $request->input('bn_name');
            $sub_districtRes->up_code = $request->input('up_code');
            $sub_districtRes->district_id = $request->input('district_id');
            $sub_districtRes->save();

            DB::commit();
            return redirect()->route('upazillas.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
//            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('location-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Upazila::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $upazilaModel = new Upazila();

        $upazilaLists = $upazilaModel->select('divisions.bn_name as division_name', 'districts.bn_name as district_name', 'upazilas.*')
            ->leftJoin('districts', 'districts.id', '=', 'upazilas.district_id')
            ->leftJoin('divisions', 'divisions.id', '=', 'districts.division_id')
            ->where('districts.name', 'like', '%' . $searchData . '%')
            ->orwhere('divisions.name', 'like', '%' . $searchData . '%')
            ->orwhere('upazilas.name', 'like', '%' . $searchData . '%')
            ->orderby('id','desc')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('upazila.ajax_list', compact('upazilaLists'));
        } else {
            return view('upazila.adminList', compact('upazilaLists'));
        }
    }

    public function adminForm()
    {
        $divisionModel = new Division();
        $districtModel = new District();
        $divisionList = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        return view('upazila.adminForm', compact('divisionList', 'districtList'));
    }

    public function adminStore(Request $request)
    {
        $upazilaModel = new Upazila();
        try {
            $data = $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'district_id' => 'required',
            ], [
                'name.required' => 'District Name is required',
                'bn_name.required' => 'District Name (Bangla) is required',
                'district_id.required' => 'Please Select a District',

            ]);

            $upazilaModel->saveData($request->except('division_id', '_token'));
            return redirect('admin/upazila/list')->with('success', 'New Upazila added successfully');
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'Data can not saved...');
            return redirect()->back();
        }

    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id, Request $request)
    {
        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();

        try {
            $upazilaData = Upazila::where('id', $id)->first();
            $divisionList = $divisionModel->pluck('bn_name', 'id')->all();
//            dd($districtList);
            $districtData = District::where('id', $upazilaData->district_id)->first();
            $divisionID = $districtData->division_id;
            $districtList = $districtModel->where('division_id', $divisionID)->pluck('bn_name', 'id')->all();
//            dd($divisionID);
            return view('upazila.adminEdit', compact('divisionList', 'upazilaData', 'divisionID', 'districtList', 'id'));
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
        }
    }

    public function adminUpdate(Request $request, $id)
    {
        $upazilaModel = new Upazila();
        try {
            $data = $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'district_id' => 'required',
            ], [
                'name.required' => 'District Name is required',
                'bn_name.required' => 'District Name (Bangla) is required',
                'district_id.required' => 'Please Select a District',

            ]);
            unset($request['division_id']);

            $upazilaModel->updateData($request);

            return redirect('admin/upazila/list')->with('success', 'Upazila edited successfully..');
        }catch (\Exception $exception){
            dd($exception);
            $request->session()->flash('error', 'Upazila can not edited successfully...');
            return redirect()->back();
        }

    }

    public function adminDestroy($id, Request $request)
    {
        $upazilaInfo = Upazila::findOrFail($id);
        if (isset($request->id)) {
            $upazilaInfo->delete();
            $request->session()->flash('success', 'Upazila Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'Upazila Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function upazilaSelectAjaxList(Request $request)
    {
        if ($request->ajax()) {

            $query = DB::table('upazilas');
            $sub_dist = auth()->user()->sub_dist;
            if (!empty($sub_dist)) {
                $query->where('id', $sub_dist);
            }
            $districtID = $request->post('districtID');
            $query->where("district_id", $districtID);
            $upazilaList = $query->pluck('bn_name', 'id');
            return json_encode($upazilaList);


//            $upazilaModel = new Upazila();
//            $districtID = $request->post('districtID');
//            $upazilaList = $upazilaModel->where("district_id", $districtID)->pluck("bn_name", "id")->all();
//            return json_encode($upazilaList);
        }
    }

    public function adminUpazilaSelectAjaxList(Request $request)
    {
//        var_dump($request->post('divisionId'));
        return $this->_upazilaSelectAjaxList($request);
    }

    public function clientUpazilaSelectAjaxList(Request $request)
    {
//        var_dump($request->post('divisionId'));
        return $this->_upazilaSelectAjaxList($request);
    }

    public function userUpazilaSelectAjaxList(Request $request)
    {
//        var_dump($request->post('divisionId'));
        return $this->_upazilaSelectAjaxList($request);
    }

    private function _upazilaSelectAjaxList($request)
    {
        if ($request->ajax()) {

            $upazilaModel = new Upazila();
            $districtID = $request->post('districtID');
            $upazilaList = $upazilaModel->where("district_id", $districtID)->pluck("bn_name", "id")->all();
            return json_encode($upazilaList);
        }
    }
}
