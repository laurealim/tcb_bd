<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLotItemAmountRequest;
use App\Http\Requests\UpdateLotItemAmountRequest;
use App\Models\LotItemAmount;
use DataTables;
use DB;
use Illuminate\Http\Request;

class LotItemAmountController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(StoreLotItemAmountRequest $request)
    {
        //
    }

    public function show(LotItemAmount $lotItemAmount)
    {
        //
    }

    public function edit(Request $request, $packageID, $lotId)
    {
        if (!auth()->user()->can('lot-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'লট সংশোধন';
        try {
            $packageInfo = getPakageInfo($packageID);
            $packageInfo = $packageInfo[0];

            $itemInfo = getItemInfo()->toArray();
            $itemInfo = keyValueSwap($itemInfo);
            $packageItemsArr = array_map('intval', json_decode($packageInfo->items, true));

            $lotInfo = getLotLists($packageID, $lotId)->toArray();
            $lotInfo = $lotInfo[0];
            $item_amount_arr = array_map('intval', json_decode($lotInfo->item_amount, true));
            $state = $lotInfo->state;

            return view('lots.edit', compact('page_title', 'packageInfo', 'itemInfo', 'packageItemsArr', 'lotInfo', 'item_amount_arr', 'state'));
        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }

    }

    public function update(UpdateLotItemAmountRequest $request, LotItemAmount $lotItemAmount)
    {
        //
    }

    public function destroy(LotItemAmount $lotItemAmount)
    {
        //
    }

    public function add(Request $request)
    {
        if (!auth()->user()->can('lot-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন লট তৈরী';
        $packageId = $request->packageID;
        try {
            $packageInfo = getPakageInfo($packageId);
            $packageInfo = $packageInfo[0];

            $itemInfo = getItemInfo()->toArray();
            $itemInfo = keyValueSwap($itemInfo);
            $packageItemsArr = array_map('intval', json_decode($packageInfo->items, true));
            $lotCount = getLotNumbers($packageId);

            return view('lots.add', compact('page_title', 'packageInfo', 'itemInfo', 'packageItemsArr', 'lotCount'));
        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


    }


    public function saveLot(Request $request, $id = null)
    {
        if (!auth()->user()->can('lot-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {

            if(empty($id)) {
                $this->validate($request, [
                    'start_date' => 'required',
                    'state' => 'required',
                    'end_date' => 'required',
                    'status' => 'required',
                ],
                    [
                        'status.required' => 'বর্তমান পর্যায় বাছাই করুন।',
                        'state.required' => 'স্ট্যাটাস বাছাই করুন।',
                        'start_date.required' => 'লট বিতরনের শুরুর দিন প্রদান করুন।',
                        'end_date.required' => 'লট বিতরনের শেষের দিন প্রদান করুন।',
                    ]
                );
            }
            else{
                $this->validate($request, [
                    'state' => 'required',
                    'status' => 'required',
                ],
                    [
                        'status.required' => 'বর্তমান পর্যায় বাছাই করুন।',
                        'state.required' => 'স্ট্যাটাস বাছাই করুন।',
                    ]
                );
            }

            $postData = $request->all();

            $package_id = $postData['package_id'];
            $lotNo = $postData['lot_no'];
            $item = $postData['item'];
            $amount = $postData['amount'];
            $total_people = $postData['total_people'];
            $state = $postData['state'];
            $status = $postData['status'];
            if(empty($id)) {
                $start_date = $postData['start_date'];
                $end_date = $postData['end_date'];
            }

            $item_amount = [];
            foreach ($item as $itemId => $value) {
                $item_amount[$itemId] = $amount[$itemId];
            }
//            dd($item_amount);
            if(!empty($id)){
                $lotDataArr = LotItemAmount::find($id);
                $lotDataArr->item_amount = json_encode($item_amount, true);
                $lotDataArr->total_people = $total_people;
//                $lotDataArr->start_date = $start_date;
//                $lotDataArr->end_date = $end_date;
                $lotDataArr->state = $state;
                $lotDataArr->status = $status;
                $lotDataArr->updated_by = auth()->user()->id;
                $lotDataArr->institute_id = auth()->user()->institute_id;

                $lotDataArr->save();
            }else{
                $lotItemAmountModel = [];

                $lotItemAmountModel['package_id'] = $package_id;
                $lotItemAmountModel['lot_no'] = $lotNo;
                $lotItemAmountModel['item_amount'] = json_encode($item_amount, true);
                $lotItemAmountModel['total_people'] = $total_people;
                $lotItemAmountModel['start_date'] = $start_date;
                $lotItemAmountModel['end_date'] = $end_date;
                $lotItemAmountModel['state'] = $state;
                $lotItemAmountModel['status'] = $status;
                $lotItemAmountModel['created_by'] = auth()->user()->id;
                $lotItemAmountModel['institute_id'] = auth()->user()->institute_id;

                LotItemAmount::create($lotItemAmountModel);
            }

            DB::commit();
            return redirect()->route('packages.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');

        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


    }
}
