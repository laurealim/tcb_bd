<?php

namespace App\Http\Controllers;

use App\Models\Institution;
use App\Models\User;
use DataTables;
use DB;
use Illuminate\Http\Request;

class InstitutionController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('institution-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        try {
            $page_title = 'প্রতিষ্ঠানের তালিকা';

            if ($request->ajax()) {
                $query = DB::table('institutions');
//                $query = userAccess($query);
                $minsListArr = getMinistryList();
                $bivagListArr = getBivagList();
                $deptListArr = getDepartmentList();
                $divListArr = getDivLists();
                $distListArr = getDistLists();
                $subDistListArr = getSubDistLists();
//                dd($query->get());

                $serial = 1;
                return DataTables::of($query)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('institution-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('institutions.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('institution-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('institutions.destroy', $row->id) . ' class="btn-sm btn-danger delete-institution action-btn"><i class="fa fa-trash-alt"></i></a>';
                        }

                        return $action;
                    })
                    ->addColumn('address', function ($row) use ($minsListArr, $bivagListArr, $deptListArr) {
                        $mins = $minsListArr[$row->mins_id];
                        $address = $mins . ", ";
                        if (!empty($row->divs_id)) {
                            $address .= $bivagListArr[$row->divs_id] . ", ";
                        }
                        if (!empty($row->dept)) {
                            if ($row->dept >= 1) {
                                $address .= $deptListArr[$row->dept] . ", ";
                            }
                        }

                        return $address;
                    })
                    ->editColumn('status', function ($row) {
                        $status = config('constants.status.arr.' . $row->status);
                        return $status;
                    })
                    ->editColumn('div', function ($row) use ($divListArr, $distListArr, $subDistListArr) {
                        $office = "মন্ত্রণালয়";
                        $flag = true;
                        $location = "";
                        if (!empty($row->sub_dist)) {
                            if ($flag) {
                                $office = 'উপজেলা কার্যালয়, ';
                                $flag = false;
                            }
                            $location .= $subDistListArr[$row->sub_dist] . ", ";
                        }
                        if (!empty($row->dist)) {
                            if ($flag) {
                                $office = 'জেলা কার্যালয়, ';
                                $flag = false;
                            }
                            $location .= $distListArr[$row->dist] . ", ";
                        }
                        if (!empty($row->div)) {
                            if ($flag) {
                                $office = 'বিভাগীয় কার্যালয়, ';
                                $flag = false;
                            }
                            $location .= $divListArr[$row->div];
                        }
                        return $office . $location;
                    })
                    ->rawColumns(['action', 'address'])
                    ->make(true);
            }
        } catch (\Exception $exception) {
//            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


        return view('institutions.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('institution-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন প্রতিষ্ঠান তৈরী';
        $minsListArr = getMinistryList();
        $divListArr = getDivLists();

        return view('institutions.create', compact('page_title', 'minsListArr', 'divListArr'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('institution-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $institutionsDataArr = $request->all();
//            dd($institutionsDataArr);

            $this->validate($request, [
                'name' => 'required',
                'mins_id' => 'required',
//                'div' => 'required',
//                'dist' => 'required',
            ],
                [
                    'name.required' => 'প্রতিষ্ঠানের নাম প্রদান করুন।',
                    'mins_id.required' => 'মন্ত্রণালয়ের নাম বাছাই করুন।',
//                    'div.required' => 'বিভাগ বাছাই করুন।',
//                    'dist.required' => 'জেলা বাছাই করুন।',
                ]
            );

            $lastInsertedId = Institution::create(array_merge($institutionsDataArr, ['institute_id' => auth()->user()->institute_id, 'created_by' => auth()->user()->id, "status" => config('constants.status.Active')]));

            DB::commit();
            return redirect()->route('institutions.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show(Institution $institution)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('institution-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'প্রতিষ্ঠানের তথ্য সংশোধন';
        DB::beginTransaction();
        try {
            $institutions = Institution::find($id);
//            dd($institutions);
            $mins_id = $institutions->mins_id;
            $divs_id = $institutions->divs_id;
            $dept_id = $institutions->dept;

            $div = $institutions->div;
            $dist = $institutions->dist;

            $bivagListArr = $deptListArr = $divListArr = $distListArr = $sub_distListArr = [];

            $minsListArr = getMinistryList();
            $divListArr = getDivLists(null, false);

            if (!empty($mins_id)) {
                if (!empty($divs_id)) {
                    $bivagListArr = getBivagListbyParentId($mins_id);
                }
                if (!empty($dept_id)) {
                    if ($dept_id === -1) {
                        $deptListArr = [-1 => 'সংশ্লিষ্ট মন্ত্রণালয়'];
                    } elseif ($dept_id === -2) {
                        $deptListArr = [-2 => 'সংশ্লিষ্ট বিভাগ'];
                    } else {
                        $deptListArr = getDepartmentListByMinsId($mins_id);
                    }
                }
            }

            if (!empty($divs_id)) {
                if ($dept_id === -2) {
                    $deptListArr = [-2 => 'সংশ্লিষ্ট বিভাগ'];
                } else {
                    $deptListArr = getDepartmentListByParentId($divs_id);
                }
            }

            if (!empty($div)) {
                $distListArr = getDistLists($div, false);
            }

            if (!empty($dist)) {
                $sub_distListArr = getSubDistLists($dist, false);
            }


            $dataArr = [
                'minsListArr' => $minsListArr,
                'distListArr' => $distListArr,
                'divListArr' => $divListArr,
                'bivagListArr' => $bivagListArr,
                'deptListArr' => $deptListArr,
                'sub_distListArr' => $sub_distListArr
            ];

            return view('institutions.edit', compact('institutions', 'page_title', 'dataArr'));
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('institution-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'mins_id' => 'required',
                'status' => 'required',
            ],
                [
                    'name.required' => 'প্রতিষ্ঠানের নাম প্রদান করুন।',
                    'mins_id.required' => 'মন্ত্রণালয়ের নাম বাছাই করুন।',
                    'status.required' => 'প্রতিষ্ঠানের অবস্থা বাছাই করুন।',
                ]
            );

            $institutionsRes = Institution::find($id);
            $institutionsRes->name = $request->input('name');
            $institutionsRes->mins_id = $request->input('mins_id');
            $institutionsRes->divs_id = $request->input('divs_id');
            $institutionsRes->dept = $request->input('dept');
            $institutionsRes->div = $request->input('div');
            $institutionsRes->dist = $request->input('dist');
            $institutionsRes->sub_dist = $request->input('sub_dist');
            $institutionsRes->status = $request->input('status');
            $institutionsRes->institute_id = auth()->user()->institute_id;

            $locatonData = [
                'institute_id' => $id,
                'div' => $institutionsRes->div,
                'dist' => $institutionsRes->dist,
                'sub_dist' => $institutionsRes->sub_dist,
                'union' => -2,
            ];
//            dd($locatonData);

            $res = User::where('institute_id', $id)
                ->update($locatonData);

            $institutionsRes->save();

            DB::commit();
            return redirect()->route('institutions.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('institution-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Institution::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
