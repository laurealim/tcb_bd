<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function cache_clear()
    {
        // Clear the cache of routes
        Artisan::call('route:cache');

        // Clear the cache of views
        Artisan::call('view:cache');

        // Clear the cache of configuration
        Artisan::call('config:cache');

        dd("Done...");
    }
}
