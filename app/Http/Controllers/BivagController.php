<?php

namespace App\Http\Controllers;

use App\Models\Bivag;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBivagRequest;
use App\Http\Requests\UpdateBivagRequest;
use Illuminate\Http\Request;

class BivagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreBivagRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBivagRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Bivag $bivag
     * @return \Illuminate\Http\Response
     */
    public function show(Bivag $bivag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Bivag $bivag
     * @return \Illuminate\Http\Response
     */
    public function edit(Bivag $bivag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateBivagRequest $request
     * @param \App\Models\Bivag $bivag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBivagRequest $request, Bivag $bivag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Bivag $bivag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bivag $bivag)
    {
        //
    }

    public function bivagSelectAjaxList(Request $request)
    {
        if ($request->ajax()) {
            $minsId = $request->post("minsId");
            $resArr = getBivagListbyParentId($minsId)->toArray();
            if (empty($resArr)) {
                $resArr = getDepartmentListByMinsId($minsId)->toArray();
                if (empty($resArr)) {
                    return response()->json(['status' => 404, 'dataId' => -1]);
                }
                return response()->json(['status' => 201, 'dataId' => $resArr]);
            }
            return response()->json(['status' => 200, 'dataId' => $resArr]);
        }
    }
}
