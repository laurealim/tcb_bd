<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFamilyMemberRequest;
use App\Imports\CheckSheetImport;
use App\Imports\FamilyMemberImport;
use App\Models\Family;
use App\Models\FamilyMember;
use DB;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class FamilyMemberController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('family-member-excel-import')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $page_title = 'পরিবারের তালিকা';
            DB::commit();
            return view('family-members.index', compact('page_title'));
        } catch (Exception $exception) {
            DB::rollback();
            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function create()
    {
        //
    }

    public function store(StoreFamilyMemberRequest $request)
    {
        //
    }

    public function show(FamilyMember $familyMember)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('family-member-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        $page_title = 'পরিবার সদস্যদের তথ্য যুক্ত/সংশোধন';
        try {
            $familyData = Family::select('id', 'card_no', 'div', 'dist', 'sub_dist', 'union', 'word', 'add', 'name', 'f_h_name', 'phone', 'nid', 'occupation')
                ->where('id', $id)
                ->first();

            $familyMemberData = FamilyMember::where('family_id', $id)->where('card_no', 'LIKE', $familyData->card_no)->get()->toArray();

            $familyMemberDataArray = [];
            if (!empty($familyMemberData)) {
                $familyMemberDataArray = familyMemberJsonDataToArray($familyMemberData[0]);
            }
//            dd($familyMemberDataArray);
            $occupationList = config('constants.occupation.arr');
            $ssnpList = config('constants.ssnp.arr');
            $relationList = config('constants.relation.arr');
            $genderList = config('constants.gender.arr');
            $eduLevelList = config('constants.edu_level.arr');
            return view('family-members.edit', compact('familyMemberDataArray', 'familyData', 'page_title', 'occupationList', 'ssnpList', 'relationList', 'genderList', 'eduLevelList'));
        } catch (Exception $exception) {
            dd($exception->getMessage());
//            DB::rollback();
            $request->session()->flash('errors', 'তথ্য খুঁজে পাওয়া সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('family-member-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        DB::beginTransaction();
        try {
            $flag = false;

            $familyHeadDataArr = Family::select('id', 'card_no')
                ->where('id', $id)->first();
            $familyMemberDataRes = FamilyMember::where('family_id', $id)->where('card_no', 'LIKE', $familyHeadDataArr->card_no)->get()->toArray();
            if (!empty($familyMemberDataRes)) {
                $flag = true;
            }

            $familyMemberData = $request->all();
            $familyMember = new FamilyMember();

//            pr($familyMemberData);
//            dd();
            if (is_null($familyMemberData['name'][0])) {
                $request->session()->flash('error', 'পরিবারের সদস্য যুক্ত করুন...');
                return redirect()->route('family-members.edit', $id);
            }

            $familyMember->family_id = $id;
            $familyMember->card_no = $familyHeadDataArr->card_no;
            $totalMember = sizeof($request->name);
            $total_male = $total_female = $total_others = $total_child = $total_old = 0;

//            Family Member Name Data
            $familyMember->m_name = json_encode($familyMemberData['name']);
//            Family Member DOB Data
            foreach ($familyMemberData['dob'] as $key => $dob) {
                if (!empty($dob)) {
                    if (getAge($dob) <= config('constants.ageSection.Baby')) {
                        $total_child++;
                    }
                    if (getAge($dob) >= config('constants.ageSection.Aged')) {
                        $total_old++;
                    }
                }
            }
            $familyMember->m_dob = json_encode($familyMemberData['dob']);
//            Family Member NID Data
            foreach ($familyMemberData['nid'] as $key => $nid) {
                if (!empty($nid))
                    $familyMemberData['nid'][$key] = bn2en($nid);
                else
                    $familyMemberData['nid'][$key] = $nid;
            }
//            pr($familyMemberData['nid']);
            $familyMember->m_nid = json_encode($familyMemberData['nid']);
//            Family Member Phone Data
            $familyMember->m_phone = json_encode($familyMemberData['phone']);
//            Family Member Gender Data
            foreach ($familyMemberData['gender'] as $key => $gender) {
                if ($gender == config('constants.gender.পুরুষ'))
                    $total_male++;
                else if ($gender == config('constants.gender.মহিলা'))
                    $total_female++;
                else if ($gender == config('constants.gender.অন্যান্ন'))
                    $total_others++;

            }
            $familyMember->m_gender = json_encode($familyMemberData['gender']);
//            Family Member Occupation Data
            $familyMember->m_occupation = json_encode($familyMemberData['occupation']);
//            Family Member Relation Data
            $familyMember->m_relation = json_encode($familyMemberData['relation']);
//            Family Member Education Data
            $familyMember->m_edu_level = json_encode($familyMemberData['education']);
//            Family Member Education Data
            $familyMember->m_ssnp = json_encode($familyMemberData['ssnp']);

            $familyMember->total_member = $totalMember;
            $familyMember->total_male = $total_male;
            $familyMember->total_female = $total_female;
            $familyMember->total_others = $total_others;
            $familyMember->total_child = $total_others;
            $familyMember->institute_id = auth()->user()->institute_id;


            if ($flag) {
                $lastInsertedId = FamilyMember::where('family_id', $id)->update($familyMember->toArray());
            } else {
                $lastInsertedId = FamilyMember::create($familyMember->toArray());
            }

            DB::commit();
            return redirect()->route('family-members.edit', $id)
                ->with('success', 'তথ্য সফলভাবে পরিবর্তন করা হয়েছে...');
        } catch (Exception $exception) {
            dd($exception->getMessage());
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(FamilyMember $familyMember)
    {
        //
    }

    public function add(Request $request)
    {

    }

    public function phoneDuplicateCheck(Request $request)
    {
        if ($request->ajax()) {
            try {
                $this->validate($request, [
                    'phone' => 'unique:families',
                ]);
                return response()->json(['msg' => '']);
            } catch (Exception $exception) {
                return response()->json(['err' => 'নম্বরটি পূর্বে ব্যবহার করা হয়েছে।']);
            }
        }
//        dd($request->post('phone'));
//        $phone = $request->post('phone');
//        $resData = phoneCheck($phone);
//        return response()->json($resData);
    }

    public function nidDuplicateCheck(Request $request)
    {
        if ($request->ajax()) {
            try {
                $this->validate($request, [
                    'nid' => 'unique:families',
                ]);
                return response()->json(['msg' => '']);
            } catch (Exception $exception) {
                return response()->json(['err' => 'নম্বরটি পূর্বে ব্যবহার করা হয়েছে।']);
            }
        }
//        dd($request->post('phone'));
//        $phone = $request->post('phone');
//        $resData = phoneCheck($phone);
//        return response()->json($resData);
    }

    public function import(Request $request)
    {
        $request->validate(
            [
                'file' => 'required|max:8243873|mimes:xlsx,xls',
            ],
            [
                'file.required' => '৩-মেগাবাইট এর অনধিক .xlsx/xls ফাইল নির্বাচন করুন।'
            ]
        );

        try {
            if (request()->has('file')) {
                $paramsData = $request->all();
                unset($paramsData['_token']);
                unset($paramsData['file']);

                $errorDataArr = [];

                if (isset($_FILES['file'])) {
                    if ($_FILES['file']['size'] > 8243873) { //10 MB (size is also in bytes)
                        $msg = ['ফাইলের সাইজ ৮ মেগাবাইটের বেশি। পুনরায় ৮ মেগাবাইটের ভিতরের ফাইল আপলোড করুন।।'];
                        return back()->with('no_access', 'ফাইলের সাইজ ৮ মেগাবাইটের বেশি। পুনরায় ৮ মেগাবাইটের ভিতরের ফাইল আপলোড করুন।।');
                    } else {
                        $file = $request->file('file')->store("import");

                        $import = new FamilyMemberImport();
                        $resData = $import->toArray($file);
                        $finalResArr = [];

                        foreach ($resData as $sheetNo => $values) {
                            if ($sheetNo < 1) {
                                foreach ($values as $value) {
                                    $hasError = false;
                                    $res = getFamilyIdByCardNo($value['family_card_no']);
//                                    pr($value['family_card_no']);
//                                    pr($res);
                                    if ($res < 1) {
                                        $errorDataArr[$value['family_card_no']][] = "কার্ড পাওয়া যায় নি।";
                                        $hasError = true;
                                    } else {
                                        $phone_res = phoneCheck(bn2en($value['m_phone']));
                                        if (isset($phone_res['err'])) {
                                            $errorDataArr[$value['family_card_no']][] = $value['m_phone'] . ' ' . $phone_res['err'];
                                            $hasError = true;
                                        }

                                        $nid_res = nidCheck(bn2en($value['m_nid']));
                                        if (isset($nid_res['err'])) {
                                            $errorDataArr[$value['family_card_no']][] = $value['m_nid'] . ' ' . $nid_res['err'];
                                            $hasError = true;
                                        }

                                        if ($hasError) {
//                                            return redirect()->route('family-members.import') ->with(['err_msg', $errorDataArr]);
//                                            return Redirect::route('clients.show, $id')->with( ['data' => $data] );
                                        } else {

                                            $finalResArr[$value['family_card_no']]['m_phone'][] = bn2en($value['m_phone']);
                                            $finalResArr[$value['family_card_no']]['m_nid'][] = bn2en($value['m_nid']);

                                            if (!isset($finalResArr[$value['family_card_no']])) {
                                                $finalResArr[$value['family_card_no']] = [];
                                            }

                                            if (!isset($finalResArr[$value['family_card_no']]['total_member'])) {
                                                $finalResArr[$value['family_card_no']]['total_member'] = 0;
                                                $finalResArr[$value['family_card_no']]['total_male'] = 0;
                                                $finalResArr[$value['family_card_no']]['total_female'] = 0;
                                                $finalResArr[$value['family_card_no']]['total_others'] = 0;
                                                $finalResArr[$value['family_card_no']]['total_old'] = 0;
                                                $finalResArr[$value['family_card_no']]['total_child'] = 0;
                                            }

                                            $m_dob = transformDate($value['m_dob']);
                                            $dob = date("Y-m-d", strtotime(bn2en($m_dob)));

                                            $finalResArr[$value['family_card_no']]['family_id'] = $res;
                                            $finalResArr[$value['family_card_no']]['card_no'] = $value['family_card_no'];
                                            $finalResArr[$value['family_card_no']]['m_name'][] = $value['m_name'];
                                            $finalResArr[$value['family_card_no']]['m_dob'][] = date("Y-m-d", strtotime(bn2en($dob)));

//                                            pr(config('constants.gender.Male'));
//                                            pr(config('constants.gender.Female'));
//                                            pr(config('constants.gender.Others'));
                                            $gender = intval(config('constants.gender.' . $value['m_gender']));
                                            $finalResArr[$value['family_card_no']]['m_gender'][] = $gender;
                                            if ($gender == config('constants.gender.Male')) {
                                                $finalResArr[$value['family_card_no']]['total_male']++;
                                            }
                                            if ($gender == config('constants.gender.Female')) {
                                                $finalResArr[$value['family_card_no']]['total_female']++;
                                            }
                                            if ($gender == config('constants.gender.Others')) {
                                                $finalResArr[$value['family_card_no']]['total_others']++;
                                            }
                                            $finalResArr[$value['family_card_no']]['total_member']++;
                                            $age = getAge($dob);
                                            if ($age <= config('constants.ageSection.Baby')) {
                                                $finalResArr[$value['family_card_no']]['total_child'] = $finalResArr[$value['family_card_no']]['total_child'] + 1;
                                            }
                                            if ($age >= config('constants.ageSection.Aged')) {
                                                $finalResArr[$value['family_card_no']]['total_old'] = $finalResArr[$value['family_card_no']]['total_old'] + 1;
                                            }


                                            if (array_search($value['m_occupation'], config('constants.occupation.arr'))) {
                                                $finalResArr[$value['family_card_no']]['m_occupation'][] = array_search($value['m_occupation'], config('constants.occupation.arr'));
                                            } else {
                                                $finalResArr[$value['family_card_no']]['m_occupation'][] = 0;
                                            }

                                            if (array_search($value['m_relation'], config('constants.relation.arr'))) {
                                                $finalResArr[$value['family_card_no']]['m_relation'][] = array_search($value['m_relation'], config('constants.relation.arr'));
                                            } else {
                                                $finalResArr[$value['family_card_no']]['m_relation'][] = 0;
                                            }

                                            if (array_search($value['m_ssnp'], config('constants.ssnp.arr'))) {
                                                $finalResArr[$value['family_card_no']]['m_ssnp'][] = array_search($value['m_ssnp'], config('constants.ssnp.arr'));
                                            } else {
                                                $finalResArr[$value['family_card_no']]['m_ssnp'][] = -1;
                                            }

                                            if (array_search($value['m_edu_level'], config('constants.edu_level.arr'))) {
                                                $finalResArr[$value['family_card_no']]['m_edu_level'][] = array_search($value['m_edu_level'], config('constants.edu_level.arr'));
                                            } else {
                                                $finalResArr[$value['family_card_no']]['m_edu_level'][] = 0;
                                            }
//                                pr($value);
                                        }

                                    }

                                    /* if error has data will not save to the $finalResArr */
                                }
                            }
                        }
                        /* Format data and prepare for save */
                        $saveDataArr = [];
                        foreach ($finalResArr as $card_no => $infos) {

                            $infos['m_name'] = json_encode($infos['m_name'], true);
                            $infos['m_dob'] = json_encode($infos['m_dob'], true);
                            $infos['m_gender'] = json_encode($infos['m_gender'], true);
                            $infos['m_occupation'] = json_encode($infos['m_occupation'], true);
                            $infos['m_relation'] = json_encode($infos['m_relation'], true);
                            $infos['m_ssnp'] = json_encode($infos['m_ssnp'], true);
                            $infos['m_edu_level'] = json_encode($infos['m_edu_level'], true);
                            $infos['m_phone'] = json_encode($infos['m_phone'], true);
                            $infos['m_nid'] = json_encode($infos['m_nid'], true);
                            $infos['status'] = config('constants.status.Active');

//                            $familuCardNo = $infos['card_no'];
                            $hasId = ifFamilyMemberExists($card_no);
                            if ($hasId >= 1) {
                                $errorDataArr[$card_no][] = 'পূর্বেই যুক্ত করা রয়েছে। ';
                            } else {
                                $resArr = FamilyMember::create($infos);
                            }

                            DB::commit();
                        }
//                        dd($resData);
                    }
                }

//                dd($errorDataArr);
                return redirect()->route('family-members.index')
                    ->with(['success' => 'তথ্য সফলভাবে যুক্ত করা হয়েছে...', 'failures' => $errorDataArr]);
            } else {
                /* No file has been found.  */
                return redirect()->route('family-members.index')
                    ->with('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি. ফাইল পাওয়া যায় নি।');
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }

    }

    public function sampledatadownload()
    {
        $file = Storage::disk('public/family_sample_data.xlsx');
        return (new Response($file, 200))
            ->header('Content-Type', 'image/jpeg');
    }

    public function shift(Request $request, $familyId = null, $memberId = null)
    {
        $page_title = 'পরিবারের সদস্য স্থানান্তর';
        if (!empty($familyId) && !empty($memberId)) {

            $familyInfo = getFamilyInfoById($familyId);
            return view('family-members.shift', compact('familyId', 'familyInfo', 'memberId', 'page_title'));
        }
    }

    public function shiftDone(Request $request)
    {
        DB::beginTransaction();
        try {
            $family_id = $request->family_id;
            $family_member_id = $request->family_member_id;
            $shift = $request->shift;
            $card_no = $request->card_no;

            /* Current Family Member Information */
            $fromFamilyData = getFamilyInfoById($family_id);
            $fromResData = getFamilyMemberInfo($family_id);
            $fromResDataArr = familyMemberJsonDataToArray(json_decode(json_encode($fromResData), true)); // get all members data as an array format.
            $shiftingMember[] = $fromResDataArr[$family_member_id - 1]; // which member is shifting


            /* Family Member Counting Update */
            $shiftingGender = $fromResDataArr[$family_member_id - 1]['m_gender'];

            if ($shiftingGender == config('constants.gender.Male')) {
                $fromResData->total_male = $fromResData->total_male - 1;
            }
            if ($shiftingGender == config('constants.gender.Female')) {
                $fromResData->total_female = $fromResData->total_female - 1;
            }
            if ($shiftingGender == config('constants.gender.Others')) {
                $fromResData->total_others = $fromResData->total_others - 1;
            }
            $fromResData->total_member = $fromResData->total_member - 1;


            /* Family Member Child Counting Update */
            $shiftingDob = $fromResDataArr[$family_member_id - 1]['m_dob'];
            $isChild = getAge($shiftingDob);
            if ($isChild <= config('constants.ageSection.Baby')) {
                $fromResData->total_child = $fromResData->total_child - 1;
            }

            unset($fromResDataArr[$family_member_id - 1]);

            if (!empty($fromResDataArr)) {
                $fromResDataArr = array_values($fromResDataArr);

                $updatedFromJsonData = familyMemberArraToJsonData($fromResDataArr);
//            pr($updatedFromJsonData);
                $updatedFromJsonData['total_member'] = $fromResData->total_member;
                $updatedFromJsonData['total_male'] = $fromResData->total_male;
                $updatedFromJsonData['total_female'] = $fromResData->total_female;
                $updatedFromJsonData['total_others'] = $fromResData->total_others;
                $updatedFromJsonData['total_child'] = $fromResData->total_child;

            }

            // Update Previous Family Member Data
            if (!empty($fromResDataArr)) {
                FamilyMember::where('family_id', $family_id)->update($updatedFromJsonData);
            } else {
                FamilyMember::where('family_id', $family_id)->delete();
            }

            /*************************************/

            if ($shift == config('constants.shiftList.Include')) {
                $this->validate($request, [
                    'card_no' => 'required',
                ]);

                /* New Family Member Information */
                $toResData = getFamilyMemberInfo(null, $card_no);
                $hasData = false;

                $toResDataArr = [];
                if (!empty($toResData)) {
                    $toResDataArr = familyMemberJsonDataToArray(json_decode(json_encode($toResData), true));
                    $hasData = true;
                }
//                pr($toResData); // where have to add the new member
//                pr($toResDataArr); // json to array conversion of the to data array
//                pr($shiftingMember); //  which member have to add

                //  add the new comer data with the existing members
                $toResDataArr = array_merge($toResDataArr, $shiftingMember);
                // remove extra array level
                $shiftingMember = $shiftingMember[0];
                // convert the new array data in JSON format
                $toResDataArr = familyMemberArraToJsonData($toResDataArr);

                /* Family Member Counting Update for New Family */
                $toGender = $shiftingMember['m_gender'];
                if ($toGender == config('constants.gender.Male')) {
                    $toResDataArr['total_male'] = $fromResData->total_male + 1;
                }
                if ($toGender == config('constants.gender.Female')) {
                    $toResDataArr['total_female'] = $fromResData->total_female + 1;
                }
                if ($toGender == config('constants.gender.Others')) {
                    $toResDataArr['total_others'] = $fromResData->total_others + 1;
                }
                $toResDataArr['total_member'] = $fromResData->total_member + 1;

                /* Family Member Child Counting Update */
                $toDob = $shiftingMember['m_dob'];
                $isChild = getAge($toDob);
                if ($isChild <= config('constants.ageSection.Baby')) {
                    $toResDataArr['total_child'] = $fromResData->total_child + 1;
                }
                /*************************************/

                /* Update Family Member Table */
                // Update Previous Family Member Data
//                FamilyMember::where('family_id', $family_id)->update($updatedFromJsonData);
                // Update Newly Joined Family Member Data
                if ($hasData) {
                    FamilyMember::where('card_no', $card_no)->update($toResDataArr);
                } else {
                    $new_family_id = getFamilyIdByCardNo($card_no);
                    $toResDataArr['family_id'] = $new_family_id;
                    $toResDataArr['card_no'] = $card_no;
                    $toResDataArr['total_member'] = 1;
                    FamilyMember::create($toResDataArr);
                }

//                DB::commit();
//                return redirect()->route('family-members.edit', $family_id)->with('success', 'তথ্য সফলভাবে পরিবর্তন করা হয়েছে...');
            } else {
                $fromFamilyData = Family::find($family_id);

                $shiftingMember = $shiftingMember[0];
                $fromFamilyData->f_h_name = $fromFamilyData->name;
                $fromFamilyData->name = $shiftingMember["m_name"];
                $fromFamilyData->dob = $shiftingMember['m_dob'];
                $fromFamilyData->gender = $shiftingMember['m_gender'];
                $fromFamilyData->education = $shiftingMember['m_edu_level'];
                $fromFamilyData->ssnp = $shiftingMember['m_ssnp'];
                $fromFamilyData->occupation = $shiftingMember['m_occupation'];

                $fromFamilyData->phone = $shiftingMember['m_phone'];
                $fromFamilyData->nid = $shiftingMember['m_nid'];


                $fromFamilyData->income = 0;
                $fromFamilyData->expense = 0;

                unset($fromFamilyData->id);
                unset($fromFamilyData->card_no);
                unset($fromFamilyData->created_at);
                unset($fromFamilyData->updated_at);

                $saveData = new Family();
                $fromFamilyData = $fromFamilyData->toArray();
                foreach ($fromFamilyData as $key => $value) {
                    $saveData->$key = $value;
                }

                $saveData->save();
                $lastInsertedId = $saveData->id;

                //                $fromFamilyData->save();

//                DB::commit();
//                return redirect()->route('families.edit', $lastInsertedId)->with('success', 'তথ্য সফলভাবে পরিবর্তন করা হয়েছে...');

//                dd($fromFamilyData);
            }

            // Update Previous Family Member Data
//            if (!empty($fromResDataArr)) {
//                FamilyMember::where('family_id', $family_id)->update($updatedFromJsonData);
//            } else {
//                FamilyMember::where('family_id', $family_id)->delete();
//            }

            if ($shift == config('constants.shiftList.Include')) {
                DB::commit();
                return redirect()->route('family-members.edit', $family_id)->with('success', 'তথ্য সফলভাবে পরিবর্তন করা হয়েছে...');
            } else {
                DB::commit();
                return redirect()->route('families.edit', $lastInsertedId)->with('success', 'তথ্য সফলভাবে পরিবর্তন করা হয়েছে...');
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function cronTest(Request $request)
    {
        $memberList = FamilyMember::get()->toArray();
        $ageList = '';
        foreach ($memberList as $key => $member) {
            $idList = preg_replace('/[^A-Za-z0-9,\-]/', '', $member['m_dob']); // Removes special chars.\
//            $idList = explode('-', $idList); // use dash (-)  as the separator.
            $mDob = explode(',', $idList);
            $calculateData = $this->ageCalculation($mDob);
            $ageList = $calculateData['ageArrayList'];
            $child = $calculateData['child'];
            $old = $calculateData['old'];
//            $resData = json_encode($ageList);
            DB::table('family_members')
                ->where('id', $member['id'])
                ->update(['m_age' => $ageList, 'total_child'=>$child, 'total_old'=>$old]);
            pr($ageList);
//            pr($resData);
        }

        dd('Done....');
    }

    public function ageCalculation($ageListArr)
    {
        $ageArr = [];
        $child = $old = 0;
        foreach ($ageListArr as $key => $age) {
            if (!empty($age)) {
                $year = getAge($age);
                $ageArr[]['age'] = $year;
                if ($year <= config('constants.ageSection.Baby')) {
                    $child++;
                }
                if ($year >= config('constants.ageSection.Aged')) {
                    $old++;
                }
            } else {
                $ageArr[]['age'] = -1;
            }
        }
        $ageArrayList = json_encode($ageArr, true);
        return ['ageArrayList' => $ageArrayList, 'child' => $child, 'old' => $old];
    }
}
