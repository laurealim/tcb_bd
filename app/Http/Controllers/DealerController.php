<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDealerRequest;
use App\Http\Requests\UpdateDealerRequest;
use App\Models\Dealer;
use App\Models\PackageVsDealer;
use App\Models\User;
use DataTables;
use DB;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class DealerController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('dealer-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'সকল ডিলারের তালিকা ';
        if ($request->ajax()) {
            $data = '';
            if (auth()->user()->can('is-su-admin')) {
                $data = Dealer::select('*');
            } else {
                $data = Dealer::select('*')
                    ->when(true, function ($query) {
                        userAccess($query);
                    });
            }
//            $sql= $data->toSql();
//            dd($sql);
//            $query = DB::table('users');
//            $query = userAccess($query);
//            $query = userDataAccess($query);
//            dd($query->get());
//            dd($data->get());

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('roles', function ($row) {
                    $str = '';
                    if (!empty($row->getRoleNames())) {
                        foreach ($row->getRoleNames() as $v) {
                            $str .= '<button type="button" class="btn bg-gradient-info">' . $v . '</button>&nbsp;';
//                            $str .= '<span class="badge badge-success btn">'. $v.'</span>&nbsp;';
                        }
                    }
                    return $str;
                })
                ->addColumn('location', function ($row) {
                    $address = 'উপজেলাঃ- ' . getSubDistName($row->sub_dist) . ', জেলাঃ- ' . getDistName($row->dist) . ', বিভাগঃ- ' . getDivName($row->div);
                    return $address;
                })
                ->addColumn('action', function ($row) {
                    $action = '';
                    if (auth()->user()->can('dealer-edit')) {
                        $action .= '<a class="btn-sm btn-primary" id="edit-dealer" href= ' . route('dealers.edit', $row->id) . '>
<i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}"> ';
                    }
                    if (auth()->user()->can('dealer-delete')) {
                        $action .= '<a id=' . $row->id . ' href=' . route('dealers.destroy', $row->id) . ' class="btn-sm btn-danger delete-dealer">
<i class="fa fa-trash-alt"></i></a> <span></span>';
                    }
                    if (auth()->user()->can('dealer-pass-reset')) {
                        $action .= '<a id=' . $row->id . ' href=' . route('dealers.resetPassword', $row->id) . ' class="btn-sm btn-info change-pass">
<i class="fa-solid fa-unlock-keyhole"></i></a>';
                    }
                    return $action;
                })
                ->rawColumns(['action', 'roles', 'location'])
                ->make(true);
        }
        return view('dealers.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('user-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন ডিলার তৈরী';
//        $institutionList = getInstitutionList(auth()->user()->institute_id);

        $divListArr = getDivLists(null, true);
//        pr(auth()->user()->institute_id);
//        dd($institutionList);
//        $roles = Role::where('is_dealer',1)->pluck('name', 'id')->all();
//        dd($roles);

        return view('dealers.create', compact('page_title', 'divListArr'));
    }

    public function store(StoreDealerRequest $request)
    {
        if (!auth()->user()->can('dealer-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $validatedData = $request->validated();

        $input = $request->all();
        $roles = Role::where('is_dealer', 1)->pluck('name', 'id')->all();

        $input['password'] = Hash::make('abc123');
        $input['status'] = config('constants.status.Active');

        DB::beginTransaction();
        try {

//            $inputwithFiles = $input;
            unset($input['picture']);
            unset($input['docs']);


//            dd($input);
            $dealer = Dealer::create($input);
            $lastInsertedId = $dealer->id;
            $roles = [$roles];
            $dealer->assignRole($roles);

            /* User Add in User Table */
            $user = new User();
            $user->name = $input['name'];
            $user->email = $input['email'];
            $user->password = $input['password'];
            $user->div = $input['div'];
            $user->dist = $input['dist'];
            $user->sub_dist = $input['sub_dist'];
            $user->phone = $input['phone'];
            $user->union = -2;
            $user->type = config('constants.user_type.dealer');
            $user->institute_id = 0;

            $user->save();

            /*  File Manipulation   */
            $filename = '';
            try {
                if ($request->hasFile('picture')) {

                    $path = "images/dealers";
                    $filePath = $path . '/' . $lastInsertedId;

                    $uploadPath = public_path($filePath);

                    $file = $request->file('picture');
                    $filename = $lastInsertedId . '.png';
//                    $filename = $lastInsertedId . '.' . $file->getClientOriginalExtension();

                    try {
                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0755, true);
                        } else {

                            if (file_exists($uploadPath . $filename)) {
                                unlink($uploadPath . $filename);
                            }
                        }

                        $file->move($uploadPath, $filename); // Save in public folder
//                    $path = $file->storeAs($filePath, $filename); // Save in Storage/app folder
//                    $request->file->move(public_path($uploadPath), $filename);
                    } catch (Exception $excp) {
                        DB::rollback();
//                        dd($excp);
                        var_dump($excp);
                    }
                }
                if ($request->hasFile('docs')) {
//                    $lastInsertedId = 111;

                    $path = "images/dealers";
                    $filePath = $path . '/' . $lastInsertedId;

                    $uploadPath = public_path($filePath);

                    $file = $request->file('docs');
                    $filename = $lastInsertedId . '.pdf';
//                    $filename = $lastInsertedId . '.' . $file->getClientOriginalExtension();

                    try {
                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0755, true);
                        } else {

                            if (file_exists($uploadPath . $filename)) {
                                unlink($uploadPath . $filename);
                            }
                        }
//                    pr($filename);
                        $file->move($uploadPath, $filename); // Save in public folder
//                    $path = $file->storeAs($filePath, $filename); // Save in Storage/app folder
//                    $request->file->move(public_path($uploadPath), $filename);
                    } catch (Exception $excp) {
                        DB::rollback();
//                        dd($excp);
                        var_dump($excp);
                    }
                }
            } catch (Exception $exception) {
                DB::rollback();
                dd($exception->getMessage());
                var_dump($exception);
            }
//            dd("aslaurealim");

            DB::commit();

            return redirect()->route('dealers.index')
                ->with('success', 'নতুন ডিলার তৈরী সম্পন্ন হয়েছে।');
        } catch (Exception $exception) {
            DB::rollback();
//            return back()->withError($exception->getMessage())->withInput();
//            dd($exception);
//            return redirect()->route('dealers.index');
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংরক্ষন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show($id)
    {
        if (!auth()->user()->can('user-show')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $user = User::find($id);

        return view('users.show', compact('user'));
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('dealer-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

//        DB::beginTransaction();
        try {

            $page_title = 'ডিলার সংশোধন';
            $dealer = Dealer::find($id);
            $roles = Role::where('is_dealer', 1)->pluck('name', 'id')->all();
            $userRole = $dealer->roles->pluck('id', 'name')->all();

            $div = $dealer->div;
            $divListArr = getDivLists(null, false);
            if (!empty($div)) {
                $divListArr = getDivLists(null, true);
            }

            $dist = $dealer->dist;
//            $sub_dist = $user->sub_dist;

            $distListArr = $sub_distListArr = [];

            if (!empty($div)) {
                $distListArr = getDistLists($div, true);
            }

            if (!empty($dist)) {
                $sub_distListArr = getSubDistLists($dist, true);
            }


            return view('dealers.edit', compact('dealer', 'roles', 'userRole', 'page_title', 'divListArr', 'distListArr', 'sub_distListArr'));
        } catch (Exception $exception) {
//            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(UpdateDealerRequest $request, $id)
//    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('dealer-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $validatedData = $request->validated();
        $input = $request->all();

        DB::beginTransaction();
        try {
            unset($input['picture']);
            unset($input['docs']);


//            $dealer = Dealer::create($input);
            $dealer = Dealer::find($id);
            $dealer->update($input);
            $email = $input['email'];

            $user = User::where('email', $email)->firstOrFail();

            $user->name = $input['name'];
            $user->div = $input['div'];
            $user->dist = $input['dist'];
            $user->sub_dist = $input['sub_dist'];
            $user->phone = $input['phone'];

            $user->save();
//            dd($user);

            /*  File Manipulation   */
            $filename = '';
            try {
                if ($request->hasFile('picture')) {
//                    $lastInsertedId = 111;

                    $path = "images/dealers";
                    $filePath = $path . '/' . $id;

                    $uploadPath = public_path($filePath);

                    $file = $request->file('picture');
                    $filename = $id . '.png';
//                    $filename = $lastInsertedId . '.' . $file->getClientOriginalExtension();

                    try {
                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0755, true);
                        } else {
                            if (file_exists($uploadPath . $filename)) {
                                unlink($uploadPath . $filename);
                            }
                        }
                        $file->move($uploadPath, $filename); // Save in public folder
//                    $path = $file->storeAs($filePath, $filename); // Save in Storage/app folder
//                    $request->file->move(public_path($uploadPath), $filename);
                    } catch (Exception $excp) {
                        var_dump($excp);
                    }
                }
                if ($request->hasFile('docs')) {
//                    $lastInsertedId = 111;

                    $path = "images/dealers";
                    $filePath = $path . '/' . $id;

                    $uploadPath = public_path($filePath);

                    $file = $request->file('docs');
                    $filename = $id . '.pdf';
//                    $filename = $lastInsertedId . '.' . $file->getClientOriginalExtension();

                    try {
                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0755, true);
                        } else {

                            if (file_exists($uploadPath . $filename)) {
                                unlink($uploadPath . $filename);
                            }
                        }
                        $file->move($uploadPath, $filename); // Save in public folder
//                    $path = $file->storeAs($filePath, $filename); // Save in Storage/app folder
//                    $request->file->move(public_path($uploadPath), $filename);
                    } catch (Exception $excp) {
                        var_dump($excp);
                    }
                }
            } catch (Exception $exception) {
                DB::rollback();
                dd($exception->getMessage());
                var_dump($exception);
            }

            DB::commit();

            return redirect()->route('dealers.index')
                ->with('success', 'তথ্য সঠিকভাবে সংশোধন করা হয়েছে...');
        } catch (Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('dealer-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            Dealer::find($id)->delete();
            DB::commit();
//            return response()->json(['status' => 'success']);

            $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
            return response()->json(['status' => 'success']);

//            return redirect()->route('dealers.index')->with('success', 'তথ্য সফল্ভাবে মুছে ফেলা হয়েছে।।।');
        } catch (Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function changePassword(Request $request)
    {
        $page_title = 'ইউজার পাসওয়ার্ড সংশোধন';
        return view('users.change_pass', compact('page_title'));
    }

    public function updatePassword(Request $request)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();

            $this->validate($request, [
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password|min:6',
            ]);

            $user = User::find(auth()->user()->id);

            $user->password = Hash::make($input['password']);
            $user->save();

            DB::commit();

            return redirect()->route('home')
                ->with('success', 'তথ্য সঠিকভাবে সংশোধন করা হয়েছে...');
        } catch (Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());

            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');

            return response()->json(['status' => 'error']);
        }

    }

    public function resetPassword(Request $request, $id)
    {

        if (!auth()->user()->can('dealer-pass-reset')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            $dealer = Dealer::find($id);

            $dealer->password = Hash::make('password');
//            dd($dealer);
            $dealer->save();

            DB::commit();

            $request->session()->flash('success', 'পাসওয়ার্ড সফল্ভাবে রিসেট ফেলা হয়েছে।।।');
            return response()->json(['status' => 'success']);

//            return redirect()->route('dealers.index')->with('success', 'পাসওয়ার্ড সফল্ভাবে রিসেট ফেলা হয়েছে।।।');
        } catch (Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function getDealerLists(Request $request)
    {
        if ($request->ajax()) {
            $packageId = $request->post('id');
            $dealerLists = getDealerLists();
            $distList = getDistName();
            $subDistList = getSubDistName();

            $pkgVsDlrList = getPackageVsDealerList($packageId);

            $assignDealerListAee = convertPkgVsDlrListToArray($pkgVsDlrList);

            if (!empty($dealerLists)) {
                return response()->json(['status' => 'success', 'dealerLists' => $dealerLists, 'distList' => $distList, 'subDistList' => $subDistList, 'packageId' => $packageId, 'assignDealerListAee' => $assignDealerListAee]);
            } else {
                return response()->json(['status' => 'error']);
            }

//            dd($dealerLists);
//            $packageId = $request->post('id');
//            try {
//                $itemTypeLists = ItemType::where('item_types.status', config('constants.status.Active'))
//                    ->with('items')
//                    ->get();
//
//                $addeditems = DB::table('packages')
//                    ->select('items')
//                    ->where('id', $packageId)
//                    ->first();
//
//                $itemList = [];
//                if(!empty($addeditems->items)){
//                    $itemList = array_map('intval', json_decode($addeditems->items, true));
//                }
//
//                return response()->json(['status' => 'success', 'itemTypeLists' => $itemTypeLists, 'packageId' => $packageId, 'itemList' => $itemList]);
//            } catch (\Exception $exception) {
//                $request->session()->flash('error', $exception->getMessage());
//                return response()->json(['status' => 'error']);
//            }
        }
    }

    public function getDealerModuleLists(Request $request){
        $familyId = $request->post('id');
        $familyInfo = getFamilyInfoById($familyId);
        $reqData = [
            'divId' => $familyInfo->div,
            'districtID' => $familyInfo->dist,
            'upazilaID' => $familyInfo->sub_dist
        ];
        $dealerLists = getDealerLists(null, $reqData);

        if (!empty($dealerLists)) {
            return response()->json(['status' => 'success', 'dealerLists' => $dealerLists]);
        } else {
            return response()->json(['status' => 'error']);
        }
    }

    public function saveDealer(Request $request)
    {
        $dealersList = $request->post('dealersList');
        $packageId = $request->post('packageId');

        $dealersListArr = explode(',', $dealersList);
        $dealersListEncode = json_encode($dealersListArr, true);

        if ($request->ajax()) {
            DB::beginTransaction();
            try {
                /* Remove Previous Assigned Dealer To A Particular Package */
                DB::table('package_vs_dealers')
                    ->where('package_id', $packageId)
                    ->delete();

                /***********************************************************/


                $dealerListArr = Dealer::whereIn('id', $dealersListArr)->get()->toArray();
                foreach ($dealerListArr as $key => $values) {
                    $dealerId = $values['id'];
                    $div = $values['div'];
                    $dist = $values['dist'];
                    $sub_dist = $values['sub_dist'];

                    /* Update Dealers Table Package_Id Field */

                    $packageArr = [];
//                    if (!empty($values['assigned_package'])) {
//                        $packageArr = json_decode($values['assigned_package'], true);
//                        $packageArr = array_merge($packageArr, [$packageId]);
//                    } else {
//                        $packageArr = [$packageId];
//                    }
//                    DB::table('dealers')
//                        ->where('id', $dealerId)
//                        ->update(array('assigned_package' => json_encode($packageArr, true)));

                    /*****************************************************/

                    /* Update or Insert Data To Package Vs Dealers Table */
                    $pakVsDealerArr = PackageVsDealer::where('div', $div)
                        ->where('dist', $dist)
                        ->where('sub_dist', $sub_dist)
                        ->where('package_id', $packageId)
                        ->get()
                        ->toArray();

                    $dataser = [];
                    $dataser['package_id'] = $packageId;
                    $dataser['div'] = $div;
                    $dataser['dist'] = $dist;
                    $dataser['sub_dist'] = $sub_dist;

                    $dataser['dealer_id'] = $dealerId;
                    $dataser['created_by'] = auth()->user()->id;
                    DB::table('package_vs_dealers')
                        ->insert($dataser);

                    /* If have to save same div, dist, sub_dist, union dealer in the same field as comma separate */
//                    if (!empty($pakVsDealerArr)) { // if any package has same div, dist, sub_dist dealer
//                        $pakVsDealerArr = $pakVsDealerArr[0];
//                        $prevDealerListArr = json_decode($pakVsDealerArr['dealer_id'], true);
//                        $dealersArr = array_merge($prevDealerListArr, [$dealerId]);
//                        $dataser['dealer_id'] = json_encode($dealersArr, true);
//                        $dataser['updated_by'] = auth()->user()->id;
//
//                        DB::table('package_vs_dealers')
//                            ->where('id', $pakVsDealerArr['id'])
//                            ->update($dataser);
//                    } else {
////                        $dataser['dealer_id'] = json_encode([$dealerId], true);
//                        $dataser['dealer_id'] = $dealerId;
//                        $dataser['created_by'] = auth()->user()->id;
//
//                        DB::table('package_vs_dealers')
//                            ->insert($dataser);
//                    }
                    /*****************************************************/
                }
                DB::commit();
                $request->session()->flash('success', 'ডিলার এসাইন সম্পন্ন করা হয়েছে।');
                return response()->json(['status' => 'success']);
            } catch (\Exception $exception) {
                DB::rollback();
                dd($exception->getMessage());
                $request->session()->flash('error', 'ডিলার এসাইন সম্পন্ন সম্ভব হয় নি।');
                return response()->json(['status' => 'error']);
            }
        }
    }

    public function getAssignDealerList(Request $request)
    {
        if ($request->ajax()) {
            $divId = $request->divId;
            $districtID = $request->districtID;
            $sub_distId = $request->sub_distId;
            $postData = [
                'divId' => $divId,
                'districtID' => $districtID,
                'upazilaID' => $sub_distId,
            ];

            $query = DB::table('dealers');
            $query = userAccess($query,$postData);
            $dealerList = $query->pluck('name','id')->toArray();

            return $dealerList;
        }
    }
}
