<?php

namespace App\Http\Controllers;

use DataTables;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
//        $this->middleware('permission:permission-list|permission-create|permission-edit|permission-delete', ['only' => ['index', 'store']]);
//        $this->middleware('permission:permission-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:permission-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (auth()->user()->can('permission-list')) {
            DB::beginTransaction();
            try {
                $page_title = 'সকল পারমিশন ';
                if ($request->ajax()) {
                    $data = Permission::select('*');
                    return DataTables::of($data)
                        ->addIndexColumn()
                        ->editColumn('tag', function ($row) {
                            $tag = config('constants.tag_list.arr.' . $row->tag);
//                        $div_name = getDivName($row->div);
                            return $tag;
                        })
                        ->addColumn('action', function ($row) {
                            $action = '';
                            if (auth()->user()->can('permission-edit')) {
                                $action .= '<a class="btn btn-primary" id="edit-role" href= ' . route('permissions.edit', $row->id) . '>
<i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}"> ';
                            }
                            if (auth()->user()->can('permission-delete')) {
                                $action .= '<a id=' . $row->id . ' href=' . route('permissions.destroy', $row->id) . ' class="btn btn-danger delete-permission">
<i class="fa fa-trash-alt"></i></a>';
                            }
                            return $action;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                }
                return view('permissions.index', compact('page_title'));
            } catch (\Exception $exception) {
                DB::rollback();
                $request->session()->flash('errors', 'তথ্য সফলভাবে সংরক্ষন সম্ভব হয় নি...');
                return response()->json(['status' => 'error']);
            }
        } else {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
//            return redirect('/')->with('status', 'Profile updated!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->can('permission-create')) {
            $page_title = 'নতুন পারমিশন তৈরী';
            return view('permissions.create', compact('page_title'));
        } else {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
//            return redirect('/')->with('status', 'Profile updated!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->can('permission-create')) {
            DB::beginTransaction();
            try {
                $this->validate($request, [
                    'name' => 'required|unique:permissions,name',
                    'tag' => 'required',
                ]);

                Permission::create(['name' => $request->input('name'),'tag' => $request->input('tag')]);

                return redirect()->route('permissions.index')
                    ->with('success', 'নতুন পারমিশন তৈরী সম্পন্ন হয়েছে।');
            } catch (\Exception $exception) {
                DB::rollback();
                $request->session()->flash('errors', 'তথ্য সফলভাবে সংরক্ষন সম্ভব হয় নি...');
                return response()->json(['status' => 'error']);
            }
        } else {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
//            return redirect('/')->with('status', 'Profile updated!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->can('permission-show')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        $permission = Permission::find($id);

        return view('permissions.show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('permission-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $page_title = 'পারমিশন সংশোধন';
            $permission = Permission::find($id);

            return view('permissions.edit', compact('permission', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('permission-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'tag' => 'required'
            ]);

            $permission = Permission::find($id);
            $permission->name = $request->input('name');
            $permission->tag = $request->input('tag');
            $permission->save();

            return redirect()->route('permissions.index')
                ->with('success', 'তথ্য সঠিকভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সংশোধন সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('permission-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            Permission::find($id)->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
