<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Division;
use App\Models\Union;
use App\Models\Upazila;
use DataTables;
use DB;
use Illuminate\Http\Request;

class UnionController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('location-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        try {
            $page_title = 'ইউনিয়নের তালিকা';
            $sub_distLists = getSubDistLists();
            $union = getUnionLists();
//            $nesteds = Nested::tree()->get()->toTree()->where("id","=", 4);
//            dd($nesteds);

            if ($request->ajax()) {
                $query = DB::table('unions');
//                $query = userAccess($query);

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->addColumn('upa_name', function ($row) use($sub_distLists) {
                        $upa_name = "";
                        $upa_name = $sub_distLists[$row->upazilla_id];
                        return $upa_name;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('location-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('unions.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('location-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('unions.destroy', $row->id) . ' class="btn-sm btn-danger delete-union action-btn"><i class="fa fa-trash-alt"></i></a>';
                        }

                        return $action;
                    })
                    ->rawColumns(['action','upa_name'])
                    ->make(true);
            }
        } catch (\Exception $exception) {
//            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


        return view('unions.index', compact('page_title', 'union'));
    }

    public function create()
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন ইউনিয়ন তৈরী';

        return view('unions.create', compact('page_title'));

    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('location-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'uni_code' => 'required',
                'upazilla_id' => 'required',
            ],
                [
                    'name.required' => 'ইউনিয়নের ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'ইউনিয়নের বাংলা নাম প্রদান করুন',
                    'uni_code.required' => 'ইউনিয়নের কোড প্রদান করুন',
                    'upazilla_id.required' => 'উপজেলা নির্বাচন করুন',
                ]
            );

            $districtDataArr = $request->all();
            $lastInsertedId = Union::create($districtDataArr);

            DB::commit();
            return redirect()->route('unions.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'ইউনিয়নের তথ্য সংশোধন';
        DB::beginTransaction();
        try {
            $union = Union::find($id);
            return view('unions.edit', compact('union', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('location-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'uni_code' => 'required',
                'upazilla_id' => 'required',
            ],
                [
                    'name.required' => 'ইউনিয়নের ইংরেজি নাম প্রদান করুন',
                    'bn_name.required' => 'ইউনিয়নের বাংলা নাম প্রদান করুন',
                    'uni_code.required' => 'ইউনিয়নের কোড প্রদান করুন',
                    'upazilla_id.required' => 'উপজেলা নির্বাচন করুন',
                ]
            );

            $unionRes = Union::find($id);
            $unionRes->name = $request->input('name');
            $unionRes->bn_name = $request->input('bn_name');
            $unionRes->uni_code = $request->input('uni_code');
            $unionRes->upazilla_id = $request->input('upazilla_id');
            $unionRes->save();

            DB::commit();
            return redirect()->route('unions.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('location-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Union::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $unionModel = new Union();

        $unionLists = $unionModel->select('divisions.bn_name as division_name', 'districts.bn_name as district_name', 'upazilas.bn_name as upazila_name', 'unions.*')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'unions.upazilla_id')
            ->leftJoin('districts', 'districts.id', '=', 'upazilas.district_id')
            ->leftJoin('divisions', 'divisions.id', '=', 'districts.division_id')
            ->where('districts.name', 'like', '%' . $searchData . '%')
            ->orwhere('divisions.name', 'like', '%' . $searchData . '%')
            ->orwhere('upazilas.name', 'like', '%' . $searchData . '%')
            ->orwhere('unions.name', 'like', '%' . $searchData . '%')
            ->orderby('id', 'desc')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('union.ajax_list', compact('unionLists'));
        } else {
            return view('union.adminList', compact('unionLists'));
        }
    }

    public function adminForm()
    {
        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $divisionList = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        return view('union.adminForm', compact('divisionList', 'districtList', 'upazilaList'));
    }

    public function adminStore(Request $request)
    {
        $unionModel = new Union();
        try {
            $data = $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'upazilla_id' => 'required',
            ], [
                'name.required' => 'District Name is required',
                'bn_name.required' => 'District Name (Bangla) is required',
                'upazilla_id.required' => 'Please Select an Upazila',

            ]);

            unset($request['division_id']);
            unset($request['district_id']);
            unset($request['_token']);

            $unionModel->saveData($request);
            return redirect('admin/union/list')->with('success', 'New Union added successfully');
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'Data can not saved...');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id, Request $request)
    {
        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();

        try {
            $unionData = Union::where('id', $id)->first();

            $divisionList = $divisionModel->pluck('bn_name', 'id')->all();

            $upazilaData = Upazila::where('id', $unionData->upazilla_id)->first();
            $districtData = District::where('id', $upazilaData->district_id)->first();

            $divisionID = $districtData->division_id;
            $districtID = $upazilaData->district_id;

            $districtList = $districtModel->where('division_id', $divisionID)->pluck('bn_name', 'id')->all();
            $upazilaList = $upazilaModel->where('district_id', $districtID)->pluck('bn_name', 'id')->all();

            return view('union.adminEdit', compact('divisionList', 'unionData', 'divisionID', 'districtID', 'districtList', 'upazilaList', 'id'));
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
        }
    }

    public function adminUpdate(Request $request, $id)
    {
        $unionModel = new Union();
        try {
            $data = $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'district_id' => 'required',
            ], [
                'name.required' => 'District Name is required',
                'bn_name.required' => 'District Name (Bangla) is required',
                'district_id.required' => 'Please Select a District',

            ]);

            unset($request['division_id']);
            unset($request['district_id']);

            $unionModel->updateData($request);

            return redirect('admin/union/list')->with('success', 'Union edited successfully..');
        } catch (\Exception $exception) {
            dd($exception);
            $request->session()->flash('error', 'Union can not edited successfully...');
            return redirect()->back();
        }
    }

    public function adminDestroy($id, Request $request)
    {
        $unionInfo = Union::findOrFail($id);
        if (isset($request->id)) {
            $unionInfo->delete();
            $request->session()->flash('success', 'Union Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'Union Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function unionSelectAjaxList(Request $request)
    {
        if ($request->ajax()) {


            $query = DB::table('unions');
            $union = auth()->user()->union;
            if (!empty($union)) {
                if ($union >= 1) {
                    $query->where('id', $union);
                } else if ($union == -1) {
                    return json_encode([-1 => 'সিটি কর্পোরেশন']);
                }
            } else {
                if ($union < 1 && $union > -1) {
                    return json_encode([0 => 'পৌরসভা']);
                }
            }
            $upazilaID = $request->post('upazilaID');
            $query->where("upazilla_id", $upazilaID);
            $unionList = $query->pluck('bn_name', 'id');

            if ($union < 1) {
                $unionList[-1] = 'সিটি কর্পোরেশন';
                $unionList[0] = 'পৌরসভা';
            }
            return json_encode($unionList);


//            $unionModel = new Union();
//            $upazilaID = $request->post('upazilaID');
//            $unionList = $unionModel->where("upazilla_id", $upazilaID)->pluck("bn_name", "id")->all();
//            return json_encode($unionList);
        }
    }

    public function adminUnionSelectAjaxList(Request $request)
    {
//        var_dump($request->post('divisionId'));
        return $this->_unionSelectAjaxList($request);
    }

    public function clientUnionSelectAjaxList(Request $request)
    {
//        var_dump($request->post('divisionId'));
        return $this->_unionSelectAjaxList($request);
    }

    public function userUnionSelectAjaxList(Request $request)
    {
//        var_dump($request->post('divisionId'));
        return $this->_unionSelectAjaxList($request);
    }

    private function _unionSelectAjaxList($request)
    {
        if ($request->ajax()) {

            $unionModel = new Union();
            $upazilaID = $request->post('upazilaID');
            $unionList = $unionModel->where("upazilla_id", $upazilaID)->pluck("bn_name", "id")->all();
            return json_encode($unionList);
        }
    }
}
