<?php

namespace App\Http\Controllers\Report;

use App\Exports\FamilyExport;
use App\Exports\PackageExport;
use App\Exports\DealerExport;
use App\Http\Controllers\Controller;
use App\Models\Family;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use Jimmyjs\ReportGenerator\Facades\ExcelReportFacade;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;

//use \App\Exports\FamilyExport;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;

class ReportController extends Controller
{
    /**************************** Family Related Report ****************************/
    public function familyReport()
    {
        $page_title = "পরিবারের রিপোর্ট";
        $divLists = getDivLists();
        return view('reports.family', compact('page_title', 'divLists'));
    }

    public function familySearchData(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
                'gender' => $request->post('gender'),
                'occupation' => $request->post('occupation'),
                'marital_status' => $request->post('marital_status'),
                'education' => $request->post('education'),
                'house_type' => $request->post('house_type'),
                'ssnp' => $request->post('ssnp'),
                'age' => $request->post('age'),
                'religious' => $request->post('religious'),
            ];

            $query = $this->__familyQery($searchData);
//            dd($query->toSql());

            return DataTables::of($query)
                ->addIndexColumn()
                ->editColumn('gender', function ($row) {
                    $gender = config('constants.gender.arr.' . $row->gender);
                    return $gender;
                })
                ->editColumn('status', function ($row) {
                    $status = config('constants.status.arr.' . $row->status);
                    return $status;
                })
                ->editColumn('occupation', function ($row) {
                    if ($row->occupation >= 1)
                        return config('constants.occupation.arr.' . $row->occupation);
                    else
                        return $row->occupation;
                })
                ->editColumn('marital_status', function ($row) {
                    return config('constants.marital_status.arr.' . $row->marital_status);
                })
                ->editColumn('education', function ($row) {
                    return config('constants.edu_level.arr.' . $row->education);
                })
                ->editColumn('house_type', function ($row) {
                    return config('constants.house_type.arr.' . $row->house_type);
                })
                ->editColumn('ssnp', function ($row) {
                    return config('constants.ssnp.arr.' . $row->ssnp);
                })
                ->editColumn('religion', function ($row) {
                    return config('constants.religious.arr.' . $row->religion);
                })
                ->addColumn('address', function ($row) {
                    $address = $row->add . ', ' . getUnionName($row->union) . ', ' . getSubDistName($row->sub_dist) . ', ' . getDistName($row->dist) . ', ' . getDivName($row->div);
                    return $address;
                })
                ->rawColumns(['action', 'gender', 'address'])
                ->make(true);
        }
    }

    public function exportFamilyReport(Request $request)
    {
        $searchData = [
            'divId' => $request->post('div'),
            'districtID' => $request->post('dist'),
            'upazilaID' => $request->post('sub_dist'),
            'unionID' => $request->post('union'),
            'gender' => $request->post('gender'),
            'occupation' => $request->post('occupation'),
            'marital_status' => $request->post('marital_status'),
            'education' => $request->post('education'),
            'ssnp' => $request->post('ssnp'),
            'house_type' => $request->post('house_type'),
            'age' => $request->post('age'),
            'religious' => $request->post('religious'),
        ];

        return Excel::download(new FamilyExport($searchData), 'Family_Report.xlsx');
    }

    public function __familyQery($searchData)
    {
        $query = DB::table('families');
        $query->select("families.*");
        $query->leftJoin('family_members', 'families.id', '=', 'family_members.family_id');
        userAccess($query, $searchData);
        if (!empty($searchData['gender'])) {
            $query->where('gender', $searchData['gender']);
        }

        if (!empty($searchData['marital_status'])) {
            $query->where('marital_status', $searchData['marital_status']);
        }

        if (!empty($searchData['education'])) {
            $query->where('education', $searchData['education']);
        }

        if (!empty($searchData['house_type'])) {
            $query->where('house_type', $searchData['house_type']);
        }

        if (!empty($searchData['ssnp'])) {
            $search = $searchData['ssnp'];
            $query->where(function ($query) use ($search) {
                $query->where('ssnp', $search)
                    ->orWhere('m_ssnp', 'LIKE', '%' . $search . '%');
            });
        }

        if (!empty($searchData['age'])) {
            $ageChild = date('Y', strtotime('- 5 years'));
            $ageOld = date('Y', strtotime('- 60 years'));
            $search = $searchData['age'];

            if ($search == 1) {
                $query->where(function ($query) use ($ageChild) {
                    $query->whereYear('dob', '>=', $ageChild)
                        ->orWhere('total_child', '>=', 1);
                });
            } else if ($search == 2) {
                $query->where(function ($query) use ($ageOld) {
                    $query->whereYear('dob', '<=', $ageOld)
                        ->orWhere('total_old', '>=', 1);
                });
            }
        }

        if (!empty($searchData['occupation'])) {
            if ($searchData['occupation'] >= 1) {
                $query->where('occupation', $searchData['occupation']);
            } else {
                $query->where('occupation', 'like', $searchData['occupation']);
            }
        }

        if (!empty($searchData['religious'])) {
            $query->where('religion', $searchData['religious']);
        }

        $query->whereNotNull('families.card_no');
        $query->orderByDesc('families.status');
        $query->orderByDesc('families.id');
        return $query;
    }


    /*******************************************************************************/

    /**************************** Aid Related Report ****************************/
    public function aidReport()
    {
        $page_title = "ত্রান রিপোর্ট";
        $divLists = getDivLists();
        return view('reports.aid', compact('page_title', 'divLists'));
    }

    public function aidSearchData(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
            ];

            $query = $this->__aidPieQery($searchData);
            $resData = $query->get()->toArray();
            $resData = $this->__calculatePiData($resData);
//            $resData =  json_encode($resData, JSON_NUMERIC_CHECK);

//            pr($resData);

            return response()->json(['data' => $resData, 'code' => 200]);
        }
    }

    public function aidSearchAidVsFamilyData(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
            ];

            $query = $this->__aidBarQery($searchData);
//            dd($query->toSql());
            $resData = $query->get()->toArray();
//            dd($resData);
            $resData = $this->__calculateBarData($resData);
//            $resData =  json_encode($resData, JSON_NUMERIC_CHECK);
//            pr($resData);

            return response()->json(['barData' => $resData, 'barCode' => 200]);
        }
    }

    public function aidSearchAidVsDealerData(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
            ];

            $query = $this->__aidDealerBarQery($searchData);
//            dd($query->toSql());
            $resData = $query->get()->toArray();
//            dd($resData);
            $resData = $this->__calculateAidDealerBarData($resData);
//            $resData =  json_encode($resData, JSON_NUMERIC_CHECK);
//            pr($resData);

            return response()->json(['barAidDealerData' => $resData, 'barCode' => 200]);
        }
    }

    public function aidSearchAidVsPackageData(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
            ];

            $query = $this->__aidPackageBarQery($searchData);
//            dd($query->toSql());
            $resData = $query->get()->toArray();
//            dd($resData);
            $resData = $this->__calculateAidPackageBarData($resData);
//            $resData =  json_encode($resData, JSON_NUMERIC_CHECK);
//            pr($resData);

            return response()->json(['barAidPackageData' => $resData, 'barCode' => 200]);
        }
    }

    public function __aidPieQery($searchData)
    {
        $query = DB::table('aid_types');
//        $query->select('aid_types.*', DB::raw('COUNT(aid_types.id) as total_package'), 'packages.name as pak_name', 'packages.id as pkj_id');
        $query->select('aid_types.id', 'aid_types.name', 'packages.name as pak_name', 'packages.id as pkj_id');
        $query->leftJoin('packages', 'aid_types.id', '=', 'packages.aid_id');
        if (!empty($searchData['divId'])) {
            $query->where('packages.assign_div', 'like', "%" . $searchData['divId'] . "%");
        }
        if (!empty($searchData['districtID'])) {
            $query->where('packages.assign_dist', 'like', "%\"" . $searchData['districtID'] . "\"%");
        }
        return $query;
    }

    public function __aidBarQery($searchData)
    {
        $query = DB::table('packages');
        $query->select(DB::raw("GROUP_CONCAT(DISTINCT package_vs_families.family_id SEPARATOR', ') as joined_family_id"), "packages.id", "packages.aid_id", "packages.name", "package_vs_families.family_id");
        $query->leftJoin('package_vs_families', 'packages.id', '=', 'package_vs_families.package_id');
        $query->whereNotNull('package_vs_families.family_id');

        if (!empty($searchData['divId'])) {
            $query->where('assign_div', 'like', "%" . $searchData['divId'] . "%");
        }
        if (!empty($searchData['districtID'])) {
            $query->where('assign_dist', 'like', "%\"" . $searchData['districtID'] . "\"%");
        }
        $query->groupBy('packages.aid_id');
        $query->orderByDesc('packages.aid_id');
        return $query;
    }

    public function __aidDealerBarQery($searchData)
    {
        $query = DB::table('packages');
        $query->select(DB::raw("GROUP_CONCAT(DISTINCT package_vs_dealers.dealer_id SEPARATOR ', ') AS joined_dealer_id"), "packages.id", "packages.aid_id");
        $query->leftJoin('package_vs_dealers', 'packages.id', '=', 'package_vs_dealers.package_id');
        $query->whereNotNull('package_vs_dealers.dealer_id');


        if (!empty($searchData['divId'])) {
            $query->where('packages.assign_div', 'like', "%" . $searchData['divId'] . "%");
            $query->where('package_vs_dealers.div', 'like', "%" . $searchData['divId'] . "%");
        }

        if (!empty($searchData['districtID'])) {
//            $query->where('packages.assign_dist', 'like', "%" . $searchData['districtID'] . "%");
            $query->where('packages.assign_dist', 'like', "%\"" . $searchData['districtID'] . "\"%");
            $query->where('package_vs_dealers.dist', 'like', "%" . $searchData['districtID'] . "%");
        }

        if (!empty($searchData['upazilaID'])) {
            $query->where('package_vs_dealers.sub_dist', 'like', "%" . $searchData['upazilaID'] . "%");
        }
        $query->groupBy('packages.aid_id');
        $query->orderByDesc('packages.aid_id');
        return $query;
    }

    public function __aidPackageBarQery($searchData)
    {
        $query = DB::table('packages');
        $query->select(DB::raw("count(packages.id) AS package_id"), "packages.aid_id", "aid_types.name");
        $query->leftJoin('aid_types', 'packages.aid_id', '=', 'aid_types.id');


        if (!empty($searchData['divId'])) {
            $query->where('packages.assign_div', 'like', "%" . $searchData['divId'] . "%");
        }

        if (!empty($searchData['districtID'])) {
            $query->where('packages.assign_dist', 'like', "%\"" . $searchData['districtID'] . "\"%");
        }

        $query->groupBy('packages.aid_id');
        $query->orderByDesc('packages.aid_id');
        return $query;
    }

    public function __calculatePiData($resData)
    {
        $total_count = sizeof($resData);
        $dataPoints = [];
        $finalResData = [];

        foreach ($resData as $key => $values) {

            if (!isset($dataPoints[$values->id])) {
                $dataPoints[$values->id] = [$values->name, 1, "cal" => 1];
            } else {
                $count = $dataPoints[$values->id]['cal'];
                $count++;
                $dataPoints[$values->id] = [$values->name, 1, "cal" => $count];
            }
            $y_count = $dataPoints[$values->id]['cal'];

            $dataPoints[$values->id] = [$values->name, $y_count, "cal" => $y_count];
        }


        foreach ($dataPoints as $key => $values) {
            unset($values['cal']);
            $finalResData[] = $values;
        }
        return $finalResData;
    }

    public function __calculateBarData($resData)
    {
        $total_count = sizeof($resData);
        $dataPoints = [];
        $finalResData = [];
        $assignedDistList = [];
        $aidList = getAidTypeList(null)->toArray();
        asort($aidList);
        foreach ($resData as $key => $values) {
            $familyCount = $this->fetchDistId($values->joined_family_id);
            $dataPoints[] = [$aidList[$values->aid_id], $familyCount];
        }

        $aidVsFamily = array_merge([['ত্রানের ধরণ', 'পরিবার']], $dataPoints);

        return $aidVsFamily;
    }

    public function __calculateAidDealerBarData($resData)
    {

        $dataPoints = [];
        $aidList = getAidTypeList(null)->toArray();
        asort($aidList);
        foreach ($resData as $key => $values) {
            $dealerCount = $this->fetchDistId($values->joined_dealer_id);
            $dataPoints[] = [$aidList[$values->aid_id], $dealerCount];
        }
        $aidVsDealer = array_merge([['ত্রানের ধরণ', 'ডিলার']], $dataPoints);
        return $aidVsDealer;
    }

    public function __calculateAidPackageBarData($resData)
    {
        $dataPoints = [];
        foreach ($resData as $key => $values) {
            $dataPoints[] = [$values->name, $values->package_id];
        }
        $aidVsDealer = array_merge([['ত্রানের ধরণ', 'প্যাকেজ']], $dataPoints);
        return $aidVsDealer;
    }

    public function fetchDistId($idList)
    {
        $idList = str_replace(',', '-', $idList); // Replaces all comma with hyphens.
        $idList = str_replace(', ', '-', $idList); // Replaces all comma and spaces with hyphens.
        $idList = preg_replace('/[^A-Za-z0-9\-]/', '', $idList); // Removes special chars.\
        $idList = explode('-', $idList); // use dash (-)  as the separator.
        $idList = array_unique($idList); // Remove the duplicate data.

        $totalFamily = sizeof($idList);

        return $totalFamily;
    }

    function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    function random_color()
    {
        return random_color_part() . random_color_part() . random_color_part();
    }


    public function setDistName($distId, $distNameList, $distNameListEmptyData)
    {
        $distNameListEmptyData[$distNameList[$distId]] = $distNameList[$distId];
        return $distNameList[$distId];
    }

    /****************************************************************************/

    /**************************** Package Related Report ****************************/

    public function packageReport(Request $request)
    {
        $page_title = "প্যাকেজ রিপোর্ট";
        $divLists = getDivLists();
        return view('reports.package', compact('page_title', 'divLists'));
    }

    public function packageSearchData(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
                'package' => $request->post('package'),
                'has_package' => $request->post('has_package'),
                'status' => $request->post('status'),
            ];

            $queryArr = $this->__packageQery($searchData);
            $query = $queryArr['query'];
            $familyListArr = $queryArr['familyListArr'];
//            dd($query->get());
//            dd($query->toSql());


            return DataTables::of($query)
                ->addIndexColumn()
                ->editColumn('gender', function ($row) {
                    $gender = config('constants.gender.arr.' . $row->gender);
                    return $gender;
                })
                ->editColumn('packages', function ($row) use ($familyListArr) {
//                    $packageList = $this->__getFamilyPackageList($row->id);
                    $familyPackage = implode(',', packageList($row->id, true));
//                    if(!empty($familyListArr)) {
//                        $packages = $familyListArr[$row->id];
//                    }else{
//                        $packages = "কোন ত্রাণ পায় নি।";
//                    }
//                    $gender = config('constants.gender.arr.' . $row->gender);
                    return $familyPackage;
                })
                ->editColumn('occupation', function ($row) {
                    if ($row->occupation >= 1)
                        return config('constants.occupation.arr.' . $row->occupation);
                    else
                        return $row->occupation;
                })
                ->editColumn('ssnp', function ($row) {
                    return config('constants.ssnp.arr.' . $row->ssnp);
                })
                ->addColumn('address', function ($row) {
                    $address = $row->add . ', ' . getUnionName($row->union) . ', ' . getSubDistName($row->sub_dist) . ', ' . getDistName($row->dist) . ', ' . getDivName($row->div);
                    return $address;
                })
                ->rawColumns(['action', 'gender', 'address', 'packages'])
                ->make(true);
        }
    }

    public function exportPackageReport(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
                'package' => $request->post('package'),
                'has_package' => $request->post('has_package'),
            ];

            return Excel::download(new PackageExport($searchData), 'Package_Report.xlsx');
        }
    }

    public function __packageQery($searchData)
    {
        $query = DB::table('packages');
//        $query->select(DB::raw("regexp_replace(GROUP_CONCAT(DISTINCT package_vs_families.family_id SEPARATOR ', '),'[\]\\[/\\|]+', '') AS final_list"));
        $query->select(DB::raw("GROUP_CONCAT(DISTINCT package_vs_families.family_id SEPARATOR ', ') AS final_list"));
        $query->leftJoin('package_vs_families', 'packages.id', '=', 'package_vs_families.package_id');

        if (!empty($searchData['divId'])) {
            $query->where('packages.assign_div', 'like', "%" . $searchData['divId'] . "%");
        }
        if (!empty($searchData['districtID'])) {
            $query->where('packages.assign_dist', 'like', "%\"" . $searchData['districtID'] . "\"%");
        }

        if (!empty($searchData['package'])) {
            if ($searchData['has_package'] == 1) {
                $query->where('packages.id', $searchData['package']);
            } else {
                $query->where('packages.id', '!=', $searchData['package']);
            }
        }

        $familyList = $query->get()->toArray();
        $familyList = $familyList[0]->final_list;
        $strClean = str_replace('[','',$familyList);
        $strClean = str_replace('], ',',',$strClean);
        $familyList = str_replace(']',',',$strClean);
        $familyListArr = stringDuplicateFilter($familyList);
        $familyPackage = $this->__getFamilyPackageList($familyListArr);

        $queryFamily = DB::table('families');
//        $queryFamily->whereNotNull('families.card_no');
        $queryFamily->where('families.status', '!=', config('constants.status.Inactive'));

        $familyListArr = array_unique($familyListArr);

        if ($searchData['has_package'] == 2) {
            if (empty($searchData['package'])) {
                $queryFamily->whereNotIn('id', $familyListArr);
                $familyPackage = [];
            } else {
                $excludeFamilyListArr = getFamilyListByPackageId($searchData['package']);
                $familyListArr = array_diff($familyListArr, $excludeFamilyListArr);
                $queryFamily->whereIn('families.id', $familyListArr);
            }
        } else {
            $queryFamily->whereIn('families.id', $familyListArr);
        }
        if (!empty($searchData['divId'])) {
            $queryFamily->where('families.div', $searchData['divId']);
        }
        if (!empty($searchData['districtID'])) {
            $queryFamily->where('families.dist', $searchData['districtID']);
        }
        if (!empty($searchData['upazilaID'])) {
            $queryFamily->where('families.sub_dist', $searchData['upazilaID']);
        }
        if (!empty($searchData['unionID'])) {
            $queryFamily->where('families.union', $searchData['unionID']);
        }

        $queryFamily->orderByDesc('families.id');

        return ['query' => $queryFamily, 'familyListArr' => $familyPackage];
    }

    public function __getFamilyPackageList($familyId)
    {
        $resData = [];

        foreach ($familyId as $key => $ids) {
            $resData[$ids] = implode(',', packageList($ids, true));
        }
        return $resData;
    }

    /********************************************************************************/

    /**************************** Dealer Related Report ****************************/

    public function dealerReport(Request $request)
    {
        $page_title = "ডিলার রিপোর্ট";
        $divLists = getDivLists();
        return view('reports.dealer', compact('page_title', 'divLists'));
    }

    public function dealerSearchData(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
                'dealer' => $request->post('dealer'),
            ];

            $queryArr = $this->__dealerQery($searchData);
            $query = $queryArr['query'];
//            $packageListArr = $queryArr['packageListArr'];
//            pr($query->get());
//            dd();


            return DataTables::of($query)
                ->addIndexColumn()
                ->editColumn('dealer_ids', function ($row) {
                    $dealerName = $row->name;
                    return $dealerName;
                })
                ->editColumn('address', function ($row) {
                    $dealerAddress = $row->address . ', ' . getSubDistName($row->d_sub_dist) . ', ' . getDistName($row->d_dist) . ', ' . getDivName($row->d_div) . "<br>";
                    return $dealerAddress;
                })
                ->editColumn('gender', function ($row) {
                    $gender = config('constants.gender.arr.'.$row->gender);
                    return $gender;
                })
                ->editColumn('familyId', function ($row) {
                    $strClean = str_replace(',, ',',',$row->familyList);
                    $familyIdArr = explode(',',$strClean);
                    $action = '';
                    foreach ($familyIdArr as $id){
                        if(!empty($id)){
                            $familyInfo = getFamilyInfoById($id);
                            $action .= '<a href= ' . route('families.edit', $id) . '>'.$familyInfo->card_no.'</a><meta name="csrf-token" content="{{ csrf_token() }}">, ';
                        }
                    }
                    return $action;
                })
                ->editColumn('package', function ($row) {
                    $packageInfo = getPakageInfo($row->package_id);
                    $packageInfo = $packageInfo[0];
                    $packageName = $packageInfo->name;

                    return $packageName;
                })
                ->rawColumns(['dealer_ids', 'familyId','address','gender'])
                ->make(true);
        }
    }

    public function __dealerQery($searchData)
    {
        $query = DB::table('dealer_vs_families')
            ->select('dealer_vs_families.id', 'dealer_vs_families.dealer_id','dealer_vs_families.package_id', 'dealer_vs_families.div', 'dealer_vs_families.dist', 'dealer_vs_families.sub_dist', 'dealers.name', 'dealers.email', 'dealers.phone', 'dealers.nid', 'dealers.gender', 'dealers.inst_name','dealers.trd_license','dealers.tin','dealers.div as d_div','dealers.dist as d_dist','dealers.sub_dist as d_sub_dist','dealers.address', DB::raw("GROUP_CONCAT(DISTINCT dealer_vs_families.families SEPARATOR ', ') AS familyList"));
        $query->leftJoin('dealers', 'dealer_vs_families.dealer_id', '=', 'dealers.id');

        if (!empty($searchData['divId'])) {
            $query->where('dealer_vs_families.div', $searchData['divId']);
        }
        if (!empty($searchData['districtID'])) {
            $query->where('dealer_vs_families.dist', $searchData['districtID']);
        }

        if (!empty($searchData['upazilaID'])) {
            $query->where('dealer_vs_families.sub_dist', $searchData['upazilaID']);
        }

        if (!empty($searchData['dealer'])) {
            $query->where('dealers.id', $searchData['dealer']);
        }

        $query->groupBy('dealer_vs_families.dealer_id','dealer_vs_families.package_id');

        return ['query' => $query];
    }

    public function exportDealerReport(Request $request)
    {
        if ($request->ajax()) {
            $searchData = [
                'divId' => $request->post('div'),
                'districtID' => $request->post('dist'),
                'upazilaID' => $request->post('sub_dist'),
                'unionID' => $request->post('union'),
                'dealer' => $request->post('dealer'),
            ];

            return Excel::download(new DealerExport($searchData), 'Dealer_Report.xlsx');
        }
    }

    /*******************************************************************************/


    /**************************** Family Related Report ****************************/

    /*******************************************************************************/

    /**************************** Family Related Report ****************************/

    /*******************************************************************************/

}
