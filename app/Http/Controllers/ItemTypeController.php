<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\ItemType;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItemTypeRequest;
use App\Http\Requests\UpdateItemTypeRequest;
use App\Models\Package;
use Illuminate\Http\Request;
use DataTables;
use DB;

class ItemTypeController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('item-type-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }
        try {
            $page_title = 'পণ্যের ধরনের তালিকা';

            if ($request->ajax()) {
                $query = DB::table('item_types');
//                $query = userAccess($query);

                $serial = 1;
                return DataTables::of($query)
                    ->addIndexColumn()
                    ->editColumn('status', function ($row) {
                        $status = config('constants.status.arr.' . $row->status);
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('item-type-edit')) {
                            $action .= '<a class="btn-sm btn-primary action-btn family_id" title="সম্পাদন" id="' . $row->id . '" href= ' . route('item-types.edit', $row->id) . '><i class="fa fa-pencil-alt"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('item-type-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('item-types.destroy', $row->id) . ' class="btn-sm btn-danger delete-item-type action-btn"><i class="fa fa-trash-alt"></i></a>';
                        }

                        return $action;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        } catch (\Exception $exception) {
//            dd($exception);
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


        return view('item-types.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('item-type-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন পণ্যের ধরন তৈরী';
//        $permission = Permission::get();

        return view('item-types.create', compact('page_title'));

    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('item-type-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
            ],
                [
                    'name.required' => 'পণ্যের ধরনের নাম প্রদান করুন।',
                ]
            );

            $itemTypeDataArr = $request->all();
            $lastInsertedId = ItemType::create(array_merge($itemTypeDataArr, ['created_by' => auth()->user()->id, "status" => config('constants.status.Active')]));

            DB::commit();
            return redirect()->route('item-types.index')->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show(ItemType $itemType)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('item-type-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'পণ্যের ধরনের তথ্য সংশোধন';
        DB::beginTransaction();
        try {
            $itemType = ItemType::find($id);
            return view('item-types.edit', compact('itemType', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('item-type-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
                'status' => 'required',
            ],
                [
                    'name.required' => 'পণ্যের ধরনের নাম প্রদান করুন',
                    'status.required' => 'বর্তমান অবস্থা বাছাই করুন',
                ]);

            $itemTypeRes = ItemType::find($id);
            $itemTypeRes->name = $request->input('name');
            $itemTypeRes->status = $request->input('status');
            $itemTypeRes->updated_by = auth()->user()->id;
            $itemTypeRes->save();

            DB::commit();
            return redirect()->route('item-types.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('item-type-delete')) {
            $request->session()->flash('আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                ItemType::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function getItemLists(Request $request)
    {
        if ($request->ajax()) {
            $packageId = $request->post('id');
            try {
                $itemTypeLists = ItemType::where('item_types.status', config('constants.status.Active'))
                    ->with('items')
                    ->get();

                $addeditems = DB::table('packages')
                    ->select('items')
                    ->where('id', $packageId)
                    ->first();

                $itemList = [];
                if(!empty($addeditems->items)){
                    $itemList = array_map('intval', json_decode($addeditems->items, true));
                }

                return response()->json(['status' => 'success', 'itemTypeLists' => $itemTypeLists, 'packageId' => $packageId, 'itemList' => $itemList]);
            } catch (\Exception $exception) {
                $request->session()->flash('error', $exception->getMessage());
                return response()->json(['status' => 'error']);
            }
        }
    }
}
