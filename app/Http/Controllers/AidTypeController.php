<?php

namespace App\Http\Controllers;

use App\Models\AidType;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Session;
use View;

class AidTypeController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('aid-type-list')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $page_title = 'ত্রানের ধরনের তালিকা';
            if ($request->ajax()) {
                $data = AidType::select('*');
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $action = '';
                        if (auth()->user()->can('aid-type-edit')) {
                            $action .= ' <a title="সম্পাদন" class="btn btn-primary" id="edit-aid-type" href= ' . route('aid-types.edit', $row->id) . '><i class="fa fa-pencil-alt"></i> </a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
//                                $action .= '<meta name="csrf-token" content="{{ csrf_token() }}">';
                        }
                        if (auth()->user()->can('aid-type-delete')) {
                            $action .= '<a title="মুছে ফেলুন" id=' . $row->id . ' href=' . route('aid-types.destroy', $row->id) . ' class="btn btn-danger delete-aid-type"><i class="fa fa-trash-alt"></i></a>';
                        }
                        return $action;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
            return view('aid-types.index', compact('page_title'));
//            $data = Role::orderBy('id', 'DESC')->paginate(10);
//
//            return view('roles.index', compact('data'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function create()
    {
        if (!auth()->user()->can('aid-type-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'নতুন ত্রানের ধরন তৈরী';

        return view('aid-types.create', compact('page_title'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('aid-type-create')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
            ]);

            AidType::create(['name' => $request->input('name'), 'status' => config('constants.status.Active')]);

            return redirect()->route('aid-types.index')
                ->with('success', 'তথ্য সফলভাবে যুক্ত করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে যুক্ত করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        if (!auth()->user()->can('aid-type-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        $page_title = 'ত্রানের ধরন সংশোধন';
        DB::beginTransaction();
        try {
            $aidType = AidType::find($id);
            return view('aid-types.edit', compact('aidType', 'page_title'));
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('aid-type-edit')) {
            return redirect()->route('home')->with('no_access', 'আপনার অনুমতি নেই।');
        }

        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $role = AidType::find($id);
            $role->name = $request->input('name');
            $role->save();

            return redirect()->route('aid-types.index')
                ->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            $request->session()->flash('errors', 'তথ্য সফলভাবে সংশোধন করা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('aid-type-delete')) {
//            Session::flash('errors', 'আপনার অনুমতি নেই।');
//            return View::make('partials/flash-messages');
            $request->session()->flash('error', 'আপনার অনুমতি নেই।');
            return response()->json(['status' => 'error']);
        }

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                AidType::find($id)->delete();
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
//                Session::flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
//                return View::make('partials/flash-messages');
                $request->session()->flash('error', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
//            Session::flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
//            return View::make('partials/flash-messages');
            $request->session()->flash('error', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
