<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Package;
use App\Models\PackageVsFamily;
use App\Models\FamilyVsPackage;
use Illuminate\Support\Facades\Auth;
use Validator;

class PackageController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;

        return $this->sendResponse($success, 'User register successfully.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
//        dd("the end");
//        return response()->json('api call', 200);

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;

            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    public function getPackageLists(Request $request){
        $family_id = $request->id;

        try {
//            $res = json_encode($family_id);
//            $res = getAllPackageList();
            $res = packageList($family_id);
            return response()->json(['status' => 'success', 'res' => $res]);
            $packageList = packageList($family_id);
            return response()->json(['status' => 'success', 'packageList' => $packageList, 'family_id' => $family_id]);
//                dd($packageList);
        } catch (\Exception $exception) {
//                dd($exception->getMessage());
            $request->session()->flash('error', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function assignPackage(Request $request){
        try{
            $family_id = $request->familyId;
            $package_id = $request->packageId;
            $resData = assignPackage($family_id, $package_id);
            return response()->json(['status' => 'success', 'res' => $resData]);
        }catch (\Exception $exception){
            return response()->json(['status' => 'error', 'msg' => $exception->getMessage()]);
        }

    }
}
