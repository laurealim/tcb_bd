<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
//    public function __construct(Auth $auth)
//    {
//        $this->auth = $auth;
//    }

//    public function handle($request, Closure $next, ...$guards)
//    {
//        pr($guards);
//        dd('sdasd');
//        $this->authenticate($request, $guards);
//
//        return $next($request);
//    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('login');
        }
    }

    /* Override this function */

//    protected function unauthenticated($request, array $guards)
//    {
//        pr($guards);
//        throw new AuthenticationException((404));
//    }
}
