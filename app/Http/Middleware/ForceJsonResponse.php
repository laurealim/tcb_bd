<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;

//use Illuminate\Support\Facades\Auth;

//use Illuminate\Contracts\Auth\Factory as Auth;
//use Illuminate\Support\Facades\Auth;

class ForceJsonResponse
{

//    protected $auth;
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {//
        // Given URL
        $url = $request->url();

        // Search substring
        $key = 'api';

        if (strpos($url, $key) == true) {
            $request->headers->set('Accept', 'application/json');
        }
        return $next($request);

//        pr(url()->current());
//        pr($request->url());
//        pr(url()->previous());
    }
}
