<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDealerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'g_name' => 'required',
            'email' => 'required|email|unique:dealers,email',
            'phone' => 'required',
            'gender' => 'required',
            'nid' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'inst_address' => 'required',
            'inst_name' => 'required',
            'trd_license' => 'required',
            'tin' => 'required',
            'picture' => 'required|mimes:png,jpg,jpeg|max:300', //314575
            'docs' => 'required|mimes:pdf|max:5120', //5242880
            'div' => 'required',
            'dist' => 'required',
            'sub_dist' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'নাম প্রদান করুন।',
            'g_name.required' => 'অভিভাবকের নাম প্রদান করুন।',
            'email.required|email' => 'ইমেইল প্রদান করুন।',
            'phone.required' => 'মোবাইল নং প্রদান করুন।',
            'gender.required' => 'লিঙ্গ বাছাই করুন।',
            'nid.required' => 'জাতীয় পরিচয়পত্র প্রদান করুন।',
            'dob.required' => 'জন্ম তারিখ প্রদান করুন।',
            'address.required' => 'স্থায়ী ঠিকানা প্রদান করুন।',
            'inst_name.required' => 'প্রতিষ্ঠানের নাম প্রদান করুন।',
            'trd_license.required' => 'ট্রেড লাইসেন্সের নাম্বার প্রদান করুন।',
            'inst_address.required' => 'প্রতিষ্ঠানের ঠিকানা প্রদান করুন।',
            'tin.required' => 'TIN নম্বর প্রদান করুন।',
            'picture.required' => 'ছবি নির্বাচন করুন।',
            'picture.mimes' => '.jpeg/.jpg/.png ফাইল নির্বাচন করুন।',
            'picture.max' => 'সর্বোচ্চ ৩০০-কিলোবাইটের এর অনধিক ফাইল নির্বাচন করুন।',
            'docs.required' => 'ডকুমেন্ট ফাইল নির্বাচন করুন।',
            'docs.mimes' => '.pdf ফাইল নির্বাচন করুন।',
            'docs.max' => 'সর্বোচ্চ ৫-মেগাবাইট এর অনধিক ফাইল নির্বাচন করুন।',
            'div.required' => 'বিভাগ বাছাই করুন।',
            'dist.required' => 'জেলা বাছাই করুন।',
            'sub_dist.required' => 'উপজেলা বাছাই করুন।'
        ];
    }
}
