<?php

use App\Models\Family;
use App\Models\FamilyMember;
use App\Models\FamilyVsPackage;
use App\Models\PackageVsFamily;
use App\Models\DealerVsFamily;
use Illuminate\Support\Facades\DB;

//use DB;

function pr($arr)
{
    echo "<pre>";
    var_dump($arr);
    echo "</pre>";
}

function bn2en($number)
{
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");

    return str_replace($bn, $en, $number);
}

function en2bn($number)
{
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");

    return str_replace($en, $bn, $number);
}

function str2int($string)
{
    return (int)$string;
}

function int2str($int)
{
    return (string)$int;
}

function stringDuplicateFilter($str)
{
    $resData = [];
    $str = explode(",", $str);
    foreach ($str as $key => $values) {
        $values = trim($values, "\"\"");
        $resData[] = str2int($values);
    }
//    $res = implode(',',array_flip(array_flip(explode(",",$str))));
    return $resData;
}

function getDivCode($id = null)
{
    $query = DB::table('divisions');//->select('name');
    if ($id) {
        $query->where('id', $id);
    }
    $divCode = $query->pluck('div_code', 'id')->toArray();
    return $divCode;
}

function getDivName($id = null)
{
    $query = DB::table('divisions');
    if ($id) {
        $query->where('id', $id);
    }
    $divName = $query->pluck('bn_name', 'id');
//    dd($divName);
    if ($id) {
        return $divName[$id];
    } else {
        return $divName;
    }
}

function getDistName($id = null)
{
    $query = DB::table('districts');
    if ($id) {
        $query->where('id', $id);
    }
    $distName = $query->pluck('bn_name', 'id');
//    dd($divName);
    if ($id) {
        return $distName[$id];
    } else {
        return $distName;
    }
}

function getSubDistName($id = null)
{
    $query = DB::table('upazilas');
    if ($id) {
        $query->where('id', $id);
    }
    $upazillaName = $query->pluck('bn_name', 'id');
//    dd($divName);
    if ($id) {
        return $upazillaName[$id];
    } else {
        return $upazillaName;
    }
}

function getUnionName($id = null)
{
    $uniName = "";
    if ($id == -1) {
        return $uniName .= 'সিটি কর্পোরেশন';
    }
    if ($id == 0) {
        return $uniName .= 'পৌরসভা';
    }
    $query = DB::table('unions');
    if ($id) {
        $query->where('id', $id);
    }
    $unionName = $query->pluck('bn_name', 'id');
//    dd($divName);
    if ($id) {
        return $unionName[$id];
    } else {
        return $unionName;
    }
}

function getDistCode($id = null, $divId = null)
{
    $query = DB::table('districts');
    if ($id) {
        $query->where('id', $id);
    }
    if ($divId) {
        $query->where('division_id', $divId);
    }
    $distCode = $query->pluck('dis_code', 'id')->toArray();
    return $distCode;
}

function getSubDistCode($id = null, $distId = null)
{
    $query = DB::table('upazilas');
    if ($id) {
        $query->where('id', $id);
    }
    if ($distId) {
        $query->where('district_id', $distId);
    }
    $subDistCode = $query->pluck('up_code', 'id')->toArray();
    return $subDistCode;
}

function getUnionCode($id = null, $subDIstId = null)
{
    $query = DB::table('unions');
    if ($id) {
        $query->where('id', $id);
    }
    if ($subDIstId) {
        $query->where('upazilla_id', $subDIstId);
    }
    $unionCode = $query->pluck('uni_code', 'id')->toArray();
    return $unionCode;
}

function getDivLists($divId = null, $check = true)
{
    $query = DB::table('divisions');
    if ($divId) {
    }
    $div = auth()->user()->div;
    if ($check) {
        if (!empty($div)) {
            $query->where('id', $div);
        }
    }
    $divList = $query->pluck('bn_name', 'id');
    return $divList;
}

function getDistLists($divId = null, $check = true)
{
    $query = DB::table('districts');
    $dist = auth()->user()->dist;
    if ($divId) {
        $query->where('division_id', $divId);
    }
    if ($check) {
        if (!empty($dist)) {
            $query->where('id', $dist);
        }
    }
    $distList = $query->pluck('bn_name', 'id');
    return $distList;
}

function getSubDistLists($distId = null, $check = true)
{
    $query = DB::table('upazilas');
    $sub_dist = auth()->user()->sub_dist;
    if ($distId) {
        $query->where('district_id', $distId);
    }
    if ($check) {
        if (!empty($sub_dist)) {
            $query->where('id', $sub_dist);
        }
    }
    $sub_distList = $query->pluck('bn_name', 'id');
    return $sub_distList;
}

function getUnionLists($sub_distId = null, $unionId = null, $check = true)
{
    $query = DB::table('unions');
    $union = auth()->user()->union;
    if ($sub_distId) {
        $query->where('upazilla_id', $sub_distId);
    }
//    pr($union);
    if (!empty($union)) {
        if ($union >= 1) {
            $query->where('id', $union);
        } else if ($union == -1) {
            if (!empty($unionId) && $unionId == $union)
                return [-1 => 'সিটি কর্পোরেশন'];
            else
                return [];
        }
    } else {
        if ($union < 1 && $union > -1) {
            if (!empty($unionId) && $unionId == $union)
                return [0 => 'পৌরসভা'];
            else
                return [];
        }
    }
    $unionList = $query->pluck('bn_name', 'id');
    return $unionList;
}

function cardNumberGeneration($data)
{
    foreach ($data as $key => $family) {

        $divCodeArr = getDivCode();
        $distCodeArr = getDistCode();
        $subDistCodeArr = getSubDistCode();
        $unionCodeArr = getUnionCode();

        $divCode = $distCode = $sub_distCode = $union = $word = 0;
        $cardId = '';
//        Division Code Add
//        if (!empty($family['div'])) {
//            $divCode = $family['div'];
//            if ((strlen($divCode) < 2)) {
//                $cardId .= '0';
//            }
//            $cardId .= $divCode;
//        }

//        if (!empty($family['dist_code'])) {
//        } else {
//        }
//        District Code Add
        if (!empty($family['dist_code'])) {
            $distCode = $family['dist_code'];
            if ((strlen($distCode) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $distCode;
        } elseif (!empty($distCodeArr[$family['dist']])) {
            $distCode = $distCodeArr[$family['dist']];
            if ((strlen($distCode) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $distCode;
        }

//        Upazilla Code Add
        if (!empty($family['sub_dist_code'])) {
            $sub_distCode = $family['sub_dist_code'];
            if ((strlen($sub_distCode) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $sub_distCode;
        } elseif (!empty($subDistCodeArr[$family['sub_dist']])) {
            $sub_distCode = $subDistCodeArr[$family['sub_dist']];
            if ((strlen($sub_distCode) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $sub_distCode;
        }

//        Union Code Add
        if (!empty($family['union_code'])) {
            if ($family['union_code'] == -1) {
                $cardId .= '99';
            } elseif ($family['union_code'] == 0) {
                $cardId .= '00';
            } else {
                $unionCode = $family['union_code'];
                $uniLen = strlen($unionCode);
                if ($uniLen < 2) {
                    $cardId .= '0';
                }
                $cardId .= $unionCode;
            }
        } elseif (!empty($unionCodeArr[$family['union']])) {
            $unionCode = $unionCodeArr[$family['union']];
            $uniLen = strlen($unionCode);
            if ($uniLen < 2) {
                $cardId .= '0';
            }
            $cardId .= $unionCode;
        } else {
            if ($family['union'] == -1) {
                $cardId .= '99';
            } elseif ($family['union'] == 0) {
                $cardId .= '00';
            }
        }

//        Word Code Add
        if (!empty($family['word'])) {
            $word = $family['word'];
            if ((strlen($word) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $word;
        }

//        Family ID Add
        $length = strlen($family['id']);
        for ($count = 8; $count > $length; $count--) {
            $cardId .= '0';
        }
        $cardId .= $family['id'];
//        return $cardId;

        $str = "name.: " . utf8_encode($family['name']) . ", ";
        $str .= "Husband/Father Name: " . utf8_encode($family['f-h_name']) . ", ";
        $str .= "Card No.: " . $family['$cardId'] . ", ";
        $str .= "Phone: " . utf8_encode($family['phone']) . ", ";
        $str .= "DOB: " . $family['dob'] . ", ";
        $str .= "NID: " . utf8_encode($family['nid']) . ", ";
        if ($family['gender'] == 1)
            $str .= "Gender: Male, ";
        elseif ($family['gender'] == 2)
            $str .= "Gender: Female, ";
        else
            $str .= "Gender: Others, ";

        Family::where('id', $family['id'])->update(['card_no' => $cardId]);


//        generateQrCode($str, $family['id']);

//        echo $cardId . ' <br/>';
    }
}

function cardNumberGenerationWithoutCode($data)
{
//    pr($data);
    $divCodeArr = getDivCode();
//    pr($divCodeArr);
//    pr($data);
    $distCodeArr = getDistCode();
    $subDistCodeArr = getSubDistCode();
    $unionCodeArr = getUnionCode();

    $cardId = '';
    unset($data['institute_id']);
    foreach ($data as $family) {
        $divCode = $distCode = $sub_distCode = $union = $word = 0;
//        pr($family['div']);
//        dd();
//        if (!empty($divCodeArr[$family['div']])) {
//            $divCode = $divCodeArr[$family['div']];
//            if ((strlen($divCode) < 2)) {
//                $cardId .= '0';
//            }
//            $cardId .= $divCode;
//        }

        if (!empty($distCodeArr[$family['dist']])) {
            $distCode = $distCodeArr[$family['dist']];
            if ((strlen($distCode) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $distCode;
        }

        if (!empty($subDistCodeArr[$family['sub_dist']])) {
            $sub_distCode = $subDistCodeArr[$family['sub_dist']];
            if ((strlen($sub_distCode) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $sub_distCode;
        }

        if (!empty($unionCodeArr[$family['union']])) {
            $divCode = $unionCodeArr[$family['union']];
            $uniLen = strlen($divCode);
            if ($uniLen < 2) {
                $cardId .= '0';
            }
            $cardId .= $divCode;
        } else {
            if ($family['union'] == -1) {
                $cardId .= '99';
            } elseif ($family['union'] == 0) {
                $cardId .= '00';
            }
        }

        if (!empty($family['word'])) {
            $word = $family['word'];
            if ((strlen($word) < 2)) {
                $cardId .= '0';
            }
            $cardId .= $word;
        }
//        echo $cardId . ' <br/>';

//        Family ID Add
        $length = strlen($family['id']);
        for ($count = 8; $count > $length; $count--) {
            $cardId .= '0';
        }
        $cardId .= $family['id'];
    }
    return $cardId;
}

function generateQrCode($str, $id)
{
    $path = "images/qr_code";
    $filename = $id . ".png";
    $filePath = $path . '/' . $id;

    $uploadPath = public_path($filePath);

    try {
        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0755, true);
        } else {
            if (file_exists($uploadPath . $filename)) {
                unlink($uploadPath . $filename);
            }
        }
        QrCode::format('png')->size(200)->color(40, 40, 40)->generate($str, $uploadPath . '/' . $filename);
    } catch (\Exception $excp) {
        dd($excp);
    }
}

function hasQrCode($id)
{
    $path = "images/qr_code";
    $filename = $id . ".png";
    $filePath = $path . '/' . $id;
    $uploadPath = public_path($filePath);

    if (file_exists($uploadPath . $filename)) {
//        unlink($uploadPath . $filename);
        return true;
    }

    return false;
}

function userAccess($query, $postData = null)
{

    $div = auth()->user()->div;
    $dist = auth()->user()->dist;
    $sub_dist = auth()->user()->sub_dist;
    $union = auth()->user()->union;
//    dd($div);

    if (!empty($postData)) {
        if (!empty($postData['divId'])) {
            $query->where('div', $postData['divId']);
        }
    } else if (!empty($div)) {
        $query->where('div', $div);
    }

    if (!empty($postData)) {
        if (!empty($postData['districtID'])) {
            $query->where('dist', $postData['districtID']);
        }
    } else if (!empty($dist)) {
        $query->where('dist', $dist);
    }

    if (!empty($postData)) {
        if (!empty($postData['upazilaID'])) {
            $query->where('sub_dist', $postData['upazilaID']);
        }
    } else if (!empty($sub_dist)) {
        $query->where('sub_dist', $sub_dist);
    }

    if (!empty($postData)) {
        if ($postData['unionID'] < 1 || !empty($postData['unionID'])) {
            $query->where('union', $postData['unionID']);
        }
    } else if ($union > -2) {
        $query->where('union', $union);
    }

    return $query;
}

function userDataAccess($query, $postData = null)
{

    $institute_id = auth()->user()->institute_id;

    if ($institute_id >= 1) {
        if (!empty($postData)) {
            if (!empty($postData['institute_id'])) {
                $query->where('institute_id', $postData['institute_id']);
            }
        } else if (!empty($institute_id)) {
            $query->where('institute_id', $institute_id);
        }
    }

    return $query;
}

function userGroupAccess($query)
{
}

function phoneCheck($phone)
{
    $msg = [];
    $resFamily = Family::where('phone', 'LIKE', '%' . $phone . '%')
        ->where('status', config('constants.status.Active'))
        ->first();
    $resFamilyMember = FamilyMember::where('m_phone', 'LIKE', '%' . $phone . '%')
        ->where('status', config('constants.status.Active'))
        ->first();


    if (!empty($resFamily) || !empty($resFamilyMember)) {
        $msg = [
            'err' => 'নম্বরটি পূর্বে ব্যবহার করা হয়েছে।',
        ];
    } else {
        $msg = [
            'msg' => '',
        ];
    }
    return $msg;
}

function nidCheck($nid)
{
    $msg = [];
    $resFamily = Family::where('nid', 'LIKE', '%' . $nid . '%')
        ->where('status', config('constants.status.Active'))
        ->first();
    $resFamilyMember = FamilyMember::where('m_nid', 'LIKE', '%' . $nid . '%')
        ->where('status', config('constants.status.Active'))
        ->first();

    if (!empty($resFamily) || !empty($resFamilyMember)) {
        $msg = [
            'err' => 'নম্বরটি পূর্বে ব্যবহার করা হয়েছে।',
        ];
    } else {
        $msg = [
            'msg' => '',
        ];
    }
    return $msg;
}

function getOccupation($id = null)
{
    if ($id >= 1)
        return config('constants.occupation.arr.' . $id);
    else
        return $id;
}

function getAge($date)
{

    $dob = new DateTime($date);

    $now = new DateTime();

    $difference = $now->diff($dob);

    $age = $difference->y;

    return $age;
}

function familyMemberJsonDataToArray($familyMemberData)
{
//    dd($familyMemberData);
    $m_name = json_decode($familyMemberData['m_name'], false);
    $m_dob = json_decode($familyMemberData['m_dob'], false);
    $m_gender = json_decode($familyMemberData['m_gender'], false);
    $m_nid = json_decode($familyMemberData['m_nid'], false);
    $m_phone = json_decode($familyMemberData['m_phone'], false);
    $m_occupation = json_decode($familyMemberData['m_occupation'], false);
    $m_relation = json_decode($familyMemberData['m_relation'], false);
    $m_ssnp = json_decode($familyMemberData['m_ssnp'], false);
    $m_edu_level = json_decode($familyMemberData['m_edu_level'], false);

    $memberSize = sizeof($m_name);
    $resArray = [];

    for ($count = 0; $count < $memberSize; $count++) {
        if (!empty($m_name))
            $resArray[$count]['m_name'] = $m_name[$count];
        if (!empty($m_dob))
            $resArray[$count]['m_dob'] = $m_dob[$count];
        if (!empty($m_gender))
            $resArray[$count]['m_gender'] = $m_gender[$count];
        if (!empty($m_nid))
            $resArray[$count]['m_nid'] = $m_nid[$count];
        if (!empty($m_phone))
            $resArray[$count]['m_phone'] = $m_phone[$count];
        if (!empty($m_occupation))
            $resArray[$count]['m_occupation'] = $m_occupation[$count];
        if (!empty($m_relation))
            $resArray[$count]['m_relation'] = $m_relation[$count];
        if (!empty($m_ssnp))
            $resArray[$count]['m_ssnp'] = $m_ssnp[$count];
        if (!empty($m_edu_level))
            $resArray[$count]['m_edu_level'] = $m_edu_level[$count];
    }

//    pr($resArray);
    return $resArray;

//    pr($m_name);
}

function familyMemberArraToJsonData($familyMemberData)
{
    $memberSize = sizeof($familyMemberData);
    $resArray = [];

    for ($count = 0; $count < $memberSize; $count++) {
        $resArray['m_name'][] = $familyMemberData[$count]['m_name'];
        $resArray['m_dob'][] = $familyMemberData[$count]['m_dob'];
        $resArray['m_gender'][] = $familyMemberData[$count]['m_gender'];
        $resArray['m_nid'][] = $familyMemberData[$count]['m_nid'];
        $resArray['m_phone'][] = $familyMemberData[$count]['m_phone'];
        $resArray['m_occupation'][] = $familyMemberData[$count]['m_occupation'];
        $resArray['m_relation'][] = $familyMemberData[$count]['m_relation'];
        $resArray['m_ssnp'][] = $familyMemberData[$count]['m_ssnp'];
        $resArray['m_edu_level'][] = $familyMemberData[$count]['m_edu_level'];
    }

    $resArray['m_name'] = json_encode($resArray['m_name'], true);
    $resArray['m_dob'] = json_encode($resArray['m_dob'], true);
    $resArray['m_gender'] = json_encode($resArray['m_gender'], true);
    $resArray['m_nid'] = json_encode($resArray['m_nid'], true);
    $resArray['m_phone'] = json_encode($resArray['m_phone'], true);
    $resArray['m_occupation'] = json_encode($resArray['m_occupation'], true);
    $resArray['m_relation'] = json_encode($resArray['m_relation'], true);
    $resArray['m_ssnp'] = json_encode($resArray['m_ssnp'], true);
    $resArray['m_edu_level'] = json_encode($resArray['m_edu_level'], true);

    return $resArray;

//    pr($m_name);
}

function slugGenerator($text, $divider = '-')
{
    // replace non letter or digits by divider
//    $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

    // transliterate
//    $text = iconv('UTF-8', 'UTF-8//IGNORE', $text);

    // remove unwanted characters
//    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, ' ');
    $text = trim($text, $divider);


    // remove duplicate divider
//    $text = preg_replace('~-+~', $divider, $text);

    $text = str_replace(" ", $divider, $text);
    // lowercase
    $text = strtolower($text);
    return $text;
}

function getAidTypeList($status = null)
{
    $query = DB::table('aid_types');
    if ($status) {
        $query->where('status', $status);
    }
    $aidTypeList = $query->pluck('name', 'id');
    return $aidTypeList;
}

function getAidTypeName($id, $status = null)
{
    $query = DB::table('aid_types');
    if ($status) {
        $query->where('status', $status);
    }
    $aidTypeList = $query->where('id', $id)->get()->toArray();
    $aidTypeList = $aidTypeList[0];
    return $aidTypeList->name;
}

function getAllPackageList()
{
    $query = DB::table('packages')
        ->where('status', '!=', config('constants.status.Inactive'));
    $query = getPackageAssignedDistricts($query);
//    $query = userDataAccess($query);
    $packageList = $query->pluck('packages.name', 'packages.id')->toArray();

//    $packageList = Package::where('status', '!=', config('constants.status.Inactive'))
//        ->pluck('packages.name', 'packages.id')->toArray();
    return $packageList;
}

function getPackageAssignedDistricts($query, $distList = null, $institutionList = null, $postData = null)
{
    $divId = auth()->user()->div;
    $distId = auth()->user()->dist;
    $institute_id = auth()->user()->institute_id;
//    dd($postData['districtID']);

    if (!empty($postData['divId'])) {
        $query->where('assign_div', 'like', '%' . $postData['divId'] . '%');
    } else {
        if (!empty($divId)) {
            $query->where('assign_div', 'like', "%" . $divId . "%");
        }
    }
    if (!empty($postData['districtID'])) {
        $query->where('assign_dist', 'like', "%\"". $postData['districtID'] ."\"%");
    } else {
        if (!empty($distId)) {
            $query->where('assign_dist', 'like', "%\"". $distId ."\"%");
        }
    }
    if (empty($divId) && !empty($institute_id)) {
        $query->where('institute_id', $institute_id);
    }
    if (!empty($institutionList)) {
        $query->whereIn('institution_id', $institutionList);
    }
//    dd($query->toSql());
    return $query;
}

function packageList($family_id = null, $onlyFamilyPackage = false)
{
    $allPackageList = getAllPackageList();

    $query = DB::table('family_vs_packages');
    if ($family_id) {
//        $query->leftJoin('package_vs_families','package_vs_families.package_id','=','packages.id')
//            ->where('package_vs_families.family_id', 'LIKE', $family_id.',%')
//            ->whereNull('package_vs_families.package_id');

//        $family_id = 1690;
        $res = $query->where('family_id', $family_id)->first();
//        pr($res);
        if (empty($res)) {
            if ($onlyFamilyPackage) {
                return ["কোন ত্রাণ পায় নি।"];
            } else {
                return $allPackageList;
            }
        } else {
            $package_ids = array_flip(json_decode($res->package_id));
//            pr($allPackageList);
//            pr($package_ids);
            $updatedPackageList = [];
            if ($onlyFamilyPackage) {
                $updatedPackageList = array_diff_key($allPackageList, array_diff_key($allPackageList, $package_ids));
                if (empty($updatedPackageList)) {
                    return ["কোন ত্রাণ পায় নি।"];
                }
            } else {
                $updatedPackageList = array_diff_key($allPackageList, $package_ids);
            }
//            dd($package_ids);
//            dd($updatedPackageList);
            return $updatedPackageList;
        }
    } else {
        return $allPackageList;
    }
}

function assignPackage($familyId, $packageId, $dealerId)
{
    DB::beginTransaction();
    try {
        /*  Check if family_vs_package has data */
        $familyId = str2int($familyId);
        $fvpResData = FamilyVsPackage::where('family_id', $familyId)
            ->first();
//        ->where('package_id', 'LIKE', '%,' . $packageId . ',%')

        if ($fvpResData) {
            $packageList = json_decode($fvpResData['package_id']);
            $newPackageList = array_merge($packageList, [$packageId]);
            $packageList = json_encode($newPackageList, true);
            FamilyVsPackage::where('family_id', $familyId)
                ->update(['package_id' => $packageList]);
        } else {
            $newPackageList = [$packageId];
            $packageList = json_encode($newPackageList, true);
            $fvpRes = FamilyVsPackage::create([
                'family_id' => $familyId,
                'package_id' => $packageList,
            ]);
        }


        /*  Check if package_vs_families has data   */
        $pvfResData = PackageVsFamily::where('package_id', $packageId)
//        ->where('family_id', 'LIKE', '%,' . $familyId . ',%')
            ->first();

        if ($pvfResData) {
//            $familyList = $pvfResData->familyId . $familyId . ',';
            $familyList = json_decode($pvfResData['family_id']);
            $newFamilyList = array_merge($familyList, [$familyId]);
            $familyList = json_encode($newFamilyList);
            PackageVsFamily::where('package_id', $packageId)
                ->update(['family_id' => $familyList]);
        } else {
//            $familyList = ',' . $familyId . ',';
            $newFamilyList = [$familyId];
            $familyList = json_encode($newFamilyList);
            $pvfRes = PackageVsFamily::create([
                'package_id' => $packageId,
                'family_id' => $familyList,
            ]);
        }

        /* Check if dealer_vs_family has data */
        $familyInfo = Family::where('id', $familyId)
            ->first();
        $divId = $familyInfo->div;
        $distId = $familyInfo->dist;
        $sub_distId = $familyInfo->sub_dist;
        $unionId = $familyInfo->union;

        $dealerInfo = getDealerLists($dealerId);
        $dealerInfo = $dealerInfo[0];
        if (($divId != $dealerInfo->div) && ($distId != $dealerInfo->dist) && ($sub_distId != $dealerInfo->sub_dist)) {
            return ['code' => 400];
        }

        /* Check id dealer_vs_family has previous data */
        $dataInfo = DealerVsFamily::where('div', $divId)
            ->where('dist', $distId)
            ->where('sub_dist', $sub_distId)
            ->where('dealer_id', $dealerId)
            ->where('package_id', $packageId)
            ->get()->toArray();

        if (!empty($dataInfo)) {
            $dataInfo = $dataInfo[0];
            $previousFamilyData = $dataInfo['families'];
            $updatedFamilyData = $previousFamilyData.$familyId.',';
            DealerVsFamily::where('div', $divId)
                ->where('dist', $distId)
                ->where('sub_dist', $sub_distId)
                ->where('dealer_id', $dealerId)
                ->where('package_id', $packageId)
                ->update(['families' => $updatedFamilyData]);
        } else {
            $dvfData = [];
            $dvfData['div'] = $divId;
            $dvfData['dist'] = $distId;
            $dvfData['sub_dist'] = $sub_distId;
            $dvfData['dealer_id'] = $dealerId;
            $dvfData['package_id'] = $packageId;
            $dvfData['families'] = $familyId.',';

            $dvfRes = DealerVsFamily::create($dvfData);
        }
        DB::commit();
        return true;
    } catch (Exception $exception) {
        DB::rollback();
//        dd("dsdsd");
        dd($exception->getMessage());
        return false;
    }

}

function bulkAssignPackage($familyId, $packageId)
{

    /*  Check if family_vs_package has data */
    $fvpResData = FamilyVsPackage::where('family_id', $familyId)
        ->first();
    $resDataArr = [
        'status' => true,
        'message' => '',
        'data' => [],
    ];
    $packageId = str2int($packageId);
    if ($fvpResData) {
        $packageList = json_decode($fvpResData['package_id']);
        if (in_array($packageId, $packageList)) {
            $resDataArr['status'] = false;
            $resDataArr['message'] = $familyId . ' এই প্যাকেজ পূর্বেই প্রদান করা হয়েছে।';
        } else {
            $newPackageList = array_merge($packageList, [$packageId]);
            $packageList = json_encode($newPackageList);
            $resDataArr['data'] = [
                'id' => $fvpResData['id'],
                'packageList' => $packageList,
            ];
        }
    } else {
        $newPackageList = [$packageId];
        $packageList = json_encode($newPackageList);
        $resDataArr['data'] = [
            'packageList' => $packageList,
        ];
    }
    return $resDataArr;
}

function getItemTypeList($id = null)
{
    $query = DB::table('item_types');
//    $query = userDataAccess($query);
    $query->where('status', config('constants.status.Active'));
    if (!empty($id)) {
        $query->where('id', $id);
    }
    $itemTypeList = $query->pluck('name', 'id');
    return $itemTypeList;
}

function getItemList($id = null, $typeId = null)
{
    $query = DB::table('items');

//    $query = userDataAccess($query);
    $query->where('status', config('constants.status.Active'));
    if (!empty($id)) {
        $query->where('id', $id);
    }
    if (!empty($typeId)) {
        $query->where('type_id', $id);
    }
    $itemList = $query->pluck('name', 'id');
    return $itemList;
}

function getItemInfo($id = null, $typeId = null)
{
    $query = DB::table('items');
//    $query = userDataAccess($query);

    $query->where('status', config('constants.status.Active'));
    if (!empty($id)) {
        $query->where('id', $id);
    }
    if (!empty($typeId)) {
        $query->where('type_id', $id);
    }
    $itemInfo = $query->get();
    return $itemInfo;
}

function getPakageInfo($packageID = null)
{
    $query = DB::table('packages');
    $query = getPackageAssignedDistricts($query);

    if (!empty($packageID)) {
        $query->where('id', $packageID);
    }
//    dd($query->toSql());
    $packageInfo = $query->get();
    return $packageInfo;
}

function getMinistryList($id = null)
{
    $query = DB::table('ministries');

    if (!empty($id)) {
        $query->where('id', $id);
    }
    $ministryList = $query->pluck('name', 'id');
    return $ministryList;
}

function getBivagList($id = null)
{
    $query = DB::table('bivags');

    if (!empty($id)) {
        $query->where('id', $id);
    }
    $bivagList = $query->pluck('name', 'id');
    return $bivagList;
}

function getBivagListbyParentId($parentId = null)
{
    $query = DB::table('bivags');

    $bivagList = [];
    if (!empty($parentId)) {
        $query->where('mins_id', $parentId);
        $bivagList = $query->pluck('name', 'id');
    }
    return $bivagList;
}

function getDepartmentList($id = null)
{
    $query = DB::table('departments');

    if (!empty($id)) {
        $query->where('id', $id);
    }
    $departmentList = $query->pluck('name', 'id');
    return $departmentList;
}

function getDepartmentListByMinsId($minsId = null)
{
    $query = DB::table('departments');

    $departmentList = [];
    if (!empty($minsId)) {
        $query->where('mins_id', $minsId);
        $departmentList = $query->pluck('name', 'id');
    }
    return $departmentList;
}

function getDepartmentListByParentId($prentId = null)
{
    $query = DB::table('departments');

    $departmentList = [];
    if (!empty($prentId)) {
        $query->where('divs_id', $prentId);
        $departmentList = $query->pluck('name', 'id');
    }
    return $departmentList;
}

function getInstitutionList($id = null)
{
    $query = DB::table('institutions');
//    $query = userDataAccess($query);

    if (!empty($id)) {
        $query->where('id', $id);
    }
    $institutionList = $query->pluck('name', 'id');
    return $institutionList;
}

function getInstitutionInfo($id = null)
{
    $query = DB::table('institutions');

    if (!empty($id)) {
        $query->where('id', $id);
    }
    $institutionInfo = $query->get();
    return $institutionInfo;
}

function getOtherInstitutionBySameMinistry($query, $userInsId, $divsCheck = false, $deptCheck = false)
{
//    $institytionInfo = Institution::select('mins_id', 'divs_id', 'dept')
//        ->when(true, function($query) use ($userInsId){
//            if ($userInsId >= 1) {
//                $query->where('id', $userInsId);
//            }
//        })
//        ->get()->toArray();

    $institytionInfo = DB::table('institutions')
        ->select('mins_id', 'divs_id', 'dept')
        ->where('id', $userInsId)
        ->get()->toArray();

//    dd($institytionInfo);
    $institytionInfo = $institytionInfo[0];

    $instituteQuery = DB::table('institutions');

    $mins_id = $institytionInfo->mins_id;
    $divs_id = $institytionInfo->divs_id;
    $dept_id = $institytionInfo->dept;
//    dd($dept_id);

    $instituteQuery->where('mins_id', $mins_id);

    if ($divsCheck) {
        $instituteQuery->where('divs_id', $divs_id);
    }
    if ($deptCheck) {
        $instituteQuery->where('dept', $dept_id);
    }

    $redInsArr = $instituteQuery->pluck('mins_id', 'id')->toArray();

    $instituteResArray = [];

    foreach ($redInsArr as $key => $value) {
        $instituteResArray[] = $key;
    }

    if (!empty($instituteResArray)) {
        $query->whereIn('institute_id', $instituteResArray);
    }

    return $query;
//    pr($redInsArr);
//    dd($instituteResArray);
//    dd($institytionInfo);
}

function getCreatedBy($query, $id = null)
{
    if (auth()->user()->can('is-su-admin')) {
        return $query;
    } else {
        if (!empty($id)) {
            $query->where('created_by', $id);
        }
        return $query;
    }

    $institutionInfo = $query->get();
}

function getLotNumbers($packageId)
{
    $query = DB::table('lot_item_amounts');
    $query = userDataAccess($query);

    $query->where('status', config('constants.status.Active'))
        ->where('package_id', $packageId);
    $lotCount = $query->get()->count();
    if ($lotCount < 1) {
        return 1;
    } else {
        return $lotCount + 1;
    }
}

function keyValueSwap($dataArr)
{
    if (!empty($dataArr)) {
        $swapArr = [];
        foreach ($dataArr as $srNo => $values) {
            $swapArr[$values->id] = $values;
        }
        return $swapArr;
    } else
        return [];
}

function getLotLists($packageId = null, $lotId = null)
{
    $queryLot = DB::table('lot_item_amounts');
    $queryPac = DB::table('packages');

    $divId = auth()->user()->div;
    $distId = auth()->user()->dist;
    $institute_id = auth()->user()->institute_id;

    if (($divId >= 1) && ($institute_id >= 1)) {
        $queryPac = getPackageAssignedDistricts($queryPac);
    } else if (($divId < 1) && ($institute_id >= 1)) {
        $queryPac = getOtherInstitutionBySameMinistry($queryPac, $institute_id);
    }

    $packageResArr = $queryPac->get()->toArray();
    $lotIdList = [];
    if (!empty($packageResArr)) {
        foreach ($packageResArr as $key => $values) {
            $lotIdList[] = $values->id;
        }

        $queryLot->whereIn('package_id', $lotIdList);
    }

//    $queryLot = userDataAccess($queryLot);

    $queryLot->where('status', config('constants.status.Active'));

    if (!empty($lotId)) {
        $queryLot->where('id', $lotId);
    }
    if (!empty($packageId)) {
        $queryLot->where('package_id', $packageId);
    }

//    $lotInfo = $queryLot->toSql();
    $lotInfo = $queryLot->get();
//    dd($lotInfo);
    return $lotInfo;
}

function getParentDiv($distLists)
{
    $query = DB::table('districts');
//    $query->whereIn('id', [4,8,9,10, 18, 22, 34, 33, 43, 29, 50 ,44, 64, 39, 41]);

    $query->whereIn('id', $distLists);
    $resList = $query->groupBy('division_id')->pluck('division_id', 'division_id')->toArray();

    $listArr = [];
    foreach ($resList as $key) {
        $listArr[] = $key;
    }

    return $listArr;
}

function getAllChildList($parent_id)
{
    $query = "with recursive cte (id) as (select id from nesteds where parent_id = " . $parent_id . " union all select p.id from nesteds p inner join cte on p.parent_id = cte.id) select * from cte;";
    $resultArr = DB::select(DB::raw($query));
    $childArr = [];

    foreach ($resultArr as $key => $vals) {
        $childArr[] = $vals->id;
    }
    return $childArr;
}

function getAllParentsList($child_id)
{
    $query = "WITH recursive cte (id,parent_id) AS ( SELECT id,parent_id FROM nesteds WHERE id = " . $child_id . " UNION ALL SELECT p.id, p.parent_id FROM nesteds p INNER JOIN cte ON p.id = cte.parent_id ) SELECT * FROM cte; ";
    $resultArr = DB::select(DB::raw($query));
    $parentArr = [];

    foreach ($resultArr as $key => $vals) {
        $parentArr[] = $vals->id;
    }
    return $parentArr;
}

function transformDate($value, $format = 'Y-m-d')
{
    try {
        return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
    } catch (\ErrorException $e) {
        return \Carbon\Carbon::createFromFormat($format, $value);
    }
}

function getFamilyIdByCardNo($cardNo, $select = true)
{
    $query = DB::table('families');

    if ($select) {
        $query->select('id');
    }
    $query->where('card_no', $cardNo);
    $res = $query->get()->toArray();

//    pr($res);
//    $res = $res[0];
//    dd($res[0]->id);

    if (!$select) {
        if (!empty($res[0])) {
            return $res[0];
        } else {
            return -1;
        }
    } else {
        if (!empty($res[0]->id)) {
            return $res[0]->id;
        } else {
            return -1;
        }
    }
}

function getFamilyInfoById($id)
{
    $query = DB::table('families');
    $query = userAccess($query);

    $query->where('id', $id);
    $res = $query->get()->toArray();
    if (!empty($res[0])) {
        return $res[0];
    } else {
        return false;
    }
}

function ifFamilyMemberExists($cardNo)
{
    $query = DB::table('family_members');

    $query->select('id');
    $query->where('card_no', $cardNo);
    $res = $query->get()->toArray();


//    $res = $res[0];

    if (!empty($res)) {
        return $res->id;
    } else {
        return -1;
    }
}

function getFamilyMemberInfo($familyId = null, $cardNo = null, $id = null)
{
    $query = DB::table('family_members');
//    userAccess($query);

    if ($familyId) {
        $query->where('family_id', $familyId);
    }
    if ($cardNo) {
        $query->where('card_no', $cardNo);
    }
    if ($id) {
        $query->where('id', $id);
    }

    $res = $query->get()->toArray();

//    pr($res);
    if (!empty($res))
        return $res[0];
    else
        return $res;
}

function getDealerLists($dealerId = null,$reqData = null)
{
    $query = DB::table('dealers');
    $query->select('id', 'name', 'inst_name', 'trd_license', 'tin', 'div', 'dist', 'sub_dist', 'address', 'nid', 'phone');
    if ($dealerId) {
        if (is_array($dealerId)) {
            $query->whereIn('id', $dealerId);
        } else {
            $query->where('id', $dealerId);
        }
    } else {
        $query = userAccess($query,$reqData);
    }
    $res = $query->get()->toArray();
//    $res = $query->get()->pluck('division_id', 'division_id')->toArray();
    return $res;
}

function getPackageVsDealerList($packageId = null, $div = null, $dist = null, $sub_dist = null)
{
    $query = DB::table('package_vs_dealers');
    $query->select('id', 'package_id', 'dealer_id');
    if (!empty($packageId)) {
        $query->where('package_id', $packageId);
    }
    if (!empty($div)) {
        $query->where('div', $div);
    }
    if (!empty($dist)) {
        $query->where('dist', $dist);
    }
    if (!empty($sub_dist)) {
        $query->where('sub_dist', $sub_dist);
    }
    $res = $query->get()->toArray();

    return $res;
}

function convertPkgVsDlrListToArray($pkgVsDlrList)
{
    $assignDealerListAee = [];
    foreach ($pkgVsDlrList as $key => $values) {
        if (!empty($values->dealer_id)) {
//            $assignDealerListAee = array_merge($assignDealerListAee, json_decode($values->dealer_id, true));
            $assignDealerListAee[] = str2int($values->dealer_id);
        }
    }
    return $assignDealerListAee;

}

function getFamilyListByPackageId($packageId, $isArray = true)
{
    $query = DB::table('package_vs_families');
    $query->select('family_id');
    $query->where('package_id', $packageId);
    $res = $query->get()->toArray();
    $res = $res[0]->family_id;
    if ($isArray) {
        $res = str_replace(array('"', '[', ']'), '', $res);
        $res = stringDuplicateFilter($res);
//        $res = explode(',',$res);
    }
//    $res = $query->get()->pluck('division_id', 'division_id')->toArray();
    return $res;
}
