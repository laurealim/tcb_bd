<?php

namespace App\Imports;

use App\Models\Family;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

//use Maatwebsite\Excel\Concerns\SkipsErrors;

class FamilyImport implements ToModel, WithHeadingRow, SkipsOnError, WithValidation, SkipsOnFailure
{
//    use Importable, SkipsErrors, SkipsFailures, RegistersEventListeners;
    use Importable, SkipsErrors, SkipsFailures;

    public function __construct($paramsData)
    {
        $this->div = $paramsData['div'];
        $this->dist = $paramsData['dist'];
        $this->sub_dist = $paramsData['sub_dist'];
        $this->union = $paramsData['union'];
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
//        foreach ($rows as $row) {
//            $user = Family::create([
//                'name' => $row['name'],
//                'f-h_name' => $row['father_name'],
//                'dob' => date("Y-m-d", strtotime(bn2en($row['dob']))),
//                'gender' => config('constants.gender.' . $row['gender']),
//                'div' => bn2en($row['division']),
//                'dist' => bn2en($row['district']),
//                'sub_dist' => bn2en($row['sub_division']),
//                'union' => bn2en($row['union_pourosova']),
//                'word' => bn2en($row['word']),
//                'add' => $row['vill'],
//                'occupation' => $row['occupation'],
//                'nid' => bn2en($row['nid_birth_certificate']),
//                'phone' => bn2en($row['phone']),
//                'ssnp' => config('constants.ssnp.' . $row['ssnp']),
//                'income' => $row['income'],
//                'expense' => $row['expense'],
//                'religion' => config('constants.religious.' . $row['religion']),
//            ]);
//        }
//        pr($this->div);
//        pr($this->dist);
//        pr($row);

        return new Family([
            'name' => $row['name'],
            'f_h_name' => $row['father_name'],
            'dob' => date("Y-m-d", strtotime($row['dob'])),
            'gender' => $row['gender'],
            'div_code' => $row['division'],
            'div' => $this->div,
            'dist_code' => $row['district'],
            'dist' => $this->dist,
            'sub_dist_code' => $row['sub_division'],
            'sub_dist' => $this->sub_dist,
            'union_code' => $row['union_pourosova'],
            'union' => $this->union,
            'word' => $row['word'],
            'add' => $row['vill'],
            'occupation' => $row['occupation'],
            'nid' => $row['nid_birth_certificate'],
            'phone' => $row['phone'],
            'ssnp' => $row['ssnp'],
            'income' => $row['income'],
            'expense' => $row['expense'],
            'religion' => $row['religion'],
            'institute_id' => auth()->user()->institute_id,
        ]);

//        return new Family([
//            'name' => $row['name'],
//            'f_h_name' => $row['father_name'],
//            'dob' => date("Y-m-d", strtotime($row['dob'])),
//            'gender' => config('constants.gender.arr.' . $row['gender']),
//            'div_code' => $row['division'],
//            'div' => $this->div,
//            'dist_code' => $row['district'],
//            'dist' => $this->dist,
//            'sub_dist_code' => $row['sub_division'],
//            'sub_dist' => $this->sub_dist,
//            'union_code' => $row['union_pourosova'],
//            'union' => $this->union,
//            'word' => $row['word'],
//            'add' => $row['vill'],
//            'occupation' => $row['occupation'],
//            'nid' => $row['nid_birth_certificate'],
//            'phone' => $row['phone'],
//            'ssnp' => config('constants.ssnp.arr.' . $row['ssnp']),
//            'income' => $row['income'],
//            'expense' => $row['expense'],
//            'religion' => config('constants.religious.arr.' . $row['religion']),
//            'institute_id' => auth()->user()->institute_id,
//        ]);
    }

    public function rules(): array
    {
        return [
            '*.phone' => ['required', 'unique:families,phone'],
            '*.nid_birth_certificate' => ['required', 'unique:families,nid'],
            'father_name' => ['required'],
        ];
        // TODO: Implement onError() method.
    }

//    public function batchSize(): int
//    {
//        return 500;
//    }

    public function chunkSize(): int
    {
        return 500;
    }


//    public function customValidationAttributes()
//    {
//        return [
//            'phone' => 'Mobile No.',
//            'nid_birth_certificate' => 'NID / Birth Certificate',
//            ];
//    }
//    public static function afterImport(AfterImport $event)
//    {
////        dd($event);
//        return back()->with('success', 'তথ্য কিউ এ জমা হয়েছে।++++++++ ');
//    }

//    public function onFailure(Failure ...$failure)
//    {
//        $exception = ValidationException::withMessages(collect($failure)->map->toArray()->all());
//
//        throw $exception;
//    }
}
