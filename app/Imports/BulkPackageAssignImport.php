<?php

namespace App\Imports;

use App\Models\FamilyVsPackage;
use App\Models\PackageVsFamily;
use App\Models\Family;
use App\Models\DealerVsFamily;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;

class BulkPackageAssignImport implements ToCollection, WithHeadingRow
{
    public function __construct($paramsData)
    {
        $this->packageID = $paramsData['packageId'];
        $this->dealerID = $paramsData['dealerId'];
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        $familyCardList = [];
        $errorList = [];
        $errorCode = [];
        $errorMsg = [];

        DB::beginTransaction();
        foreach ($rows as $row) {
            $familyCardId = $row['card_no'];
            $familyId = getFamilyIdByCardNo($familyCardId);
            $familyId = str2int($familyId);

            /* Check if dealer_vs_family has data */
            $familyInfo = Family::where('id', $familyId)
                ->first();
            $divId = $familyInfo->div;
            $distId = $familyInfo->dist;
            $sub_distId = $familyInfo->sub_dist;
            $unionId = $familyInfo->union;

            $dealerInfo = getDealerLists($this->dealerID);
            $dealerInfo = $dealerInfo[0];
            if (($divId != $dealerInfo->div) || ($distId != $dealerInfo->dist) || ($sub_distId != $dealerInfo->sub_dist)) {
                $msg = 'পরিবারটি (কার্ড নংঃ- ' . $familyCardId . ') উক্ত ডিলারের অধিক্ষেত্রের অন্তরভুক্ত নয়...';
                array_push($errorMsg, $msg);
                continue;
            } else {
                /* Check id dealer_vs_family has previous data */
                $dataInfo = DealerVsFamily::where('div', $divId)
                    ->where('dist', $distId)
                    ->where('sub_dist', $sub_distId)
                    ->where('dealer_id', $this->dealerID)
                    ->where('package_id', $this->packageID)
                    ->get()->toArray();

                if (!empty($dataInfo)) {
                    $dataInfo = $dataInfo[0];
                    $previousFamilyData = $dataInfo['families'];
                    $updatedFamilyData = $previousFamilyData . $familyId . ',';
                    DealerVsFamily::where('div', $divId)
                        ->where('dist', $distId)
                        ->where('sub_dist', $sub_distId)
                        ->where('dealer_id', $this->dealerID)
                        ->where('package_id', $this->packageID)
                        ->update(['families' => $updatedFamilyData]);
                } else {
                    $dvfData = [];
                    $dvfData['div'] = $divId;
                    $dvfData['dist'] = $distId;
                    $dvfData['sub_dist'] = $sub_distId;
                    $dvfData['dealer_id'] = $this->dealerID;
                    $dvfData['package_id'] = $this->packageID;
                    $dvfData['families'] = $familyId . ',';

                    $dvfRes = DealerVsFamily::create($dvfData);
                }
            }

            if ($familyId == -1) {
                array_push($errorCode, $familyCardId);
            } else {
                $resData = bulkAssignPackage($familyId, $this->packageID);
                if ($resData['status']) {
                    if (isset($resData['data']['id'])) {
                        FamilyVsPackage::where('id', $resData['data']['id'])
                            ->update(['package_id' => $resData['data']['packageList']]);
//                        ->update(['family_id' => $familyId, 'package_id' => $resData['data']['packageList']]);
                    } else {
                        FamilyVsPackage::create([
                            'family_id' => $familyId,
                            'package_id' => $resData['data']['packageList']
                        ]);
                    }
                    array_push($familyCardList, $familyId);

                } else {
                    array_push($errorList, $familyCardId);
                }
            }
        }

        $pvfResData = PackageVsFamily::where('package_id', $this->packageID)
            ->first();

        if (!empty($familyCardList)) {
            if ($pvfResData) { // if previous data exists
                $familyList = json_decode($pvfResData['family_id']); // get previously added family data
                $commonData = array_intersect($familyList, $familyCardList); // get only the common values as an array.
                $newAssignedData = array_diff($familyCardList, $commonData); // get only the uncommon values from the 1st array.
                $newAssignedData = array_merge($familyList, $newAssignedData);
                $newFamilyList = json_encode($newAssignedData);

                PackageVsFamily::where('id', $pvfResData['id'])
                    ->update(['family_id' => $newFamilyList, 'package_id' => $this->packageID]);
            } else {
                PackageVsFamily::create([
                    'family_id' => json_encode($familyCardList),
                    'package_id' => $this->packageID
                ]);
            }
        }

        DB::commit();

        if (!empty($errorList) || !empty($errorCode) || !empty($errorMsg)) {
            $str = '';
            if (!empty($errorList)) {
                foreach ($errorList as $error) {
                    $str .= $error . ', ';
                }
                $str .= ' নং আইডি পরিবারকে পূর্বেই এই প্যাকেজ প্রদান করা হয়েছে।';
            }

            if (!empty($errorCode)) {
                foreach ($errorCode as $errors) {
                    $str .= $errors . ', ';
                }
                $str .= ' নং আইডি পরিবারকে খুজে পাওয়া যায় নি।';
            }

            if (!empty($errorMsg)) {
                foreach ($errorMsg as $errors) {
                    $str .= $errors . ', ';
                }
            }
            throw new \Exception($str);
        }
    }
}
