<?php

namespace App\Imports;

use App\Models\FamilyMember;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;

class FamilyMemberImport implements ToModel, WithHeadingRow, SkipsOnError, WithValidation, SkipsOnFailure
//class FamilyMemberImport implements ToCollection
{
    use Importable, SkipsErrors, SkipsFailures;

//    public function __construct($paramsData)
//    {
//
//    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
//    public function collection(Collection $rows)
    {
//        dd($row);
        return new FamilyMember([
//            'm_gender' => $row['m_gender'],
            'f_h_name' => $row['family_card_no'],
            'm_dob' => transformDate($row['dob']),
            'div_code' => $row['head_nid'],
            'm_name' => $row['m_name'],
            'm_relation' => $row['m_relation'],
            'm_edu_level' => $row['m_edu_level'],
            'm_occupation' => $row['m_occupation'],
            'm_nid' => $row['m_nid'],
            'm_phone' => $row['m_phone'],
            'm_ssnp' => $row['m_ssnp'],
//            'div' => $this->div,
//            'dist_code' => bn2en($row['district']),
//            'dist' => $this->dist,
//            'sub_dist_code' => bn2en($row['sub_division']),
//            'sub_dist' => $this->sub_dist,
//            'union_code' => bn2en($row['union_pourosova']),
//            'union' => $this->union,
//            'word' => bn2en($row['word']),
//            'add' => $row['vill'],
//            'occupation' => $row['occupation'],
//            'nid' => bn2en($row['nid_birth_certificate']),
//            'phone' => bn2en($row['phone']),
//            'ssnp' => config('constants.ssnp.' . $row['ssnp']),
//            'income' => $row['income'],
//            'expense' => $row['expense'],
//            'religion' => config('constants.religious.' . $row['religion']),
//            'institute_id' => auth()->user()->institute_id,
//            'gender' => config('constants.gender.' . $row['gender']),
        ]);
    }

    public function rules(): array
    {
        return [
//            '*.phone' => ['required', 'unique:families,phone'],
//            '*.nid_birth_certificate' => ['required', 'unique:families,nid']
        ];
        // TODO: Implement onError() method.
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
