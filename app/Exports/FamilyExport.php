<?php

namespace App\Exports;

use App\Models\Family;

//use Illuminate\Contracts\View\View;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

//use Maatwebsite\Excel\Concerns\FromView;
use DB;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class FamilyExport extends DefaultValueBinder implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize, WithColumnFormatting, WithCustomValueBinder
{
    protected $divId;
    protected $distId;
    protected $union;
    protected $searchData;
    protected $sub_distId;
    protected $gender;
    protected $occupation;
    protected $marital_status;
    protected $education;
    protected $house_type;
    protected $ssnp;
    protected $age;
    protected $religious;

//
    public function __construct($searchData)
    {
        $this->searchData = $searchData;
        $this->divId = $searchData['divId'];
        $this->districtID = $searchData['districtID'];
        $this->upazilaID = $searchData['upazilaID'];
        $this->union = $searchData['unionID'];
        $this->gender = $searchData['gender'];
        $this->occupation = $searchData['occupation'];
        $this->marital_status = $searchData['marital_status'];
        $this->education = $searchData['education'];
        $this->house_type = $searchData['house_type'];
        $this->ssnp = $searchData['ssnp'];
        $this->age = $searchData['age'];
        $this->religious = $searchData['religious'];
    }

    public function query()
    {
//        $query = DB::table('families');
//        userAccess($query);
//        $query->orderByDesc('status')->get();

        $query = DB::table('families');
        $query->select("families.*");
        $query->leftJoin('family_members', 'families.id', '=', 'family_members.family_id');
        userAccess($query,$this->searchData);
        if(!empty($this->gender)){
            $query->where('gender',$this->gender);
        }
        if(!empty($this->occupation)){
            $query->where('occupation',$this->occupation);
        }
        if(!empty($this->marital_status)){
            $query->where('marital_status',$this->marital_status);
        }
        if(!empty($this->education)){
            $query->where('education',$this->education);
        }
        if(!empty($this->house_type)){
            $query->where('house_type',$this->house_type);
        }
        if (!empty($this->ssnp)) {
            $search = $this->ssnp;
            $query->where(function($query) use ($search){
                $query->where('ssnp', $search)
                    ->orWhere('m_ssnp', 'LIKE', '%'.$search.'%');
            });
        }
        if (!empty($this->occupation)) {
            if ($this->occupation >= 1) {
                $query->where('occupation', $this->occupation);
            } else {
                $query->where('occupation', 'like', $this->occupation);
            }
        }
        if (!empty($this->age)) {
            $ageChild = date('Y', strtotime('- 5 years'));
            $ageOld = date('Y', strtotime('- 60 years'));
            $search = $this->age;

            if($search == 1){
                $query->where(function($query) use ($ageChild){
                    $query->whereYear('dob','>=', $ageChild)
                        ->orWhere('total_child', '>=', 1);
                });
            }
            else if($search == 2){
                $query->where(function($query) use ($ageOld){
                    $query->whereYear('dob','<=', $ageOld)
                        ->orWhere('total_old', '>=', 1);
                });
            }
        }

        if (!empty($this->religious)) {
            $query->where('religion', $this->religious);
        }

        $query->whereNotNull('families.card_no');
        $query->orderByDesc('families.status');
        $query->orderByDesc('families.id');
        return $query;
    }


    public function headings(): array
    {
        return [
            'লিঙ্গ',
            'নাম',
            'কার্ড নাম্বার',
            'অভিভাবক',
            'মোবাইল',
            'জাতীয় পরিচয়পত্র',
            'জন্ম তারিখ',
            'ঠিকানা',
            'বাসার ধরন',
            'পেশা',
            'বৈবাহিক অবস্থা',
            'শিক্ষাগত যোগ্যতা',
            'সামাজিক নিরাপত্তা সেবা',
        ];
    }

    public function map($item): array
    {
        return [
            config('constants.gender.arr.' . $item->gender),
            $item->name,
            $item->card_no,
            $item->f_h_name,
            $item->phone,
            $item->nid,
            date("d/m/Y", strtotime($item->dob)),
            $item->add . ', ' . getUnionName($item->union) . ', ' . getSubDistName($item->sub_dist) . ', ' . getDistName($item->dist) . ', ' . getDivName($item->div),
            config('constants.house_type.arr.' . $item->house_type),
            config('constants.occupation.arr.' . $item->occupation),
            config('constants.marital_status.arr.' . $item->marital_status),
            config('constants.edu_level.arr.' . $item->education),
            config('constants.ssnp.arr.' . $item->ssnp),
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function columnFormats(): array
    {
        return [
//            'C' => NumberFormat::FORMAT_TEXT,
        ];
    }

}
