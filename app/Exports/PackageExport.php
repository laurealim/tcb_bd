<?php

namespace App\Exports;

use App\Models\Package;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class PackageExport extends DefaultValueBinder implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize, WithColumnFormatting, WithCustomValueBinder
{
    protected $divId;
    protected $districtID;
    protected $union;
    protected $upazilaID;
    protected $package;
    protected $has_package;
    protected $searchData;

    public function __construct($searchData)
    {
        $this->searchData = $searchData;
        $this->divId = $searchData['divId'];
        $this->districtID = $searchData['districtID'];
        $this->upazilaID = $searchData['upazilaID'];
        $this->union = $searchData['unionID'];
        $this->package = $searchData['package'];
        $this->has_package = $searchData['has_package'];
    }

    public function query()
    {
        $query = DB::table('packages');
        $query->select(DB::raw("GROUP_CONCAT(DISTINCT package_vs_families.family_id SEPARATOR ', ') AS final_list"));
        $query->leftJoin('package_vs_families', 'packages.id', '=', 'package_vs_families.package_id');

        if (!empty($this->divId)) {
            $query->where('packages.assign_div', 'like', "%" . $this->divId . "%");
        }
        if (!empty($this->districtID)) {
            $query->where('packages.assign_dist', 'like', "%\"" . $this->districtID . "\"%");
        }

        if (!empty($this->package)) {
            if ($this->has_package == 1) {
                $query->where('packages.id', $this->package);
            } else {
                $query->where('packages.id', '!=', $this->package);
            }
        }

        $familyList = $query->get()->toArray();
        $familyList = $familyList[0]->final_list;
        $strClean = str_replace('[','',$familyList);
        $strClean = str_replace('], ',',',$strClean);
        $familyList = str_replace(']',',',$strClean);
        $familyListArr = stringDuplicateFilter($familyList);

        $queryFamily = DB::table('families');
//        $queryFamily->whereNotNull('families.card_no');
        $queryFamily->where('families.status','!=',config('constants.status.Inactive'));
        $familyListArr = array_unique($familyListArr);
//            ->whereIn('id', $familyListArr);
        if ($this->has_package == 2) {
            if (empty($this->package)) {
                $queryFamily->whereNotIn('id', $familyListArr);
                $familyPackage = [];
            }else{
                $excludeFamilyListArr = getFamilyListByPackageId($this->package);
                $familyListArr = array_diff($familyListArr,$excludeFamilyListArr);
                $queryFamily->whereIn('families.id', $familyListArr);
            }
        } else {
            $queryFamily->whereIn('families.id', $familyListArr);
        }
        if (!empty($this->divId)) {
            $queryFamily->where('families.div', $this->divId);
        }
        if (!empty($this->districtID)) {
            $queryFamily->where('families.dist', $this->districtID);
        }
        if (!empty($this->upazilaID)) {
            $queryFamily->where('families.sub_dist', $this->upazilaID);
        }
        if (!empty($this->union)) {
            $queryFamily->where('families.union', $this->union);
        }

        $queryFamily->orderByDesc('families.id');

        return $queryFamily;
    }

    public function headings(): array
    {
        return [
            'লিঙ্গ',
            'নাম',
            'কার্ড নাম্বার',
            'অভিভাবক',
            'মোবাইল',
            'জাতীয় পরিচয়পত্র',
            'জন্ম তারিখ',
            'ঠিকানা',
            'বাসার ধরন',
            'পেশা',
            'বৈবাহিক অবস্থা',
            'শিক্ষাগত যোগ্যতা',
            'প্যাকেজ',
            'সামাজিক নিরাপত্তা সেবা',
        ];
    }

    public function map($item): array
    {
        $familyPackage = implode(',',packageList($item->id,true));
        return [
            config('constants.gender.arr.' . $item->gender),
            $item->name,
            $item->card_no,
            $item->f_h_name,
            $item->phone,
            $item->nid,
            date("d/m/Y", strtotime($item->dob)),
            $item->add . ', ' . getUnionName($item->union) . ', ' . getSubDistName($item->sub_dist) . ', ' . getDistName($item->dist) . ', ' . getDivName($item->div),
            config('constants.house_type.arr.' . $item->house_type),
            config('constants.occupation.arr.' . $item->occupation),
            config('constants.marital_status.arr.' . $item->marital_status),
            config('constants.edu_level.arr.' . $item->education),
            $familyPackage,
            config('constants.ssnp.arr.' . $item->ssnp),
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function columnFormats(): array
    {
        return [
//            'C' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
