<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class DealerExport extends DefaultValueBinder implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize, WithColumnFormatting, WithCustomValueBinder
{
    protected $divId;
    protected $districtID;
    protected $upazilaID;
    protected $union;
    protected $dealer;
    protected $searchData;

    public function __construct($searchData)
    {
        $this->searchData = $searchData;
        $this->divId = $searchData['divId'];
        $this->districtID = $searchData['districtID'];
        $this->upazilaID = $searchData['upazilaID'];
        $this->union = $searchData['unionID'];
        $this->dealer = $searchData['dealer'];
    }

    public function query()
    {
        $query = DB::table('dealer_vs_families')
            ->select('dealer_vs_families.id', 'dealer_vs_families.dealer_id','dealer_vs_families.package_id', 'dealer_vs_families.div', 'dealer_vs_families.dist', 'dealer_vs_families.sub_dist', 'dealers.name','dealers.id as d_id', 'dealers.email', 'dealers.phone', 'dealers.nid', 'dealers.gender', 'dealers.inst_name','dealers.trd_license','dealers.tin','dealers.div as d_div','dealers.dist as d_dist','dealers.sub_dist as d_sub_dist','dealers.address', DB::raw("GROUP_CONCAT(DISTINCT dealer_vs_families.families SEPARATOR ', ') AS familyList"));
        $query->leftJoin('dealers', 'dealer_vs_families.dealer_id', '=', 'dealers.id');

        if (!empty($this->divId)) {
            $query->where('dealer_vs_families.div', $this->divId);
        }
        if (!empty($this->districtID)) {
            $query->where('dealer_vs_families.dist', $this->districtID);
        }

        if (!empty($this->upazilaID)) {
            $query->where('dealer_vs_families.sub_dist', $this->upazilaID);
        }

        if (!empty($this->dealer)) {
            $query->where('dealers.id', $this->dealer);
        }

        $query->groupBy('dealer_vs_families.dealer_id','dealer_vs_families.package_id');
        $query->orderByDesc('d_id');

        return $query;
    }

    public function headings(): array
    {
        return [
            'ডিলারের নাম',
            'প্যাকেজের নাম',
            'মোবাইল',
            'এন,আই,ডি',
            'ইমেইল',
            'প্রতিষ্ঠানের নাম',
            'ট্রেড লাইসেন্স',
            'TIN',
            'লিঙ্গ',
            'ঠিকানা',
            'পরিবারের তালিকা',
        ];
    }

    public function map($item): array
    {
        $packageInfo = getPakageInfo($item->package_id);
        $packageInfo = $packageInfo[0];
        $packageName = $packageInfo->name;

        $dealerAddress = $item->address . ', ' . getSubDistName($item->d_sub_dist) . ', ' . getDistName($item->d_dist) . ', ' . getDivName($item->d_div);

        $strClean = str_replace(',, ',',',$item->familyList);
        $familyIdArr = explode(',',$strClean);
        $familyCardNo = '';
        foreach ($familyIdArr as $id){
            if(!empty($id)){
                $familyInfo = getFamilyInfoById($id);
                $familyCardNo .= $familyInfo->card_no.', ';
            }
        }

        return [
            $item->name,
            $packageName,
            $item->phone,
            $item->nid,
            $item->email,
            $item->inst_name,
            $item->trd_license,
            $item->tin,
            config('constants.gender.arr.' . $item->gender),
            $dealerAddress,
            $familyCardNo
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function columnFormats(): array
    {
        return [
//            'C' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
