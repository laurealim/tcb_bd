@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> {{ $page_title }} </h1>
                            <div class="float-sm-right">

                            </div>
                        </div>
                        <div class="card-body">
                            <form class="" method="post" action="{{ route('report.exportFamilyReport') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>বিভাগ</label>
                                            <select class="form-control select2" id="div" name="div"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                @foreach($divLists as $divId => $divName)
                                                    <option value="{{ $divId }}">{{ $divName }}</option>
                                                @endforeach
                                            </select>
                                            @error('div')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>জেলা</label>
                                            <select class="form-control select2" id="dist" name="dist"
                                                    style="width: 100%;">
                                            </select>
                                            @error('sub_dist')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>উপজেলা</label>
                                            <select class="form-control select2" id="sub_dist" name="sub_dist"
                                                    style="width: 100%;">
                                            </select>
                                            @error('sub_dist')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ইউনিয়ন / পৌরসভা</label>
                                            <select class="form-control select2" id="union" name="union"
                                                    style="width: 100%;">
                                            </select>
                                            @error('union')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>লিঙ্গ</label>
                                            <select class="form-control select2" id="gender" name="gender"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                <?php $gender = config('constants.gender.arr'); ?>
                                                @foreach($gender as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('gender')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>পেশা</label>
                                            <select class="form-control select2" id="occupation" name="occupation"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                <?php $occupation = config('constants.occupation.arr'); ?>
                                                @foreach($occupation as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('occupation')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>বৈবাহিক অবস্থা</label>
                                            <select class="form-control select2" id="marital_status" name="marital_status"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                <?php $marital_status = config('constants.marital_status.arr'); ?>
                                                @foreach($marital_status as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('marital_status')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>শিক্ষাগত যোগ্যতা</label>
                                            <select class="form-control select2" id="education" name="education"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                <?php $marital_status = config('constants.edu_level.arr'); ?>
                                                @foreach($marital_status as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('education')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>বাসার ধরন</label>
                                            <select class="form-control select2" id="house_type" name="house_type"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                    <?php $gender = config('constants.house_type.arr'); ?>
                                                @foreach($gender as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('house_type')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>সামাজিক নিরাপত্তা</label>
                                            <select class="form-control select2" id="ssnp" name="ssnp"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                    <?php $ssnp = config('constants.ssnp.arr'); ?>
                                                @foreach($ssnp as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('ssnp')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>বয়স ভিত্তিক তালিকা</label>
                                            <select class="form-control select2" id="age" name="age"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                <option value='1'>শিশু (৫ বছর বা তার নিচে)</option>
                                                <option value='2'>বৃদ্ধ/ বৃদ্ধা (৬০ বছর বা তদুর্ধ) </option>
                                            </select>
                                            @error('age')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ধর্ম</label>
                                            <select class="form-control select2" id="religious" name="religious"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                    <?php $religious = config('constants.religious.arr'); ?>
                                                @foreach($religious as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('religious')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <a class="btn btn-info search"
                                           href=""><i class="fa-solid  fa-search"></i> সার্চ</a>
                                        &nbsp;
                                        @can('report-family')
                                            <button class="btn btn-success export_file">
                                                <i class="fa-solid fa-file-arrow-down"></i> ডাউনলোড রিপোর্ট
                                            </button>
{{--                                            <a class="btn btn-success export_file" type="sub"--}}
{{--                                               href=""></a>--}}
                                        @endcan
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th>পরিবারের কার্ড নং</th>
                                        <th>পরিবার প্রধানের নাম</th>
                                        <th>পিতা বা স্বামীর নাম</th>
                                        <th>মোনাইল</th>
                                        <th>এনাইডি / জন্মসনদ</th>
                                        <th>ঠিকানা</th>
                                        <th>বাসার ধরন</th>
                                        <th>জন্ম তারিখ</th>
                                        <th>লিংগ</th>
                                        <th>পেশা</th>
                                        <th>বৈবাহিক অবস্থা</th>
                                        <th>শিক্ষাগত যোগ্যতা</th>
                                        <th>সামাজিক নিরাপত্তা (SSNP)</th>
                                        <th>অবস্থা</th>
                                        <th>ধর্ম</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>

                        <hr/>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.data-table').dataTable({
            });

            $(".search").on('click', function (e) {
                e.preventDefault();
                var divId = $('#div').val();
                var distId = $('#dist').val();
                var sub_distId = $('#sub_dist').val();
                var unionId = $('#union').val();
                var gender = $('#gender').val();
                var occupation = $('#occupation').val();
                var marital_status = $('#marital_status').val();
                var education = $('#education').val();
                var house_type = $('#house_type').val();
                var ssnp = $('#ssnp').val();
                var age = $('#age').val();
                var religious = $('#religious').val();

                var token = $("input[name='_token']").val();
                var url = "{{ route('report.familySearchData') }}";
                var dataRes = {div: divId, dist: distId, sub_dist: sub_distId, union: unionId, gender: gender, occupation: occupation, marital_status: marital_status, education: education, house_type: house_type, ssnp: ssnp, age: age, religious: religious, _token: token};

                if (divId && distId && sub_distId) {
                    loadDataTable(dataRes, url);
                    // window.location.reload();
                    // $.ajax({
                    //     url: url,
                    //     method: 'POST',
                    //     data: {div: divId, dist: distId, sub_dist: sub_distId, union: unionId, _token: token},
                    //     dataType: "json",
                    //     success: function (data) {
                    //         console.log(data.data);
                    //         // window.location.reload();
                    //
                    //         // var table = $('.data-table').DataTable({
                    //         //     processing: true,
                    //         //     serverSide: true,
                    //         //     bDestroy: true,
                    //         //     data:data.data,
                    //         //     columns: [
                    //         //         {data: 'id', name: 'id'},
                    //         //         {data: 'card_no', name: 'card_no'},
                    //         //         {data: 'name', name: 'name'},
                    //         //         {data: 'f_h_name', name: 'f_h_name'},
                    //         //         {data: 'phone', name: 'phone'},
                    //         //         {data: 'nid', name: 'nid'},
                    //         //         {data: 'address', name: 'address'},
                    //         //         {data: 'dob', name: 'dob'},
                    //         //         {data: 'gender', name: 'gender'},
                    //         //         {data: 'status', name: 'status'},
                    //         //         {data: 'action', name: 'action', orderable: false, searchable: false},
                    //         //     ]
                    //         // });
                    //     },
                    //     error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //         alert("Status: " + textStatus);
                    //         alert("Error: " + errorThrown);
                    //         console.log(XMLHttpRequest);
                    //         console.log(textStatus);
                    //         console.log(errorThrown);
                    //         // window.location.reload();
                    //     }
                    // });
                } else {
                    alert('বিভাগ, জেলা ও উপজেলা বাছাই করুন...');
                }
                {{--var familyId = $(this).attr('family_id');--}}
                {{--// var divId = $(this).attr('id');--}}



            })

            $("#modal-lg").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                // var divId = $(this).attr('id');
                var token = $("input[name='_token']").val();
                var url = "{{ route('packages.getPackageLists') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {id: id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);

                        $('select[name="package"]').empty();
                        $('select[name="package"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data.packageList, function (key, value) {
                            $('select[name="package"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                        $("#assign-package").attr('family_id', data.family_id);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });

                {{--$.get( "/controller/" + id, function( data ) {--}}
                {{--    $(".modal-body").html(data.html);--}}
                {{--});--}}

            });

            $(document).on("click", "a.delete-family", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    districtList(divId, token, url, 'pre');
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    upazillaList(districtID, token, url, 'pre');
                } else {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#sub_dist').on('change', function () {
                var upazilaID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('union.unionSelectAjaxList') }}";
                if (upazilaID) {
                    $('select[name="union"]').empty();
                    unionList(upazilaID, token, url, 'pre');
                } else {
                    $('select[name="union"]').empty();
                }
            });

            $(".export_file").on('click',function(e){
                e.preventDefault();
                $('.export_file').html('<i class="fa-solid fa-file-arrow-down"></i> রিপোর্ট প্রস্তুত হচ্ছে.........');
                $('.export_file').prop('disabled', true);

                var divId = $('#div').val();
                var distId = $('#dist').val();
                var sub_distId = $('#sub_dist').val();
                var unionId = $('#union').val();
                var gender = $('#gender').val();
                var occupation = $('#occupation').val();
                var marital_status = $('#marital_status').val();
                var education = $('#education').val();
                var house_type = $('#house_type').val();
                var ssnp = $('#ssnp').val();
                var age = $('#age').val();
                var religious = $('#religious').val();

                var token = $("input[name='_token']").val();
                var url = "{{ route('report.exportFamilyReport') }}";
                var dataRes = {div: divId, dist: distId, sub_dist: sub_distId, union: unionId, gender: gender, occupation: occupation, marital_status: marital_status, education: education, house_type: house_type, ssnp: ssnp, age: age, religious: religious, _token: token};

                if (divId && distId && sub_distId) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: dataRes,
                        xhrFields:{
                            responseType: 'blob'
                        },
                        success: function (data) {
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(data);
                            link.download = `Family_Report.xlsx`;
                            link.click();
                            $('.export_file').html('<i class="fa-solid fa-file-arrow-down"></i> ডাউনলোড রিপোর্ট');
                            $('.export_file').prop('disabled', false);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                            console.log(XMLHttpRequest);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });
                } else {
                    $('.export_file').html('<i class="fa-solid fa-file-arrow-down"></i> ডাউনলোড রিপোর্ট');
                    $('.export_file').prop('disabled', false);
                    alert('বিভাগ, জেলা ও উপজেলা বাছাই করুন...');
                }
            });

            function districtList(divId, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function unionList(upazilaID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {upazilaID: upazilaID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $('select[name="union"]').append('<option value="' + -1 + '">' + "সিটি কর্পোরেশন" + '</option>');
                        // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function loadDataTable(dataRes, url) {
                // var token = $("input[name='_token']").val();
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    ajax: {
                        url: url,
                        type: "POST",
                        data: dataRes,
                        dataType: "json",
                    },
                    columns: [
                        {data: 'card_no', name: 'card_no'},
                        {data: 'name', name: 'name'},
                        {data: 'f_h_name', name: 'f_h_name'},
                        {data: 'phone', name: 'phone'},
                        {data: 'nid', name: 'nid'},
                        {data: 'address', name: 'address'},
                        {data: 'house_type', name: 'house_type'},
                        {data: 'dob', name: 'dob'},
                        {data: 'gender', name: 'gender'},
                        {data: 'occupation', name: 'occupation'},
                        {data: 'marital_status', name: 'marital_status'},
                        {data: 'education', name: 'education'},
                        {data: 'ssnp', name: 'ssnp'},
                        {data: 'religion', name: 'religion'},
                        {data: 'status', name: 'status'},
                    ]
                });
            }

        });
    </script>
@endsection
