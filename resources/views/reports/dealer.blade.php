@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> {{ $page_title }} </h1>
                            <div class="float-sm-right">

                            </div>
                        </div>
                        <div class="card-body">
                            <form class="" method="post" action="{{ route('report.exportDealerReport') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>বিভাগ</label>
                                            <select class="form-control select2" id="div" name="div"
                                                    style="width: 100%;">
                                                <option value="">-- বাছাই করুন --</option>
                                                @foreach($divLists as $divId => $divName)
                                                    <option value="{{ $divId }}">{{ $divName }}</option>
                                                @endforeach
                                            </select>
                                            @error('div')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>জেলা</label>
                                            <select class="form-control select2" id="dist" name="dist"
                                                    style="width: 100%;">
                                            </select>
                                            @error('sub_dist')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>উপজেলা</label>
                                            <select class="form-control select2" id="sub_dist" name="sub_dist"
                                                    style="width: 100%;">
                                            </select>
                                            @error('sub_dist')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ইউনিয়ন / পৌরসভা</label>
                                            <select class="form-control select2" id="union" name="union"
                                                    style="width: 100%;">
                                            </select>
                                            @error('union')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ডিলার তালিকা</label>
                                            <select class="form-control select2" id="dealer" name="dealer"
                                                    style="width: 100%;">
                                            </select>
                                            @error('dealer')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

{{--                                    <div class="col-md-3">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>প্যাকেজ গ্রহণকারী / প্যাকেজ বিহীন</label>--}}
{{--                                            <select class="form-control select2" id="has_package" name="has_package"--}}
{{--                                                    style="width: 100%;">--}}
{{--                                                <option value="1">প্যাকেজ গ্রহণকারী</option>--}}
{{--                                                <option value="2">প্যাকেজ বিহীন</option>--}}
{{--                                            </select>--}}
{{--                                            @error('has_package')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                            <strong>{{ $message }}</strong>--}}
{{--                                        </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-3">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>অবস্থা</label>--}}
{{--                                            <select class="form-control select2" id="status" name="status"--}}
{{--                                                    style="width: 100%;">--}}
{{--                                                <option value="">-- বাছাই করুন --</option>--}}
{{--                                                    <?php $status = config('constants.status.arr'); ?>--}}
{{--                                                @foreach($status as $id => $value)--}}
{{--                                                    <option value="{{ $id }}">{{ $value }}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                            @error('status')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                            <strong>{{ $message }}</strong>--}}
{{--                                        </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>

                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <a class="btn btn-info search"
                                           href=""><i class="fa-solid  fa-search"></i> সার্চ</a>
                                        &nbsp;
                                        @can('report-family')
                                            <button class="btn btn-success export_file">
                                                <i class="fa-solid fa-file-arrow-down"></i> ডাউনলোড রিপোর্ট
                                            </button>
                                            {{--                                            <a class="btn btn-success export_file" type="sub"--}}
                                            {{--                                               href=""></a>--}}
                                        @endcan
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th>নং</th>
                                        <th>ডিলারের নাম</th>
                                        <th>প্যাকেজের নাম</th>
                                        <th>মোবাইল</th>
                                        <th>এন,আই,ডি</th>
                                        <th>ইমেইল</th>
                                        <th>প্রতিষ্ঠানের নাম</th>
                                        <th>ট্রেড লাইসেন্স</th>
                                        <th>TIN</th>
                                        <th>লিঙ্গ</th>
                                        <th>ঠিকানা</th>
                                        <th>পরিবারের তালিকা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>

                        <hr/>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.data-table').dataTable({});

            $(".search").on('click', function (e) {
                e.preventDefault();
                var divId = $('#div').val();
                var distId = $('#dist').val();
                var sub_distId = $('#sub_dist').val();
                var unionId = $('#union').val();
                var dealer = $('#dealer').val();
                // var has_package = $('#has_package').val();
                // var status = $('#status').val();

                var token = $("input[name='_token']").val();
                var url = "{{ route('report.dealerSearchData') }}";
                var dataRes = {
                    div: divId,
                    dist: distId,
                    sub_dist: sub_distId,
                    union: unionId,
                    dealer: dealer,
                    // has_package: has_package,
                    // status: status,
                    _token: token
                };

                if (divId && distId) {
                    loadDataTable(dataRes, url);
                } else {
                    alert('বিভাগ, জেলা ও উপজেলা বাছাই করুন...');
                }
                {{--var familyId = $(this).attr('family_id');--}}
                {{--// var divId = $(this).attr('id');--}}



            });

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    districtList(divId, token, url, 'pre');
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    upazillaList(districtID, token, url, 'pre');
                    getDealerList();
                } else {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#sub_dist').on('change', function () {
                var upazilaID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('union.unionSelectAjaxList') }}";
                if (upazilaID) {
                    $('select[name="union"]').empty();
                    unionList(upazilaID, token, url, 'pre');
                } else {
                    $('select[name="union"]').empty();
                }
            });

            $(".export_file").on('click',function(e){
                e.preventDefault();
                $('.export_file').html('<i class="fa-solid fa-file-arrow-down"></i> রিপোর্ট প্রস্তুত হচ্ছে.........');
                $('.export_file').prop('disabled', true);

                var token = $("input[name='_token']").val();
                var url = "{{ route('report.exportDealerReport') }}";
                var divId = $('#div').val();
                var distId = $('#dist').val();
                var sub_distId = $('#sub_dist').val();
                var unionId = $('#union').val();
                var dealer = $('#dealer').val();
                var dataRes = {
                    div: divId,
                    dist: distId,
                    sub_dist: sub_distId,
                    union: unionId,
                    dealer: dealer,
                    _token: token
                };

                if (divId && distId) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: dataRes,
                        xhrFields:{
                            responseType: 'blob'
                        },
                        success: function (data) {
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(data);
                            link.download = `Dealer_Report.xlsx`;
                            link.click();
                            $('.export_file').html('<i class="fa-solid fa-file-arrow-down"></i> ডাউনলোড রিপোর্ট');
                            $('.export_file').prop('disabled', false);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                            console.log(XMLHttpRequest);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });
                } else {
                    $('.export_file').html('<i class="fa-solid fa-file-arrow-down"></i> ডাউনলোড রিপোর্ট');
                    $('.export_file').prop('disabled', false);
                    alert('বিভাগ ও জেলা বাছাই করুন...');
                }
            });


        });

        function districtList(divId, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {divId: divId, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function upazillaList(districtID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    // $('select[name="union"]').append('<option value="' + -1 + '">' + "সিটি কর্পোরেশন" + '</option>');
                    // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function getDealerList() {
            var divId = $('#div').val();
            var districtID = $('#dist').val();
            var sub_distId = $('#sub_dist').val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('dealers.getAssignDealerList') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: {divId: divId, districtID: districtID, sub_distId: sub_distId, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="dealer"]').empty();
                    $('select[name="dealer"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    // $('select[name="union"]').append('<option value="' + -1 + '">' + "সিটি কর্পোরেশন" + '</option>');
                    // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="dealer"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });

        }

        function loadDataTable(dataRes, url) {
            // var token = $("input[name='_token']").val();
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: {
                    url: url,
                    type: "POST",
                    data: dataRes,
                    dataType: "json",
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'dealer_ids', name: 'dealer_ids'},
                    {data: 'package', name: 'package'},
                    {data: 'phone', name: 'phone'},
                    {data: 'nid', name: 'nid'},
                    {data: 'email', name: 'email'},
                    {data: 'inst_name', name: 'inst_name'},
                    {data: 'trd_license', name: 'trd_license'},
                    {data: 'tin', name: 'tin'},
                    {data: 'gender', name: 'gender'},
                    {data: 'address', name: 'address'},
                    {data: 'familyId', name: 'familyId'},
                ]
            });
        }
    </script>
@endsection

