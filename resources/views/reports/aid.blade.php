@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> {{ $page_title }} </h1>
                            <div class="float-sm-right">

                            </div>
                        </div>
                        <div class="card-body">
                            <form class="" method="post" action="{{ route('report.exportAidReport') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>বিভাগ</label>
                                            <select class="form-control select2" id="div" name="div"
                                                    style="width: 100%;">
                                                <option value="">Please Select</option>
                                                @foreach($divLists as $divId => $divName)
                                                    <option value="{{ $divId }}">{{ $divName }}</option>
                                                @endforeach
                                            </select>
                                            @error('div')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>জেলা</label>
                                            <select class="form-control select2" id="dist" name="dist"
                                                    style="width: 100%;">
                                            </select>
                                            @error('sub_dist')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>উপজেলা</label>
                                            <select class="form-control select2" id="sub_dist" name="sub_dist"
                                                    style="width: 100%;">
                                            </select>
                                            @error('sub_dist')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ইউনিয়ন / পৌরসভা</label>
                                            <select class="form-control select2" id="union" name="union"
                                                    style="width: 100%;">
                                            </select>
                                            @error('union')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            {{--                                                @foreach($roles as $key => $role)--}}
                                            {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                            {{--                                                @endforeach--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-12">
                                    <div class="col-md-3">
                                        <a class="btn btn-info search"
                                           href=""><i class="fa-solid  fa-search"></i> সার্চ</a>
                                        &nbsp;
                                        @can('report-family')
                                            <button class="btn btn-success export_file">
                                                <i class="fa-solid fa-file-arrow-down"></i> ডাউনলোড রিপোর্ট
                                            </button>
                                            {{--                                            <a class="btn btn-success export_file" type="sub"--}}
                                            {{--                                               href=""></a>--}}
                                        @endcan
                                    </div>
                                </div>


                            </form>
                        </div>
                        <div class="card-body">
                            <div class="row col-md-12 justify-content-center">
{{--                                <h2>ত্রান</h2>--}}
                                <div id="chart_div" style="width:50%; height:100%"></div>
                            </div>
                            <hr>
                            <div class="row col-mb-6 justify-content-center">
{{--                                <h2>জেলা প্রতি ত্রান</h2>--}}
                                <div id="aidVsFamily" style="width: 50%; height: 300px;"></div>
                            </div>
                            <hr>
                            <div class="row col-mb-6 justify-content-center">
{{--                                <h2>জেলা প্রতি ত্রান</h2>--}}
                                <div id="aidVsDealer" style="width: 50%; height: 300px;"></div>
                            </div>
                            <hr>
                            <div class="row col-mb-6 justify-content-center">
{{--                                <h2>জেলা প্রতি ত্রান</h2>--}}
                                <div id="aidVsPackage" style="width: 50%; height: 300px;"></div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                        <hr/>
                    </div>

                    {{--                    <div class="card">--}}

                    <!-- /.modal -->
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart', 'bar'], 'callback': drawCharts});
        // google.charts.setOnLoadCallback(drawChart);
        // google.charts.load('current', {'packages': ['bar']});
        // google.charts.setOnLoadCallback(drawBarChart);

        function drawCharts() {
            drawChart();
            drawBarChart();
            drawBarChartAidVsDealer();
            drawBarChartAidVsPackages();
        }

        function drawChart(resData = null) {
            // Create the data table.
            var data = new google.visualization.DataTable(resData);
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            console.log(resData)
            if (resData) {
                data.addRows(
                    resData,
                );
            } else {
                data.addRows([]);
            }

            // Set chart options
            var options = {
                'title': 'ত্রানের সংখ্যা',
                is3D: true,
                'width': 800,
                'height': 600,
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }

        function drawBarChart(resData = null) {
            var barChartData = []
            if (resData) {
                barChartData = resData;
            } else {
                barChartData = [
                    ['ত্রানের ধরণ', 'পরিবার']
                ]
            }
            var data = google.visualization.arrayToDataTable(barChartData);

            var options = {
                chart: {
                    title: 'ত্রান ভিত্তিক পরিবার সংখ্যা ',
                }
            };

            var chart = new google.charts.Bar(document.getElementById('aidVsFamily'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }

        function drawBarChartAidVsDealer(resData = null){
            var barChartData = []
            if (resData) {
                barChartData = resData;
            } else {
                barChartData = [
                    ['Aid Type', 'Dealer']
                ]
            }
            var data = google.visualization.arrayToDataTable(barChartData);

            var options = {
                chart: {
                    title: 'ত্রান ভিত্তিক ডিলার সংখ্যা',
                }
            };

            var chart = new google.charts.Bar(document.getElementById('aidVsDealer'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }

        function drawBarChartAidVsPackages(resData = null){
            var barChartData = []
            if (resData) {
                barChartData = resData;
            } else {
                barChartData = [
                    ['Aid Type', 'Packages']
                ]
            }
            var data = google.visualization.arrayToDataTable(barChartData);

            var options = {
                chart: {
                    title: 'ত্রান ভিত্তিক প্যাকেজের সংখ্যা',
                }
            };

            var chart = new google.charts.Bar(document.getElementById('aidVsPackage'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }

        // $(document).ready(function () {

        $(".search").on('click', function (e) {
            e.preventDefault();
            loadInitPieData();
            loadInitAidVsFamilyData();
            loadInitAidVsDealerData();
            loadInitAidVsPackageData();
        });

        $('#div').on('change', function () {
            var divId = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('district.districtSelectAjaxList') }}";
            if (divId) {
                $('select[name="dist"]').empty();
                $('select[name="sub_dist"]').empty();
                $('select[name="union"]').empty();
                districtList(divId, token, url, 'pre');
            } else {
                $('select[name="dist"]').empty();
                $('select[name="sub_dist"]').empty();
                $('select[name="union"]').empty();
            }
        });

        $('#dist').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="sub_dist"]').empty();
                $('select[name="union"]').empty();
                upazillaList(districtID, token, url, 'pre');
            } else {
                $('select[name="sub_dist"]').empty();
                $('select[name="union"]').empty();
            }
        });

        $('#sub_dist').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.unionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="union"]').empty();
                unionList(upazilaID, token, url, 'pre');
            } else {
                $('select[name="union"]').empty();
            }
        });

        function districtList(divId, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {divId: divId, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function upazillaList(districtID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    // $('select[name="union"]').append('<option value="' + -1 + '">' + "সিটি কর্পোরেশন" + '</option>');
                    // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function loadInitPieData() {
            var divId = $('#div').val();
            var distId = $('#dist').val();
            var sub_distId = $('#sub_dist').val();
            var unionId = $('#union').val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('report.aidSearchData') }}";
            var dataRes = {
                div: divId,
                dist: distId,
                sub_dist: sub_distId,
                union: unionId,
                _token: token
            };

            $.ajax({
                url: url,
                method: 'POST',
                data: dataRes,
                dataType: "json",
                success: function (data) {
                    console.log(data.data);
                    drawChart(data.data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    // window.location.reload();
                }
            });
        }

        function loadInitAidVsFamilyData() {
            var divId = $('#div').val();
            var distId = $('#dist').val();
            var sub_distId = $('#sub_dist').val();
            var unionId = $('#union').val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('report.aidSearchAidVsFamilyData') }}";
            var dataRes = {
                div: divId,
                dist: distId,
                sub_dist: sub_distId,
                union: unionId,
                _token: token
            };

            $.ajax({
                url: url,
                method: 'POST',
                data: dataRes,
                dataType: "json",
                success: function (data) {
                    console.log(data.barData);
                    arrayBarData = data.barData;
                    // var array = $.map(data.barData, function(value, index){
                    //     return [value];
                    // });
                    drawBarChart(arrayBarData);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    // window.location.reload();
                }
            });
        }

        function loadInitAidVsDealerData(){
            var divId = $('#div').val();
            var distId = $('#dist').val();
            var sub_distId = $('#sub_dist').val();
            var unionId = $('#union').val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('report.aidSearchAidVsDealerData') }}";
            var dataRes = {
                div: divId,
                dist: distId,
                sub_dist: sub_distId,
                union: unionId,
                _token: token
            };

            $.ajax({
                url: url,
                method: 'POST',
                data: dataRes,
                dataType: "json",
                success: function (data) {
                    console.log(data.barAidDealerData);
                    arrayBarAidDealerData = data.barAidDealerData;
                    // var array = $.map(data.barData, function(value, index){
                    //     return [value];
                    // });
                    drawBarChartAidVsDealer(arrayBarAidDealerData);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    // window.location.reload();
                }
            });
        }

        function loadInitAidVsPackageData(){
            var divId = $('#div').val();
            var distId = $('#dist').val();
            var sub_distId = $('#sub_dist').val();
            var unionId = $('#union').val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('report.aidSearchAidVsPackageData') }}";
            var dataRes = {
                div: divId,
                dist: distId,
                sub_dist: sub_distId,
                union: unionId,
                _token: token
            };

            $.ajax({
                url: url,
                method: 'POST',
                data: dataRes,
                dataType: "json",
                success: function (data) {
                    console.log(data.barAidPackageData);
                    arrayBarAidPackageData = data.barAidPackageData;
                    // var array = $.map(data.barData, function(value, index){
                    //     return [value];
                    // });
                    drawBarChartAidVsPackages(arrayBarAidPackageData);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    // window.location.reload();
                }
            });
        }

        function piChart(res_data = null) {

        }

        function picChart(dataPoints) {
            var chart = new CanvasJS.Chart("chartContainer", {
                theme: "light2",
                animationEnabled: true,
                title: {
                    text: "ত্রান"
                },
                data: [{
                    type: "pie",
                    indexLabel: "{label} ({y})",
                    yValueFormatString: "#,##0.00\"%\"",
                    // yValueFormatString: "#,##0.00\"%\"",
                    // indexLabel: "{label} ({y})",
                    // indexLabelPlacement: "outside",
                    indexLabelFontColor: "#36454F",
                    indexLabelFontSize: 14,
                    indexLabelFontWeight: "bolder",
                    // showInLegend: true,
                    // legendText: "{indexLabel}",
                    dataPoints: dataPoints
                }]
            });
            chart.render();
        }

        drawCharts();
        // loadInitPieData();
        // loadInitBarData();

        // });

        // piChart();
    </script>
@endsection
