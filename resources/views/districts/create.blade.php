@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="" method="post" action="{{ route('districts.store') }}">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('location-list')
                                <a class="btn btn-primary" href="{{ route('districts.index') }}"> জেলার
                                    তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>নাম(English) *</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="জেলার নাম" required>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>কোড *</label>
                                    <input type="number" min="0" max="200" class="form-control" name="div_code"
                                           id="dis_code"
                                           placeholder="" required>
                                    @error('dis_code')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>নাম(বাংলা) *</label>
                                    <input type="text" class="form-control" name="bn_name" id="bn_name"
                                           placeholder="জেলার নাম" required>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('bn_name')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label> বিভাগের নাম <span class="required">*</span></label>
                                    <select class="form-control select2" id="division_id" name="division_id"
                                            value="{{ old('division_id') }}"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $division_list = getDivLists();
                                        foreach ($division_list as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('division_id')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->

                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">সংরক্ষন</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

        });
    </script>
@endsection
