@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="form-horizontal" method="POST" action="{{ route('institutions.update', $institutions->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('institution-list')
                                <a class="btn btn-primary" href="{{ route('institutions.index') }}"> প্রতিষ্ঠানের তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{-- Name --}}
                                <div class="form-group">
                                    <label>প্রতিষ্ঠানের নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="প্রতিষ্ঠানের নাম" value="{{ $institutions->name }}" required>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-4">
                                {{-- Status --}}
                                <div class="form-group">
                                    <label> অবস্থা <span class="required">*</span></label>
                                    <select class="form-control select2" id="status" name="status"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $status = config('constants.status.arr');
                                        //                                        dd($status);
                                        foreach ($status as $id => $name) { ?>
                                        <option value="{{ $id }}" {{ $id == $institutions->status ? 'selected="selected"' : '' }} > {{ $name }} </option>
                                        <?php }?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('status')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="card-header">
                        <h3 class="card-title">কেন্দ্রীয় পর্যায়ের অফিস</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{-- Ministry --}}
                                <div class="form-group">
                                    <label>মন্ত্রনালয় <span class="required">*</span></label>
                                    <select class="form-control select2" id="mins_id" name="mins_id"
                                            value="{{ old('mins_id') }}" required
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($dataArr['minsListArr'] as $minsId => $minsName)
                                            <option
                                                value="{{ $minsId }}" {{ $minsId == $institutions->mins_id ? 'selected="selected"' : '' }} >{{ $minsName }}</option>
                                        @endforeach
                                    </select>
                                    @error('mins_id')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                {{-- Bivag --}}
                                <div class="form-group">
                                    <label>মন্ত্রনালয় এর অধিনস্ত বিভাগ</label>
                                    <select class="form-control select2" id="divs_id" name="divs_id"
                                            value="{{ old('divs_id') }}"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($dataArr['bivagListArr'] as $bivagId => $bivagName)
                                            <option
                                                value="{{ $bivagId }}" {{ $bivagId == $institutions->divs_id ? 'selected="selected"' : '' }} >{{ $bivagName }}</option>
                                        @endforeach
                                    </select>
                                    @error('divs_id')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                {{-- Department --}}
                                <div class="form-group">
                                    <label>অধিদপ্তর<span class="required">*</span></label>
                                    <select class="form-control select2" id="dept" name="dept" value="{{ old('dept') }}"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($dataArr['deptListArr'] as $deptId => $deptName)
                                            <option
                                                value="{{ $deptId }}" {{ $deptId == $institutions->dept ? 'selected="selected"' : '' }} >{{ $deptName }}</option>
                                        @endforeach
                                    </select>
                                    @error('dept')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="card-header">
                        <h3 class="card-title">মাঠ পর্যায়ের অফিস</h3>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{-- Division --}}
                                <div class="form-group">
                                    <label>বিভাগ </label>
                                    <select class="form-control select2" id="div" name="div" value="{{ old('div') }}"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($dataArr['divListArr'] as $divId => $divName)
                                            <option
                                                value="{{ $divId }}" {{ $divId == $institutions->div ? 'selected="selected"' : '' }} >{{ $divName }}</option>
{{--                                            <option value="{{ $divId }}">{{ $divName }}</option>--}}
                                        @endforeach
                                    </select>
                                    @error('div')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- District --}}
                                <div class="form-group">
                                    <label>জেলা</label>
                                    <select class="form-control select2" id="dist" name="dist"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($dataArr['distListArr'] as $distId => $distName)
                                            <option
                                                value="{{ $distId }}" {{ $distId == $institutions->dist ? 'selected="selected"' : '' }} >{{ $distName }}</option>
                                        @endforeach
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- Sub-District --}}
                                <div class="form-group">
                                    <label>উপজেলা</label>
                                    <select class="form-control select2" id="sub_dist" name="sub_dist"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($dataArr['sub_distListArr'] as $sib_distId => $sib_distName)
                                            <option
                                                value="{{ $sib_distId }}" {{ $sib_distId == $institutions->sub_dist ? 'selected="selected"' : '' }} >{{ $sib_distName }}</option>
                                        @endforeach
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">সংরক্ষন</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            $('#mins_id').on('change', function () {
                var minsId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('bivag.bivagSelectAjaxList') }}";
                if (minsId) {
                    $('select[name="divs_id"]').empty();
                    $('select[name="dept"]').empty();
                    // $('select[name="union"]').empty();
                    bivagList(minsId, token, url);
                } else {
                    $('select[name="divs_id"]').empty();
                    $('select[name="dept"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            $('#divs_id').on('change', function () {
                var divsId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('department.deptSelectAjaxList') }}";
                if (divsId) {
                    // $('select[name="divs_id"]').empty();
                    $('select[name="dept"]').empty();
                    // $('select[name="union"]').empty();
                    deptList(divsId, token, url);
                } else {
                    // $('select[name="divs_id"]').empty();
                    $('select[name="dept"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    districtList(divId, token, url);
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    upazillaList(districtID, token, url);
                } else {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            function bivagList(minsId, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {minsId: minsId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if(data.status === 200){
                            $('select[name="divs_id"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $.each(data.dataId, function (key, value) {
                                $('select[name="divs_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        if(data.status === 201){
                            $('select[name="divs_id"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $('select[name="dept"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $.each(data.dataId, function (key, value) {
                                $('select[name="dept"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        if(data.status === 404){
                            $('select[name="divs_id"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $('select[name="dept"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $('select[name="dept"]').append('<option value="' + data.dataId + '">সংশ্লিষ্ট মন্ত্রণালয়/বিভাগ</option>');
                            // $.each(data.dataId, function (key, value) {
                            // });
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            }

            function deptList(divsId, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divsId: divsId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if(data.status === 200){
                            $('select[name="dept"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $.each(data.dataId, function (key, value) {
                                $('select[name="dept"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        if(data.status === 404){
                            $('select[name="dept"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $('select[name="dept"]').append('<option value="' + data.dataId + '">সংশ্লিষ্ট মন্ত্রণালয়/বিভাগ</option>');
                            // $.each(data.dataId, function (key, value) {
                            // });
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            }

            function districtList(divId, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

        });
    </script>
@endsection
