@extends('layouts.app')
@section('content-header')
    <h3>প্যাকেজ এসাইন</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> প্যাকেজ এসাইন তালিকা আপলোড </h1>
                        </div>
                        @can('package-assign')
                            <div class="card-body">

                                {{--                            @if (isset($errors) && $errors->any())--}}
                                {{--                                <div class="alert alert-danger">--}}
                                {{--                                    @foreach($errors->all() as $error)--}}
                                {{--                                        {{ $error }}--}}
                                {{--                                    @endforeach--}}
                                {{--                                </div>--}}
                                {{--                            @endif--}}
                                <form action="{{ route('packages.saveBulkAssignPackage') }}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="row col-12">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {{--                                                <label>প্যাকেজের নামঃ</label>--}}
                                                <select class="form-control select2" id="packageId" name="packageId"
                                                        style="width: 100%;" required>
                                                    <option value="">প্যাকেজ বাছাই করুন</option>
                                                    @foreach($packageList as $id => $val)
                                                        <option value="{{ $id }}">{{ $val }}</option>
                                                    @endforeach
                                                </select>
                                                @error('packageId')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {{--                                                <label>প্যাকেজের নামঃ</label>--}}
                                                <select class="form-control select2" id="dealerId" name="dealerId"
                                                        style="width: 100%;" required>
                                                    <option value="">ডিলার বাছাই করুন</option>
                                                    @foreach($dealerList as $key => $val)
                                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('packageId')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="file" name="file" class="custom-file-input"
                                                       id="customFile">
                                                <label class="custom-file-label" for="customFile">ফাইল বাছাই
                                                    করুন</label>
                                            </div>
                                            <div id="docFile"></div>
                                        </div>

                                        <div class="col-md-1">
                                            <div class="form-group" align="center">
                                                <button class="btn btn-primary"><i class="fa fa-file-upload"></i> আপলোড</button>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <a class="btn btn-success"
                                                   href="{{ asset('assign_pacakge_sample_data.xlsx') }}"><i class="fa fa-file-download"></i> সেম্পল ডাটা
                                                    ফাইল</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <hr/>

                                @if(session()->has("failures"))

                                    <table class="table table-danger">
                                        <tr>
                                            <th colspan="4">ভুল তালিকা</th>
                                        </tr>
                                        <tr>
                                            <th>রো নং</th>
                                            <th>কলাম</th>
                                            <th>এরর মেসেজ</th>
                                            <th>ডাটা</th>
                                        </tr>
                                        @foreach(session()->get('failures') as $error)
                                            <?php $errMsg = ""; ?>
                                            @foreach($error->errors() as $e)
                                                <?php $errMsg = $e; ?>
                                            @endforeach
                                            <tr>
                                                <td>{{ $error->row() }}</td>
                                                <td>{{ $error->attribute() }}</td>
                                                <td>
                                                    {{ $errMsg }}
                                                </td>
                                                <td>{{ $error->values()[$error->attribute()] }}</td>
                                            </tr>
                                        @endforeach

                                    </table>
                                @endif
                            </div>
                        @endcan
                    </div>

                {{--                    <div class="card">--}}

                <!-- /.modal -->
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#assign-package").on('click', function (e) {
                e.preventDefault();
                var familyId = $(this).attr('family_id');
                var packageId = $('#package').val();
                // var divId = $(this).attr('id');
                var token = $("input[name='_token']").val();
                var url = "{{ route('packages.assignPackage') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {familyId: familyId, packageId: packageId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        window.location.reload();

                        // $('select[name="package"]').empty();
                        // $('select[name="package"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $.each(data.packageList, function (key, value) {
                        //     $('select[name="package"]').append('<option value="' + key + '">' + value + '</option>');
                        // });
                        // $("#assign-package").attr('family_id', data.family_id);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                        window.location.reload();
                    }
                });
            })

            $("#modal-lg").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                // var divId = $(this).attr('id');
                var token = $("input[name='_token']").val();
                var url = "{{ route('packages.getPackageLists') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {id: id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);

                        $('select[name="package"]').empty();
                        $('select[name="package"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data.packageList, function (key, value) {
                            $('select[name="package"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                        $("#assign-package").attr('family_id', data.family_id);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });

                {{--$.get( "/controller/" + id, function( data ) {--}}
                {{--    $(".modal-body").html(data.html);--}}
                {{--});--}}

            });

            $(document).on("click", "a.delete-family", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('families.index') }}",
                    columns: [
                        {data: 'card_no', name: 'card_no'},
                        {data: 'name', name: 'name'},
                        {data: 'f_h_name', name: 'f_h_name'},
                        {data: 'phone', name: 'phone'},
                        {data: 'nid', name: 'nid'},
                        {data: 'address', name: 'address'},
                        {data: 'dob', name: 'dob'},
                        {data: 'gender', name: 'gender'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    districtList(divId, token, url, 'pre');
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    upazillaList(districtID, token, url, 'pre');
                } else {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#sub_dist').on('change', function () {
                var upazilaID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('union.unionSelectAjaxList') }}";
                if (upazilaID) {
                    $('select[name="union"]').empty();
                    unionList(upazilaID, token, url, 'pre');
                } else {
                    $('select[name="union"]').empty();
                }
            });

            function districtList(divId, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function unionList(upazilaID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {upazilaID: upazilaID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $('select[name="union"]').append('<option value="' + -1 + '">' + "সিটি কর্পোরেশন" + '</option>');
                        // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            $('#customFile').on('change', function () {
                var input = $('#customFile').val();
                $('#docFile').text(input);
            });

        });
    </script>
@endsection
