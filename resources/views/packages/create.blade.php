@extends('layouts.app')
@section('content-header')
    <h3>প্যাকেজ ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="" method="post" action="{{ route('packages.store') }}">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('package-list')
                                <a class="btn btn-primary" href="{{ route('packages.index') }}"> প্যাকেজ
                                    তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ত্রানের ধরনের <span class="required">*</span></label>
                                    <select class="form-control select2" id="religion" name="aid_id" style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        foreach ($aidType as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('aid_id')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>প্যাকেজের নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="প্যাকেজের নাম" required>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-12">
                                <div class="form-group">
                                    <label>জেলা অনুমোদন <span class="required">*</span></label>
                                    <select class="form-control duallistbox"  multiple="multiple" id="assign_dist" name="assign_dist[]" >
                                        <?php
                                        foreach ($distList as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">সংরক্ষন</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            //Bootstrap Duallistbox
            $('.duallistbox').bootstrapDualListbox();

        });
    </script>
@endsection
