@extends('layouts.app')
@section('content-header')
    <h3>প্যাকেজ ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="form-horizontal" method="POST" action="{{ route('packages.update', $package->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('package-list')
                                <a class="btn btn-primary" href="{{ route('packages.index') }}"> প্যাকেজের তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ত্রানের ধরনের <span class="required">*</span></label>
                                    <select class="form-control select2" id="status" name="aid_id" style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        foreach ($aidType as $id => $name) { ?>
                                        <option
                                            value="{{ $id }}" {{ $id == $package->aid_id ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        <!--                                            --><?php //echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('aid_id')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>অবস্থা *</label>
                                    <select class="form-control select2" id="status" name="status" style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $status = config('constants.status.arr');
                                        foreach ($status as $key => $val) { ?>
                                        <option
                                            value="{{ $key }}" {{ $key == $package->status ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                        <!--                                            --><?php //echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('status')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>প্যাকেজের নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="প্যাকেজের নাম" value="{{ $package->name }}" required>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-12">
                                <div class="form-group">
                                    <label>জেলা অনুমোদন <span class="required">*</span></label>
                                    <select class="form-control duallistbox" multiple="multiple" id="assign_dist"
                                            name="assign_dist[]">
                                        <?php
                                        foreach ($distList as $id => $name) { ?>
                                        <option
                                            value="{{ $id }}" {{ in_array($id,json_decode($package->assign_dist, true)) ? 'selected="selected"' : '' }} > {{ $name }} </option>
<!--                                        --><?php //echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">সংরক্ষন</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
            //Bootstrap Duallistbox
            $('.duallistbox').bootstrapDualListbox();

        });
    </script>
@endsection
