@extends('layouts.app')
@section('content-header')
    <h3>প্যাকেজ ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12  card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"> {{ $page_title }} </h3>
                            <div class="float-sm-right">
                                @can('package-create')
                                    <a class="btn btn-primary" href="{{ route('packages.create') }}"> নতুন প্যাকেজ
                                        তৈরি</a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th>ক্রমিক নং</th>
                                        <th>ত্রানের ধরন</th>
                                        <th>প্যাকেজের নাম</th>
                                        <th>লটের তালিকা <br/>
                                            <span class="btn-sm button-lot-waiting">অপেক্ষমাণ</span>
                                            <span class="btn-sm button-lot-running">চলমান</span>
                                            <span class="btn-sm button-lot-finished">সম্পন্ন</span>
                                        </th>
                                        <th>স্ট্যাটাস</th>
                                        <th width="280px">ব্যবস্থা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>

                        {{--    Item Assign Modal    --}}
                        <div class="modal fade" id="modal-lg">
                            <form method="post" id="itemsForm">
                                {{--                                @csrf--}}
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">পণ্য যুক্ত করুন</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row col-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>পণ্যের তালিকা</label>
                                                        <span id="itemsTbl"></span>
                                                        {{--                                                    <select class="form-control select2" id="package" name="package"--}}
                                                        {{--                                                            style="width: 100%;">--}}
                                                        {{--                                                    </select>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="package_id" name="package_id">
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">বন্ধ করুন
                                            </button>
                                            <button type="submit" package_id="" id="add_items" class="btn btn-primary">
                                                পন্য যুক্ত করুন
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </form>
                            <!-- /.modal-dialog -->
                        </div>


                        {{--    Dealer Assign Modal    --}}
                        <div class="modal fade" id="modal-lg-dealer">
                            <form method="post" id="dealersForm">
                                {{--                                @csrf--}}
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">ডিলার এসাইন করুন</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row col-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>ডিলারের তালিকা</label>
                                                        <span id="delersTbl"></span>
                                                        {{--                                                    <select class="form-control select2" id="package" name="package"--}}
                                                        {{--                                                            style="width: 100%;">--}}
                                                        {{--                                                    </select>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="package_id_dealer" name="package_id_dealer">
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">বন্ধ করুন
                                            </button>
                                            <button type="submit" package_id_dealer="" id="add_dealer" class="btn btn-primary">
                                                ডিলার যুক্ত করুন
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </form>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#itemsForm").submit(function (e) {
                e.preventDefault();

                /*  Checked Data fetch and Make a ',' separated list  */
                var formData = [];
                $.each($("input[type=checkbox]:checked"), function () {
                    formData.push($(this).val());
                });
                var intValList = formData.join(",");
                console.log(intValList);

                var packageId = $("#package_id").val();
                var itemList = intValList;
                var token = $("input[name='_token']").val();
                var url = "{{ route('packages.saveItem') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {packageId: packageId, itemList: itemList, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        window.location.reload();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                        window.location.reload();
                    }
                });


            });

            $("#dealersForm").submit(function (e) {
                e.preventDefault();

                /*  Checked Data fetch and Make a ',' separated list  */
                var formData = [];
                $.each($("input[type=checkbox]:checked"), function () {
                    formData.push($(this).val());
                });
                var intDealerValList = formData.join(",");
                console.log(intDealerValList);

                var packageId = $("#package_id").val();
                var dealersList = intDealerValList;
                var token = $("input[name='_token']").val();
                var url = "{{ route('dealers.saveDealer') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {packageId: packageId, dealersList: dealersList, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        window.location.reload();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                        window.location.reload();
                    }
                });


            });

            $("#modal-lg").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                var token = $("input[name='_token']").val();
                var url = "{{ route('item-types.getItemLists') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {id: id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.itemList);
                        var table = '<table id="example1" class="table table-bordered table-striped data-table">';

                        $.each(data.itemTypeLists, function (key, value) {
                            table += '<thead style="background: #a1e7f5"><tr>';
                            table += '<th colspan="4">' + value.name + '</th>';
                            table += '</thead>';
                            var length = value.items.length;
                            var row = Math.floor(length / 4);
                            var col = (length % 4);

                            /*  Row print   */
                            table += '<tbody>';
                            if (row >= 1) {
                                for (var i = 0; i < row; i++) {
                                    var chk1 = "";
                                    var chk2 = "";
                                    var chk3 = "";
                                    var chk4 = "";
                                    if ($.inArray(value.items[(i * 4)].id, data.itemList) !== -1) {
                                        chk1 = "checked";
                                    }
                                    if ($.inArray(value.items[(i * 4) + 1].id, data.itemList) !== -1) {
                                        chk2 = "checked";
                                    }
                                    if ($.inArray(value.items[(i * 4) + 2].id, data.itemList) !== -1) {
                                        chk3 = "checked";
                                    }
                                    if ($.inArray(value.items[(i * 4) + 3].id, data.itemList) !== -1) {
                                        chk4 = "checked";
                                    }

                                    table += '<tr>';
                                    table += '<td><input ' + chk1 + ' type="checkbox" id="id_' + value.items[(i * 4)].id + '" name="items" value="' + value.items[(i * 4)].id + '" >&nbsp;&nbsp;&nbsp; ' + value.items[(i * 4)].name + '</td>';

                                    table += '<td><input ' + chk2 + ' type="checkbox" id="id_' + value.items[(i * 4) + 1].id + '" name="items" value="' + value.items[(i * 4) + 1].id + '">&nbsp;&nbsp;&nbsp; ' + value.items[(i * 4) + 1].name + '</td>';

                                    table += '<td><input ' + chk3 + ' type="checkbox" id="id_' + value.items[(i * 4) + 2].id + '" name="items" value="' + parseInt(value.items[(i * 4) + 2].id) + '">&nbsp;&nbsp;&nbsp; ' + value.items[(i * 4) + 2].name + '</td>';

                                    table += '<td><input ' + chk4 + ' type="checkbox" id="id_' + value.items[(i * 4) + 3].id + '" name="items" value="' + parseInt(value.items[(i * 4) + 3].id) + '">&nbsp;&nbsp;&nbsp; ' + value.items[(i * 4) + 3].name + '</td>';

                                    i = i + 4;
                                }
                            }

                            /*  Column Print    */
                            if (col >= 1) {
                                var chk5 = "";
                                var chk6 = "";
                                var chk7 = "";

                                if ($.inArray(value.items[length - col].id, data.itemList) !== -1) {
                                    chk5 = "checked";
                                }
                                table += '<tr>';
                                table += '<td><input ' + chk5 + ' type="checkbox" id="' + value.items[length - col].id + '" name="items" value="' + parseInt(value.items[length - col].id) + '">&nbsp;&nbsp;&nbsp; ' + value.items[length - col].name + '</td>';

                                col = col - 1;
                                if (col >= 1) {
                                    if ($.inArray(value.items[length - col].id, data.itemList) !== -1) {
                                        chk6 = "checked";
                                    }
                                    table += '<td><input ' + chk6 + ' type="checkbox" id="' + value.items[length - col].id + '" name="items" value="' + parseInt(value.items[length - col].id) + '">&nbsp;&nbsp;&nbsp; ' + value.items[length - col].name + '</td>';
                                }

                                col = col - 1;
                                if (col >= 1) {
                                    if ($.inArray(value.items[length - col].id, data.itemList) !== -1) {
                                        chk7 = "checked";
                                    }
                                    table += '<td><input ' + chk7 + ' type="checkbox" id="' + value.items[length - col].id + '" name="items" value="' + parseInt(value.items[length - col].id) + '">&nbsp;&nbsp;&nbsp; ' + value.items[length - col].name + '</td>';
                                }
                                table += '</tr>';
                            }
                            table += '</tbody>';
                        });
                        table += '</table>';

                        $("#itemsTbl").empty();
                        $('#itemsTbl').append(table);

                        $("#package_id").val(data.packageId);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            });

            $("#modal-lg-dealer").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                var token = $("input[name='_token']").val();
                var url = "{{ route('dealers.getDealerLists') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {id: id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data.dealerLists);
                        console.log(data);
                        // console.log(data.itemList);
                        var table = '<table id="dealer_assign_list" class="table table-bordered table-striped data-table">';
                        table += '<thead style="background: #a1e7f5">' +
                            '<tr>' +
                            '<th> বাছাই করুন</th>' +
                            '<th> আইডি</th>' +
                            '<th> নাম</th>' +
                            '<th> প্রতিষ্ঠানের নাম</th>' +
                            '<th> ট্রেড লাইসেন্স</th>' +
                            '<th> TIN</th>' +
                            '<th> ঠিকানা</th>' +
                            '</tr>' +
                            '</thead>';

                        table += '<tbody>';
                        $.each(data.dealerLists, function (key, value) {
                            var chk1 = "";
                            if ($.inArray(value.id, data.assignDealerListAee) !== -1) {
                                chk1 = "checked";
                            }
                            console.log(value);
                            console.log(value.id);
                            console.log(data.assignDealerListAee);
                            table += '<tr>';
                            table += '<td><input ' + chk1 + ' type="checkbox" id="id_' + value.id + '" name="dealers" value="' + value.id + '" ></td>';
                            table += '<td>' + value.id + '</td>' +
                                '<td>' + value.name + '</td>' +
                                '<td>' + value.inst_name + '</td>' +
                                '<td>' + value.trd_license + '</td>' +
                                '<td>' + value.tin + '</td>' +
                                '<td>' + data.subDistList[value.sub_dist] +', '+ data.distList[value.dist]  + '</td>' +
                                '</tr>';
                        });
                        table += '</tbody> </table>';

                        $("#delersTbl").empty();
                        $('#delersTbl').append(table);

                        $("#package_id").val();
                        $("#package_id").val(data.packageId);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            });

            $(document).on("click", "a.delete-package", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                                window.location.reload();
                                // data.request->session()->flash('status', 'Task was successful!');
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                            window.location.reload();
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('packages.index') }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'aid_id', name: 'aid_id'},
                        {data: 'name', name: 'name'},
                        {data: 'lot', name: 'lot'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

        });
    </script>
@endsection
