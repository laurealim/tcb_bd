@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12  card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"> {{ $page_title }} </h3>
                            <div class="float-sm-right">
                                @can('dealer-create')
                                    <a class="btn btn-primary" href="{{ route('dealers.create') }}"> নতুন ডিলার তৈরি</a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-sm table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th width="2%">ক্রমিক নং</th>
                                        <th width="10%">নাম</th>
                                        <th width="8%">ইমেইল</th>
                                        <th width="7%">মোবাইল</th>
                                        <th width="10%">প্রতিষ্ঠানের নাম</th>
                                        <th width="15%">প্রতিষ্ঠানের ঠিকানা</th>
                                        <th width="10%">ট্রেড লাইসেন্স</th>
                                        <th width="10%">আয়কর সনদ (TIN) নং</th>
                                        <th width="15%">অধিক্ষেত্র</th>
                                        <th width="6%">রোল</th>
                                        <th width="7%">ব্যবস্থা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", "a.delete-dealer", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি ডিলারের তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
                   // data.request->session()->flash('status', 'Task was successful!');
                   // setInterval(function() {
                   // }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(document).on("click", "a.change-pass", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি ডিলারের পাসওয়ার্ড রিসেট করতে চান?")) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            console.log(data);
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
                            // data.request->session()->flash('status', 'Task was successful!');
                            // setInterval(function() {
                            // }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('dealers.index') }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'phone', name: 'phone'},
                        {data: 'inst_name', name: 'inst_name'},
                        {data: 'inst_address', name: 'inst_address'},
                        {data: 'trd_license', name: 'trd_license'},
                        {data: 'tin', name: 'tin'},
                        {data: 'location', name: 'location'},
                        {data: 'roles', name: 'roles'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

        });
    </script>
@endsection


