@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="form-horizontal" method="POST" action="{{ route('dealers.update', $dealer->id) }}" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    <div class="card-header">
                        <h2 class="card-title text-bold text-olive">{{ $page_title }}</h2>

                        <div class="card-tools">
                            @can('dealer-edit')
                                <a class="btn btn-primary" href="{{ route('dealers.index') }}"> ডিলারের তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পুর্ণ নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           value="{{ $dealer->name }}"
                                           placeholder="পুর্ণ নাম" required>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                    {{--                                    @error('name')--}}
                                    {{--                                    <span class="invalid-feedback" role="alert">--}}
                                    {{--                                            <strong>{{ $message }}</strong>--}}
                                    {{--                                        </span>--}}
                                    {{--                                    @enderror--}}
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ইমেইল <span class="required">*</span></label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           value="{{ $dealer->email }}"
                                           placeholder="ইমেইল" required>
                                    @if ($errors->has('email'))
                                        <span class="text-danger"><strong>{{ $errors->first('email') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label> লিঙ্গ <span class="required">*</span></label>
                                    <select class="form-control select2" id="gender" name="gender"
                                            value="{{ old('gender') }}"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $gender_list = config('constants.gender.arr');
                                        foreach ($gender_list as $id => $name) { ?>
                                        <option value="{{ $id }}" {{ $id == $dealer->gender ? 'selected="selected"' : '' }} > {{ $name }} </option>
<!--                                            --><?php //echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span
                                            class="text-danger"><strong>{{ $errors->first('gender') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>জাতীয় পরিচয়পত্র <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="nid" id="nid" value="{{ $dealer->nid }}"
                                           placeholder="জাতীয় পরিচয়পত্র" required>
                                    @if ($errors->has('nid'))
                                        <span class="text-danger"><strong>{{ $errors->first('nid') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পিতা/স্বামী/স্ত্রীর নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="g_name" id="g_name"
                                           value="{{ $dealer->g_name }}"
                                           placeholder="পিতা/স্বামী/স্ত্রীর নাম" required>
                                    @if ($errors->has('g_name'))
                                        <span
                                            class="text-danger"><strong>{{ $errors->first('g_name') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>মোবাইল <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="phone" id="phone"
                                           value="{{ $dealer->phone }}"
                                           placeholder="01xxxxxxxxx" required>
                                    @if ($errors->has('phone'))
                                        <span class="text-danger"><strong>{{ $errors->first('phone') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>জন্ম তারিখ <span class="required">*</span></label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="text" class="form-control" data-inputmask-alias="datetime" id="dob"
                                               name="dob" value="{{ $dealer->dob }}"
                                               data-inputmask-inputformat="yyyy-mm-dd" data-mask/>

                                        @if ($errors->has('dob'))
                                            <span class="text-danger"><strong>{{ $errors->first('dob') }}</strong> </span>
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>স্থায়ী ঠিকানা <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="address" id="address"
                                           value="{{ $dealer->address }}"
                                           placeholder="স্থায়ী ঠিকানা" required>
                                    @if ($errors->has('address'))
                                        <span
                                            class="text-danger"><strong>{{ $errors->first('address') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-header">
                        <h2 class="card-title text-bold text-olive">ডিলার তথ্য</h2>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>প্রতিষ্ঠানের নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="inst_name" id="inst_name"
                                           value="{{ $dealer->inst_name }}"
                                           placeholder="প্রতিষ্ঠানের নাম" required>
                                    @if ($errors->has('inst_name'))
                                        <span
                                            class="text-danger"><strong>{{ $errors->first('inst_name') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ট্রেড লাইসেন্স <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="trd_license" id="trd_license"
                                           value="{{ $dealer->trd_license }}"
                                           placeholder="ট্রেড লাইসেন্স" required>
                                    @if ($errors->has('trd_license'))
                                        <span class="text-danger"><strong>{{ $errors->first('trd_license') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ছবি আপলোড করুন </br>
                                        <span class="required small"> Max Size 300 KB</span>
                                    </label>
                                    <div class="custom-file text-left">
                                        <input type="file" name="picture" class="custom-file-input" id="picture">
                                        <label class="custom-file-label" for="picture">ছবি বাছাই করুন</label>
                                    </div>

                                    <div id="picFile"></div>
                                    <?php
//                                    $url = public_path('images/dealers/'.$dealer->id.'/'.$dealer->id.'.png');
                                    $picture = asset("images/dealers/".$dealer->id.'/'.$dealer->id.'.png');
                                    ?>
                                    <br/>
                                    <div class="image">
                                        <img src="{{ $picture }}" class="img-circles elevation-2" width="80" height="100" alt="User Image">
                                    </div>


                                    @if ($errors->has('picture'))
                                        <span
                                            class="text-danger"><strong>{{ $errors->first('picture') }}</strong> </span>
                                    @endif
                                    {{--                                    @error('picture')--}}
                                    {{--                                    <span class="invalid-feedback" role="alert">--}}
                                    {{--                                            <strong>{{ $message }}</strong>--}}
                                    {{--                                        </span>--}}
                                    {{--                                    @enderror--}}
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>প্রতিষ্ঠানের ঠিকানা <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="inst_address" id="inst_address"
                                           value="{{ $dealer->inst_address }}"
                                           placeholder="প্রতিষ্ঠানের ঠিকানা" required>
                                    @if ($errors->has('inst_address'))
                                        <span class="text-danger"><strong>{{ $errors->first('inst_address') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>আয়কর সনদ (TIN) নং <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="tin" id="tin" value="{{ $dealer->tin }}"
                                           placeholder="আয়কর সনদ (TIN) নং" required>
                                    @if ($errors->has('tin'))
                                        <span class="text-danger"><strong>{{ $errors->first('tin') }}</strong> </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>দলিলপত্র</br>
                                        <span class="required small"> (ট্রেড লাইসেন্স, ব্যাংক সল্ভেন্সি সার্টিফিকেট, NID, TIN, ইত্যাদি একত্রে একটি PDF ফাইলে আপলোড করুন ) Max Size 5 MB</span>
                                    </label>
                                    <div class="custom-file text-left">
                                        <input type="file" name="docs" class="custom-file-input" id="docs">
                                        <label class="custom-file-label" for="picture">দলিলপত্র বাছাই করুন</label>
                                    </div>
                                    <div id="docFile"></div>
                                    <?php
                                    //                                    $url = public_path('images/dealers/'.$dealer->id.'/'.$dealer->id.'.png');
                                    $doc = asset("images/dealers/".$dealer->id.'/'.$dealer->id.'.pdf');
                                    ?>
                                    <br/>
{{--                                    <div class="image">--}}
                                        <a href="{{ $doc }}">Download Previous Doc.</a>
{{--                                        <img src="{{ $picture }}" class="img-circles elevation-2" width="80" height="100" alt="User Image">--}}
{{--                                    </div>--}}
                                    @if ($errors->has('docs'))
                                        <span class="text-danger"><strong>{{ $errors->first('docs') }}</strong></span>
                                    @endif
                                    @error('docs')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>

                    <div class="card-header">
                        <h2 class="card-title text-bold text-olive">অধিক্ষেত্র</h2>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{-- Division --}}
                                <div class="form-group">
                                    <label>বিভাগ </label>
                                    <select class="form-control select2" id="div" name="div" value="{{ old('div') }}"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($divListArr as $divId => $divName)
                                            <option value="{{ $divId }}" {{ $divId == $dealer->div ? 'selected="selected"' : '' }} > {{ $divName }} </option>
{{--                                            <option value="{{ $divId }}">{{ $divName }}</option>--}}
                                        @endforeach
                                    </select>
                                    @if ($errors->has('div'))
                                        <span class="text-danger"><strong>{{ $errors->first('div') }}</strong> </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- District --}}
                                <div class="form-group">
                                    <label>জেলা</label>
                                    <select class="form-control select2" id="dist" name="dist"
                                            style="width: 100%;">
                                        @foreach($distListArr as $id => $name)
                                            <option
                                                value="{{ $id }}" {{ $id == $dealer->dist ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('dist'))
                                        <span class="text-danger"><strong>{{ $errors->first('dist') }}</strong> </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- Sub-District --}}
                                <div class="form-group">
                                    <label>উপজেলা</label>
                                    <select class="form-control select2" id="sub_dist" name="sub_dist"
                                            style="width: 100%;">
                                        @foreach($sub_distListArr as $id => $name)
                                            <option
                                                value="{{ $id }}" {{ $id == $dealer->sub_dist ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('sub_dist'))
                                        <span
                                            class="text-danger"><strong>{{ $errors->first('sub_dist') }}</strong> </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">আপডেট</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    districtList(divId, token, url);
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    upazillaList(districtID, token, url);
                } else {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            function districtList(divId, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            $('#picture').on('change', function () {
                var input = $('#picture').val();
                $('#picFile').text(input);
            });

            $('#docs').on('change', function () {
                var input = $('#docs').val();
                $('#docFile').text(input);
            });

        });
    </script>
@endsection
