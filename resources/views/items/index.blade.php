@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12  card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"> {{ $page_title }} </h3>
                            <div class="float-sm-right">
                                @can('item-create')
                                    <a class="btn btn-primary" href="{{ route('items.create') }}"> নতুন পণ্য
                                        তৈরী</a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
{{--                            <div>--}}
{{--                                @foreach ($nesteds as $nested)--}}
{{--                                    <x-nested-item :nestedItems="$nested" />--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th>ক্রমিক নং</th>
                                        <th>পণ্যের ধরণ</th>
                                        <th>পণ্যের নাম</th>
                                        <th>ইউনিট</th>
                                        <th>অবস্থা</th>
                                        <th width="280px">ব্যবস্থা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", "a.delete-item", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                                window.location.reload();
                                // data.request->session()->flash('status', 'Task was successful!');
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                            window.location.reload();
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('items.index') }}",
                    columns: [
                        {data: "id", name: "id"},
                        {data: 'type_id', name: 'type_id'},
                        {data: 'name', name: 'name'},
                        {data: 'unit', name: 'unit'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

        });
    </script>
@endsection
