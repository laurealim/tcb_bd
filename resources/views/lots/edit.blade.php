@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="" method="post" action="{{ route('lot-item-amount.saveLot',$lotInfo->id) }}">
                    <input type="hidden" name="_method" value="POST">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>
                        <?php
                        $disabled = '';
                        $readonly = '';
                        if ($state != 2) {
                            $readonly = 'readonly = "readonly"';
                            $disabled = 'disabled = "disabled"';
                        }
                        ?>

                        <div class="card-tools">
                            @can('package-list')
                                <a class="btn btn-primary" href="{{ route('packages.index') }}"> প্যাকেজ
                                    তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div class="text-center">
                                    <h3>প্যাকেজের নামঃ- {{ $packageInfo->name }}</h3>
                                    <h5>লট নংঃ- {{ en2bn($lotInfo->lot_no) }} </h5>
                                    <input type="hidden" name="lot_no" value="{{ $lotInfo->lot_no }}">
                                    <input type="hidden" name="package_id" value="{{ $lotInfo->package_id }}">
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card card-cyan">
                        <div class="card-header">
                            <h3 class="card-title">লটের তথ্য</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>লটের পণ্যের পরিমাণ</label>
                                        <div class="table-responsive">
                                            <table id="item_amount"
                                                   class="table table-bordered table-striped data-table">
                                                <thead class="text-center">
                                                <tr>
                                                    <th>#</th>
                                                    <th>পণ্যের নাম</th>
                                                    <th>পরিমাণ(জনপ্রতি)</th>
                                                    <th>ইউনিট</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($packageItemsArr as $item){
                                                $u_amount = 0;
                                                $checked = '';
                                                if (isset($item_amount_arr[$item])) {
                                                    $u_amount = $item_amount_arr[$item];
                                                    $checked = 'checked="checked"';
                                                }
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" {{ $checked }} id="{{ $item }}"
                                                               name="item[{{ $item }}]"/>
                                                    </td>
                                                    <td>{{ $itemInfo[$item]->name }}</td>
                                                    <td>

                                                        <input type="number" class="form-control unit_amount"
                                                               name="amount[{{ $item }}]" id="amount_{{ $item }}"
                                                               value="{{ $u_amount }}" min="0" {{ $readonly }}/></td>
                                                    <td>
                                                        <?php
                                                        $unitArr = config('constants.unit.arr'); ?>
                                                        {{ $unitArr[$itemInfo[$item]->unit] }}
                                                    </td>
                                                </tr>
                                                <?php }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>হিসাব <span class="required">*</span></label>
                                        <select class="form-control select2" id="constants" name="constants"
                                                style="width: 100%;" {{ $disabled }}>
                                            <?php
                                            $counting = config('constants.counting.arr');
                                            foreach ($counting as $key => $value) { ?>
                                            {{--                                            <option--}}
                                            {{--                                                value="{{ $key }}" {{ $key == config('constants.state.Waiting') ? 'selected="selected"' : '' }} >{{ $value }}</option>--}}
                                            <?php echo "<option value='{$key}'>{$value}</option>"; ?>
                                            <?php }?>
                                        </select>
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        @error('constants')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                    </div>
                                    <div class="form-group">
                                        <label>মোট সুবিধাভোগীর সংখ্যা <span class="required">*</span></label>
                                        <input type="number" min="0" class="form-control" name="total_people"
                                               id="total_people" value="{{ $lotInfo->total_people }}"
                                               placeholder="মোট সুবিধাভোগীর সংখ্যা" {{ $readonly }} required>
                                        @error('total_people')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>লটের মোট পণ্যের পরিমাণ</label>
                                        <div class="table-responsive">
                                            <table id="total_amount"
                                                   class="table table-bordered table-striped data-table">
                                                <thead class="text-center">
                                                <tr>
                                                    <th>পণ্যের নাম</th>
                                                    <th>মোট পরিমাণ</th>
                                                    <th>ইউনিট</th>
                                                </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                <?php
                                                foreach ($packageItemsArr as $item){ ?>
                                                <tr>
                                                    <td>{{ $itemInfo[$item]->name }}</td>
                                                    <td><input type="number" class="form-control total_res_amount"
                                                               id="total_amount_{{ $item }}" readonly/></td>
                                                    <td>
                                                        <?php
                                                        $unitArr = config('constants.unit.arr'); ?>
                                                        {{ $unitArr[$itemInfo[$item]->unit] }}
                                                    </td>
                                                </tr>
                                                <?php }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>বর্তমান পর্যায় <span class="required">*</span></label>
                                        <select class="form-control select2" id="state" name="state"
                                                style="width: 100%;">
                                            <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            $state = config('constants.state.arr');
                                            foreach ($state as $key => $value) {
                                                if($key <= $lotInfo->state){ ?>
                                            <option
                                                value="{{ $key }}" {{ $key == $lotInfo->state ? 'selected="selected"' : '' }} >{{ $value }}</option>
                                                <?php }
                                                ?>
                                            <?php }?>
                                        </select>
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        @error('state')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>বিতরণ শুরু <span class="required">*</span></label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <?php
                                            if(!empty($lotInfo->id)){ ?>
                                            <input type="text" value="{{ $lotInfo->start_date }}" class="form-control"
                                                   {{ $disabled }} id="end_date" name="end_date"
                                            />
                                            <?php } else{
                                            ?>
                                            <input type="text" class="form-control"
                                                   {{ $disabled }} data-inputmask-alias="datetime"
                                                   id="start_date" name="start_date" value="{{ $lotInfo->start_date }}"
                                                   data-inputmask-inputformat="yyyy-mm-dd" data-mask/>
                                            <?php }?>

                                            @error('start_date')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>স্ট্যাটাস <span class="required">*</span></label>
                                        <select class="form-control select2" id="status" name="status"
                                                style="width: 100%;">
                                            <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            $status = config('constants.status.arr');
                                            foreach ($status as $id => $name) { ?>
                                            <option
                                                value="{{ $id }}" {{ $id == $lotInfo->status ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                            <!--                                            --><?php //echo "<option value='{$id}'>{$name}</option>"; ?>
                                            <?php }?>
                                        </select>
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        @error('status')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>বিতরণ শেষ <span class="required">*</span></label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <?php
                                            if(!empty($lotInfo->id)){
                                            ?>
                                            <input type="text" value="{{ $lotInfo->end_date }}" class="form-control"
                                                   {{ $disabled }} id="end_date" name="end_date"/>
                                            <?php } else{
                                            echo "else";
                                            ?>
                                            <input type="text" class="form-control"
                                                   {{ $disabled }} data-inputmask-alias="datetime"
                                                   id="end_date" name="end_date" value="{{ $lotInfo->end_date }}"
                                                   data-inputmask-inputformat="yyyy-mm-dd" data-mask/>
                                        <?php }?>

                                            @error('end_date')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">সংরক্ষন</button>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy-mm-dd', {'placeholder': 'yyyy-mm-dd'});
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('yyyy-mm-dd', {'placeholder': 'yyyy-mm-dd'});
            //Money Euro
            $('[data-mask]').inputmask();
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            checkInitTotalAmount();

        });

        $('.unit_amount').on('change', function () {
            var constantsValue = parseInt($("#constants").val());
            if (constantsValue === 1) {
                updateTotalAmount($(this)); //Done
            }
            if (constantsValue === 3) {
                updateTotalBeneficiary($(this)); //Done
            }
        });

        $('#total_people').on('change', function () {
            var constantsValue = parseInt($("#constants").val());
            if (constantsValue === 1) {
                updateTotalAmount($(this), true); //Done
            }
            if (constantsValue === 2) {
                updateUnitAmount($(this), true); //Done
            }
        });

        $('.total_res_amount').on('change', function () {
            var constantsValue = parseInt($("#constants").val());
            if (constantsValue === 2) {
                updateUnitAmount($(this)); //Done
            }
            if (constantsValue === 3) {
                updateTotalBeneficiary($(this), true); //Done
            }
        });

        $('#constants').on('change', function () {
            var changeValue = parseInt($(this).val());
            if (changeValue === 1) {
                $(".total_res_amount").attr('readonly', 'readonly');
                $(".unit_amount").removeAttr('readonly');
                $("#total_people").removeAttr('readonly');
            } else if (changeValue === 2) {
                $(".total_res_amount").removeAttr('readonly');
                $(".unit_amount").attr('readonly', 'readonly');
                $("#total_people").removeAttr('readonly');
            } else if (changeValue === 3) {
                $(".total_res_amount").removeAttr('readonly');
                $(".unit_amount").removeAttr('readonly');
                $("#total_people").attr('readonly', 'readonly');
            }
        });

        function checkInitTotalAmount() {
            // $('input[type="number"]').each(function () {
            $('.unit_amount').each(function () {
                updateTotalAmount($(this));
            });
        }

        function checkUnitAmount() {
            // $('input[type="number"]').each(function () {
            $('.total_res_amount').each(function () {
                updateUnitAmount($(this));
            });
        }

        function updateTotalAmount(thisObj, eachVal = false) {
            if (eachVal) {
                $('.unit_amount').each(function () {
                    var amount = parseFloat($(this).val());
                    var idName = $(this).attr('id');
                    var totalIdName = '#total_' + idName;
                    var totalPeople = parseInt(thisObj.val());

                    var newVal = amount * totalPeople;
                    $(totalIdName).val(newVal);
                });
            } else {
                var amount = parseFloat(thisObj.val());
                var idName = thisObj.attr('id');
                var totalIdName = '#total_' + idName;
                var totalPeople = parseInt($('#total_people').val());

                var newVal = amount * totalPeople;
                $(totalIdName).val(newVal);
                console.log(newVal);
            }
        }

        function updateUnitAmount(thisObj, eachVal = false) {
            if (eachVal) {
                $('.total_res_amount').each(function () {
                    var amount = parseFloat($(this).val());
                    var idName = $(this).attr('id').split("_", 3);
                    var unitIdName = '#' + idName[1] + "_" + idName[2];
                    var totalPeople = parseInt(thisObj.val());
                    var newVal = parseFloat(amount / totalPeople).toFixed(2);

                    // alert('amount = '+ amount +', idName = '+idName+', unitIdName = '+unitIdName+', newVal = '+newVal);
                    $(unitIdName).val(newVal);
                });
            } else {
                var amount = parseInt(thisObj.val());
                var idName = thisObj.attr('id').split("_", 3);
                var unitIdName = '#' + idName[1] + "_" + idName[2];
                var totalPeople = parseInt($('#total_people').val());

                var newVal = parseFloat(amount / totalPeople).toFixed(2);
                $(unitIdName).val(newVal);
            }
        }

        function updateTotalBeneficiary(thisObj, eachVal = false) {
            var unit_sum = 0;
            var total_sum = 0;
            var min_benf = 999999999;
            if (eachVal) {
                $('.unit_amount').each(function () {
                    var amount = parseFloat($(this).val());
                    var idName = $(this).attr('id');
                    var totalIdName = '#total_' + idName;
                    var totalAmount = parseFloat($(totalIdName).val());

                    min_benf = calculateBeneficiary(amount, totalAmount, min_benf);
                    console.log('amount = ' + amount + 'totalAmount = ' + totalAmount + 'unit_sum = ' + unit_sum + 'total_sum = ' + total_sum + 'min_benf = ' + min_benf);
                });
            } else {
                $('.total_res_amount').each(function () {
                    var totalAmount = parseFloat($(this).val()); // Total Each Amount
                    var idName = $(this).attr('id').split("_", 3);
                    var unitIdName = '#' + idName[1] + "_" + idName[2];
                    var amount = parseInt($(unitIdName).val());

                    min_benf = calculateBeneficiary(amount, totalAmount, min_benf);
                    console.log('amount = ' + amount + 'totalAmount = ' + totalAmount + 'unit_sum = ' + unit_sum + 'total_sum = ' + total_sum + 'min_benf = ' + min_benf);
                });
            }
            $('#total_people').val(min_benf);

        }

        function calculateBeneficiary(unit_sum, total_sum, min_benf) {
            var min_people = parseInt(total_sum / unit_sum);
            if (min_people < 1) {
                return min_benf;
            }
            if (min_people < min_benf) {
                return min_people;
            } else {
                return min_benf;
            }
        }
    </script>
@endsection
