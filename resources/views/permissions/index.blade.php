@extends('layouts.app')
@section('content-header')
    <h3>পারমিশন ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12  card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"> {{ $page_title }} </h3>
                            <div class="float-sm-right">
                                @can('user-create')
                                    <a class="btn btn-primary" href="{{ route('permissions.create') }}"> নতুন পারমিশন
                                        তৈরি</a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th>ক্রমিক নং</th>
                                        <th>অনুমতির নাম</th>
                                        <th>ট্যাগ নাম</th>
                                        <th width="280px">ব্যবস্থা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", "a.delete-permission", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('permissions.index') }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'tag', name: 'tag'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

        });
    </script>
@endsection



{{--@section('content')--}}
{{--    <div class="row">--}}
{{--        <div class="col-lg-12 margin-tb">--}}
{{--            <div class="pull-left">--}}
{{--                <h2>Permission Management</h2>--}}
{{--            </div>--}}
{{--            <div class="pull-right">--}}
{{--                --}}{{--                @can('role-create')--}}
{{--                <a class="btn btn-success" href="{{ route('permissions.create') }}"> Create New Permission</a>--}}
{{--                --}}{{--                @endcan--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    @if ($message = Session::get('success'))--}}
{{--        <div class="alert alert-success">--}}
{{--            <p>{{ $message }}</p>--}}
{{--        </div>--}}
{{--    @endif--}}
{{--    <table class="table table-bordered">--}}
{{--        <tr>--}}
{{--            <th>No</th>--}}
{{--            <th>Name</th>--}}
{{--            <th width="280px">Action</th>--}}
{{--        </tr>--}}
{{--        @foreach ($data as $key => $permission)--}}
{{--            <tr>--}}
{{--                <td>{{ ++$key }}</td>--}}
{{--                <td>{{ $permission->name }}</td>--}}
{{--                <td>--}}
{{--                    <a class="btn btn-info" href="{{ route('roles.show',$permission->id) }}">Show</a>--}}
{{--                    @can('role-edit')--}}
{{--                        <a class="btn btn-primary" href="{{ route('roles.edit',$permission->id) }}">Edit</a>--}}
{{--                    @endcan--}}
{{--                    @can('role-delete')--}}
{{--                        {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $permission->id],'style'=>'display:inline']) !!}--}}
{{--                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
{{--                        {!! Form::close() !!}--}}
{{--                    @endcan--}}
{{--                </td>--}}
{{--            </tr>--}}
{{--        @endforeach--}}
{{--    </table>--}}
{{--    {!! $data->render() !!}--}}
{{--    <p class="text-center text-primary"><small>Tutorial by Tutsmake.com</small></p>--}}
{{--@endsection--}}
