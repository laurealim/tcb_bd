@extends('layouts.app')
@section('content-header')
    <h3>পারমিশন ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="form-horizontal" method="POST" action="{{ route('permissions.update', $permission->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('permission-list')
                                <a class="btn btn-primary" href="{{ route('permissions.index') }}"> পারমিশনের তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>অনুমতির নাম</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           value="{{ $permission->name }}"
                                           placeholder="অনুমতির নাম" required>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ট্যাগ নাম <span class="required">*</span></label>
                                    <select class="form-control select2" id="tag" name="tag" style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            $tag_name = config('constants.tag_list.arr');
                                        foreach ($tag_name as $id => $name) { ?>
                                        <option value="{{ $id }}" {{ $id == $permission->tag ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('tag')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">আপডেট</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        });
    </script>
@endsection
