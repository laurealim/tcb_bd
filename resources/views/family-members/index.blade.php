@extends('layouts.app')
@section('content-header')
    <h3>পরিবার ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> পরিবারের সদস্যদের তালিকা আপলোড </h1>
                        </div>
                        @can('family-excel-import')
                            <div class="card-body">
                                @if(session()->has("failures"))

                                    @if(!empty(session()->get('failures')))
                                        <table class="table table-danger">
                                            <tr>
                                                <th colspan="4">ভুল তালিকা</th>
                                            </tr>
                                            <tr>
                                                <th>নং</th>
                                                <th>পারিবারিক কার্ড নং</th>
                                                <th>এরর মেসেজ</th>
                                            </tr>
                                            <?php $count = 0;
//                                            pr(session()->get('failures'));
                                            ?>
                                            @foreach(session()->get('failures') as $cardId =>$error)
<!--                                                --><?php //pr($error); ?>
                                                @foreach($error as $e)
                                                    <tr>
                                                        <td>{{ $count++ }}</td>
                                                        <td>{{ $cardId }}</td>
                                                        <td>
                                                            {{ $e }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach

                                        </table>
                                    @endif
                                @endif
                                <form action="{{ route('family-members.import') }}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{--                                            <div class="form-group">--}}
                                            {{--                                                <div class="input-group">--}}
                                            {{--                                                    <div class="custom-file">--}}
                                            {{--                                                        <input type="file" class="custom-file-input"--}}
                                            {{--                                                               id="exampleInputFile">--}}
                                            {{--                                                        <label class="custom-file-label" for="exampleInputFile">Choose--}}
                                            {{--                                                            file</label>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                </div>--}}
                                            {{--                                            </div>--}}


                                            <div class="form-group">
                                                <div class="custom-file text-left input-group">
                                                    <input type="file" name="file" class="custom-file-input"
                                                           id="customFile" onchange="javascript:updateList()">
                                                    <label class="custom-file-label" for="customFile">ফাইল বাছাই
                                                        করুন</label>
                                                </div>
                                                <div id="fileList"></div>
                                                {{--                                                @error('file')--}}
                                                {{--                                                <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                    <strong>{{ $message }}</strong>--}}
                                                {{--                                                </span>--}}
                                                {{--                                                @enderror--}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <button class="btn btn-primary">ফাইল আপলোড</button>
                                                {{--                                <a class="btn btn-success" href="{{ route('families.export') }}">Export data</a>--}}
                                                <a class="btn btn-success"
                                                   href="{{ asset('family_member_sample_data.xlsx') }}">সেম্পল
                                                    ডাটা
                                                    ফাইল ডাউনলোড</a>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endcan
                        <hr/>

                        {{--                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">--}}
                        {{--                            Launch Extra Large Modal--}}
                        {{--                        </button>--}}
                    </div>

                {{--                    <div class="card">--}}

                <!-- /.modal -->
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#assign-package").on('click', function (e) {
                e.preventDefault();
                var familyId = $(this).attr('family_id');
                var packageId = $('#package').val();
                // var divId = $(this).attr('id');
                var token = $("input[name='_token']").val();
                var url = "{{ route('packages.assignPackage') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {familyId: familyId, packageId: packageId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        window.location.reload();

                        // $('select[name="package"]').empty();
                        // $('select[name="package"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $.each(data.packageList, function (key, value) {
                        //     $('select[name="package"]').append('<option value="' + key + '">' + value + '</option>');
                        // });
                        // $("#assign-package").attr('family_id', data.family_id);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                        window.location.reload();
                    }
                });
            })

            $("#modal-lg").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                // var divId = $(this).attr('id');
                var token = $("input[name='_token']").val();
                var url = "{{ route('packages.getPackageLists') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {id: id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);

                        $('select[name="package"]').empty();
                        $('select[name="package"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data.packageList, function (key, value) {
                            $('select[name="package"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                        $("#assign-package").attr('family_id', data.family_id);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });

                {{--$.get( "/controller/" + id, function( data ) {--}}
                {{--    $(".modal-body").html(data.html);--}}
                {{--});--}}

            });

            $(document).on("click", "a.delete-family", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('families.index') }}",
                    columns: [
                        {data: 'card_no', name: 'card_no'},
                        {data: 'name', name: 'name'},
                        {data: 'f_h_name', name: 'f_h_name'},
                        {data: 'phone', name: 'phone'},
                        {data: 'nid', name: 'nid'},
                        {data: 'address', name: 'address'},
                        {data: 'dob', name: 'dob'},
                        {data: 'gender', name: 'gender'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

            $('#customFile').on('change', function () {
                var input = $('#customFile').val();
                $('#fileList').text(input);
            });
        });
    </script>
@endsection
