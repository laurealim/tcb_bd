@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <form class="" method="post" action="{{ route('family-members.update',$familyData->id) }}"
                  enctype="multipart/form-data" name="add_member" id="add_member">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="family_id" id="family_id" value="{{ $familyData->id }}">
                @csrf
                {{--                পরিবার প্রধানের তথ্য    --}}
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card card-cyan">
                            <div class="card-header">
                                <h3 class="card-title">পরিবার প্রধানের তথ্য</h3>

                                {{--                        <div class="card-tools">--}}
                                {{--                            @can('user-create')--}}
                                {{--                                <a class="btn btn-success" href="{{ route('users.index') }}"> ইউজারের তালিকা</a>--}}
                                {{--                            @endcan--}}
                                {{--                        </div>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-4">পরিবার প্রধানের নামেঃ</dt>
                                    <dd class="col-sm-8">{{ $familyData->name }}</dd>
                                    <dt class="col-sm-4">মোবাইল নং</dt>
                                    <dd class="col-sm-8">{{ $familyData->phone }}</dd>
                                    <dt class="col-sm-4">জাতীয় পরিচয়পত্র নম্বরঃ</dt>
                                    <dd class="col-sm-8">{{ $familyData->nid }}</dd>
                                    <dt class="col-sm-4">ঠিকানাঃ</dt>
                                    <dd class="col-sm-8">{{ $familyData->add }},ওয়ার্ডঃ {{ $familyData->word }},
                                        ইউনিয়ন/পৌরসভাঃ {{ getUnionName($familyData->union) }},
                                        উপজেলাঃ {{ getSubDistName($familyData->sub_dist) }},
                                        জেলাঃ {{ getDistName($familyData->dist) }},
                                        বিভাগঃ {{ getDivName($familyData->div) }}</dd>
                                    <dt class="col-sm-4">পেশাঃ</dt>
                                    <dd class="col-sm-8">{{ getOccupation($familyData->occupation) }}</dd>
                                </dl>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>

                {{--                পরিবার সদস্যদের তথ্য    --}}
                <div class="card card-cyan">
                    <div class="card-header">
                        <h3 class="card-title">পরিবার সদস্যদের তথ্য</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowraps" id="dynamic_field">
                            <thead>
                            <tr>
                                <th>পরিবার সদস্যের নাম</th>
                                <th>জন্ম নিবন্ধন / এন আই ডি নম্বর</th>
                                <th>জন্ম তারিখ</th>
                                <th>মোবাইল নং</th>
                                <th>লিঙ্গ</th>
                                <th>পেশা</th>
                                <th>পরিবার প্রধানের সাথে সম্পর্ক</th>
                                <th>শিক্ষাগত যোগ্যতা</th>
                                <th>সামাজিক নিরাপত্তা ভাতা(SSNP)</th>
                                <th width="8%">ব্যবস্থা</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($familyMemberDataArray))
                                @foreach($familyMemberDataArray as $key=>$vals)
                                    <tr id="row{{$key+1}}" class="dynamic-added">
                                        <td>
                                            <input type="text" class="form-control" id="name0" name="name[]"
                                                   placeholder="পরিবার সদস্যের নাম" value="{{ $vals['m_name'] }}"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control nid" id="nid{{$key}}" name="nid[]"
                                                   placeholder="জন্ম নিবন্ধন / এন আই ডি নম্বর"
                                                   value="{{ $vals['m_nid'] }}"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" data-inputmask-alias="datetime"
                                                   id="dob{{$key}}"
                                                   name="dob[]" data-inputmask-inputformat="yyyy-mm-dd" data-mask
                                                   value="{{ $vals['m_dob'] }}">
                                        </td>
                                        <td>
                                            <input type="number" maxlength="11"
                                                   oninput="this.value=this.value.slice(0,this.maxLength)"
                                                   id="phone{{$key}}"
                                                   class="form-control phone" name="phone[]"
                                                   placeholder="মোবাইল নং" value="{{ $vals['m_phone'] }}"/>
                                        </td>
                                        <td>
                                            <select class="form-control select2" id="gender{{$key}}" name="gender[]"
                                                    style="width: 100%;">
                                                <option value="">--- বাছাই করুন ---</option>
                                                <?php
                                                foreach ($genderList as $id => $name) { ?>
                                                <option
                                                    value="{{ $id }}" {{ $id == $vals['m_gender'] ? 'selected="selected"' : '' }} > {{ $name }} </option>
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control select2" id="occupation{{$key}}"
                                                    name="occupation[]"
                                                    style="width: 100%;">
                                                <option value="">--- বাছাই করুন ---</option>
                                                <?php
                                                foreach ($occupationList as $id => $name) { ?>
                                                <option
                                                    value="{{ $id }}" {{ $id == $vals['m_occupation'] ? 'selected="selected"' : '' }} > {{ $name }} </option>
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control select2" id="relation{{$key}}" name="relation[]"
                                                    style="width: 100%;">
                                                <option value="">--- বাছাই করুন ---</option>
                                                <?php
                                                foreach ($relationList as $id => $name) { ?>
                                                <option
                                                    value="{{ $id }}" {{ $id == $vals['m_relation'] ? 'selected="selected"' : '' }} > {{ $name }} </option>
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control select2" id="education{{$key}}"
                                                    name="education[]"
                                                    style="width: 100%;">
                                                <option value="">--- বাছাই করুন ---</option>
                                                <?php
                                                foreach ($eduLevelList as $id => $name) { ?>
                                                <option
                                                    value="{{ $id }}" {{ $id == $vals['m_edu_level'] ? 'selected="selected"' : '' }} > {{ $name }} </option>
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control select2" id="ssnp{{$key}}" name="ssnp[]"
                                                    style="width: 100%;">
                                                <option value="">--- বাছাই করুন ---</option>
                                                <?php
                                                foreach ($ssnpList as $id => $name) { ?>
                                                <option
                                                    value="{{ $id }}" {{ $id == $vals['m_ssnp'] ? 'selected="selected"' : '' }} > {{ $name }} </option>
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td>
                                            @if($key < 1)
                                                <button type="button" name="add" id="add" class="btn btn-success"><i
                                                        class="fa fa-plus-circle"></i></button>
                                            @else
                                                <button type="button" name="remove" id="{{ ($key+1)  }}"
                                                        class="btn btn-danger btn_remove">X
                                                </button>
                                            @endif
                                            @can('family-member-shift')
                                                <?php
                                                $url = route('family-members.shift', [$familyData->id, $key + 1]);
                                                ?>
                                                <a href="{{ $url }}"
                                                   class="btn btn-info action-btn family_id" id="{{ ($key+1)  }}">
                                                    <i
                                                        class="fa-solid fa-person-walking-luggage"
                                                        title="সদস্য স্থানান্তর"></i>
                                                </a>
                                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" id="name0" name="name[]"
                                               placeholder="পরিবার সদস্যের নাম"/>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control nid" id="nid0" name="nid[]"
                                               placeholder="জন্ম নিবন্ধন / এন আই ডি নম্বর"/>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" data-inputmask-alias="datetime"
                                               id="dob0"
                                               name="dob[]" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                                    </td>
                                    <td>
                                        <input type="number" maxlength="11"
                                               oninput="this.value=this.value.slice(0,this.maxLength)" id="phone0"
                                               class="form-control phone" name="phone[]"
                                               placeholder="মোবাইল নং"/>
                                    </td>
                                    <td>
                                        <select class="form-control select2" id="gender0" name="gender[]"
                                                style="width: 100%;">
                                            <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            foreach ($genderList as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control select2" id="occupation0" name="occupation[]"
                                                style="width: 100%;">
                                            <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            foreach ($occupationList as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control select2" id="relation0" name="relation[]"
                                                style="width: 100%;">
                                            <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            foreach ($relationList as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control select2" id="education0" name="education[]"
                                                style="width: 100%;">
                                            <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            foreach ($eduLevelList as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control select2" id="ssnp0" name="ssnp[]"
                                                style="width: 100%;">
                                            <option value="">--- বাছাই করুন ---</option>
                                            <?php
                                            foreach ($ssnpList as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <button type="button" name="add" id="add" class="btn btn-success"><i
                                                class="fa fa-plus-circle"></i></button>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="card-footer">
                    <button type="submit" id="submit_form" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            var i = <?php if (!empty($familyMemberDataArray)) {
                echo sizeof($familyMemberDataArray);
            } else {
                echo 1;
            } ?>;
            // var i = 1;
            // let flag = true;
            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy-mm-dd', {'placeholder': 'yyyy-mm-dd'});
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('yyyy-mm-dd', {'placeholder': 'yyyy-mm-dd'});
            //Money Euro
            $('[data-mask]').inputmask();

            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            $('#add').on('click', function () {
                var task = $("#task").val();
                i++;
                var str = '<tr id="row' + i + '" class="dynamic-added">' +
                    '<td> <input type="text" name="name[]" id="name' + i + '" class="form-control name_list" value="" /> </td>' +
                    '<td><input type="text" id="nid' + i + '" class="form-control nid" name="nid[]"  /></td>' +
                    '<td><input type="text" class="form-control dob" data-inputmask-alias="datetime" id="dob' + i + '" name="dob[]" data-inputmask-inputformat="yyyy-mm-dd" data-mask></td>' +
                    '<td><input type="number" pattern="^(01)[0-9]{9}$" id="phone' + i + '" maxlength="11" oninput="this.value=this.value.slice(0,this.maxLength)" class="form-control phone" name="phone[]" /></td>' +
                    '<td> <select class="form-control select2" id="gender' + i + '" name="gender[]" style="width: 100%;"> <option value="">--- বাছাই করুন ---</option> <?php foreach ($genderList as $id => $name) { ?> <?php echo "<option value=\'{$id}\'>{$name}</option>"; ?> <?php }?></select> </td>' +
                    '<td> <select class="form-control select2" id="occupation' + i + '" name="occupation[]" style="width: 100%;"> <option value="">--- বাছাই করুন ---</option> <?php foreach ($occupationList as $id => $name) { ?> <?php echo "<option value=\'{$id}\'>{$name}</option>"; ?> <?php }?></select> </td>' +
                    '<td> <select class="form-control select2" id="relation' + i + '" name="relation[]" style="width: 100%;"> <option value="">--- বাছাই করুন ---</option> <?php foreach ($relationList as $id => $name) { ?> <?php echo "<option value=\'{$id}\'>{$name}</option>"; ?> <?php }?></select> </td>' +
                    '<td> <select class="form-control select2" id="education' + i + '" name="education[]" style="width: 100%;"> <option value="">--- বাছাই করুন ---</option> <?php foreach ($eduLevelList as $id => $name) { ?> <?php echo "<option value=\'{$id}\'>{$name}</option>"; ?> <?php }?></select> </td>' +
                    '<td> <select class="form-control select2" id="ssnp' + i + '" name="ssnp[]" style="width: 100%;"> <option value="">--- বাছাই করুন ---</option> <?php foreach ($ssnpList as $id => $name) { ?> <?php echo "<option value=\'{$id}\'>{$name}</option>"; ?> <?php }?></select> </td>' +
                    '<td> <button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button> </td>' +
                    '</tr>';
                $('#dynamic_field').append(str);
                $('.dob').inputmask("yyyy-mm-dd", {reverse: true});
                $('.select2').select2();
                // $('#dynamic_field').append('<tr id="row' + i + '" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter task" class="form-control name_list" value="' + task + '" /></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
            });

            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

            $(document).on('change', '.phone', function () {
                let phone = $(this).val();
                var phoneId = $(this).attr("id");
                this.value = this.value.slice(0, this.maxLength)
                var token = $("input[name='_token']").val();
                var url = "{{ route('families.phoneDuplicateCheck') }}";
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {phone: phone, _token: token},
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        var phone_msg = $('#' + phoneId + '');
                        if (data.err) {
                            // phone_msg.html("");
                            phone_msg.siblings().remove();
                            phone_msg.after("<span class='alert-default-danger'>" + data.err + "</span>");
                            // $('#submit_form').prop('disabled', true);
                        } else {
                            phone_msg.siblings().remove();
                            // $('#submit_form').prop('disabled', false);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // alert("Status: " + textStatus);
                        // alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest.responseText.errors);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            });

            $(document).on('change', '.nid', function () {
                var nid = $(this).val();
                var nidId = $(this).attr("id");
                // console.log(nid);
                // var token = $("input[name='_token']").val();
                var url = "{{ route('families.nidDuplicateCheck') }}";
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {nid: nid, _token: token},
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        // var nid_msg = $('#nid_msg');
                        var nid_msg = $('#' + nidId + '');
                        if (data.err) {
                            nid_msg.html("");
                            nid_msg.siblings().remove();
                            nid_msg.after("<span class='alert-default-danger'>" + data.err + "</span>");
                            // nid_msg.append("<strong>" + data.err + "</strong>");
                            // $('#submit_form').prop('disabled', true);
                        } else {
                            nid_msg.siblings().remove();
                            // nid_msg.html("");
                            // $('#submit_form').prop('disabled', false);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // alert("Status: " + textStatus);
                        // alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            })
        });
    </script>
@endsection



