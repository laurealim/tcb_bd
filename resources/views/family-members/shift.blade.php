@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <form class="" method="post" action="{{ route('family-members.shiftDone') }}"
                  enctype="multipart/form-data" name="add_member" id="add_member">
{{--                <input type="hidden" name="_method" value="PUT">--}}
                <input type="hidden" name="family_id" id="family_id" value="{{ $familyInfo->id }}">
                <input type="hidden" name="family_member_id" id="family_id" value="{{ $memberId }}">
                @csrf
                {{--                পরিবার প্রধানের তথ্য    --}}
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <div class="card card-cyan">
                            <div class="card-header">
                                <h3 class="card-title">পরিবারের সদস্য স্থানান্তর</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> সদস্য স্থানান্তরের ধরন <span class="required">*</span></label>
                                            <select class="form-control select2" id="shift" name="shift"
                                                    style="width: 100%;">
                                                <option value="">--- বাছাই করুন ---</option>
                                                <?php
                                                $shiftList = config('constants.shiftList.arr');
                                                foreach ($shiftList as $id => $name) { ?>
                                                    <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group family_card_no">
                                            <label>পরিবার খুঁজুন(পরিবার কার্ড নম্বর দিয়ে) <span
                                                    class="required">*</span></label>
                                            <input type="number" min="0" class="form-control" name="card_no"
                                                   id="card_no" value="{{ old('card_no') }}"
                                                   placeholder="পরিবার কার্ড নম্বর">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card card-cyan">
                            <div class="card-header">
                                <h3 class="card-title">বর্তমান পরিবার প্রধানের তথ্য</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-6">পরিবার প্রধানের নাম:</dt>
                                    <dd class="col-sm-6">{{ $familyInfo->name }}</dd>
                                    <dt class="col-sm-6">মোবাইল নং</dt>
                                    <dd class="col-sm-6">{{ $familyInfo->phone }}</dd>
                                    <dt class="col-sm-6">জাতীয় পরিচয়পত্র নম্বরঃ</dt>
                                    <dd class="col-sm-6">{{ $familyInfo->nid }}</dd>
                                    <dt class="col-sm-6">ঠিকানাঃ</dt>
                                    <dd class="col-sm-6">{{ $familyInfo->add }},ওয়ার্ডঃ {{ $familyInfo->word }},
                                        ইউনিয়ন/পৌরসভাঃ {{ getUnionName($familyInfo->union) }},
                                        উপজেলাঃ {{ getSubDistName($familyInfo->sub_dist) }},
                                        জেলাঃ {{ getDistName($familyInfo->dist) }},
                                        বিভাগঃ {{ getDivName($familyInfo->div) }}</dd>
                                    <dt class="col-sm-6">পেশাঃ</dt>
                                    <dd class="col-sm-6">{{ getOccupation($familyInfo->occupation) }}</dd>
                                </dl>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-4 new_family">
                        <div class="card card-cyan">
                            <div class="card-header">
                                <h3 class="card-title">স্থানান্তরিত পরিবার প্রধানের তথ্য</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-6">পরিবার প্রধানের নাম:</dt>
                                    <dd class="col-sm-6" id="name"></dd>
                                    <dt class="col-sm-6">মোবাইল নং</dt>
                                    <dd class="col-sm-6" id="f_phone"></dd>
                                    <dt class="col-sm-6">জাতীয় পরিচয়পত্র নম্বরঃ</dt>
                                    <dd class="col-sm-6" id="f_nid"></dd>
                                    <dt class="col-sm-6">ঠিকানাঃ</dt>
                                    <dd class="col-sm-6" id="f_add"></dd>
                                    <dt class="col-sm-6">পেশাঃ</dt>
                                    <dd class="col-sm-6" id="f_occupation"></dd>
                                </dl>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>


                </div>
                <div class="card-footer">
                    <button type="submit" id="submit_form" class="btn btn-primary">স্থানান্তর</button>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            $('.new_family').hide();
            $('.family_card_no').hide();

            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

            // $(document).on('change', '#shift', function () {
            $('#shift').on('change', function () {
                let shift = parseInt($(this).val());
                if (shift === 2) {
                    $('.family_card_no').show();
                    $('.new_family').show();
                } else {
                    $('.family_card_no').hide();
                    $('.new_family').hide();
                }
            });

            $('#card_no').on('change', function () {
                let card_no = parseInt($(this).val());
                if (card_no.toString().length == 16) {
                    var token = $("input[name='_token']").val();
                    var url = "{{ route('families.getFamilyInfo') }}";
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {card_no: card_no, _token: token},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            $('#name').html("");
                            $('#f_phone').html("");
                            $('#f_nid').html("");
                            $('#f_add').html("");
                            $('#f_occupation').html("");

                            $('#name').html(data.name);
                            $('#f_phone').html(data.phone);
                            $('#f_nid').html(data.nid);
                            $('#f_add').html(data.add);
                            $('#f_occupation').html(data.occupation);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // alert("Status: " + textStatus);
                            // alert("Error: " + errorThrown);
                            console.log(XMLHttpRequest.responseText.errors);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });
                }


            });

            // $('#submit_form').on('click',function(e){
            //     e.preventDefault();
            // });
        });
    </script>
@endsection



