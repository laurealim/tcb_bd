@extends('layouts.app')
@section('content-header')
    <h3>Drag Form</h3>
@endsection
@section('title')
    Drag Forms
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <form class="build-form clearfix"></form>
                <div class="render-form" id="fb-editor"></div>
            </div>
{{--            <div class="container render-btn-wrap" id="editor-action-buttons">--}}
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#fb-editor').formBuilder()
        });
    </script>
@endsection
