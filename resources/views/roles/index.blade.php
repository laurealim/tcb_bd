@extends('layouts.app')
@section('content-header')
    <h3>রোল ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12  card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"> {{ $page_title }} </h3>
                            <div class="float-sm-right">
                                @can('role-create')
                                    <a class="btn btn-primary" href="{{ route('roles.create') }}"> নতুন রোল তৈরি</a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th>ক্রমিক নং</th>
                                        <th>রোল নাম</th>
                                        <th width="280px">ব্যবস্থা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", "a.delete-role", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('roles.index') }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

        });
    </script>
@endsection



{{--@section('custom_script')--}}
{{--    <script>--}}
{{--        $(document).on("click", "a.delete-role", function (ev) {--}}
{{--            ev.preventDefault();--}}
{{--            let url = $(this).attr("href");--}}
{{--            let id = $(this).attr("id");--}}
{{--            if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {--}}
{{--                $.ajax({--}}
{{--                    type: 'DELETE',--}}
{{--                    url: url,--}}
{{--                    dataType: 'json',--}}
{{--                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
{{--                    data: {id: id, "_token": "{{ csrf_token() }}"},--}}

{{--                    success: function (data) {--}}
{{--                        if (data.status == 'success') {--}}
{{--                            window.location.reload();--}}
{{--                        } else if (data.status == 'error') {--}}
{{--                        }--}}
{{--//                    data.request->session()->flash('status', 'Task was successful!');--}}
{{--//                    setInterval(function() {--}}
{{--//                    }, 5900);--}}
{{--                    },--}}
{{--                    error: function (data) {--}}
{{--                    }--}}
{{--                });--}}

{{--            } else {--}}
{{--                return false;--}}
{{--            }--}}
{{--        });--}}
{{--        $(function () {--}}
{{--            $("#example1").DataTable({--}}
{{--                "responsive": true, "lengthChange": false, "autoWidth": false,--}}
{{--                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]--}}
{{--            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');--}}
{{--            $('#example2').DataTable({--}}
{{--                "paging": true,--}}
{{--                "lengthChange": false,--}}
{{--                "searching": false,--}}
{{--                "ordering": true,--}}
{{--                "info": true,--}}
{{--                "autoWidth": false,--}}
{{--                "responsive": true,--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}
