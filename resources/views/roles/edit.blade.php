@extends('layouts.app')
@section('content-header')
    <h3>রোল ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="form-horizontal" method="POST" action="{{ route('roles.update', $role->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('role-list')
                                <a class="btn btn-primary" href="{{ route('roles.index') }}"> রোলের তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>রোল নাম</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="রোল নাম" value="{{ $role->name }}" required>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
{{--                                            <strong>{{ $message }}</strong>--}}
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        {{--                    <h5>Custom Color Variants</h5>--}}
                        {{--                    <div class="row">--}}
                        {{--                        <div class="col-12 col-sm-6">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label>Minimal (.select2-danger)</label>--}}
                        {{--                                <select class="form-control select2 select2-danger"--}}
                        {{--                                        data-dropdown-css-class="select2-danger" style="width: 100%;">--}}
                        {{--                                    <option selected="selected">Alabama</option>--}}
                        {{--                                    <option>Alaska</option>--}}
                        {{--                                    <option>California</option>--}}
                        {{--                                    <option>Delaware</option>--}}
                        {{--                                    <option>Tennessee</option>--}}
                        {{--                                    <option>Texas</option>--}}
                        {{--                                    <option>Washington</option>--}}
                        {{--                                </select>--}}
                        {{--                            </div>--}}
                        {{--                            <!-- /.form-group -->--}}
                        {{--                        </div>--}}
                        {{--                        <!-- /.col -->--}}
                        {{--                        <div class="col-12 col-sm-6">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label>Multiple (.select2-purple)</label>--}}
                        {{--                                <div class="select2-purple">--}}
                        {{--                                    <select class="select2" multiple="multiple" data-placeholder="Select a State"--}}
                        {{--                                            data-dropdown-css-class="select2-purple" style="width: 100%;">--}}
                        {{--                                        <option>Alabama</option>--}}
                        {{--                                        <option>Alaska</option>--}}
                        {{--                                        <option>California</option>--}}
                        {{--                                        <option>Delaware</option>--}}
                        {{--                                        <option>Tennessee</option>--}}
                        {{--                                        <option>Texas</option>--}}
                        {{--                                        <option>Washington</option>--}}
                        {{--                                    </select>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <!-- /.form-group -->--}}
                        {{--                        </div>--}}
                        {{--                        <!-- /.col -->--}}
                        {{--                    </div>--}}
                        <!-- /.row -->
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h5>পারমিশন লিস্ট</h5>
                                <div class="card card-lightblue">
                                    @foreach($permissionArrange as $key=>$values)
                                        <div class="card-header">
                                            <h5>{{ config('constants.tag_list.arr.'.$key) }}</h5>
                                        </div>
                                        <div class="card-body">
                                            @foreach($values as $key=>$value)
                                                    <?php $checked = in_array($value['id'], $rolePermissions) ? 'checked="checked"' : '' ?>
                                                <div class="icheck-primary form-check form-check-inline col-2">
                                                    <input type="checkbox" name="permission[]" id="{{ $value['id'] }}"
                                                           value="{{ $value['id'] }}" {{ $checked }}">
                                                    <label for="{{ $value['id'] }}">
                                                        {{ $value['name'] }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <p></p><p></p>
                                    @endforeach
                                </div>
                                {{--                                @foreach($permission as $value)--}}
                                {{--                                        <?php $checked = in_array($value->id, $rolePermissions) ? 'checked="checked"' : '' ?>--}}
                                {{--                                    <div class="icheck-primary form-check form-check-inline col-2">--}}
                                {{--                                        <input type="checkbox" name="permission[]" id="{{ $value->id }}"--}}
                                {{--                                               value="{{ $value->id }}" {{ $checked }}">--}}
                                {{--                                        <label for="{{ $value->id }}">--}}
                                {{--                                            {{ $value->name }}--}}
                                {{--                                        </label>--}}
                                {{--                                    </div>--}}
                                {{--                                @endforeach--}}
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">সাবমিট</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

        });
    </script>
@endsection
