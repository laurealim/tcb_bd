@extends('layouts.app')
@section('content-header')
    <h3>{{ $page_title }}</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="form-horizontal" method="POST" action="{{ route('upazillas.update', $sub_district->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('location-list')
                                <a class="btn btn-primary" href="{{ route('upazillas.index') }}"> জেলার তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>নাম(English) *</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="উপজেলার নাম" value="{{ $sub_district->name }}" required>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>কোড *</label>
                                    <input type="number" min="0" max="800" class="form-control" name="up_code"
                                           id="up_code"
                                           placeholder="" value="{{ $sub_district->up_code }}" required>
                                    @error('up_code')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>নাম(বাংলা) *</label>
                                    <input type="text" class="form-control" name="bn_name" id="bn_name"
                                           placeholder="জেলার নাম" value="{{ $sub_district->bn_name }}" required>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('bn_name')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group --><div class="form-group">
                                    <label> জেলার নাম <span class="required">*</span></label>
                                    <select class="form-control select2" id="district_id" name="district_id"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $dist_list = getDistLists();
                                        foreach ($dist_list as $id => $name) { ?>
                                        <option value="{{ $id }}" {{ $id == $sub_district->district_id ? 'selected="selected"' : '' }} > {{ $name }} </option>
                                        <?php }?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('district_id')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->

                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">সংরক্ষন</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

        });
    </script>
@endsection
