@extends('layouts.app')
@section('content-header')
    <h3>নতুন পরিবার তৈরি</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <form class="" method="post" action="{{ route('families.store') }}" enctype="multipart/form-data">
                <div class="card card-cyan">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">যোগাযোগ</h3>

                        {{--                        <div class="card-tools">--}}
                        {{--                            @can('user-create')--}}
                        {{--                                <a class="btn btn-success" href="{{ route('users.index') }}"> ইউজারের তালিকা</a>--}}
                        {{--                            @endcan--}}
                        {{--                        </div>--}}
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>মোবাইল</label>
                                    <input type="number" class="form-control" name="phone" id="phone" value="{{ old('phone') }}"
                                           placeholder="01xxxxxxxxx">
                                    <span id="phone_msg" class="alert-default-danger">

                                    </span>
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>এনআইডি কার্ড নাম্বার</label>
                                    <input type="text" class="form-control" name="nid" id="nid" value="{{ old('nid') }}"
                                           placeholder="">
                                    <span id="nid_msg" class="alert-default-danger">
                                    @error('nid')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="card card-cyan">
                    <div class="card-header">
                        <h3 class="card-title">সাধারন তথ্য</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পুর্ণ নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}"
                                           placeholder="পুর্ণ নাম">

                                    @error('add')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>জন্ম তারিখ <span class="required">*</span></label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="text" class="form-control" data-inputmask-alias="datetime" id="dob" name="dob" value="{{ old('dob') }}"
                                               data-inputmask-inputformat="yyyy-mm-dd" data-mask>

                                        @error('dob')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label> ধর্ম <span class="required">*</span></label>
                                    <select class="form-control select2" id="religion" name="religion" value="{{ old('religion') }}"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $gender_list = config('constants.religious.arr');
                                        foreach ($gender_list as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @error('religion')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                    </span>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>শিক্ষাগত যোগ্যতা <span class="required">*</span></label>
                                    <select class="form-control select2" id="education" name="education" value="{{ old('education') }}"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $education = config('constants.edu_level.arr');
                                        foreach ($education as $key=>$value){ ?>
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    @error('education')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পিতা/স্বামীর নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="f_h_name" id="f_h_name" value="{{ old('f_h_name') }}"
                                           placeholder="পিতা/স্বামীর নাম">
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label> লিঙ্গ <span class="required">*</span></label>
                                    <select class="form-control select2" id="gender" name="gender" value="{{ old('gender') }}"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $gender_list = config('constants.gender.arr');
                                        foreach ($gender_list as $id => $name) { ?>
                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                        <?php }?>
                                    </select>
                                    @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>বৈবাহিক অবস্থা <span class="required">*</span></label>
                                    <select class="form-control select2" id="marital_status" name="marital_status" value="{{ old('marital_status') }}"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $marital_status = config('constants.marital_status.arr');
                                        foreach ($marital_status as $key=>$value){ ?>
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    @error('marital_status')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.form-group -->
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="card card-cyan">
                    <div class="card-header">
                        <h3 class="card-title">বর্তমান ঠিকানা</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>বিভাগ <span class="required">*</span></label>
                                    <select class="form-control select2" id="div" name="div" value="{{ old('div') }}"
                                            style="width: 100%;">
                                        <option value="">Please Select</option>
                                        @foreach($divLists as $divId => $divName)
                                            <option value="{{ $divId }}">{{ $divName }}</option>
                                        @endforeach
                                    </select>
                                    @error('div')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>উপজেলা <span class="required">*</span></label>
                                    <select class="form-control select2" id="sub_dist" name="sub_dist"
                                            style="width: 100%;">
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                                <div class="form-group">
                                    <label>ওয়ার্ড <span class="required">*</span></label>
                                    <select class="form-control select2" id="word" name="word"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        for($word_pre_id = 1; $word_pre_id < 15; $word_pre_id++){ ?>
                                        <option value="{{ $word_pre_id }}">ওয়ার্ড নং {{ $word_pre_id }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    @error('word')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>বাসার ধরন <span class="required">*</span></label>
                                    <select class="form-control select2" id="house_type" name="house_type"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $house_type = config('constants.house_type.arr');
                                        foreach ($house_type as $key=>$value){ ?>
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    @error('house_type')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>জেলা <span class="required">*</span></label>
                                    <select class="form-control select2" id="dist" name="dist"
                                            style="width: 100%;">
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                                <div class="form-group">
                                    <label>ইউনিয়ন / পৌরসভা <span class="required">*</span></label>
                                    <select class="form-control select2" id="union" name="union"
                                            style="width: 100%;">
                                    </select>
                                    @error('union')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                                <div class="form-group">
                                    <label>মহল্লা/রোড/হোল্ডিং/গ্রাম/বাসা নং <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="add" id="add" value="{{ old('add') }}"
                                           placeholder="মহল্লা/রোড/হোল্ডিং/গ্রাম/বাসা নং">
                                    @error('add')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>

                <div class="card card-cyan">
                    <div class="card-header">
                        <h3 class="card-title">বিবিধ</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পরিবারের মাসিক আয় <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="income" id="income" value="{{ old('income') }}"
                                           placeholder="">

                                    @error('income')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>পেশা <span class="required">*</span></label>
                                    <select class="form-control select2" id="occupation" name="occupation"
                                            style="width: 100%;">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $occupation = config('constants.occupation.arr');
                                        foreach ($occupation as $key=>$value){ ?>
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    @error('occupation')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ছবি বাছাই করুন</label>
                                    <div class="custom-file text-left">
{{--                                        <input type="file" class="custom-file-input" id="picture">--}}
{{--                                        <label class="custom-file-label" for="picture">ছবি বাছাই করুন</label>--}}
                                        <input type="file" name="picture" class="custom-file-input" id="picture">
                                        <label class="custom-file-label" for="picture">ফাইল বাছাই করুন</label>
                                    </div>
                                    @error('picture')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পরিবারের মাসিক ব্যয় <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="expense" id="expense" value="{{ old('expense') }}"
                                           placeholder="">

                                    @error('expense')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>সামাজিক নিরাপত্তা ভাতা(SSNP)</label>
                                    <select class="form-control select2" id="ssnp" name="ssnp"
                                            style="width: 100%;">house_type
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        $ssnp = config('constants.ssnp.arr');
                                        foreach ($ssnp as $key=>$value){ ?>
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    @error('ssnp')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card-footer">
                    <button type="submit" id="submit_form" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            let flag = true;
            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy-mm-dd', {'placeholder': 'yyyy-mm-dd'});
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('yyyy-mm-dd', {'placeholder': 'yyyy-mm-dd'});
            //Money Euro
            $('[data-mask]').inputmask();

            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            $('#phone').on('change', function (){
                var phone = $(this).val();
                console.log(phone);
                var token = $("input[name='_token']").val();
                var url = "{{ route('families.phoneDuplicateCheck') }}";
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {phone: phone, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if(data.err){
                            $('#phone_msg').html("");
                            $('#phone_msg').append("<strong>"+data.err +"</strong>");
                            // $('#submit_form').prop('disabled', true);
                        }
                        else{
                            $('#phone_msg').html("");
                            // $('#submit_form').prop('disabled', false);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // alert("Status: " + textStatus);
                        // alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest.responseText.errors);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            });

            $('#nid').on('change', function (){
                var nid = $(this).val();
                console.log(nid);
                var token = $("input[name='_token']").val();
                var url = "{{ route('families.nidDuplicateCheck') }}";
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {nid: nid, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if(data.err){
                            $('#nid_msg').html("");
                            $('#nid_msg').append("<strong>"+data.err +"</strong>");
                            // $('#submit_form').prop('disabled', true);
                        }
                        else{
                            $('#nid_msg').html("");
                            // $('#submit_form').prop('disabled', false);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // alert("Status: " + textStatus);
                        // alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            })

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    districtList(divId, token, url, 'pre');
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    upazillaList(districtID, token, url, 'pre');
                } else {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#sub_dist').on('change', function () {
                var upazilaID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('union.unionSelectAjaxList') }}";
                if (upazilaID) {
                    $('select[name="union"]').empty();
                    unionList(upazilaID, token, url, 'pre');
                } else {
                    $('select[name="union"]').empty();
                }
            });

            function districtList(divId, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function unionList(upazilaID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {upazilaID: upazilaID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $('select[name="union"]').append('<option value="' + -1 + '">' + "সিটি কর্পোরেশন" + '</option>');
                        // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

        });
    </script>
@endsection



