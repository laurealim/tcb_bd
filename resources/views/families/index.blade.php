@extends('layouts.app')
@section('content-header')
    <h3>পরিবার ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> পরিবারের তালিকা আপলোড </h1>
                        </div>
                        @can('family-excel-import')
                            <div class="card-body">
                                @if(session()->has("failures"))

                                    <table class="table table-danger">
                                        <tr>
                                            <th colspan="4">ভুল তালিকা</th>
                                        </tr>
                                        <tr>
                                            <th>রো নং</th>
                                            <th>কলাম</th>
                                            <th>এরর মেসেজ</th>
                                            <th>ডাটা</th>
                                        </tr>
                                        @foreach(session()->get('failures') as $error)
                                                <?php $errMsg = ""; ?>
                                            @foreach($error->errors() as $e)
                                                    <?php $errMsg = $e; ?>
                                            @endforeach
                                            <tr>
                                                <td>{{ $error->row() }}</td>
                                                <td>{{ $error->attribute() }}</td>
                                                <td>
                                                    {{ $errMsg }}
                                                </td>
                                                <td>{{ $error->values()[$error->attribute()] }}</td>
                                            </tr>
                                        @endforeach

                                    </table>
                                @endif
                                {{--                            @if (isset($errors) && $errors->any())--}}
                                {{--                                <div class="alert alert-danger">--}}
                                {{--                                    @foreach($errors->all() as $error)--}}
                                {{--                                        {{ $error }}--}}
                                {{--                                    @endforeach--}}
                                {{--                                </div>--}}
                                {{--                            @endif--}}
                                <form action="{{ route('families.import') }}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="row col-12">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>বিভাগ</label>
                                                <select class="form-control select2" id="div" name="div"
                                                        style="width: 100%;">
                                                    <option value="">Please Select</option>
                                                    @foreach($divLists as $divId => $divName)
                                                        <option value="{{ $divId }}">{{ $divName }}</option>
                                                    @endforeach
                                                </select>
                                                @error('div')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>জেলা</label>
                                                <select class="form-control select2" id="dist" name="dist"
                                                        style="width: 100%;">
                                                </select>
                                                @error('sub_dist')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                                {{--                                                @foreach($roles as $key => $role)--}}
                                                {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                                {{--                                                @endforeach--}}
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>উপজেলা</label>
                                                <select class="form-control select2" id="sub_dist" name="sub_dist"
                                                        style="width: 100%;">
                                                </select>
                                                @error('sub_dist')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                                {{--                                                @foreach($roles as $key => $role)--}}
                                                {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                                {{--                                                @endforeach--}}
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>ইউনিয়ন / পৌরসভা</label>
                                                <select class="form-control select2" id="union" name="union"
                                                        style="width: 100%;">
                                                </select>
                                                @error('union')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                                {{--                                                @foreach($roles as $key => $role)--}}
                                                {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                                {{--                                                @endforeach--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                        <div class="custom-file text-left">
                                            <input type="file" name="file" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">ফাইল বাছাই করুন</label>
                                        </div>
                                        @error('file')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <button class="btn btn-primary">ফাইল আপলোড</button>
                                    {{--                                <a class="btn btn-success" href="{{ route('families.export') }}">Export data</a>--}}
                                    <a class="btn btn-success" href="{{ asset('family_sample_data.xlsx') }}">সেম্পল ডাটা
                                        ফাইল ডাউনলোড</a>
                                    {{--                                <a href={{ asset('family_sample_data.xlsx') }}>--}}
                                    {{--                                                        <span class="btn btn-primary download">--}}
                                    {{--                                                            <i class="fa fa-cloud-download bigger-120"--}}
                                    {{--                                                               aria-hidden="true"></i>--}}
                                    {{--                                                            Download Sample Data--}}
                                    {{--                                                        </span>--}}
                                    {{--                                </a>--}}
                                        <?php
                                        //                                $p_path = public_path('share_doc/family_sample_data.xlsx');
                                        //                                $s_path = storage_path('/app/share_doc/family_sample_data.xlsx');
                                        //                                $path = \Illuminate\Support\Facades\Storage::path('share_doc/family_sample_data.xlsx');
                                        ?>
                                    {{--                                <a class="btn btn-success" href={{ $path }} >সেম্পল ডাটা ফাইল</a>--}}
                                </form>
                            </div>
                        @endcan
                        <hr/>

                        {{--                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">--}}
                        {{--                            Launch Extra Large Modal--}}
                        {{--                        </button>--}}

                        <div class="card-header">
                            <h1 class="card-title"> {{ $page_title }} </h1>
                            <div class="float-sm-right">
                                @can('family-create')
                                    <a class="btn btn-primary" href="{{ route('families.create') }}"> নতুন পরিবার
                                        তৈরী</a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped data-table">
                                    <thead>
                                    <tr>
                                        <th>নং</th>
                                        <th>পরিবারের কার্ড নং</th>
                                        <th>পরিবার প্রধানের নাম</th>
                                        <th>পিতা বা স্বামীর নাম</th>
                                        <th>মোনাইল</th>
                                        <th>এনাইডি / জন্মসনদ</th>
                                        <th>ঠিকানা</th>
                                        <th>জন্ম তারিখ</th>
                                        <th>লিংগ</th>
                                        <th>অবস্থা</th>
                                        <th width="13%">ব্যবস্থা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer"></div>

                        <div class="modal fade" id="modal-lg">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">ত্রান প্রদান</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row col-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>প্যাকেজের তালিকা</label>
                                                    <select class="form-control select2" id="package" name="package"
                                                            style="width: 100%;">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>ডিলারের তালিকা</label>
                                                    <select class="form-control select2" id="dealerId" name="dealerId"
                                                            style="width: 100%;">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">বন্ধ করুন
                                        </button>
                                        <button type="button" family_id="" id="assign-package" class="btn btn-primary">
                                            প্যাকেজ প্রদান করুন
                                        </button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <hr/>
                    </div>

                    {{--                    <div class="card">--}}

                    <!-- /.modal -->
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#assign-package").on('click', function (e) {
                e.preventDefault();
                var familyId = $(this).attr('family_id');
                var packageId = $('#package').val();
                var dealerId = $('#dealerId').val();
                if (!packageId || !dealerId) {
                    alert("প্যাকেজ ও ডিলার বাছাই  করুন।");
                } else {
                    // var divId = $(this).attr('id');
                    var token = $("input[name='_token']").val();
                    var url = "{{ route('packages.assignPackage') }}";

                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {familyId: familyId, packageId: packageId, dealerId: dealerId, _token: token},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            window.location.reload();

                            // $('select[name="package"]').empty();
                            // $('select[name="package"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            // $.each(data.packageList, function (key, value) {
                            //     $('select[name="package"]').append('<option value="' + key + '">' + value + '</option>');
                            // });
                            // $("#assign-package").attr('family_id', data.family_id);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                            console.log(XMLHttpRequest);
                            console.log(textStatus);
                            console.log(errorThrown);
                            window.location.reload();
                        }
                    });
                }
            })

            $("#modal-lg").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                // var divId = $(this).attr('id');
                var token = $("input[name='_token']").val();
                var packageUrl = "{{ route('packages.getPackageLists') }}";
                var dealerUrl = "{{ route('dealers.getDealerModuleLists') }}";
                var data = {id: id, _token: token};

                setPackageList(packageUrl, data);
                setDealerList(dealerUrl, data);



                {{--$.get( "/controller/" + id, function( data ) {--}}
                {{--    $(".modal-body").html(data.html);--}}
                {{--});--}}

            });

            $(document).on("click", "a.delete-family", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('families.index') }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'card_no', name: 'card_no'},
                        {data: 'name', name: 'name'},
                        {data: 'f_h_name', name: 'f_h_name'},
                        {data: 'phone', name: 'phone'},
                        {data: 'nid', name: 'nid'},
                        {data: 'address', name: 'address'},
                        {data: 'dob', name: 'dob'},
                        {data: 'gender', name: 'gender'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    districtList(divId, token, url, 'pre');
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    upazillaList(districtID, token, url, 'pre');
                } else {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                }
            });

            $('#sub_dist').on('change', function () {
                var upazilaID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('union.unionSelectAjaxList') }}";
                if (upazilaID) {
                    $('select[name="union"]').empty();
                    unionList(upazilaID, token, url, 'pre');
                } else {
                    $('select[name="union"]').empty();
                }
            });

            function districtList(divId, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function unionList(upazilaID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {upazilaID: upazilaID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $('select[name="union"]').append('<option value="' + -1 + '">' + "সিটি কর্পোরেশন" + '</option>');
                        // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function setPackageList(packageUrl, data) {
                $.ajax({
                    url: packageUrl,
                    method: 'POST',
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);

                        $('select[name="package"]').empty();
                        $('select[name="package"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data.packageList, function (key, value) {
                            $('select[name="package"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                        $("#assign-package").attr('family_id', data.family_id);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function setDealerList(dealerUrl, data) {
                $.ajax({
                    url: dealerUrl,
                    method: 'POST',
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data.dealerLists);

                        $('select[name="dealerId"]').empty();
                        $('select[name="dealerId"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data.dealerLists, function (key, value) {
                            $('select[name="dealerId"]').append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                        // $("#assign-package").attr('family_id', data.family_id);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

        });
    </script>
@endsection
