<div>
    <h4>{{ $nested->title }} ({{ $nested->id }})</h4>
    @foreach ($nested->children as $child)
        <div style="margin-left: 20px;">
{{--            <x-category-item :category="$child" />--}}
            <x-nested-item :nestedItems="$child" />
{{--            {{ $child->title }} ({{ $child->id }})--}}
        </div>
    @endforeach
</div>
