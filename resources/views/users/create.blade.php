@extends('layouts.app')
@section('content-header')
    <h3>ইউজার ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="" method="post" action="{{ route('users.store') }}">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('user-create')
                                <a class="btn btn-primary" href="{{ route('users.index') }}"> ইউজারের তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পুর্ণ নাম <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="পুর্ণ নাম" required>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ইমেইল <span class="required">*</span></label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="ইমেইল" required>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>প্রতিষ্ঠান বাছাই করুন <span class="required">*</span></label>
                                    <select name="institute" class="select2" id="institute"
                                            style="width: 100%;" required>
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($institutionList as $institute_id => $name)
                                            <option value="{{ $institute_id }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->

                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পাসওয়ার্ড <span class="required">*</span></label>
                                    <input type="password" class="form-control" name="password" id="password"
                                           placeholder="পাসওয়ার্ড" required>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>মোবাইল <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="phone" id="phone"
                                           placeholder="01xxxxxxxxx" required>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ইউজার রোল বাছাই করুন <span class="required">*</span></label>
                                    <select name="roles[]" class="select2" multiple="multiple" data-placeholder="--- বাছাই করুন ---"
                                            style="width: 100%;" required>
                                        @foreach($roles as $key => $role)
                                            <option value="{{ $key }}">{{ $role }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->

                    <div class="card-header">
                        <h3 class="card-title">মাঠ পর্যায়ের অফিস</h3>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{-- Division --}}
                                <div class="form-group">
                                    <label>বিভাগ </label>
                                    <select class="form-control select2" id="div" name="div" value="{{ old('div') }}"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($divListArr as $divId => $divName)
                                            <option value="{{ $divId }}">{{ $divName }}</option>
                                        @endforeach
                                    </select>
                                    @error('div')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- District --}}
                                <div class="form-group">
                                    <label>জেলা</label>
                                    <select class="form-control select2" id="dist" name="dist"
                                            style="width: 100%;">
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- Sub-District --}}
                                <div class="form-group">
                                    <label>উপজেলা</label>
                                    <select class="form-control select2" id="sub_dist" name="sub_dist"
                                            style="width: 100%;">
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })


            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    districtList(divId, token, url);
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    upazillaList(districtID, token, url);
                } else {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });


            function districtList(divId, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

        });
    </script>
@endsection



