@extends('layouts.app')
@section('content-header')
    <h3>ইউজার ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <form class="form-horizontal" method="POST" action="{{ route('users.update', $user->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ $page_title }}</h3>

                        <div class="card-tools">
                            @can('user-edit')
                                <a class="btn btn-primary" href="{{ route('users.index') }}"> ইউজারের তালিকা</a>
                            @endcan
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>পুর্ণ নাম</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           value="{{ $user->name }}"
                                           placeholder="পুর্ণ নাম" required>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ইমেইল</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           value="{{ $user->email }}"
                                           placeholder="ইমেইল" required>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>প্রতিষ্ঠান বাছাই করুন <span class="required">*</span></label>
                                    <select name="institute" class="select2" id="institute"
                                            style="width: 100%;" required>
                                        @foreach($institutionList as $institute_id => $name)
                                            <option
                                                value="{{ $institute_id }}" {{ ($institute_id == $user->institute_id) ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>মোবাইল</label>
                                    <input type="text" class="form-control" name="phone" id="phone"
                                           value="{{ $user->phone }}"
                                           placeholder="01xxxxxxxxx" required>
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>ইউজার রোল বাছাই করুন</label>
                                    <select name="roles[]" class="select2" multiple="multiple"
                                            data-placeholder="Select a State"
                                            style="width: 100%;" required>
                                        @foreach($roles as $key => $role)
                                            {{--                                            <option value="{{ $key }}">{{ $role }}</option>--}}
                                            <option
                                                value="{{ $key }}" {{ in_array($key, $userRole) ? 'selected="selected"' : '' }}>{{ $role }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-header">
                        <h3 class="card-title">মাঠ পর্যায়ের অফিস</h3>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{-- Division --}}
                                <div class="form-group">
                                    <label>বিভাগ </label>
                                    <select class="form-control select2" id="div" name="div" value="{{ old('div') }}"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($divListArr as $divId => $divName)
                                            <option
                                                value="{{ $divId }}" {{ $divId == $user->div ? 'selected="selected"' : '' }} >{{ $divName }}</option>
                                            {{--                                            <option value="{{ $divId }}">{{ $divName }}</option>--}}
                                        @endforeach
                                    </select>
                                    @error('div')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- District --}}
                                <div class="form-group">
                                    <label>জেলা</label>
                                    <select class="form-control select2" id="dist" name="dist"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($distListArr as $distId => $distName)
                                            <option
                                                value="{{ $distId }}" {{ $distId == $user->dist ? 'selected="selected"' : '' }} >{{ $distName }}</option>
                                        @endforeach
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- Sub-District --}}
                                <div class="form-group">
                                    <label>উপজেলা</label>
                                    <select class="form-control select2" id="sub_dist" name="sub_dist"
                                            style="width: 100%;">
                                        <option value="">---- বাছাই করুন ----</option>
                                        @foreach($sub_distListArr as $sib_distId => $sib_distName)
                                            <option
                                                value="{{ $sib_distId }}" {{ $sib_distId == $user->sub_dist ? 'selected="selected"' : '' }} >{{ $sib_distName }}</option>
                                        @endforeach
                                    </select>
                                    @error('sub_dist')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    {{--                                                @foreach($roles as $key => $role)--}}
                                    {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                    {{--                                                @endforeach--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    districtList(divId, token, url);
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                    upazillaList(districtID, token, url);
                } else {
                    $('select[name="sub_dist"]').empty();
                    // $('select[name="union"]').empty();
                }
            });

            function districtList(divId, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

        });
    </script>
@endsection
