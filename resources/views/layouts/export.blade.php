<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>টি,সি,বি ব্যবস্থাপনা এপ্লিকেশন</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
{{--    <link rel="stylesheet" href="{{ asset("/vendors/fa/css/brands.css") }} ">--}}
{{--    <link rel="stylesheet" href="{{ asset("/vendors/fa/css/fontawesome.css") }} ">--}}
{{--    <link rel="stylesheet" href="{{ asset("/vendors/fa/css/regular.css") }} ">--}}
{{--    <link rel="stylesheet" href="{{ asset("/vendors/fa/css/solid.css") }} ">--}}
{{--    <link rel="stylesheet" href="{{ asset("/vendors/fa/css/svg-with-js.css") }} ">--}}
    <link rel="stylesheet" href="{{ asset("/vendors/fa/css/all.min.css") }} ">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset("/vendors/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css") }}">
    <link rel="stylesheet"
          href="{{ asset("/vendors/plugins/datatables-responsive/css/responsive.bootstrap4.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendors/plugins/datatables-buttons/css/buttons.bootstrap4.min.css") }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset("/vendors/plugins/daterangepicker/daterangepicker.css") }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css' )}} ">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css' )}} ">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css' )}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/toastr/toastr.min.css' )}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/vendors/dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' )}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/daterangepicker/daterangepicker.css' )}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/summernote/summernote-bs4.min.css' )}}">

    <!-- formeo -->
{{--    <script src="https://draggable.github.io/formeo/assets/js/formeo.min.js"></script>--}}
{{--    <link rel="stylesheet" href="{{ asset('/vendors/plugins/formeo/demo.min.css' )}}">--}}


{{--    formbuilder--}}
{{--    <script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>--}}

<!-- Custom SCC -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->

        <div class="content">

            <div class="container-fluid">
                <div class="flash-message"></div>
                @yield('content')
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('/vendors/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('/vendors/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script>
    // $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- DataTables  & Plugins -->
<script src="{{ asset('/vendors/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

<!-- InputMask -->
<script src="{{ asset('/vendors/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('/vendors/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- SweetAlert2 -->
<script src="{{ asset('/vendors/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('/vendors/plugins/toastr/toastr.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/vendors/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
{{--<script src="../../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>--}}
<script src="{{ asset('/vendors/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.js') }}"></script>

<!-- bs-custom-file-input -->
<script src="{{ asset('/vendors/plugins/bs-custom-file-input/bs-custom-file-input.js') }}"></script>

<!-- formeo -->
{{--<script src="{{ asset('/vendors/plugins/formeo/demo.min.js') }}"></script>--}}

<!-- overlayScrollbars -->
<script src="{{ asset('/vendors/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script type="text/javascript">
    var url = window.location;
    // for single sidebar menu
    $('ul.nav-sidebar a').filter(function () {
        return this.href == url;
    }).css({'display': 'block'})
        .addClass('active');

    // for sidebar menu and treeview
    $('ul.nav-treeview a').filter(function () {
        return this.href == url;
    }).parentsUntil(".nav-sidebar > .nav-treeview")
        .css({'display': 'block'})
        .addClass('menu-open').prev('a')
        .addClass('active');

    // for sidebar menu and treeview
    // $('ul.nav-treeview-child a').filter(function () {
    //     return this.href == url;
    // }).parentsUntil(".nav-sidebar > .nav-treeview > .nav-treeview-child")
    //     .addClass('menu-open').prev('a')
    //     .addClass('active');


    var AdminLTEOptions = {
//Enable sidebar expand on hover effect for sidebar mini
//This option is forced to true if both the fixed layout and sidebar mini
//are used together
        sidebarExpandOnHover: true,
//BoxRefresh Plugin
        enableBoxRefresh: true,
//Bootstrap.js tooltip
        enableBSToppltip: true
    };
</script>
<!-- AdminLTE App -->
<script src="{{ asset("/vendors/dist/js/adminlte.min.js") }}"></script>
<script>
    /** add active class and stay opened when selected */
    // var url = window.location;
    // const allLinks = document.querySelectorAll('.nav-item a');
    // const currentLink = [...allLinks].filter(e => {
    //     return e.href == url;
    // });
    //
    // currentLink[0].classList.add("active")
    // currentLink[0].closest(".nav-treeview").style.display = "block";
    // currentLink[0].closest(".has-treeview").classList.add("active");
</script>


@yield('custom_style')
@yield('custom_script')
</body>
</html>
