<nav class="mt-1">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    ড্যাসবোর্ড
                    {{--                    <span class="right badge badge-danger">New</span>--}}
                </p>
            </a>
        </li>

        @hasanyrole('su_admin|admin')
        <li class="nav-item">
            <a href="#" class="nav-link">
                {{--                <i class="nav-icon fas fa-tachometer-alt"></i>--}}
                <i class="nav-icon fa-solid fa-screwdriver-wrench"></i>
                <p>
                    ব্যবস্থাপনা
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">

                @can('role-list')
                    {{--                Role--}}
                    <li class="nav-item">
                        <a href="{{ route('roles.index') }}" class="nav-link">
                            {{--                        <i class="far fa-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-pen-ruler nav-icon"></i>
                            <p>রোল ব্যবস্থাপনা</p>
                        </a>
                    </li>

                @endcan
                @can('permission-list')
                    {{--                Permission--}}
                    <li class="nav-item">
                        <a href="{{ route('permissions.index') }}" class="nav-link">
                            {{--                        <i class="far fa-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-user-lock nav-icon"></i>
                            <p>অনুমতি ব্যবস্থাপনা</p>
                        </a>
                    </li>

                @endcan
                @can('user-list')
                    {{--                User--}}
                    <li class="nav-item">
                        <a href="{{ route('users.index') }}" class="nav-link">
                            {{--                        <i class="far fa-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-user-group nav-icon"></i>
                            <p>ব্যবহারকারী ব্যবস্থাপনা</p>
                        </a>
                    </li>

                @endcan
                {{--                Institution--}}
                @can('institution-list')

                    <li class="nav-item">
                        <a href="{{ route('institutions.index') }}" class="nav-link">
                            {{--                        <i class="far fa-circle nav-icon"></i>--}}
                            {{--                        <i class="fa-solid fa-user-group nav-icon"></i>--}}
                            <i class="fa-solid fa-building-flag nav-icon"></i>
                            <p>প্রতিষ্ঠান ব্যবস্থাপনা</p>
                        </a>
                    </li>

                @endcan
            </ul>
        </li>
        @can('location-list')

            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                        <i class="far fa-circle nav-icon"></i>--}}
                    <i class="fa-solid fa-map-location-dot nav-icon"></i>
                    <p>স্থান ব্যবস্থাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('divisions.index') }}" class="nav-link">
                            {{--                                <i class="far fa-dot-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-city nav-icon"></i>
                            <p>বিভাগ ব্যবস্থাপনা</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('districts.index') }}" class="nav-link">
                            {{--                                <i class="far fa-dot-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-hotel nav-icon"></i>
                            <p>জেলা ব্যবস্থাপনা</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('upazillas.index') }}" class="nav-link">
                            {{--                                <i class="far fa-dot-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-building nav-icon"></i>
                            <p>উপজেলা ব্যবস্থাপনা</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('unions.index') }}" class="nav-link">
                            {{--                                <i class="far fa-dot-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-house-chimney nav-icon"></i>
                            <p>ইউনিয়ন ব্যবস্থাপনা</p>
                        </a>
                    </li>
                </ul>
            </li>
        @endcan
        @endhasanyrole

        @can('family-list')
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                <i class="nav-icon fas fa-tachometer-alt"></i>--}}
                    <i class="nav-icon fa-solid fa-person-chalkboard"></i>
                    <p>
                        পরিবারের তথ্য
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('families.index') }}" class="nav-link">
                            {{--                        <i class="far fa-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-address-book nav-icon"></i>
                            <p>পরিবারের তালিকা</p>
                        </a>
                    </li>
                    @can('family-member-shift')
                        <li class="nav-item">
                            <a href="{{ route('family-members.index') }}" class="nav-link">
                                {{--                        <i class="far fa-circle nav-icon"></i>--}}
                                <i class="fa-solid fa-upload nav-icon"></i>
                                <p>সদস্যদ আপলোড</p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan

        @can('pending-card-number')
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                <i class="nav-icon fas fa-tachometer-alt"></i>--}}
                    <i class="nav-icon fa-solid fa-id-card-clip"></i>
                    <p>
                        পারিবারিক কার্ড
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('family-card.pendingCards') }}" class="nav-link">
                            {{--                        <i class="far fa-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-people-roof nav-icon"></i>
                            <p>অপেক্ষমাণ পরিবার</p>
                        </a>
                    </li>
                    @can('print-family-card')
                        <li class="nav-item">
                            <a href="{{ route('family-card.printCards') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                <i class="fa-solid fa-print nav-icon"></i>
                                <p>পারিবারিক কার্ড প্রিন্ট</p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan

        @can('dealer-list')
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                <i class="nav-icon fas fa-tachometer-alt"></i>--}}
                    <i class="nav-icon fa-solid fa-users-gear"></i>
                    <p>
                        ডিলার ব্যবস্থাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('dealers.index') }}" class="nav-link">
                            {{--                        <i class="far fa-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-people-group nav-icon "></i>
                            <p>ডিলারের তালিকা</p>
                        </a>
                    </li>
                    {{--                    @can('print-family-card')--}}
                    {{--                        <li class="nav-item">--}}
                    {{--                            <a href="{{ route('family-card.printCards') }}" class="nav-link">--}}
                    {{--                                --}}{{--                            <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                <i class="fa-solid fa-print nav-icon"></i>--}}
                    {{--                                <p>পারিবারিক কার্ড প্রিন্ট</p>--}}
                    {{--                            </a>--}}
                    {{--                        </li>--}}
                    {{--                    @endcan--}}
                </ul>
            </li>
        @endcan


        @can('package-list')
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                <i class="nav-icon fas fa-tachometer-alt"></i>--}}
                    <i class="nav-icon fa-solid fa-gifts"></i>
                    <p>
                        ত্রান ব্যবস্থাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @hasanyrole('su_admin')
                    @can('aid-type-list')
                        <li class="nav-item">
                            <a href="{{ route('aid-types.index') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                <i class="nav-icon fa-solid fa-kitchen-set"></i>
                                <p>ত্রানের ধরন</p>
                            </a>
                        </li>
                    @endcan
                    @endhasanyrole

                    <li class="nav-item">
                        <a href="{{ route('packages.index') }}" class="nav-link">
                            <i class="nav-icon fa-solid fa-box-archive"></i>
                            <p>প্যাকেজ তালিকা</p>
                        </a>
                    </li>

                    @can('package-assign')
                        <li class="nav-item">
                            <a href="{{ route('packages.bulkAssignPackage') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                <i class="fa-solid fa-file-arrow-up nav-icon"></i>
                                <p>আপলোড প্যাকেজ এসাইন</p>
                            </a>
                        </li>
                    @endcan

                    @can('package-unassign')
                        <li class="nav-item">
                            <a href="{{ route('packages.unAssignPackage') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                <i class="fa-solid fa-file-export nav-icon"></i>
                                <p>প্যাকেজ আন-এসাইন</p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan


        @can('item-list')
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                <i class="nav-icon fas fa-"></i>--}}
                    <i class="nav-icon fa-solid fa-basket-shopping"></i>
                    <p>
                        পণ্য ব্যবস্থাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @can('item-type-list')
                        <li class="nav-item">
                            <a href="{{ route('item-types.index') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                <i class="fa-solid fa-store nav-icon"></i>
                                <p>পণ্যের ধরন</p>
                            </a>
                        </li>
                    @endcan
                    <li class="nav-item">
                        <a href="{{ route('items.index') }}" class="nav-link">
                            {{--                            <i class="far fa-circle nav-icon"></i>--}}
                            <i class="fa-solid fa-square-poll-horizontal nav-icon"></i>
                            <p>পণ্যের তালিকা</p>
                        </a>
                    </li>
                </ul>
            </li>
        @endcan

        @can('report-access')
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                <i class="nav-icon fas fa-"></i>--}}
                    {{--                    <i class="nav-icon fa-solid fa-basket-shopping"></i>--}}
                    <i class="nav-icon fa-solid fa-square-poll-vertical"></i>
                    <p>
                        রিপোর্ট
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @can('report-family')
                        <li class="nav-item">
                            <a href="{{ route('report.familyReport') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                {{--                                <i class="fa-solid fa-store nav-icon"></i>--}}
                                <i class="fa-solid fa-people-roof nav-icon"></i>
                                <p>পরিবার ভিত্তিক</p>
                            </a>
                        </li>
                    @endcan
                    @can('report-aid')
                        <li class="nav-item">
                            <a href="{{ route('report.aidReport') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                {{--                                <i class="fa-solid fa-store nav-icon"></i>--}}
                                <i class="fa-solid fa-boxes-packing nav-icon"></i>
                                <p>ত্রান ভিত্তিক</p>
                            </a>
                        </li>
                    @endcan
                    @can('report-package')
                        <li class="nav-item">
                            <a href="{{ route('report.packageReport') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                {{--                                <i class="fa-solid fa-store nav-icon"></i>--}}
                                <i class="fa-solid fa-boxes-stacked nav-icon"></i>
                                <p>প্যাকেজ ভিত্তিক</p>
                            </a>
                        </li>
                    @endcan
                    @can('report-dealer')
                        <li class="nav-item">
                            <a href="{{ route('report.dealerReport') }}" class="nav-link">
                                {{--                            <i class="far fa-circle nav-icon"></i>--}}
                                {{--                                <i class="fa-solid fa-store nav-icon"></i>--}}
                                <i class="fa-solid fa-people-carry-box nav-icon"></i>
                                <p>ডিলার ভিত্তিক</p>
                            </a>
                        </li>
                    @endcan
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="{{ route('items.index') }}" class="nav-link">--}}
                    {{--                            --}}{{--                            <i class="far fa-circle nav-icon"></i>--}}
                    {{--                            <i class="fa-solid fa-square-poll-horizontal nav-icon"></i>--}}
                    {{--                            <p>পণ্যের তালিকা</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                </ul>
            </li>
        @endcan

        @guest
        @else
            <li class="nav-item">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form-nav').submit();" class="nav-link">
                    <i class="nav-icon fa-solid fa-arrow-right-from-bracket"></i>
                    <p>
                        {{ __('লগ আউট') }}
                        {{--                    <span class="right badge badge-danger">New</span>--}}
                    </p>
                </a>
                <form id="logout-form-nav" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>

{{--            <li class="nav-item">--}}
{{--                <a class="dropdown-item nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();--}}
{{--                            document.getElementById('logout-form-nav').submit();">--}}
{{--                    --}}{{--                    <i class="nav-icon fas fa-tachometer-alt"></i>--}}
{{--                    <i class="nav-icon fa-solid fa-arrow-right-from-bracket"></i>--}}
{{--                    {{ __('লগ আউট') }}--}}
{{--                </a>--}}

{{--                <form id="logout-form-nav" action="{{ route('logout') }}" method="POST" class="d-none">--}}
{{--                    @csrf--}}
{{--                </form>--}}
{{--            </li>--}}
        @endguest
    </ul>
</nav>
