<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>পরিবার পরিচিতি কার্ড</title>

    <!-- Font Awesome Icons -->
{{--    <link rel="stylesheet" href="{{ asset("/vendors/plugins/fontawesome-free/css/fontawesome.css") }} ">--}}
{{--    <link rel="stylesheet" href="{{ asset("/vendors/plugins/fontawesome-free/css/all.min.css") }} ">--}}
    <!-- Ionicons -->
{{--    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">--}}
    <!-- overlayScrollbars -->
{{--    <link rel="stylesheet" href="{{ asset('/vendors/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' )}}">--}}
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                {{--                @yield('content')--}}
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('/vendors/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('/vendors/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script>
    // $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- DataTables  & Plugins -->
<script src="{{ asset('/vendors/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('/vendors/plugins/pdfmake/vfs_fonts.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('/vendors/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script type="text/javascript">
</script>
<!-- AdminLTE App -->
<script src="{{ asset("/vendors/dist/js/adminlte.min.js") }}"></script>
<script>
</script>


@yield('custom_style');
@yield('custom_script');
</body>
</html>
