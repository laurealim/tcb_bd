<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset("/vendors/plugins/fontawesome-free/css/fontawesome.css") }} ">
    <link rel="stylesheet" href="{{ asset("/vendors/plugins/fontawesome-free/css/all.min.css") }} ">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/vendors/dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' )}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/daterangepicker/daterangepicker.css' )}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('/vendors/plugins/summernote/summernote-bs4.min.css' )}}">

    <!-- Custom SCC -->
    <link rel="stylesheet" href="{{ asset('css/login_style.css') }}">
</head>
<body class="hold-transition sidebar-mini" style="height: 100%">
<div class="wrapper" style="height: 100%">

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper login_bg" style="margin: auto; height: 100%">

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
            @yield('content')
            <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
{{--    <footer class="main-footer" style="margin: auto; text-align: center">--}}
{{--        @include('includes.footer')--}}
{{--    </footer>--}}
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('/vendors/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('/vendors/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script>
    // $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- overlayScrollbars -->
<script src="{{ asset('/vendors/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>


@yield('custom_style')
@yield('custom_script')

</body>
</html>
