<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel 7 PDF Example</title>
    {{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">--}}
{{--    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>--}}
    <style>
        @font-face {
            font-family: 'Nikosh';
            font-style: normal;
            font-weight: normal;
            src: url({{ storage_path('fonts\Nikosh.ttf') }}) format("truetype");
        }
        body {
            font-family: 'Nikosh', sans-serif;
        }
    </style>
</head>
<body>
<h1>{{ $name }}</h1>
<h1>{{ $card_no }}</h1>
<p>10-10-10</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</body>
</html>
