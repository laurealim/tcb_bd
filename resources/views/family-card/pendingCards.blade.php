@extends('layouts.app')
@section('content-header')
    <h3>পরিবার ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12  card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> পারিবারিক কার্ড নম্বর তৈরী </h1>
                            <div class="card-tools">
                                @can('generate-card-number')
                                    <a class="btn btn-primary generate-card" id="edit-role"
                                       href="{{ route('family-card.generateCardNumbers') }} ">
                                        কার্ড নম্বর
                                        তৈরী
                                    </a>
{{--                                    <meta name="csrf-token" content="{{ csrf_token() }}">--}}
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped data-table">
                                <thead>
                                <tr>
                                    <th>ক্রমিক নং</th>
                                    <th>পরিবার প্রধানের নাম</th>
                                    <th>পিতা বা স্বামীর নাম</th>
                                    <th>মোনাইল</th>
                                    <th>এনাইডি / জন্মসনদ</th>
                                    <th>বিভাগ</th>
                                    <th>জেলা</th>
                                    <th>উপজেলা</th>
                                    <th>ইউনিয়ন/পৌরসভা</th>
                                    <th>জন্ম তারিখ</th>
                                    <th>লিংগ</th>
                                    <th>পেশা</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", "a.generate-card", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি পরিবার পরিচিতি কার্ড নাম্বার তৈরি করতে চান?")) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('family-card.pendingCards') }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'f_h_name', name: 'f_h_name'},
                        {data: 'phone', name: 'phone'},
                        {data: 'nid', name: 'nid'},
                        {data: 'div', name: 'div'},
                        {data: 'dist', name: 'dist'},
                        {data: 'sub_dist', name: 'sub_dist'},
                        {data: 'union', name: 'union'},
                        {data: 'dob', name: 'dob'},
                        {data: 'gender', name: 'gender'},
                        {data: 'occupation', name: 'occupation'},
                    ]
                });
            });
        });
    </script>
@endsection
