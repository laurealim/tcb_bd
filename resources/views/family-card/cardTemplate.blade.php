<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="/css/app.css" rel="stylesheet" type="text/css"/>
    {{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">--}}
    {{--    <link href="css/app.css" rel="stylesheet" type="text/css"/>--}}
    <style type="text/css">
        .tbl_stl {
            width: 100%;
        }

        .msg-photo {
            padding: 10px;
        }

        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 0;
            }
        }

        .table td {
            border: 1px solid black !important;
        }

        #printMe {
            /*margin: 10px 8px 10px 12px ;*/
        }

        #card_border {
            /*margin: 15px;*/
        }

    </style>
</head>
<body>
<div class="container mt-5">
    <h2 class="text-center mb-3">হিজলি স্মার্ট ভিলেজ কার্ড</h2>
    <div class="d-flex justify-content-end mb-4">
        {{--        @if(isset($id))--}}
        {{--        <a class="btn btn-primary" href="{{ route('family-card.createCard',['id'=>$data['id'],'download'=>'pdf'])  }}">--}}
        {{--            Export to PDF </a>--}}
        {{--        <a class="btn btn-primary" href="{{ route('family-card.printCard', $id ? $id:'')  }}"> Export to PDF </a>--}}
        {{--        @endif--}}
    </div>
    <div class="wrapper">
        <div class="content-wrapper">
            <div class="content">
                <div class="container-fluid">
                    <div class="col-sm-12">
                        <button class="btn btn-primary" onclick="printDiv('printMe')">প্রিন্ট কার্ড</button>
                        <a href="{{ route('family-card.printCards') }}" class="btn btn-info">ফিরে যান</a>
{{--                        <span><b>Total number of print of this card is: <?php echo $reliefData['print_hit']; ?></b></span>--}}
                    </div>
                    {{--                        {{ pr($data) }}--}}
                    {{--                        {{ dd() }}--}}

                    <div id='printMe' class="row col-12">
                        @foreach($dataArr as $data)
                            <?php if (!hasQrCode($data['id'])) {
                                $str = "name.: " . utf8_encode($data['name']) . ", ";
                                $str .= "Husband/Father Name: " . utf8_encode($data['f_h_name']) . ", ";
                                $str .= "Card No.: " . $data['card_no'] . ", ";
                                $str .= "Phone: " . utf8_encode($data['phone']) . ", ";
                                $str .= "DOB: " . $data['dob'] . ", ";
                                $str .= "NID: " . utf8_encode($data['nid']) . ", ";
                                if ($data['gender'] == 1)
                                    $str .= "Gender: Male, ";
                                elseif ($data['gender'] == 2)
                                    $str .= "Gender: Female, ";
                                else
                                    $str .= "Gender: Others, ";
                                generateQrCode($str, $data['id']);
                            } ?>
                            <div class="row" style="margin: 1px">
                                {{--                            <div class="row col-12">--}}
                                {{--                <h3 class="header smaller lighter blue">--}}
                                {{--                    কার্ড নং--}}
                                {{--                </h3>--}}

                                <div id="card_border" class="col-sm-12">
                                    <table class="table table-bordered mb-5">
                                        <tr>
                                            <td>
                                                <?php
                                                $idArr = array();
                                                for($count = 1; $count < 2; $count++){
                                                ?>
                                                <div class="card" width="100%">
                                                    <table class="tbl_stl" width="100%">
                                                        <tr>
                                                            <td rowspan="2">
                                                                <table align="center">
                                                                    <tr>
                                                                        <td style="padding: 5px">
                                                                            <img
                                                                                src="/images/bdgov.jpg"
                                                                                class="msg-photos" width="160px"
                                                                                height="140"
                                                                                style="border: 1px solid silver"/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table width="100%" height="120px">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <h3 class="id_card"
                                                                                style="text-align: center">হিজলি স্মার্ট ভিলেজ কার্ড</h3>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="1px"
                                                                                   align="center" hight="auto"
                                                                                   class="tbl_stls">
                                                                                <tr>
                                                                                    <td align="left" width='10%'>কার্ড
                                                                                        নং:
                                                                                    </td>
                                                                                    <?php
                                                                                    //                                                                    pr($reliefData['card_no']);
                                                                                    $arrList = str_split($data['card_no']);
                                                                                    foreach ($arrList as $key => $value){ ?>

                                                                                    <td width="7%"
                                                                                        align="center"><?php echo $value; ?></td>
                                                                                    <?php }
                                                                                    ?>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td rowspan="2">
                                                                <table align="center">
                                                                    <tr>
                                                                        <td style="padding: 5px">
                                                                            <img
                                                                                src="/images/qr_code/{{ $data['id'] }}/{{ $data['id'] }}.png"
                                                                                class="msg-photo" alt="Kate's Avatar"
                                                                                width="140px" height="140"/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>

                                                    <table class="tbl_stl" width="100%" hight=""
                                                           style="font-size: 14px;">
                                                        <tr width="100%">
                                                            <td width="50%" valign="top">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="25%" valign="top">নাম</td>
                                                                        <td>:</td>
                                                                        <td><?php echo $data['name'] ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="25%" valign="top">পেশা:</td>
                                                                        <td>:</td>
                                                                        <td><?php
                                                                            if($data['occupation'] >= 1)
                                                                                echo config('constants.occupation.arr.' . $data['occupation']);
                                                                            else
                                                                                echo $data['occupation'];
                                                                            ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="25%" valign="top">মোবাইল</td>
                                                                        <td>:</td>
                                                                        <td><?php echo $data['phone'] ?></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="50%">
                                                                <table width="100%">
                                                                    <tr>


                                                                        <td width="25%" valign="top">স্বামী/স্ত্রী</td>
                                                                        <td>:</td>
                                                                        <td><?php echo $data['f_h_name'] ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="25%" valign="top">ঠিকানা:</td>
                                                                        <td>:</td>
                                                                        <td>
                                                                            <?php
                                                                            $uniName = 'পৌরসভা';
                                                                            if ($data['union'] == -1) {
                                                                                $uniName = 'সিটি কর্পোরেশন';
                                                                            }
                                                                            else if ($data['union'] != 0)
                                                                                $uniName = getUnionName($data['union']);

                                                                            echo 'বাড়ি নংঃ ' . $data['add'] . ', ওয়ার্ড নংঃ ' . en2bn($data['word']) . ', ইউনিয়নঃ ' . $uniName . ', উপজেলাঃ ' . getSubDistName($data['sub_dist']) . ', জেলাঃ ' . getDistName($data['dist']); ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br/>

                                                    <table border="1px" style="font-size:12px;" width="100%"
                                                           class="tbl_stl">
                                                        <tr align="center" style="font-weight: bold; font-size: 14px">
                                                            <td Width="15%" height="25px">সেবা প্রদানের তারিখ</td>
                                                            <td Width="45%">সেবার বিবরণ</td>
                                                            <td Width="25%">পরিমাণ</td>
                                                            <td Width="15%">মন্তব্য</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="blank_row" height="40px"></td>
                                                            <td height="15px" style="text-align: center"></td>
                                                            <td height="15px"></td>
                                                            <td height="15px"></td>
                                                        </tr>
                                                        <?php
                                                        for($rows = 1; $rows < 25; $rows++){ ?>
                                                        <tr>
                                                            <td class="blank_row" height="40px"></td>
                                                            <td height="15px"></td>
                                                            <td height="15px"></td>
                                                            <td height="15px"></td>
                                                        </tr>

                                                        <?php }
                                                        ?>
                                                    </table>

                                                    <br/>
                                                    <table class="center text-center" width="100%">
                                                        <tr>
                                                            <td>
                                                                <span>জেলা আইসিটি অফিসার (প্রোগ্রামার), তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর</span>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    {{--                                    <div style="margin: auto; text-align: center">--}}
                                                    {{--                                        <span style="font-weight: bold">জেলা প্রশাসন, গোপালগঞ্জ</span>--}}
                                                    {{--                                    </div>--}}

                                                </div>

                                                <?php } ?>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                                {{--                            </div>--}}
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function printDiv(divName) {

        // console.log(scatterSeries);
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;

    }
</script>
{{--<script src="/js/app.js" type="text/js"></script>--}}
</body>
</html>
