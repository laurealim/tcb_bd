@extends('layouts.app')
@section('content-header')
    <h3>পরিবার ম্যানেজমেন্ট</h3>
@endsection
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12  card-primary">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"> পরিবারের তালিকা আপলোড </h1>
                        </div>
                        @can('family-excel-import')
                            <form action="{{ route('family-card.printCards') }}" target="_blank" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="row col-12">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>বিভাগ</label>
                                                <select class="form-control select2" id="div" name="div"
                                                        style="width: 100%;">
                                                    <option value="">Please Select</option>
                                                    @foreach($divLists as $divId => $divName)
                                                        <option value="{{ $divId }}">{{ $divName }}</option>
                                                    @endforeach
                                                </select>
                                                @error('div')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>জেলা</label>
                                                <select class="form-control select2" id="dist" name="dist"
                                                        style="width: 100%;">
                                                </select>
                                                @error('sub_dist')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                                {{--                                                @foreach($roles as $key => $role)--}}
                                                {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                                {{--                                                @endforeach--}}
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>উপজেলা</label>
                                                <select class="form-control select2" id="sub_dist" name="sub_dist"
                                                        style="width: 100%;">
                                                </select>
                                                @error('sub_dist')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                                {{--                                                @foreach($roles as $key => $role)--}}
                                                {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                                {{--                                                @endforeach--}}
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>ইউনিয়ন / পৌরসভা</label>
                                                <select class="form-control select2" id="union" name="union"
                                                        style="width: 100%;">
                                                </select>
                                                @error('union')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                                {{--                                                @foreach($roles as $key => $role)--}}
                                                {{--                                                    <option value="{{ $key }}">{{ $role }}</option>--}}
                                                {{--                                                @endforeach--}}
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>প্রিন্ট কার্ড</label>
                                                <select class="form-control select2" id="print_card" name="print_card"
                                                        style="width: 100%;">
                                                    {{--                                                    <?php--}}
                                                    {{--                                                    $per_chunk = 20;--}}
                                                    {{--                                                    $extra_value = ($countData % $per_chunk);--}}
                                                    {{--                                                    $total_row = ($countData - $extra_value) / $per_chunk;--}}
                                                    {{--                                                    for($count = 0; $count <= $total_row; $count++){ ?>--}}
                                                    {{--                                                    <option value="{{ $count }}">Print Card--}}
                                                    {{--                                                        from {{ (($per_chunk * $count) + 1) }}--}}
                                                    {{--                                                        to {{ ($per_chunk*($count + 1)) }}</option>--}}
                                                    {{--                                                    <?php }--}}
                                                    {{--                                                    ?>--}}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="card-footer">
                                    <button class="btn btn-primary">কার্ড দেখুন</button>
                                </div>
                            </form>
                        @endcan

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", "a.delete-family", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("আপনি কি তথ্য মুছে ফেলতে চান?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('families.index') }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'card_no', name: 'card_no'},
                        {data: 'name', name: 'name'},
                        {data: 'f_h_name', name: 'f_h_name'},
                        {data: 'phone', name: 'phone'},
                        {data: 'nid', name: 'nid'},
                        {data: 'div', name: 'div'},
                        {data: 'dist', name: 'dist'},
                        {data: 'sub_dist', name: 'sub_dist'},
                        {data: 'union', name: 'union'},
                        {data: 'dob', name: 'dob'},
                        {data: 'gender', name: 'gender'},
                        {data: 'occupation', name: 'occupation'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

            $('#div').on('change', function () {
                var divId = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('district.districtSelectAjaxList') }}";
                if (divId) {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    $('select[name="print_card"]').empty();
                    districtList(divId, token, url, 'pre');
                } else {
                    $('select[name="dist"]').empty();
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    $('select[name="print_card"]').empty();
                }
            });

            $('#dist').on('change', function () {
                var districtID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
                if (districtID) {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    $('select[name="print_card"]').empty();
                    upazillaList(districtID, token, url, 'pre');
                } else {
                    $('select[name="sub_dist"]').empty();
                    $('select[name="union"]').empty();
                    $('select[name="print_card"]').empty();
                }
            });

            $('#sub_dist').on('change', function () {
                var upazilaID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('union.unionSelectAjaxList') }}";
                if (upazilaID) {
                    $('select[name="union"]').empty();
                    $('select[name="print_card"]').empty();
                    unionList(upazilaID, token, url, 'pre');
                } else {
                    $('select[name="union"]').empty();
                    $('select[name="print_card"]').empty();
                }
            });

            $('#union').on('change', function () {
                var divId = $('#div').val();
                var districtID = $('#dist').val();
                var upazilaID = $('#sub_dist').val();
                var unionID = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('family-card.chunkCards') }}";
                $('select[name="print_card"]').empty();
                chunkCard(divId, districtID, upazilaID, unionID, token, url);
            });

            function districtList(divId, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function upazillaList(districtID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="sub_dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_dist"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function unionList(upazilaID, token, url, type) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {upazilaID: upazilaID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                        $.each(data, function (key, value) {
                            $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function chunkCard(divId, districtID, upazilaID, unionID, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divId: divId, districtID: districtID, upazilaID: upazilaID, unionID: unionID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        var per_chunk = 20;
                        var extra_value = 0;
                        var total_row = 0;
                        if(data < per_chunk && data >= 1){
                            extra_value = data;
                        }else{
                            extra_value = (data % per_chunk);
                            total_row = (data - extra_value) / per_chunk;
                        }
                        console.log(extra_value);
                        console.log(total_row);
                        {{--<option value="{{ $count }}">Print Card--}}
                            {{--    from {{ (($per_chunk * $count) + 1) }}--}}
                            {{--    to {{ ($per_chunk*($count + 1)) }}</option>--}}

                        if (total_row >= 1 || extra_value > 0) {
                            for (let i = 0; i <= total_row;) {
                                $('select[name="print_card"]').append('<option value="' + i + '">Print Card From ' + ((per_chunk * i) + 1) + ' to ' + (per_chunk * (i + 1)) + '</option>');
                                ++i;
                            }
                        }

                        // $('select[name="dist"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        // $.each(total_row, function (key, value) {
                        //     $('select[name="print_card"]').append('<option value="' + key + '">' + value + '</option>');
                        // });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            }

        });
    </script>
@endsection
