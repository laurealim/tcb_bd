<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
{{--    <meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
    <meta http-equiv="Content-Type" content="text/html;"/>
    <title>Laravel 7 PDF Example</title>
{{--        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">--}}
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/app.css" rel="stylesheet" type="text/css"/>
    <style>
        @font-face {
            font-family: 'Nikosh';
            font-style: normal;
            font-weight: normal;
            src: url({{ storage_path('fonts\Nikosh.ttf') }}) format("truetype");
        }
        body {
            font-family: Nikosh, sans-serif;
        }
    </style>
</head>
<body>
<div class="container mt-5">
    <h2 class="text-center mb-3">পরিবার পরিচিতি কার্ড</h2>
    <div class="d-flex justify-content-end mb-4">
        {{--        @if(isset($id))--}}
        <a class="btn btn-primary" href="{{ route('family-card.createCard',['id'=>$data['id'],'download'=>'pdf'])  }}">
            Export to PDF </a>
        {{--        <a class="btn btn-primary" href="{{ route('family-card.printCard', $id ? $id:'')  }}"> Export to PDF </a>--}}
        {{--        @endif--}}
    </div>
    <div class="wrapper">
        <div class="content-wrapper">
            <div class="content">
                <div class="container-fluid">
                    <div class="col-sm-12">
                        <button class="btn btn-primary" onclick="printDiv('printMe')">Print Card</button>
                        {{--                                    <span><b>Total number of print of this card is: <?php echo $reliefData['print_hit']; ?></b></span>--}}
                    </div>
                    <div class="hideMes" id='printMe' class="row col-12">
                        <div class="row">

                            <div id="card_border" class="col-sm-12">
                                <div class="row row-12">
                                    <div class="col-2 border">
                                        <div class="text-center">
                                            <img
                                            src="images/bdgov.jpg"
                                            class="profile-user-img msg-img img-fluid "/>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="col-12 text-center">
                                            <h3 class="id_card">পরিবার পরিচিতি কার্ড</h3>
                                        </div>
                                        <div class="col-12 text-center">
                                            <span class="cd-span-hd">কার্ড নং:
                                            </span>
{{--                                            <?php--}}
{{--                                            //                                                                    pr($reliefData['card_no']);--}}
{{--                                            $arrList = str_split($data['card_no']);--}}
{{--                                            foreach ($arrList as $key => $value){ --}}
{{--                                                ?>--}}

{{--                                            <span class="cd-span"><?php echo $value; ?></span>--}}
{{--                                            <?php }--}}
{{--                                            ?>--}}
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        bg3
                                    </div>
                                </div>
                            </div>
                            {{--                            </div>--}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        </div>
                        <div class="col-12">
                            <table class="table table-bordered mb-5">
                                <thead>
                                <tr class="table-danger">
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">DOB</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--                                @foreach($data  as $data)--}}
                                <tr>
                                    <th scope="row">{{ $data['id'] }}</th>
                                    <td>{{ $data['name'] }}</td>
                                    <td>{{ $data['nid'] }}</td>
                                    <td>{{ $data['phone'] }}</td>
                                    <td>{{ $data['dob'] }}</td>
                                </tr>
                                {{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('hideMe').hide();
    function printDiv(divName) {

        // console.log(scatterSeries);
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;

        {{--let hitVal = 1;--}}
        {{--var h_id = "<?php echo $id;?>";--}}
        {{--var token = $("input[name='_token']").val();--}}
        {{--var url = "{{ route('registration.adminCountPrintHit') }}";--}}

        {{--var printContents = document.getElementById(divName).innerHTML;--}}
        {{--var originalContents = document.body.innerHTML;--}}

        {{--$.ajax({--}}
        {{--    url: url,--}}
        {{--    method: 'POST',--}}
        {{--    data: {h_id: h_id, hitVal: hitVal, _token: token},--}}
        {{--    dataType: "json",--}}
        {{--    success: function (scatterSeries) {--}}
        {{--        console.log(scatterSeries);--}}
        {{--        document.body.innerHTML = printContents;--}}
        {{--        window.print();--}}
        {{--        document.body.innerHTML = originalContents;--}}
        {{--    },--}}
        {{--    error: function (XMLHttpRequest, textStatus, errorThrown) {--}}
        {{--        console.log(XMLHttpRequest);--}}
        {{--        console.log(textStatus);--}}
        {{--        console.log(errorThrown);--}}
        {{--        // $("#submit").disable();--}}
        {{--        // console.log("adasd");--}}
        {{--    }--}}
        {{--});--}}
    }
</script>
{{--<script src="{{ asset('js/app.js') }}" type="text/js"></script>--}}
</body>
</html>
