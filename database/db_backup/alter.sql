ALTER TABLE `permissions`
    ADD COLUMN `tag`  text NULL AFTER `name`;

ALTER TABLE `permissions`
    MODIFY COLUMN `tag`  int(11) NULL DEFAULT 0 AFTER `name`;

