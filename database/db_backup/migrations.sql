/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : tbc

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-08-01 22:16:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2019_08_19_000000_create_failed_jobs_table', '1');
INSERT INTO `migrations` VALUES ('4', '2019_12_14_000001_create_personal_access_tokens_table', '1');
INSERT INTO `migrations` VALUES ('5', '2022_02_24_135219_create_permission_tables', '1');
INSERT INTO `migrations` VALUES ('6', '2022_02_26_101908_create_families_table', '2');
INSERT INTO `migrations` VALUES ('7', '2022_02_26_152017_create_family_members_table', '2');
INSERT INTO `migrations` VALUES ('8', '2022_02_28_103858_create_jobs_table', '3');
INSERT INTO `migrations` VALUES ('9', '2022_04_13_190454_create_aid_types_table', '4');
INSERT INTO `migrations` VALUES ('10', '2022_04_14_075136_create_packages_table', '5');
INSERT INTO `migrations` VALUES ('11', '2022_04_14_092152_create_package_vs_families_table', '5');
INSERT INTO `migrations` VALUES ('12', '2022_04_14_092240_create_family_vs_packages_table', '6');
INSERT INTO `migrations` VALUES ('13', '2016_06_01_000001_create_oauth_auth_codes_table', '7');
INSERT INTO `migrations` VALUES ('14', '2016_06_01_000002_create_oauth_access_tokens_table', '7');
INSERT INTO `migrations` VALUES ('15', '2016_06_01_000003_create_oauth_refresh_tokens_table', '7');
INSERT INTO `migrations` VALUES ('16', '2016_06_01_000004_create_oauth_clients_table', '7');
INSERT INTO `migrations` VALUES ('17', '2016_06_01_000005_create_oauth_personal_access_clients_table', '7');
INSERT INTO `migrations` VALUES ('18', '2022_06_12_155115_create_nesteds_table', '8');
INSERT INTO `migrations` VALUES ('19', '2022_06_21_090112_create_item_types_table', '9');
INSERT INTO `migrations` VALUES ('20', '2022_06_21_090139_create_items_table', '9');
INSERT INTO `migrations` VALUES ('21', '2022_06_21_090237_create_lot_item_amounts_table', '9');
INSERT INTO `migrations` VALUES ('22', '2022_07_02_134921_create_institutions_table', '10');
INSERT INTO `migrations` VALUES ('23', '2022_07_04_073033_create_ministries_table', '11');
INSERT INTO `migrations` VALUES ('24', '2022_07_04_073327_create_bivags_table', '11');
INSERT INTO `migrations` VALUES ('25', '2022_07_04_073346_create_departments_table', '11');
INSERT INTO `migrations` VALUES ('26', '2022_08_01_081722_create_dealers_table', '12');
