/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : tbc

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-09-10 17:56:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `family_members`
-- ----------------------------
DROP TABLE IF EXISTS `family_members`;
CREATE TABLE `family_members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `family_id` int(11) NOT NULL,
  `card_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `m_age` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`m_age`)),
  `m_name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_dob` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`m_dob`)),
  `m_gender` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_nid` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_phone` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_occupation` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_relation` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_ssnp` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_edu_level` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_member` int(11) DEFAULT 0,
  `total_male` int(11) DEFAULT 0,
  `total_female` int(11) DEFAULT 0,
  `total_others` int(11) DEFAULT 0,
  `total_child` int(11) DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `institute_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `card_no` (`card_no`) USING BTREE,
  UNIQUE KEY `family_no` (`family_id`) USING BTREE,
  KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of family_members
-- ----------------------------
INSERT INTO `family_members` VALUES ('1', '5', '3543120100000005', null, '[\"\\u09ae\\u09c3\\u09a4: \\u0986\\u09b0\\u09cb\\u099c \\u0986\\u09b2\\u09c0 \\u0996\\u09be\\u0995\\u09c0\",\"\\u09ae\\u09cb\\u0983 \\u099b\\u09be\\u09ae\\u09be\\u09a6 \\u09ae\\u09cb\\u09b2\\u09cd\\u09b2\\u09be\",\"\\u09ae\\u09c3\\u09a4: \\u09b8\\u09c1\\u09b0\\u09c7\\u09a8 \\u09a6\\u09be\\u09b8\"]', 0x5B22313939352D30352D3130222C22323031382D30322D3031222C22313936322D30392D3038225D, '[1,3,2]', '[\"28579408902\",\"19782693016503324\",\"19623514311357980\"]', '[\"01724759322\",\"01722344889\",\"01745437640\"]', '[8,29,31]', '[2,3,4]', '[-1,2,4]', '[3,5,7]', '3', '1', '1', '1', '1', '1', null, '2022-07-31 07:23:25', '2022-07-31 07:23:25');
INSERT INTO `family_members` VALUES ('2', '8', '3543120100000008', null, '[\"\\u0986\\u09b2\\u09bf \\u0986\\u0995\\u09ac\\u09b0 \\u09b8\\u09bf\\u0995\\u09a6\\u09be\\u09b0\",\"\\u0986\\u09b2\\u09c0 \\u09b6\\u09c7\\u0996\"]', 0x5B22313938382D30312D3032222C22323031372D30352D3131225D, '[1,3]', '[\"19883514311000009\",\"2857940537\"]', '[\"01710135017\",\"01758624466\"]', '[26,7]', '[4,5]', '[3,-1]', '[6,6]', '2', '1', '0', '1', '1', '1', null, '2022-07-31 07:23:25', '2022-07-31 10:57:58');
INSERT INTO `family_members` VALUES ('3', '11', '3543120100000011', null, '[\"\\u09ae\\u09c3\\u09a4: \\u09ad\\u09a6\\u09cd\\u09b0 \\u0995\\u09be\\u09a8\\u09cd\\u09a4 \\u09ac\\u09be\\u09b2\\u09be\",\"\\u09b8\\u099e\\u09cd\\u099c\\u09bf\\u09a4 \\u0985\\u09a7\\u09bf\\u0995\\u09be\\u09b0\\u09c0\",\"\\u09ae\\u09a8\\u09bf\\u09b0 \\u09b9\\u09cb\\u09b8\\u09c7\\u09a8 \\u09ab\\u0995\\u09bf\\u09b0\"]', 0x5B22313937302D30372D3034222C22323032302D30322D3132222C22323031372D30372D3235225D, '[1,2,1]', '[\"19703514311359257\",\"19853514311358552\",\"19863514121383751\"]', '[\"01911345389\",\"01753400830\",\"01319808656\"]', '[3,4,24]', '[4,4,6]', '[2,4,3]', '[8,4,3]', '3', '2', '1', '0', '2', '1', null, '2022-07-31 07:23:25', '2022-07-31 07:23:25');
INSERT INTO `family_members` VALUES ('6', '2765', '3543120100002765', null, '[\"\\u09ae\\u09cb\\u0983 \\u09b8\\u09b9\\u09bf\\u09a6\\u09c1\\u09b2 \\u0987\\u09b8\\u09b2\\u09be\\u09ae\"]', 0x5B22313936362D30382D3035225D, '[2]', '[\"19863514121383123\"]', '[\"01319808657\"]', '[23]', '[8]', '[-1]', '[6]', '1', '0', '1', '0', '0', '1', null, '2022-07-31 10:59:30', '2022-07-31 10:59:30');
INSERT INTO `family_members` VALUES ('7', '2763', '3532000400002763', null, '[\"Moriom begum\"]', 0x5B22313935362D31322D3330225D, '[\"2\"]', '[\"1956400000971\"]', '[\"01456565656\"]', '[\"17\"]', '[\"1\"]', '[\"3\"]', '[\"1\"]', '1', '0', '1', '0', '0', '1', '0', '2022-08-01 04:33:22', '2022-08-01 04:39:47');
INSERT INTO `family_members` VALUES ('8', '2766', '3532000400002766', null, '[\"Akib Md. Sadiqul\"]', 0x5B22313938362D31302D3230225D, '[\"1\"]', '[\"1986223311445\"]', '[\"01495333322\"]', '[\"6\"]', '[\"5\"]', '[\"-1\"]', '[\"6\"]', '1', '1', '0', '0', '0', '1', '0', '2022-08-01 04:38:48', '2022-08-01 05:37:36');
INSERT INTO `family_members` VALUES ('9', '2767', '3532000400002767', null, '[\"Habibur Rahman\",\"Tamanna Rahman\",\"Taslim Mitu\",\"Samia Islam\"]', 0x5B22313939382D31302D3130222C22313939332D31302D3230222C22313939362D30392D3330222C22323030302D30342D3133225D, '[\"1\",\"2\",\"2\",\"2\"]', '[\"1998232323232\",\"19931122332211098\",\"19963343453545343\",\"1984637590274\"]', '[\"01463232323\",\"01409809837\",\"01347499371\",\"01745222190\"]', '[\"4\",\"9\",\"25\",\"25\"]', '[\"3\",\"4\",\"6\",\"6\"]', '[\"-1\",\"-1\",\"-1\",\"4\"]', '[\"5\",\"6\",\"3\",\"4\"]', '4', '1', '3', '0', '0', '1', '0', '2022-08-01 05:37:36', '2022-09-08 07:45:13');
