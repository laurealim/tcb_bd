<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotItemAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_item_amounts', function (Blueprint $table) {
            $table->id();
            $table->integer('package_id');
            $table->integer('lot_no');
            $table->string('item_amount');
            $table->integer('total_people')->nullable()->default(0);
            $table->string('start_date',125);
            $table->string('end_date',125)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable()->default(0);
            $table->integer('state')->default(2);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_item_amounts');
    }
}
