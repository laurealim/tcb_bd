<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->string('card_no',125)->nullable();
            $table->integer('div');
            $table->integer('dist');
            $table->integer('sub_dist');
            $table->integer('union');
            $table->integer('word');
            $table->integer('add');
            $table->string('name',125);
            $table->string('f-h_name',125);
            $table->string('dob',125)->nullable();
            $table->tinyInteger('gender')->default(1);
            $table->string('occupation',125)->nullable();
            $table->string('phone',125);
            $table->string('nid',125);
            $table->tinyInteger('ssnp')->nullable();
            $table->integer('income')->nullable()->default(0);
            $table->integer('expense')->nullable()->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->unique(['phone', 'nid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
