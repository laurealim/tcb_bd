<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('mins_id');
            $table->integer('divs_id')->nullable()->default(0);
            $table->integer('dept')->nullable()->default(0);
            $table->integer('div')->nullable()->default(0);
            $table->integer('dist')->nullable()->default(0);
            $table->integer('sub_dist')->nullable()->default(0);
            $table->integer('union')->nullable()->default(0);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
    }
}
