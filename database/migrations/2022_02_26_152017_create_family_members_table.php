<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_members', function (Blueprint $table) {
            $table->id();
            $table->integer('family_id');
            $table->string('card_no');
            $table->json('m_name')->nullable();
            $table->json('m_dob')->nullable();
            $table->json('m_nid')->nullable();
            $table->json('m_phone')->nullable();
            $table->json('m_occupation')->nullable();
            $table->json('m_relation')->nullable();
            $table->json('m_ssnp')->nullable();
            $table->integer('total_member')->nullable()->default(0);
            $table->integer('total_male')->nullable()->default(0);
            $table->integer('total_female')->nullable()->default(0);
            $table->integer('total_others')->nullable()->default(0);
            $table->integer('total_child')->nullable()->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_members');
    }
}
