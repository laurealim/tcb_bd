<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone');
            $table->integer('nid');
            $table->integer('gender');
            $table->string('address');
            $table->string('inst_address');
            $table->string('g_name');
            $table->string('dob');
            $table->string('inst_name');
            $table->string('trd_license');
            $table->string('tin');
            $table->integer('div')->nullable()->default(0);
            $table->integer('dist')->nullable()->default(0);
            $table->integer('sub_dist')->nullable()->default(0);
            $table->integer('union')->nullable()->default(0);
            $table->integer('created_by');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealers');
    }
}
