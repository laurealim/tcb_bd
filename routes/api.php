<?php

use App\Http\Controllers\Api\ApiLoginController;
use App\Http\Controllers\Api\PackageController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

//use \App\Http\Controllers\Auth\LoginController;

//require_once('routes/web.php');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::post('api/login', [ApiLoginController::class, 'login'])->name('login');
//Route::post('login', [RegisterController::class, 'login']);
//$this->get('api/login', 'Auth\LoginController@showLoginForm')->name('login');
//$this->post('api/login', 'Auth\LoginController@login');

//Route::prefix('api')->group(function () {
//    dd("asa");
//    Route::get('password/reset/{token}', 'Api\Auth\ResetPasswordController@showResetForm')->name('password.request');
//    Route::post('password/reset', 'Api\Auth\ResetPasswordController@postReset')->name('password.reset');
//});

//Route::middleware(['cors', 'json.response', 'auth:api'])->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => ['cors', 'json.response']], function () {
    Route::post('/login', [ApiLoginController::class, 'login'])->name('login.api');
    Route::post('/register',[ApiLoginController::class, 'register'])->name('register.api');
//    Route::get('/user', [ApiLoginController::class, 'user'])->name('user');
//    Route::post('register', [ApiLoginController::class, 'register']);
//    Route::post('dealer/login', [ApiLoginController::class, 'dealerLogin']);
});


Route::middleware('auth:api')->group(function () {
    Route::post('/logout', [ApiLoginController::class, 'logout'])->name('logout.api');
    Route::get('getPackageLists', [ApiLoginController::class, 'getPackageLists'])->name('api.getPackageLists');
//    Route::post('packages/getPackageLists', [PackageController::class, 'getPackageLists'])->name('packages.getPackageLists');
//    Route::post('packages/assignPackage', [PackageController::class, 'assignPackage'])->name('packages.assignPackage');
//    Route::apiResource('packages', PackageController::class);
//    Route::post('login', [ApiLoginController::class, 'login']);

});
