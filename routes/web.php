<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\FamilyController;
use \App\Http\Controllers\FamilyCardController;
use \App\Http\Controllers\DragController;
use \App\Http\Controllers\FamilyMemberController;
use \App\Http\Controllers\AidTypeController;
use \App\Http\Controllers\PackageController;
use \App\Http\Controllers\DivisionController;
use \App\Http\Controllers\DistrictController;
use \App\Http\Controllers\UpazilaController;
use \App\Http\Controllers\UnionController;
use \App\Http\Controllers\ItemController;
use \App\Http\Controllers\ItemTypeController;
use \App\Http\Controllers\LotItemAmountController;
use \App\Http\Controllers\InstitutionController;
use \App\Http\Controllers\BivagController;
use \App\Http\Controllers\DepartmentController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\DealerController;
use \App\Http\Controllers\Report\ReportController;

//require_once('routes/api.php');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::group(['middleware' => ['auth']], function () {
//    Route::get('/', [App\Http\Controllers\HomeController::class, 'cache_clear'])->name('cache_clear');

    //*  Families Route Section */
    Route::post('/families/import', [FamilyController::class, 'import'])->name('families.import');
    Route::post('/families/export', [FamilyController::class, 'export'])->name('families.export');
    Route::get('/families/sampledatadownload', [FamilyController::class, 'sampledatadownload'])->name('families.sampledatadownload');
    Route::post('/families/phoneDuplicateCheck', [FamilyController::class, 'phoneDuplicateCheck'])->name('families.phoneDuplicateCheck');
    Route::post('/families/nidDuplicateCheck', [FamilyController::class, 'nidDuplicateCheck'])->name('families.nidDuplicateCheck');
    Route::post('/families/getFamilyInfo', [FamilyController::class, 'getFamilyInfo'])->name('families.getFamilyInfo');

    Route::post('/family-members/import', [FamilyMemberController::class, 'import'])->name('family-members.import');
    Route::get('/family-members/sampledatadownload', [FamilyMemberController::class, 'sampledatadownload'])->name('family-members.sampledatadownload');
    Route::get('/family-members/shift/{familyId}/{memberId}', [FamilyMemberController::class, 'shift'])->name('family-members.shift');
    Route::post('/family-members/shiftDone', [FamilyMemberController::class, 'shiftDone'])->name('family-members.shiftDone');

    Route::get('family-card/pendingCards', [FamilyCardController::class, 'pendingCards'])->name('family-card.pendingCards');
    Route::post('family-card/generateCardNumbers', [FamilyCardController::class, 'generateCardNumbers'])->name('family-card.generateCardNumbers');
    Route::get('family-card/createCard/{id}/{download?}', [FamilyCardController::class, 'createCard'])->name('family-card.createCard');
    Route::get('family-card/printCards', [FamilyCardController::class, 'printCards'])->name('family-card.printCards');
    Route::post('family-card/chunkCards', [FamilyCardController::class, 'chunkCards'])->name('family-card.chunkCards');
    Route::post('family-card/printCards', [FamilyCardController::class, 'printCards'])->name('family-card.printCards');
    Route::post('packages/getPackageLists', [PackageController::class, 'getPackageLists'])->name('packages.getPackageLists');
    Route::post('packages/getAssignedPackageLists', [PackageController::class, 'getAssignedPackageLists'])->name('packages.getAssignedPackageLists');
    Route::get('packages/bulkAssignPackage', [PackageController::class, 'bulkAssignPackage'])->name('packages.bulkAssignPackage');
    Route::post('packages/saveBulkAssignPackage', [PackageController::class, 'saveBulkAssignPackage'])->name('packages.saveBulkAssignPackage');
    Route::post('packages/assignPackage', [PackageController::class, 'assignPackage'])->name('packages.assignPackage');
    Route::post('packages/unAssignPackage', [PackageController::class, 'unAssignPackage'])->name('packages.unAssignPackage');
    Route::post('item-types/getItemLists', [ItemTypeController::class, 'getItemLists'])->name('item-types.getItemLists');
    Route::post('packages/saveItem', [PackageController::class, 'saveItem'])->name('packages.saveItem');
    Route::post('bivag/bivagSelectAjaxList', [BivagController::class, 'bivagSelectAjaxList'])->name('bivag.bivagSelectAjaxList');
    Route::post('department/deptSelectAjaxList', [DepartmentController::class, 'deptSelectAjaxList'])->name('department.deptSelectAjaxList');
    Route::get('users/changePassword', [UserController::class, 'changePassword'])->name('users.changePassword');
    Route::post('users/updatePassword', [UserController::class, 'updatePassword'])->name('users.updatePassword');
    Route::get('lot-item-amount/add/{packageID}', [LotItemAmountController::class, 'add'])->name('lot-item-amount.add');
    Route::get('lot-item-amount/edit/{packageID}/{lotId}', [LotItemAmountController::class, 'edit'])->name('lot-item-amount.edit');
    Route::post('lot-item-amount/saveLot/{id?}', [LotItemAmountController::class, 'saveLot'])->name('lot-item-amount.saveLot');
    Route::post('dealers/resetPassword/{id}', [DealerController::class, 'resetPassword'])->name('dealers.resetPassword');
    Route::post('dealers/getDealerLists', [DealerController::class, 'getDealerLists'])->name('dealers.getDealerLists');
    Route::post('dealers/getDealerModuleLists', [DealerController::class, 'getDealerModuleLists'])->name('dealers.getDealerModuleLists');
    Route::post('dealers/saveDealer', [DealerController::class, 'saveDealer'])->name('dealers.saveDealer');
    Route::post('dealers/getAssignDealerList', [DealerController::class, 'getAssignDealerList'])->name('dealers.getAssignDealerList');
//    Route::get('users/profile', [UserController::class, 'profile'])->name('users.deptSelectAjaxList');
//    Route::get('users/profile', [UserController::class, 'profile'])->name('users.deptSelectAjaxList');

    //*  District Route Section */
    Route::post('district/districtSelectAjaxList', '\App\Http\Controllers\DistrictController@districtSelectAjaxList')->name('district.districtSelectAjaxList');    // Ajax District List

    //*  Upazila-- Route Section */
    Route::post('upazila/upazilaSelectAjaxList', '\App\Http\Controllers\UpazilaController@upazilaSelectAjaxList')->name('upazila.upazilaSelectAjaxList');    // Ajax Upazila List

    //*  Union Route Section */
    Route::post('union/unionSelectAjaxList', '\App\Http\Controllers\UnionController@unionSelectAjaxList')->name('union.unionSelectAjaxList');    // Ajax District List

    /* Report Route */
    Route::get('report/familyReport', [ReportController::class, 'familyReport'])->name('report.familyReport');
    Route::post('report/familySearchData', [ReportController::class, 'familySearchData'])->name('report.familySearchData');
    Route::post('report/exportFamilyReport', [ReportController::class, 'exportFamilyReport'])->name('report.exportFamilyReport');

    Route::get('report/aidReport', [ReportController::class, 'aidReport'])->name('report.aidReport');
    Route::post('report/aidSearchData', [ReportController::class, 'aidSearchData'])->name('report.aidSearchData');
    Route::post('report/aidSearchAidVsFamilyData', [ReportController::class, 'aidSearchAidVsFamilyData'])->name('report.aidSearchAidVsFamilyData');
    Route::post('report/aidSearchAidVsDealerData', [ReportController::class, 'aidSearchAidVsDealerData'])->name('report.aidSearchAidVsDealerData');
    Route::post('report/aidSearchAidVsPackageData', [ReportController::class, 'aidSearchAidVsPackageData'])->name('report.aidSearchAidVsPackageData');
    Route::post('report/exportAidReport', [ReportController::class, 'exportAidReport'])->name('report.exportAidReport');

    Route::get('report/packageReport', [ReportController::class, 'packageReport'])->name('report.packageReport');
    Route::post('report/packageSearchData', [ReportController::class, 'packageSearchData'])->name('report.packageSearchData');
    Route::post('report/exportPackageReport', [ReportController::class, 'exportPackageReport'])->name('report.exportPackageReport');

    Route::get('report/dealerReport', [ReportController::class, 'dealerReport'])->name('report.dealerReport');
    Route::post('report/dealerSearchData', [ReportController::class, 'dealerSearchData'])->name('report.dealerSearchData');
    Route::post('report/exportDealerReport', [ReportController::class, 'exportDealerReport'])->name('report.exportDealerReport');


    /* Cron Tasks Routes */
    Route::get('cron/test', [FamilyMemberController::class, 'cronTest'])->name('cron.cronTest');


    /*  Resource Route  */
    Route::resource('users', \App\Http\Controllers\UserController::class);
    Route::resource('roles', \App\Http\Controllers\RoleController::class);
    Route::resource('permissions', \App\Http\Controllers\PermissionController::class);
    Route::resource('families',FamilyController::class);
    Route::resource('drags',DragController::class);
    Route::resource('family-members',FamilyMemberController::class);
    Route::resource('aid-types',AidTypeController::class);
    Route::resource('packages',PackageController::class);
    Route::resource('divisions',DivisionController::class);
    Route::resource('districts',DistrictController::class);
    Route::resource('upazillas',UpazilaController::class);
    Route::resource('unions',UnionController::class);
    Route::resource('item-types',ItemTypeController::class);
    Route::resource('items',ItemController::class);
    Route::resource('institutions',InstitutionController::class);
    Route::resource('dealers',DealerController::class);



//    Route::get('/clear-route-cache', function() {
//        $exitCode = \Illuminate\Support\Facades\Artisan::call('route:cache');
//        return 'All routes cache has just been removed';
//    });
//
//    Route::get('/clear-cache', function() {
//        Artisan::call('cache:clear');
//        Artisan::call('route:cache');
//        Artisan::call('config:cache');
//        Artisan::call('view:clear');
//        return "Cache cleared";
//    });
});

//Route::group(['middleware' => ['role:developer|manager|superadmin']], function () {
//});

//  php artisan vendor:publish --provider="Barryvdh\DomPDF\ServiceProvider" --tag="config"
//php artisan view:cache
//php artisan view:clear
//php artisan config:cache
//php artisan config:clear
//php artisan event:cache
//php artisan event:clear
//php artisan route:cache
//php artisan route:clear

/*
Verb            URI                         Action          Route Name
GET             /photos                     index           photos.index
GET             /photos/create              create          photos.create
POST            /photos                     store           photos.store
GET             /photos/{photo}             show            photos.show
GET             /photos/{photo}/edit        edit            photos.edit
PUT/PATCH       /photos/{photo}             update          photos.update
DELETE          /photos/{photo}             destroy         photos.destroy
*/


/*
 *
 *
***********************************
            Permission
***********************************
++++++++++++++++++++++++++++++++++
Permission Related Access.
==================================
@can('edit articles')

@endcan
//or
@if(auth()->user()->can('edit articles') && $some_other_condition)
  //
@endif

You can use @can, @cannot, @canany, and @guest to test for permission-related access.

==================================

***********************************
            Role
***********************************
++++++++++++++++++++++++++++++++++
Check for a specific role:
==================================
@role('writer')
    I am a writer!
@else
    I am not a writer...
@endrole
//or
@hasrole('writer')
    I am a writer!
@else
    I am not a writer...
@endhasrole
==================================

++++++++++++++++++++++++++++++++++
Check for any role in a list:
==================================
@hasanyrole($collectionOfRoles)
    I have one or more of these roles!
@else
    I have none of these roles...
@endhasanyrole
// or
@hasanyrole('writer|admin')
    I am either a writer or an admin or both!
@else
    I have none of these roles...
@endhasanyrole
==================================

++++++++++++++++++++++++++++++++++
Check for all roles:
==================================
@hasallroles($collectionOfRoles)
    I have all of these roles!
@else
    I do not have all of these roles...
@endhasallroles
// or
@hasallroles('writer|admin')
    I am both a writer and an admin!
@else
    I do not have all of these roles...
@endhasallroles
==================================

++++++++++++++++++++++++++++++++++
Alternatively, @unlessrole gives the reverse for checking a singular role, like this:
==================================
@unlessrole('does not have this role')
    I do not have the role
@else
    I do have the role
@endunlessrole
==================================

++++++++++++++++++++++++++++++++++
You can also determine if a user has exactly all of a given list of roles:
==================================
@hasexactroles('writer|admin')
    I am both a writer and an admin and nothing else!
@else
    I do not have all of these roles or have more other roles...
@endhasexactroles
==================================

*
*/
